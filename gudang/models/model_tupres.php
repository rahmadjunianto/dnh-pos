<?php
/*
 * Januari 2014
 */

class Model_tupres extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                                $this->load->library("class_public");
	}
                function get_tupres_standar() {                        
                        $retval = 0;
//                        $tunjabstandar = $this->class_public->getSettingGaji("tunjab standar");
//                        $this->log_message("tunjab standar: " . $tunjabstandar);
//                        $retval = $tunjabstandar;
                        return $retval;
                }
                //
                public function get_grid_data($request_data){
                    try{
                        $this->log_message(serialize($request_data));
                        $page = $request_data['page']; // get the requested page
                        $limit = $request_data['rows']; // get how many rows we want to have into the grid
                        $totalrows = isset($request_data['totalrows']) ? $request_data['totalrows']: false;
                        $this->log_message(" search : " . $request_data['_search']);
                        if($totalrows) {
                            $limit = $totalrows;
                        }
                        $kodecabang = "";
                        $jabatan ="";
                        if (isset($request_data['kodecabang'])) {
                            $kodecabang= $request_data['kodecabang'];                           
                        }
                        if (isset($request_data['kodejabatan'])) {
                            $jabatan = strtolower($request_data['kodejabatan']);
                        }                        
                         $this->log_message("kode cabang $kodecabang");
                        $this->db->select(" tp_minmasakerja as MIN, tp_maxmasakerja as MAX,tp_nominal as Nominal,tp_jabatan as jabatan  ",false);
                        $this->db->from('tupres_nonstruktural');
                        $this->db->where('lower(trim(tp_kodecabang))',$kodecabang);
                        $this->db->where('lower(trim(tp_jabatan))',$jabatan);
                        $this->db->order_by("tp_minmasakerja");
                        //$this->db->where("lower(trim(lower(tp_jabatan)))",$rdata);
                        $total_rows = $this->db->get()->result();

                        $count = count($total_rows);
                        $response = new stdClass();
                        if( $count >0 ) {
                            $total_pages = ceil($count/$limit);
                        } else {
                            $total_pages = 0;
                        }
                        if ($page > $total_pages){
                            $page = $total_pages;
                        }
                        if ($limit <= 0){
                            $limit = 0;
                        }
                        $start = $limit * $page - $limit;
                        if ($start<=0){
                            $start = 0;
                        }
                        $response->page = $page;
                        $response->total = $total_pages;
                        $response->records = $count;
                        $response->start = $start;
                        $response->limit = $limit;

                        $eligible_rows = array_slice($total_rows, $start, $limit);

                        $i = 0;
                        foreach($eligible_rows as $row) {
                            $response->rows[$i]['id'] = $row->min;
                            $response->rows[$i++]['cell'] = array($row->min, $row->max, $row->nominal);
                        }
                        return $response;
                    }catch (Exception $e){
                        $this->handle_exception($e);
                    }
                }        
                
                function getAllData($start,$limit,$sidx,$sord,$where){
                  $this->db->select(' mg_grade,mg_nominal,mg_sma,mg_smak ');
                  //$this->db->from("mastergapok");
                  $this->db->limit($limit);
                  if($where != NULL)
                      $this->db->where($where,NULL,FALSE);
                  $this->db->order_by($sidx,$sord);
                  $query = $this->db->get('mastergapok',$limit,$start);

                  return $query->result();
                }        
                //
                function test_basic() {
                                $this->log_message("gapok called ");
		$q = $this->db->query("select * from mastergapok");
		return $q;                    
                }
//                
	function get_data_tupres($idcabang) {//dipake
		$retval = array();
                                $sql = "SELECT * FROM cabang  where idcabang=?";
                                $query = $this->db->query($sql, array($idcabang));
		foreach ($query->result() as $row) {
			$retval[ ] = array(
                                                    'rad' => $row->tupres_radiografer,
                                                    'p_rad' => $row->tupres_pembantu_radiografer
			);
		}
		return $retval;
	}
                //get_data_tupres_struktural
	function get_data_tupres_struktural($idcabang) {//dipake
		$retval = array();
                                $sql = "SELECT * FROM cabang  where idcabang=?";
                                $query = $this->db->query($sql, array($idcabang));
		foreach ($query->result() as $row) {

			$retval[ ] = array(
				'spv3' => $row->tupres_spv3,
				'spv2' => $row->tupres_spv2,
				'spv1' => $row->tupres_spv1,
				'wakacab' => $row->tupres_wakacab,
				'kacab' => $row->tupres_kacab
			);
		}
		return $retval;                                
	}                
                //end of tupres struk
                //save tupres struk
                function save_tupres_struktural_cabang($idcabang,$spv1,$spv2,$spv3,$wakacab,$kacab ) {                                        
                    //
                    $spv1 = $this->class_public-> AvoidNullNum($spv1);
                    $spv2 = $this->class_public->AvoidNullNum($spv2);
                    $spv3 = $this->class_public->AvoidNullNum($spv3);
                    $wakacab = $this->class_public->AvoidNullNum($wakacab);
                    $kacab = $this->class_public->AvoidNullNum($kacab);
                    $strSQL = " UPDATE cabang SET ";
                    $strSQL = $strSQL . " tupres_spv1= ? , ";
                    $strSQL = $strSQL . " tupres_spv2= ? ,";
                    $strSQL = $strSQL . " tupres_spv3= ? ,";
                    $strSQL = $strSQL .  " tupres_wakacab= ? ,";
                    $strSQL = $strSQL .  " tupres_kacab= ?  ";
                    $strSQL = $strSQL  . " WHERE idcabang= ? ";
                    $query = $this->db->query($strSQL, array($spv1,$spv2,$spv3,$wakacab,$kacab,$idcabang));                    
                    return 0;
                }                
                //end of save tupres spv
        //distinct jabatan
	function get_distinct_jabatan() {//dipake
		$retval = array();
                                $sql = " SELECT distinct(tp_jabatan) as jabatan  FROM tupres_nonstruktural ORDER BY tp_jabatan ";
                                $query = $this->db->query($sql);
                                return $query->result();
//		foreach ($query->result() as $row) {
//                                                $this->log_message("ok $row->jabatan");
//			$retval[ ] = array(
//                                                    'jabatan' => $row->jabatan
//			);
//		}
//		return $retval;
	}        
//jabatan by idcabang
	function get_distinct_jabatan_bycabang($idcabang) {//dipake
		$retval = array();
                                $sql = "  distinct(tp_jabatan) as jabatan  ";
                                $this->db->select($sql,false);
                                $this->db->from("tupres_nonstruktural");
                                $this->db->where('tp_kodecabang',$idcabang);        
                                $query = $this->db->get();
                                return $query->result();
                                //$query = $this->db->query($sql);
                                //return $query->result();

	}        
//        
                //save std tunjab
                function save_tupres_cabang($idcabang,$min,$max,$jabatan,$nominal) {                                        
                    //
                    $min = $this->class_public-> AvoidNullNum($min);
                    $max = $this->class_public->AvoidNullNum($max);
                    $nominal = $this->class_public->AvoidNullNum($nominal);
                    //
                    $strSQL = " UPDATE tupres_nonstruktural  SET ";
                    $strSQL = $strSQL . " tp_nominal= ?  ";                    
                    $strSQL = $strSQL  . " WHERE tp_kodecabang= ? ";
                    $strSQL = $strSQL  . " and lower(trim(tp_jabatan))= ? ";
                    $strSQL = $strSQL  . " and tp_minmasakerja= ? ";
                    $strSQL = $strSQL  . " and tp_maxmasakerja= ? ";
                    $query = $this->db->query($strSQL, array($nominal,$idcabang,$jabatan,$min,$max));                    
                    return 0;
                }
                function calc_tupres($sdmid) {
                    $this->load->model('model_grade');                    
                    $masakerja = $this->class_public->AvoidNullNum($this->model_grade->get_masa_kerja_diakui_pegawai($sdmid));
                    
                    $this->load->model('model_sdm');
                    $idcabang = $this->model_sdm->get_cabang_sdm_byid($sdmid);
                    $jabatan = strtolower(trim($this->model_grade->get_jabatan_pegawai($sdmid)));
                    $this->log_message("masa kerja $masakerja jabatan $jabatan cabang $idcabang");
                    $struktural = array();
                    $struktural = $this->get_data_tupres_struktural($idcabang);
                    $retval = 0;
                    switch(trim(strtolower($jabatan))){
                    case "supervisor 1":
                        $retval = $struktural[0]['spv1'];
                        break;
                    case "supervisor 2":
                        $retval = $struktural[0]['spv2'];
                        break;
                    case "supervisor 3":
                        $retval = $struktural[0]['spv3'];
                        break;
                    case "wakacab":
                        $retval = $struktural[0]['wakacab'];
                        break;                    
                    case "kacab":
                        $retval = $struktural[0]['kacab'];
                        break;                    
                    default:
                        $this->log_message("masa kerja $masakerja");
                        $sql = " SELECT * FROM tupres_nonstruktural   WHERE ?  >=tp_minmasakerja AND  ?  <tp_maxmasakerja and lower(trim(tp_jabatan))=? and tp_kodecabang=?";
                        $query = $this->db->query($sql, array($masakerja,$masakerja,$jabatan,$idcabang));
                        foreach ($query->result() as $row) {
                                $retval =$row->tp_nominal;
                        }                        
                    } //end of switch
                    return $retval;                    
                }
                //end of save std tunjab
}

?>
