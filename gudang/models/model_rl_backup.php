<?php
/*
 * Sep 2014
 */

class Model_rl extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                //$this->load->library("class_public");
                $this->user_id = $this->acl->get_users_id();
	}
        function get_coa_name($code) {
            $retval="";
            $sql=" SELECT * FROM coa WHERE lower(trim(coa_code))=?";
            $query=$this->db->query($sql,array($code));
            foreach($query->result() as $row) {
                $retval = $row->coa_name;
            }
            return $retval;            
        }        
        function tr_id_is_paid($trid) {
            $outstanding=0;            
            $data = array();
            $sql = " SELECT payment_trid as tr_id,payment_amount as amount ,payment_date as date FROM payment  ";
            $sql = $sql . " ";
            $sql = $sql . " WHERE payment_trid=?";
            $this->log_message("payment info $sql dgn lab $trid ");
            $query=  $this->db->query($sql,array($trid));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $totalpaid = $totalpaid + $row->amount;
            }
            //debugger;
            $totalprice=0;
            $sql = " SELECT trans_total  FROM trans_detail ";
            $sql = $sql . " WHERE trans_id=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            foreach($query->result() as $row) {
                $totalprice = $totalprice + $row->trans_total;
            }
            
            $outstanding = $totalprice - $totalpaid;
            $paid=0;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid']=$paid;
            $data['outstanding']=$outstanding;
            return $data;
        }                

        function tr_id_is_paid_void($trid) {
            $outstanding=0;            
            $data = array();
            $sql = " SELECT payment_trid as tr_id,payment_amount as amount ,payment_date as date FROM payment  ";
            $sql = $sql . " ";
            $sql = $sql . " WHERE payment_trid=?";
            $this->log_message("payment info $sql dgn lab $trid ");
            $query=  $this->db->query($sql,array($trid));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $totalpaid = $totalpaid + $row->amount;
            }
            //debugger;
            $totalprice=0;
            $sql = " SELECT orders_net  FROM orders_detail ";
            $sql = $sql . " WHERE orders_no=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            foreach($query->result() as $row) {
                $totalprice = $totalprice + $row->orders_net;
            }
            
            $outstanding = $totalprice - $totalpaid;
            $paid=0;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid']=$paid;
            $data['outstanding']=$outstanding;
            return $data;
        }            
         function outstanding($filterdate,$filterdate2) {
            //$this->load->model('model_payment','mp');            
            $retval=array();
            $start="2014-09-01";
            $finish="2014-12-31";
            $sql =" SELECT tro_id,cl_name as name FROM transout inner join client on transout.tro_cust_id=client.cl_id WHERE to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=?  ORDER BY tro_id ";
            $global_q = $this->db->query($sql,array($filterdate,$filterdate2));
            $total=0;
            $counter=0;
            $this->log_message($sql);
            foreach($global_q->result() as $row_global) {
                $this->log_message($row_global->tro_id);
                $trans_id=$row_global->tro_id;
                $arr_paid = $this->tr_id_is_paid($trans_id);
                //$row_global->trans_id);
                $paid=$arr_paid['paid'];
                $outstanding = $arr_paid['outstanding'];
                if($paid==0) {
                        $counter=$counter+1;
                        $name = $row_global->name;
                        $retval[ ] = array(
			'counter' => $counter,
                        'trans_id' => $row_global->tro_id,
                        'name' => $name,
                        'outstanding' => $outstanding
                        );
                        $total=$total+$outstanding;
                }
            }
            $data=array();            
            $data['curPage']=1;
            $data['totalRecords']= 1000;
            $data['data']=$retval;            
            //$this->log_message("DATA : $data");
            //echo json_encode($data);
            return $data;
        }                

        function outstanding_void($filterdate,$filterdate2) {
            
            $this->log_message("OUTSTANDING MODEL CALLED");
            $retval=array();
            $start="2014-09-01";
            $finish="2014-12-31";
            $sql =" SELECT orders_no,cl_name as name,partner_name FROM orders inner join client on orders.orders_client_id=client.cl_id INNER JOIN partner on partner.partner_id=orders.orders_partner_id  WHERE to_date(orders_date::text,'YYYY-MM-DD')>=? AND to_date(orders_date::text,'YYYY-MM-DD')<=?  ORDER BY orders_no ";
            $this->log_message("SQL $sql");
            $global_q = $this->db->query($sql,array($filterdate,$filterdate2));
            $total=0;
            $counter=0;
            $this->log_message($sql);
            foreach($global_q->result() as $row_global) {
                $this->log_message($row_global->orders_no);
                $trans_id=$row_global->orders_no;
                $arr_paid = $this->tr_id_is_paid($trans_id);
                //$row_global->trans_id);
                $paid=$arr_paid['paid'];
                $outstanding = $arr_paid['outstanding'];
                if($paid==0) {
                        $counter=$counter+1;
                        $name = $row_global->name;
                        $retval[ ] = array(
			'counter' => $counter,
                        'trans_id' => $row_global->orders_no,
                        'name' => $name,
                        'outstanding' => $outstanding
                        );
                        $total=$total+$outstanding;
                }
            }

            $data=array();            
            $data['curPage']=1;
            $data['totalRecords']= 1000;
            $data['data']=$retval;            
            //$this->log_message("DATA : $data");
            //echo json_encode($data);
            return $data;
        }          
        function calc_rl($start,$finish){            
            $data = array();
            $sql = "SELECT trans_detail.*,(coalesce(client.cl_title,' ') || client.cl_name) as cl_name," ;
            $sql = $sql .  " transout.tro_id   FROM trans_detail  ";
            $sql = $sql .  " INNER JOIN transout   ON transout.tro_id =trans_detail.trans_id  ";
            $sql = $sql .  " INNER JOIN client on transout.tro_cust_id=client.cl_id ";            
            $sql = $sql . " WHERE  ";
            $sql = $sql . " to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=?";            
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;            
            //
            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            
            $retval[ ] = array(
                    'counter' =>"" ,
		    'date'=>"",
                    'name' => "PENDAPATAN",
                    'package_name' => "",
                    'debet' => "",
                    'kredit' => "",
                    'desc' => "",
                );            
            
            foreach($query->result() as $row) {              
                $name = $row->cl_name . "";
                //                                                                
                $totalnetto = $totalnetto + $row->trans_total;
                $totaldisc = $totaldisc + 0;
                $totalprice = $totalprice + $row->trans_price;
                //'package_name' => $row->orders_svc_name,
                
		$retval[ ] = array(
                        'counter' => $counter,
		    	'date'=>"",
			'name' => $name,
                        'package_name' => $row->trans_name,
                        'debet' => "",
                        'kredit' => $row->trans_total,
                        'desc' => "",
                    );

                $counter = $counter + 1;                
            }
            //
            
		$retval[ ] = array(
                        'counter' =>"",
		    	'date'=>"",
			'name' => "TOTAL PENDAPATAN",
                        'package_name' => "",
                        'debet' => "",
                        'kredit' => $totalnetto,
                        'desc' => "",
                    );
            $totalpendapatan=$totalnetto;
            
            //
            //piutang
            //$counter = $counter + 1;                
            $retval[ ] = array(
                    'counter' =>"",
		    'date'=>"",
                    'name' => "PIUTANG",
                    'package_name' => "",
                    'debet' => "",
                    'kredit' => "",
                    'desc' => "",
                );            
            $CI =& get_instance();
            $CI->load->model('model_payment');            
            $this->modpayment = $CI->model_payment;
            $totalpiutang=0;
            $dataoutstand=$this->outstanding($start,$finish);
            $counter = $counter + 1;                
            foreach($dataoutstand['data'] as $row) {
                $retval[ ] = array(
                        'counter' =>$counter ,
	                'date'=>"",
                        'name' => $row['name'],
                        'package_name' => "",
                        'kredit' => "",
                        'debet' => $row['outstanding'],
                        'desc' => "",
                    );                
                $totalpiutang=$totalpiutang + $row['outstanding'];
                $counter = $counter + 1;                
            }
            //
            $retval[ ] = array(
                    'counter' =>"" ,
		    'date'=>"",
                    'name' => "TOTAL PIUTANG",
                    'package_name' => "",
                    'kredit' => "",
                    'debet' => $totalpiutang,
                    'desc' => "",
                );                            
            
            //
            
            //Biaya
            $retval[ ] = array(
                    'counter' =>"" ,
		    'date'=>"",
                    'name' => "BIAYA",
                    'package_name' => "",
                    'debet' => "",
                    'kredit' => "",
                    'desc' => "",
                );                        
            //            
            $databiaya=$this->cost_recap($start,$finish);
            $totalbiaya=0;
            //
            foreach($databiaya as $row) {
                $retval[ ] = array(
                        'counter' =>$counter ,
		        'date'=>"",
                        'name' => $row['name'],
                        'package_name' => $row['package_name'],
                        'kredit' => $row['kredit'],
                        'debet' => $row['debet'],
                        'desc' => $row['desc']
                    );                
                $totalbiaya=$totalbiaya + $row['debet'];
                $counter = $counter + 1;                
            }
            //
            $retval[ ] = array(
                    'counter' =>"" ,
		    'date'=>"",
                    'name' => "TOTAL BIAYA",
                    'package_name' => "",
                    'debet' => $totalbiaya,
                    'kredit' => "",
                    'desc' => "",
                );                                   
	    //HUTANG DAGANG 
            $retval[ ] = array(
                    'counter' =>"" ,
		    'date'=>"",
                    'name' => "BAYAR SUPPLIER",
                    'package_name' => "",
                    'debet' => "",
                    'kredit' => "",
                    'desc' => "",
                );                        
            //            
            $datahd=$this->recap_hutang_dagang($start,$finish);
            $totalhd=0;
            //
            foreach($datahd as $row) {
                $retval[ ] = array(
                        'counter' =>$counter ,
		        'date'=>"",
                        'name' => $row['name'],
                        'package_name' => $row['package_name'],
                        'kredit' => $row['kredit'],
                        'debet' => $row['debet'],
                        'desc' => $row['desc']
                    );                
                $totalhd=$totalhd + $row['debet'];
                $counter = $counter + 1;                
            }
            //
            $retval[ ] = array(
                    'counter' =>"" ,
		    'date'=>"",
                    'name' => "TOTAL BYR SUPPLIER",
                    'package_name' => "",
                    'debet' => $totalhd,
                    'kredit' => "",
                    'desc' => "",
                );                                   
            //TOTAL RUGI LABA
	    $rugilaba = $totalpendapatan - $totalpiutang - $totalbiaya - $totalhd; 
            $retval[ ] = array(
                    'counter' =>"" ,
		    'date'=>"",
                    'name' => "RUGI LABA",
                    'package_name' => "",
                    'debet' => $rugilaba,
                    'kredit' => "",
                    'desc' => "",
                );                                   


            return $retval;            
        }          
        function cost_recap($start,$finish) {
            $retval=array();
            $sql = " SELECT * FROM journal WHERE j_type=1 ";
            $sql = $sql . " AND to_date(j_date::text,'YYYY-MM-DD')>=? ";
            $sql = $sql . " AND to_date(j_date::text,'YYYY-MM-DD')<=? ";
            $sql = $sql . " ORDER BY j_date ";
            $this->log_message("$sql , $start , $finish");
            $qry = $this->db->query($sql,array($start,$finish));
            foreach($qry->result() as $row) {
                $coa_code=$row->j_d_code;
                $coa_name=$this->get_coa_name($coa_code);
                $value = $row->j_d_value;
                $retval[ ] = array(
                        'counter' =>"",
		        'date'=>"",
                        'name' => $coa_name,
                        'package_name' => "",
                        'debet' => $value,
                        'kredit' => "",
                        'desc' => $row->j_desc,
                    );                                        
            }
            return $retval;
        }
	function get_supplier_name($trans_id) {
		$retval="";
		$sql = " SELECT supp_name FROM transin INNER JOIN supplier ON supplier.supp_id=transin.tri_supp_id  WHERE tri_supp_id=? ";
		$qry = $this->db->query($sql,array($trans_id));
		foreach($qry->result() as $row) {
			//
			$retval = $row->supp_name;
		}
		return $retval;
	}
        function recap_hutang_dagang($start,$finish) {
            $retval=array();
            $sql = " SELECT * FROM journal WHERE j_type='5' ";
            $sql = $sql . " AND to_date(j_date::text,'YYYY-MM-DD')>=? ";
            $sql = $sql . " AND to_date(j_date::text,'YYYY-MM-DD')<=? ";
            $sql = $sql . " ORDER BY j_date ";
            $this->log_message("$sql , $start , $finish");
            $qry = $this->db->query($sql,array($start,$finish));
            foreach($qry->result() as $row) {
		$supp_name = $this->get_supplier_name($row->j_desc);
                $coa_code=$row->j_d_code;
                $coa_name=$this->get_coa_name($coa_code);
                $value = $row->j_d_value;
                $retval[ ] = array(
                        'counter' =>"",
		        'date'=>"",
                        'name' => $coa_name . " ke " . $supp_name,
                        'package_name' => "",
                        'debet' => $value,
                        'kredit' => "",
                        'desc' => $row->j_desc,
                    );                                        
            }
            return $retval;
        }

        function get_check_up_date($lab_id) {
            $sql = " SELECT date_name(to_date(frontend_date::text,'YYYY-MM-DD')) as date FROM frontend WHERE ";
            $sql = $sql . " frontend_lab_id=?";
            $query = $this->db->query($sql,array($lab_id));
            $date = "";
            foreach($query->result() as $row) {
                $date =$row->date;
            }
            return $date;
        }
        function get_omzet_by_group($groupname,$lab_id) {
            $groupname = strtolower($groupname);
            $retval = 0;
            $sql = "SELECT ";
            $sql = $sql . " coalesce(sum(result_detail.rd_net),0) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " WHERE  1=1 ";
            $sql = $sql . " AND rd_frontend_lab_id =?";
            $sql = $sql . " AND lower(substr(trim(rd_pxcode),1,1))=?";            
            $sql = $sql . " AND length(rd_pxcode)=5 ";
            $this->log_message("GROUP GET $sql dan lab id $lab_id group name $groupname");
            $query = $this->db->query($sql,array($lab_id,$groupname));
            foreach($query->result() as $row) {
                $retval = $row->net;
            }
            return $retval;
            //rd_frontend_lab_id 
        }
        function calc_omzet_detail($start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT orders_detail.*,(coalesce(client.cl_title,' ') || client.cl_name) as cl_name," ;
            $sql = $sql .  " orders.orders_bank_id     FROM orders_detail ";
            $sql = $sql .  " INNER JOIN orders  ON orders.orders_no=orders_detail.orders_no ";
            $sql = $sql .  " INNER JOIN client on orders.orders_client_id=client.cl_id ";            
            $sql = $sql . " WHERE  ";
            $sql = $sql . " to_date(orders_date::text,'YYYY-MM-DD')>=? AND to_date(orders_date::text,'YYYY-MM-DD')<=?";            
            $sql = $sql . " AND orders_svc_type=1 "; 
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            foreach($query->result() as $row) {              
                $name = $row->cl_name;                
                //                                                                
                $totalnetto = $totalnetto + $row->orders_net;
                $totaldisc = $totaldisc + $row->orders_disc;
                $totalprice = $totalprice + $row->orders_price;

		$retval[ ] = array(
                        'counter' => $counter,
			'name' => $row->cl_name,
                        'package_name' => $row->orders_svc_name,
                        'price' => $row->orders_price,
                        'netto' => $row->orders_net,
                        'disc' => $row->orders_disc,
                    );
                $counter = $counter + 1;                
            }
            //
		$retval[ ] = array(
                        'counter' =>"" ,
			'name' => "TOTAL",
                        'package_name' => "",
                        'price' => $totalprice,
                        'netto' => $totalnetto,
                        'disc' => $totaldisc,
                    );
            
            return $retval;            
        }  
        //
        function calc_doc_omzet_detail($start,$finish,$doct_id) {
            $CI =& get_instance();
            $CI->load->model('model_pasien');            
            $this->modpasien = $CI->model_pasien;
            
            $data = array();
            $sql = "SELECT date_name(frontend_date) as date,frontend_lab_id as lab_id,patient_name, sum(result_detail.rd_price) as bruto,";
            $sql = $sql . " sum(result_detail.rd_disc) as disc,sum(result_detail.rd_net) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " INNER JOIN frontend ON frontend.frontend_lab_id=result_detail.rd_frontend_lab_id ";
            $sql = $sql . " INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql . " WHERE length(rd_pxcode)=5 AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " AND frontend_doct_id=? ";
            $sql = $sql . " GROUP BY frontend_lab_id,frontend_date,patient_name ORDER BY lab_id";
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish,$doct_id));
            $counter = 1;            
            $totalnetto =0;
            $hema = 0;
            $kimia = 0;
            $imun = 0;
            $faeces = 0;
            $urine = 0;
            $analisalain = 0;
            $mikro = 0;
            $infeksilain = 0;
            $radiologi = 0;
            $ekg = 0;
            $usg = 0;            
            $totalhema = 0;
            $totalkimia = 0;
            $totalimun = 0;
            $totalfaeces = 0;
            $totalurine = 0;
            $totalanalisalain = 0;
            $totalmikro = 0;
            $totalinfeksilain = 0;
            $totalradiologi = 0;
            $totalekg = 0;
            $totalusg = 0;               
            $grandtotal = 0;
            foreach($query->result() as $row) {              
                $name = $this->modpasien->get_name_by_labid($row->lab_id);
                //$name = "";
                $date = $this->get_check_up_date($row->lab_id);
                $hema = 0;
                $kimia = 0;
                $imun = 0;
                $faeces = 0;
                $urine = 0;
                $analisalain = 0;
                $mikro = 0;
                $infeksilain = 0;
                $radiologi = 0;
                $ekg = 0;
                $usg = 0;                
                
                $hema = $this->modomzet->get_omzet_by_group('H', $row->lab_id);                
                $kimia = $this->modomzet->get_omzet_by_group('K', $row->lab_id);
                $imun = $this->modomzet->get_omzet_by_group('I', $row->lab_id);
                $faeces = $this->modomzet->get_omzet_by_group('F', $row->lab_id);
                $urine = $this->modomzet->get_omzet_by_group('U', $row->lab_id);
                $analisalain = $this->modomzet->get_omzet_by_group('A', $row->lab_id);
                $mikro = $this->modomzet->get_omzet_by_group('M', $row->lab_id);
                $infeksilain = $this->modomzet->get_omzet_by_group('S', $row->lab_id);
                $radiologi = $this->modomzet->get_omzet_by_group('R', $row->lab_id);
                $ekg = $this->modomzet->get_omzet_by_group('E', $row->lab_id);
                $usg = $this->modomzet->get_omzet_by_group('U', $row->lab_id);
                $totalhema = $totalhema + $hema;
                $totalkimia = $totalkimia + $kimia;
                $totalimun = $totalimun + $imun;
                $totalfaeces = $totalfaeces + $faeces;
                $totalurine = $totalurine + $urine;
                $totalanalisalain = $totalanalisalain + $analisalain;
                $totalmikro = $totalmikro + $mikro;
                $totalinfeksilain = $totalinfeksilain + $infeksilain;
                $totalradiologi = $totalradiologi + $radiologi;
                $totalekg = $totalekg + $ekg;
                $totalusg = $totalusg + $usg;
                //                                                                
                $totalnetto = $hema + $kimia + $imun + $faeces + $urine + $analisalain + $mikro + $infeksilain +  $radiologi + $ekg + $usg;
                $grandtotal = $grandtotal + $totalnetto;
		$retval[ ] = array(
                        'counter' => $counter,
			'lab_id' => $row->lab_id,
                        'name' => $name,
                        'date' => $date,
                        'hema' => $hema,
                        'kimia' => $kimia,
			'imun' => $imun,
                        'faeces' => $faeces,
                        'urine' => $urine,
                        'analisalain' => $analisalain,
                        'mikro' => $mikro,
                        'infeksilain' => $infeksilain,
                        'radiologi' => $radiologi,
                        'ekg' => $ekg,
                        'usg' => $usg,
                        'netto' => $totalnetto
                    );
                $counter = $counter + 1;                
            }
            //
		$retval[ ] = array(
                        'counter' => "",
			'lab_id' => "TOTAL",
                        'name' => "",
                        'date' => "",
                        'hema' => $totalhema,
                        'kimia' => $totalkimia,
			'imun' => $totalimun,
                        'faeces' => $totalfaeces,
                        'urine' => $totalurine,
                        'analisalain' => $totalanalisalain,
                        'mikro' => $totalmikro,
                        'infeksilain' => $totalinfeksilain,
                        'radiologi' => $totalradiologi,
                        'ekg' => $totalekg,
                        'usg' => $totalusg,
                        'netto' => $grandtotal
                    );            
            //
            $percenthema= $this->get_percent_of_group('h');
            $percentkimia=$this->get_percent_of_group('k');
            $percentimun=$this->get_percent_of_group('i');
            $percentfaeces=$this->get_percent_of_group('f');
            $percenturine=$this->get_percent_of_group('u');
            $percentanalisalain=$this->get_percent_of_group('a');
            $percentmikro=$this->get_percent_of_group('m');
            $percentinfeksilain=$this->get_percent_of_group('s');
            $percentradiologi=$this->get_percent_of_group('r');
            $percentekg=$this->get_percent_of_group('e');
            $percentusg=$this->get_percent_of_group('g');
            //
            $totalhema=$percenthema*$totalhema;
            $totalkimia=$percentkimia*$totalkimia;
            $totalimun=$percentimun*$totalimun;
            $totalfaeces=$percentfaeces*$totalfaeces;
            $totalurine=$percenturine*$totalurine;
            $totalanalisalain=$percentanalisalain*$totalanalisalain;
            $totalmikro=$percentmikro*$totalmikro;
            $totalinfeksilain=$percentinfeksilain*$totalinfeksilain;
            $totalradiologi=$percentradiologi*$totalradiologi;
            $totalekg=$percentekg*$totalekg;
            $totalusg=$percentusg*$totalusg;
            //
            $totalpercent = $totalhema + $totalkimia + $totalimun + $totalfaeces + $totalurine + $totalanalisalain + $totalmikro + $totalinfeksilain +  $totalradiologi + $totalekg + $totalusg;
            //
            $retval[ ] = array(
                    'counter' => "",
                    'lab_id' => "% CN",
                    'name' => "",
                    'date' => "",
                    'hema' => $percenthema,
                    'kimia' => $percentkimia,
                    'imun' => $percentimun,
                    'faeces' => $percentfaeces,
                    'urine' => $percenturine,
                    'analisalain' => $percentanalisalain,
                    'mikro' => $percentmikro,
                    'infeksilain' => $percentinfeksilain,
                    'radiologi' => $percentradiologi,
                    'ekg' => $percentekg,
                    'usg' => $percentusg,
                    'netto' => ""
                );
            //
            $retval[ ] = array(
                    'counter' => "",
                    'lab_id' => "TOTAL CN",
                    'name' => "",
                    'date' => "",
                    'hema' => $totalhema,
                    'kimia' => $totalkimia,
                    'imun' => $totalimun,
                    'faeces' => $totalfaeces,
                    'urine' => $totalurine,
                    'analisalain' => $totalanalisalain,
                    'mikro' => $totalmikro,
                    'infeksilain' => $totalinfeksilain,
                    'radiologi' => $totalradiologi,
                    'ekg' => $totalekg,
                    'usg' => $totalusg,
                    'netto' => $totalpercent
                );                        
            //
            return $retval;                        
        }  
        //end of d  oct omzet
        function get_percent_of_group($group_code) {
            $percent=0;
            $group_code = strtolower($group_code);
            $sql = " SELECT gp_percent FROM grouppx WHERE lower(trim(gp_name))=?";
            $query = $this->db->query($sql,array($group_code));
            foreach($query->result() as $row) {
                $percent = $row->gp_percent;
            }
            return $percent;
        }
        //
        function calc_omzet_global($start,$finish) {
            $data = array();
            $sql = "SELECT frontend_lab_id as lab_id,patient_name, sum(result_detail.rd_price) as bruto,";
            $sql = $sql . " sum(result_detail.rd_disc) as disc,sum(result_detail.rd_net) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " INNER JOIN frontend ON frontend.frontend_lab_id=result_detail.rd_frontend_lab_id ";
            $sql = $sql . " INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql . " WHERE length(rd_pxcode)=5 AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " GROUP BY frontend_lab_id,patient_name ORDER BY lab_id";
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;
            $totalbruto = 0;
            $totaldisc =0;
            $totalnetto =0;
            foreach($query->result() as $row) {              
                $date = $this->get_check_up_date($row->lab_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'lab_id' => $row->lab_id,
                        'date' => $date,                    
			'name' => $row->patient_name,
                        'bruto' => $row->bruto,
                        'disc' => $row->disc,
                        'net' => $row->net,
		);                      
                $counter = $counter + 1;
                $totalbruto = $totalbruto + $row->bruto;
                $totaldisc = $totaldisc + $row->disc;
                $totalnetto = $totalnetto + $row->net;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'lab_id' =>"",
                    'date'=>"",
                    'name' => "TOTAL",
                    'bruto' => $totalbruto,
                    'disc' => $totaldisc,
                    'net' => $totalnetto
            ); 
            return $retval;
        }  
        
        //doc
        function patient_count_per_month($month,$year,$doct_id) {
            $sql = " SELECT COUNT(*) as jumlah FROM frontend ";
            $sql = $sql . " WHERE frontend_doct_id=? " ;                   
            $sql = $sql . " AND extract(month from frontend_date)=? AND extract(year from frontend_date)=? ";
            $this->log_message("patient doct $doct_id dengan $sql dan $month serta $year");
            $query = $this->db->query($sql,array($doct_id,$month,$year));
            $count = 0;
            foreach($query->result() as $row) {
                $count = $row->jumlah;
            }
            return $count;
        }
        
        //
        function patient_count($start,$finish,$doct_id) {
            $sql = " SELECT COUNT(*) as jumlah FROM frontend ";
            $sql = $sql . " WHERE frontend_doct_id=? " ;                   
            $sql = $sql . " AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $query = $this->db->query($sql,array($doct_id,$start,$finish));
            $count = 0;
            foreach($query->result() as $row) {
                $count = $row->jumlah;
            }
            return $count;
        }
        function calc_omzet_doct_global($start,$finish) {
            $data = array();            
            $sql = "SELECT doct_id as doct_id,doct_name, sum(result_detail.rd_price) as bruto,";
            $sql = $sql . " sum(result_detail.rd_disc) as disc,sum(result_detail.rd_net) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " INNER JOIN frontend ON frontend.frontend_lab_id=result_detail.rd_frontend_lab_id ";
            $sql = $sql . " INNER JOIN doctor ON frontend.frontend_doct_id=doctor.doct_id ";
            $sql = $sql . " WHERE length(rd_pxcode)=5 AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " GROUP BY doct_id,doct_name ORDER BY doct_id";
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;
            $totalbruto = 0;
            $totaldisc =0;
            $totalnetto =0;
            $totalcount = 0;
            foreach($query->result() as $row) {                              
                $count = $this->patient_count($start,$finish,$row->doct_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'doct_id' => $row->doct_id,
                        'name' => $row->doct_name,
                        'count' => $count,
                        'bruto' => $row->bruto,
                        'disc' => $row->disc,
                        'net' => $row->net,
		);                      
                $counter = $counter + 1;
                $totalbruto = $totalbruto + $row->bruto;
                $totaldisc = $totaldisc + $row->disc;
                $totalnetto = $totalnetto + $row->net;
                $totalcount = $totalcount + $count;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'doct_id' => "",
                    'count' => $totalcount,
                    'name' => "TOTAL",
                    'bruto' => $totalbruto,
                    'disc' => $totaldisc,
                    'net' => $totalnetto
            );                                  
            return $retval;
        }          
}
?>
