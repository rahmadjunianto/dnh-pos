<?php
/*
 * Januari 2014
 */

class Model_jabatan extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                                $this->load->library("class_public");
	}
                function get_jabatan_standar() {                        
                        $retval = 0;
//                        $jabatanstandar = $this->class_public->getSettingGaji("jabatan standar");
//                        $this->log_message("jabatan standar: " . $jabatanstandar);
//                        $retval = $jabatanstandar;
                        return $retval;
                }
                
	function get_data_master_jabatan() {//dipake
		$retval = array();
                                $sql = " SELECT * FROM masterjabatan ORDER by mj_no ";
                                $query = $this->db->query($sql);
                                return $query->result();
	}
                //save std jabatan
                function save_jabatan_cabang($idcabang,$spv1,$spv2,$spv3,$wakacab,$kacab ) {                                        
                    //
                    $spv1 = $this->class_public-> AvoidNullNum($spv1);
                    $spv2 = $this->class_public->AvoidNullNum($spv2);
                    $spv3 = $this->class_public->AvoidNullNum($spv3);
                    $wakacab = $this->class_public->AvoidNullNum($wakacab);
                    $kacab = $this->class_public->AvoidNullNum($kacab);
                    $strSQL = " UPDATE cabang SET ";
                    $strSQL = $strSQL . " jabatan_spv1= ? , ";
                    $strSQL = $strSQL . " jabatan_spv2= ? ,";
                    $strSQL = $strSQL . " jabatan_spv3= ? ,";
                    $strSQL = $strSQL .  " jabatan_wakacab= ? ,";
                    $strSQL = $strSQL .  " jabatan_kacab= ?  ";
                    $strSQL = $strSQL  . " WHERE idcabang= ? ";
                    $query = $this->db->query($strSQL, array($spv1,$spv2,$spv3,$wakacab,$kacab,$idcabang));                    
                    return 0;
                }
                //end of save std jabatan
}

?>
