<?php

class Model_grade extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
	}
	function get_distinct_grade() {//dipake
//		$retval = array();
                                $query = $this->db->query('SELECT distinct(mg_grade) as grade ,mg_nominal from mastergapok order by mg_nominal');
                                return $query->result();
	}
                function get_jabatan_pegawai($idsdm) {
                        $retval = "-";
                        $sql = " select gp_jabatan  from gradepegawai WHERE gp_sdmid=? ";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $retval = $row->gp_jabatan;
                        }                  
                        return $retval;
                }
                //
                function get_grade_gapok_pegawai($idsdm) {
                        $retval = "-";
                        $grade = "-";
                        $sql = " select gp_grade  from gradepegawai WHERE gp_sdmid=? ";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $grade = trim($row->gp_grade);
                        }
                        $retval = $grade;
                        return $retval;                    
                }
                //gp_masakerja_prestasi
                function get_masa_kerja_diakui_pegawai($idsdm) {
                        $retval = 0;
                        $sql = " select gp_masakerja_prestasi from gradepegawai WHERE gp_sdmid=? ";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $retval = trim($row->gp_masakerja_prestasi);
                        }
                        return $retval;                    
                }                
                //gp_masakerja_prestasi
                //tunjab,tulok,tupres
                function get_all_tunjangan_grade($idsdm) {
                        $retval = array();
                        $sql = " select * from gradepegawai WHERE gp_sdmid=? ";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {                            
                            $retval[ ] = array(
                                    'mg_min' => $row->min,
                                    'mg_max' => $row->max
                                    );
                        }
                        return $retval;                    
                }                
                //tunjab/tulok/tupres
                //gp_gajitraining
                function get_gaji_training($idsdm) {
                        $retval = 0;
                        $sql = " select gp_gajitraining from gradepegawai WHERE gp_sdmid=? ";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $retval = trim($row->gp_gajitraining);
                        }
                        return $retval;                    
                }                                
                //gp_gajitraining
                function get_grade_simulasi() {
                            $sql=" SELECT mg_grade as Grade,mg_nominal as Nominal FROM mastergapok WHERE mg_sma=1 ORDER BY mg_min,mg_max,mg_grade";
                            $query = $this->db->query($sql);
                            return $query->result();
                }
        //
	function get_grade_value($grade) {//dipake
                            
                                $retval=0;
                                $query = $this->db->query("SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))='" . strtolower($grade) . "'");
                                return $query->result();
                                //foreach ($query->result() as $row) {
                                    //$retval = $row->nominal;
                                //}
                                //return $retval;
	}
                function get_masa_kerja_grade_by_pendidikan($grade,$pendidikan) {//dipake
                            
                                $retval=0;
                                switch(strtolower(trim($pendidikan))) {
                                    case "sma":
                                        $sql = "SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))=? and mg_sma=1 ";
                                        break;
                                    case "smak":
                                        $sql = "SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))=? and mg_smak=1 ";
                                        break;   
                                    case "d1":
                                        $sql = "SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))=? and mg_d1=1 ";
                                        break;                                    
                                    case "d3":
                                        $sql = "SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))=? and mg_d3=1 ";
                                        break;                                    
                                    case "s1":
                                        $sql = "SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))=? and mg_s1=1 ";
                                        break;
                                    default:
                                        $sql = "SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))=? and mg_sma=1 ";
                                }
                                $query = $this->db->query($sql, array($grade));
                                foreach ($query->result() as $row) {
                                    $retval = $row->mg_min;
                                }
                                return $retval;
	}
	function get_grade_value_scalar($grade) {//dipake
                            
                                $retval=0;
                                $query = $this->db->query("SELECT mg_nominal  as nominal,mg_min,mg_max from mastergapok WHERE lower(trim(mg_grade))='" . strtolower($grade) . "'");                                
                                foreach ($query->result() as $row) {
                                    $retval = $row->nominal;
                                }
                                $this->log_message("nominal scalar $retval");
                                return $retval;
	}        
                function update_grade_nominal($grade,$nominal) {
                    $data = array(
                                   'mg_nominal' => $nominal
                                );
                    $this->db->where('mg_grade', $grade);
                    $this->db->update('mastergapok', $data); ;
                    $this->log_message("selesai grade");
                    return 0;
                }
                //update grade pegawai
                function update_grade_pegawai($sdmid,$grade,$masakerja,$masakerjaprestasi,$jabatan,$training) {
                    $countsdm=0;
                    $sql  = " SELECT COUNT(*) as jumlah FROM gradepegawai WHERE gp_sdmid=?";
                    $query = $this->db->query($sql, array($sdmid));
                    foreach ($query->result() as $row) {
                        $countsdm = trim($row->jumlah);
                    }                    
                    If ($countsdm>0) {                        
                        $sql  = " update gradepegawai SET  point_tulok=100,point_tupres=100,point_tures=100,gp_grade=?,gp_masakerja=?,gp_masakerja_prestasi=?,gp_jabatan=?,gp_gajitraining=? WHERE gp_sdmid=?";
                        $this->log_message($sql);
                        $query = $this->db->query($sql, array($grade,$masakerja,$masakerjaprestasi,$jabatan,$training,$sdmid));
                    }  else {
                        $sql = " INSERT INTO gradepegawai(point_tures,point_tupres,point_tulok,gp_grade,gp_masakerja,gp_masakerja_prestasi,gp_jabatan,gp_sdmid,gp_gajitraining)VALUES(";
                         $sql = $sql .  "100,100,100,?,?,?,?,?,?)";
                         $query = $this->db->query($sql, array($grade,$masakerja,$masakerjaprestasi,$jabatan,$sdmid,$training)); 
                    }
                    return 0;
//                    Else
//                        strSQL = " INSERT INTO gradepegawai(point_tures,point_tupres,point_tulok,gp_grade,gp_masakerja,gp_masakerja_prestasi,gp_jabatan,gp_sdmid)VALUES("
//                        strSQL = strSQL & "100,100,100,'" & UCase(Trim(sgrade)) & "'," & sMasaKerja & "," & AvoidNUllNum(sMasakerjaPrestasi) & ",'" & sjabatan & "','" & idpegawai & "')"
//                    End If                    
                }
                ///update grade tunjangan
                
}
?>
