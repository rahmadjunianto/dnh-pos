<?php
/*
 * Januari 2014
 */

class Model_panel extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                //$this->load->library("class_public");
	}
        function delpx($code,$panel_id) {
            $code=strtolower(trim($code));
            $sql = " DELETE FROM paket_detail WHERE dp_mpid=? and lower(trim(dp_pxcode))=?";
            $this->log_message("DELETE $sql $panel_id dan $code ");
            $this->db->query($sql,array($panel_id,$code));
            return 1;
        }
        function get_pxname($code) {
            $sql = " SELECT pc_pxname FROM pxcode WHERE pc_pxcode=?";
            $query = $this->db->query($sql,array($code));
            $name = "";
            foreach($query->result() as $row) {
                $name = $row->pc_pxname;
            }
            return $name;
        }
        function addpx($code,$panel_id) {
            $pxname = $this->get_pxname($code);
            $sql = " INSERT INTO paket_detail(dp_mpid,dp_pxcode,dp_pxname)VALUES(?,?,?)";
            $this->log_message($sql . " panel ID : $panel_id $code $pxname ");
            $this->db->query($sql,array($panel_id,$code,$pxname));
            return 1;
        }
        function get_panel() {
            $sql = " SELECT  * FROM master_paket  ORDER by mp_name ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function detail_panel($id) {
            $sql = " SELECT  * FROM paket_detail WHERE dp_mpid=? ORDER by dp_pxname ";
            $query = $this->db->query($sql,array($id));
            return $query->result();            
        }
        function panel_info($id) {
            $sql = " SELECT mp_id,mp_name,mp_bruto,mp_netto,dp_pxcode,dp_pxname,pc_price as price,pc_remarks as desc  from master_paket ";
            $sql = $sql . " INNER JOIN paket_detail ON paket_detail.dp_mpid=master_paket.mp_id ";
            $sql = $sql . " INNER JOIN pxcode  ON pxcode.pc_pxcode=paket_detail.dp_pxcode ";
            $sql = $sql . " WHERE  master_paket.mp_id=? ORDER BY dp_pxcode";
            $this->log_message("SQL dengan id $id");
            $query = $this->db->query($sql,array($id));
            return $query->result();
        }
}
?>
