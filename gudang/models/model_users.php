<?php
/*
 * Januari 2014
 */

class Model_users extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                //$this->log_message("CALLED");
	}
        function add_user_to_group($gu_id,$id) {
            $name=$this->get_users_name($id);
            $sql = " INSERT INTO groupmember(gm_guid,gm_userid,gm_username)VALUES(?,?,?)";
            $this->db->query($sql,array($gu_id,$id,$name));
            return 1;
        }
        function get_group_name($id) {
            $retval="";
            $sql = " SELECT * FROM group_user WHERE gu_id=?";
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row)  {
                $retval = $row->gu_name;
            }
            return $retval;
        }
        //delete 
        function add_group_to_sectable($gu_id,$id) {
            $name=$this->get_group_name($gu_id);
            $this->log_message("group name $name untuk id $gu_id");
            $sql = " INSERT INTO usersec(us_group_id,us_form_id,us_group_name)VALUES(?,?,?)";
            $this->log_message("SQL $sql sectable");
            $this->db->query($sql,array($gu_id,$id,$name));
            return 1;
        }        
        //
        function get_users_data($user_id) {
            $sql = " SELECT *  FROM users WHERE users_id=?";
            $query = $this->db->query($sql,array($user_id));
            return $query->result();
        }
        function get_group() {
            $sql = " SELECT gu_id as id, gu_name as name FROM group_user ORDER BY gu_name";
            $query = $this->db->query($sql);
            return $query->result();
            
        }
        function search_users($name) {
            //$this->log_message("CALLED $name");
            $name= strtolower($name);
            $sql = " SELECT users_id as id,users_login as login,users_name as name  FROM users WHERE lower(trim(users_name)) LIKE '%" . $name . "%'";
            $this->log_message("name $sql");
            $query = $this->db->query($sql);
            return $query->result();            
        }
        function search_groups($name) {
            //$this->log_message("CALLED $name");
            $name= strtolower($name);
            $sql = " SELECT *  FROM group_user WHERE lower(trim(gu_name)) LIKE '%" . $name . "%'";
            $this->log_message("name $sql");
            $query = $this->db->query($sql);
            return $query->result();            
        }        
        function get_users_name($user_id) {
            $sql = " SELECT *  FROM users WHERE users_id=?";
            $query = $this->db->query($sql,array($user_id));
            $name ="";
            foreach($query->result() as $row) {
                $name = $row->users_name;
            }
            return $name;
        }        
        //
        function get_mkt_percent($user_id) {
            $sql = " SELECT *  FROM users WHERE users_id=?";
            $query = $this->db->query($sql,array($user_id));
            $percent =0;
            foreach($query->result() as $row) {
                $percent = $row->users_percent;
            }
            return $percent;
        }                
}
?>
