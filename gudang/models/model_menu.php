<?php
/*
 * Januari 2014
 */

class Model_menu extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                //$this->load->library("class_public");
                $CI =& get_instance();
                $this->modpublic = $CI->load->model('model_public');                                       
	}
        //delete 
        function get_header_info() {
            $retval = $this->mod_public->get_settings('header info');
            return $retval;
        }
        function get_update_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
            $this->log_message("update url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_updateurl;
            }
            $this->log_message("update url " . $retval);
            return $retval;
        }                
        //delete url
        function get_delete_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_delurl  FROM tableforms WHERE tf_code=? ";
            $this->log_message("del  url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_delurl;
            }
            $this->log_message("delurl url " . $retval);
            return $retval;
        }                        
        //data url : tf_dataurl_addr
        function get_dataurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_dataurl_addr;
            }
            return $retval;
        }        
        //
        function get_newrowurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_newrow_url FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_newrow_url;
            }
            return $retval;
        }                
        //sorting column
        function get_default_sort_column($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_default_sortcol;
            }
            return $retval;
        }
        
        //table title
        function get_table_title($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_table_title FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_table_title;
            }
            return $retval;
        }
        //
        function get_table_info($tableindex) {
            $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order ";
            $this->log_message("Log table $tableindex  dan $sql ");
            $query  = $this->db->query($sql,array($tableindex));
            $retval = $query->result();
            return $retval;
        }
        //get table name
        function get_table_name($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_tablename;
            }
            return $retval;
        } 
        //get column name
        function get_column_name($tableindex,$colorder) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $this->log_message("COLUMN NAME $sql $tableindex order $colorder");
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_colname;
            }
            $this->log_message("NAMA COLOM $retval");
            return $retval;
        }         
        //col type
        function get_column_type($tableindex,$colorder) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tableforms_detail.tfd_coldatatype  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_coldatatype;
            }
            return (int)$retval;
        }         
        //get key_column
        //
        function get_key_column($tableid) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tf_keycolumn   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn;
            }
            return $retval;
        }         
        function get_key_column_index($tableid) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn_index;
            }
            return $retval;
        }              
        //
        //get sequence name
        function get_table_seq_name($tableid) {
            $retval = 0;
            $sql = " SELECT tf_seq_name   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_seq_name;
            }
            return $retval;            
        }
        //
        //get sequence name
        function get_table_code($tableid) {
            $retval = 0;
            $sql = " SELECT tf_code   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_code;
            }
            return $retval;            
        }        
        //function selected column
        function get_selected_column($tableindex) {
            //return "contract";
            $x=1;
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? ORDER BY tfd_order";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                if($x==1) {
                    $retval= $retval . $row->tfd_colname ;
                }else {
                    $retval= $retval . "," . $row->tfd_colname ;
                }
                $x++;
            }
            $this->log_message("SELECTED COL : " . $retval);
            return $retval;
        }         
        //end of selected col
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();                    
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }
        //filter
        function get_selection_filter_sql($tableindex) {
            $retval = "";
            $sql = " SELECT tf_selection_filter   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_selection_filter;
            }
            return $retval;                        
        }                
}
?>
