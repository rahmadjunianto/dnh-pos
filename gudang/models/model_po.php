<?php
/*
 * Januari 2014
 */

class Model_po extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                $this->load->library("class_public");         
	}
        function po_info($tr_id){
            $data = array();            
            $sql = " SELECT supp_address as addr,supp_id as id,supp_name as name,supp_address,date_name(localtimestamp) as jamsekarang,po.po_id,po_supp_id as id ,date_name(po.po_date) as podate,po_users_id  ";
            $sql = $sql .  " FROM po INNER JOIN po_detail ON po.po_id=po_detail.po_id";            
            $sql = $sql .  " INNER JOIN supplier ON supplier.supp_id=po.po_supp_id";            
            $sql = $sql . "  WHERE po.po_id= ? ";
            $this->log_message($sql . " ");
            $query = $this->db->query($sql,array($tr_id));
            return $query->result();
        }              
        function del_lab_id($lab_id) {
            $data=array();
            $status = -1;
            //fontend
            $doct_id="";
            $doct_name="";
            $patient_id ="";
            $patient_name ="";
            $sql = " SELECT frontend_doct_id,frontend_patient_id,(patient_title || ' ' || patient_name) as name,doct_name ";
            $sql = $sql . " FROM frontend INNER JOIN doctor ON doctor.doct_id=frontend.frontend_doct_id ";
            $sql = $sql . " INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql .  " WHERE frontend_lab_id=?";
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $doct_id = $row->frontend_doct_id;
                $doct_name = $row->doct_name;
                $patient_id = $row->frontend_patient_id;
                $patient_name = $row->name;                            
            }
            //insert into history;
            $user_id = $data['logged_in_user_name'] = $this->acl->get_users_id();
            $user_name = $this->acl->get_real_users_id();
            $sql = " SELECT * FROM result_detail WHERE rd_frontend_lab_id=? AND length(rd_pxcode)=5";
            $this->log_message(" DEL: SELECT RD : $sql dgn $lab_id");
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $pxcode = $row->rd_pxcode;
                $name = $row->rd_pxname;
                $price = $row->rd_price;
                $disc = $row->rd_disc;
                $net = $row->rd_net;
                $sql = " INSERT INTO delhistory(dh_lab_id,dh_pxcode,dh_pxname,dh_bruto,dh_disc,dh_net,dh_userid,dh_username,";
                $sql = $sql . "dh_patient_id,dh_patient_name,dh_doct_id,dh_doct_name)";
                $sql = $sql . "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                $this->log_message("DELETE $sql PARAM $lab_id,$pxcode,$name,$price,$disc,$net,$user_id,$user_name ");
                $this->db->query($sql,array($lab_id,$pxcode,$name,$price,$disc,$net,$user_id,$user_name,$patient_id,$patient_name,$doct_id,$doct_name));
            }
            //delete 
            $sql = " DELETE FROM result_detail WHERE rd_frontend_lab_id=?";
            $this->db->query($sql,array($lab_id));
            //electromedic
            $sql = " DELETE FROM em WHERE em_labid=?";
            $this->db->query($sql,array($lab_id));            
            //payment
            $sql = " DELETE FROM payment WHERE payment_labid=?";
            $this->db->query($sql,array($lab_id));                        
            //
            $sql = " DELETE FROM frontend WHERE frontend_lab_id=?";
            $this->db->query($sql,array($lab_id));                                    
            $status = 1;
            return $status;

        }
        //delete 
        function delpx($pxcode,$lab_id) {
            $pxcode=strtolower($pxcode);
            $sql = " DELETE  FROM result_detail WhERE  lower(rd_pxcode) LIKE '$pxcode%' and rd_frontend_lab_id=?";
            $this->log_message("DEL PX $sql nomer lab $lab_id nomer px $pxcode");
            $query = $this->db->query($sql,array($lab_id));
            return 0;
        }
        //
        function get_px_name($pxcode) {
            $retval ="";
            $pxname = "";
            $sql = " SELECT pc_pxcode,pc_pxname FROM pxcode WHERE lower(trim(pc_pxcode))=?";
            $this->log_message("SQL $sql dan pxcode $pxcode");
            $query = $this->db->query($sql,array($pxcode));
            foreach($query->result() as $row) {
                $pxname = $row->pc_pxname;
            }
            $retval = $pxname;
            return $retval;
        }
        function is_grouped_into_electromedic($pxcode,$lab_id) {
            $pxname = $this->get_px_name($pxcode);
            $first_char=strtolower(substr($pxcode,0,1));
            $this->log_message("first char $first_char");            
            $pxcode=strtoupper($pxcode);
            if($first_char=="r") {
                //rontgen
                $sql = " INSERT INTO em(em_labid,em_pxcode,em_pxname,em_type)VALUES(?,?,?,?)";
                $this->log_message("Rontgen $sql $lab_id, $pxcode , $pxname");
                $this->db->query($sql,array($lab_id,$pxcode,$pxname,1));
            }
            if($first_char=="g") {
                //rontgen
                $sql = " INSERT INTO em(em_labid,em_pxcode,em_pxname,em_type)VALUES(?,?,?,?)";
                $this->log_message("USG $sql $lab_id, $pxcode , $pxname");
                $this->db->query($sql,array($lab_id,$pxcode,$pxname,2));                
            }            
            if($first_char=="e") {
                //rontgen
                $sql = " INSERT INTO em(em_labid,em_pxcode,em_pxname,em_type)VALUES(?,?,?,?)";
                $this->log_message("ECG $sql $lab_id, $pxcode , $pxname");
                $this->db->query($sql,array($lab_id,$pxcode,$pxname,3));                
            }            
        }
        //
        function get_px_parent($pxcode) {
            $pxcode=strtolower($pxcode);
            $data = array();
            $first_char=strtolower(substr($pxcode,0,1));
            $sql = " SELECT *  FROM grouppx WHERE  lower(trim(gp_name))=?";
            $this->log_message("$sql dengan name $first_char");
            $query = $this->db->query($sql,array($first_char));
            foreach($query->result() as $row ) {
                $name = $row->gp_remarks;
                $order = $row->gp_order;
            }
            $data['parent']= $name;
            $data['order']= $order;
            return $data;
        }
        function get_mkt_id($trans_id) {
            $retval="";
            $sql= " SELECT client.cl_mktid FROM client INNER JOIN transout ON transout.tro_cust_id=client.cl_id WHERE tro_id=?";
            $query = $this->db->query($sql,array($trans_id));
            foreach($query->result() as $row) {
                $retval = $row->cl_mktid;
            }
            return $retval;                                        
        }
        function get_buy_price($code) {
            $retval=0;
            $sql = " SELECT prod_buyprice FROM products WHERE prod_code=?";
            $query = $this->db->query($sql,array($code));
            foreach($query->result() as $row) {
                $retval=$row->prod_buyprice;
            }
            return $retval;
        }
        function save_stock($lab_id,$code,$name,$price,$qty,$total,$type,$disc) {
            $buyprice=$this->get_buy_price($code);
            $mkt_id=$this->get_mkt_id($lab_id);
            $sqldetail = " INSERT INTO trans_detail (trans_id,trans_code,trans_name,trans_price,trans_qty,trans_total,trans_type,trans_mkt_id,trans_buyprice,trans_disc)VALUES";
            $sqldetail =  $sqldetail . "(";
            $sqldetail =  $sqldetail . " ?,?,?,?,?,?,?,?,?,?)";
            $this->db->query($sqldetail,array($lab_id,$code,$name,$price,$qty,$total,$type,$mkt_id,$buyprice,$disc));
            $this->log_message("AFTER QUERY");
        }
        //
        function save_po_detail($lab_id,$code,$name,$price,$qty,$total,$type) {
            $sqldetail = " INSERT INTO po_detail (po_id,po_code,po_name,po_price,po_qty,po_total,po_type)VALUES";
            $sqldetail =  $sqldetail . "(";
            $sqldetail =  $sqldetail . " ?,?,?,?,?,?,?)";
            $this->db->query($sqldetail,array($lab_id,$code,$name,$price,$qty,$total,$type));
            $this->log_message("AFTER QUERY");
        }
        function save_stock_opname($lab_id,$code,$name,$price,$qty,$total,$type) {
            $sqldetail = " INSERT INTO trans_detail (trans_id,trans_code,trans_name,trans_price,trans_qty,trans_total,trans_type)VALUES";
            $sqldetail =  $sqldetail . "(";
            $sqldetail =  $sqldetail . " ?,?,?,?,?,?,?)";
            $this->db->query($sqldetail,array($lab_id,$code,$name,$price,$qty,$total,$type));
            $this->log_message("AFTER QUERY");

        }        
        //
        function save_note($lab_id,$note) {
            $sql = " UPDATE  frontend SET frontend_lab_note=? WHERE frontend_lab_id=?";
            $this->db->query($sql,array($note,$lab_id));
            return 1;
        }
        function get_note($lab_id) {
            $sql = " SELECT frontend_lab_note as note FROM frontend WHERE frontend_lab_id=?";
            $query =$this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $retval = $row->note;
            }
            return $retval;
        }        
}
?>
