<?php

class Model_jamaah extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'pasien';
		$this->_id = 'pas_id';
	}
        
        function get_gender($id) {
            $gender="";
            $sql = " SELECT patient_gender FROM patient WHERE patient_id=?";
            $this->log_message("sql get gender $sql  dgn id $id ");            
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $gender = $row->patient_gender;
            }
            return $gender;
        }
        function get_gender_by_labid($lab_id) {
            $gender="";
            $this->log_message("CALLED ");
            $sql = " SELECT patient_gender FROM patient INNER JOIN frontend ";
            $sql = $sql . " ON frontend.frontend_patient_id=patient.patient_id  WHERE frontend_lab_id=? ";
            
            $this->log_message("sql get gender $sql  dgn id $lab_id ");            
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $gender = $row->patient_gender;
            }
            return $gender;
        }
        function get_name_by_labid($lab_id) {
            $name="";
            $this->log_message("get name by lab id CALLED ");
            $sql = " SELECT (patient_title || ' ' || patient_name) as name FROM patient INNER JOIN frontend ";
            $sql = $sql . " ON frontend.frontend_patient_id=patient.patient_id  WHERE frontend_lab_id=? ";
            
            $this->log_message("sql get name $sql  dgn id $lab_id ");            
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $name = $row->name;
            }
            return $name;
        }        
        //
        function get_name_by_id($id) {
            $name="";            
            $this->log_message("get name by id CALLED ");
            $sql = " SELECT (j_title || ' ' || j_name) as name FROM jamaah   ";
            $sql = $sql . "   WHERE j_id=? ";
            
            $this->log_message("sql get name $sql  dgn id $id ");            
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $name = $row->name;
            }
            return $name;
        }                
        
        function savejamaah($id,$title,$name,$sex,$age,$addr,$city,$phone,$phonecell,$passport,$issuing_office,$issue_date,$regist_mahram_rel,$regist_mahram_rel_ke,$regist_dokumen_pendukung,$regist_birth_place,$mahram) {
            $this->log_message("lohg");
            $sqlcount = " SELECT COUNT(*) as jumlah FROM jamaah WHERE j_id=? ";
            $this->log_message("about sqlcount  $sqlcount $id ");
            $query = $this->db->query($sqlcount,array($id));
            $this->log_message("after qry ");
            //$this->db->query($sqlcount);
            $jumlah=0;
            foreach($query->result() as $row) {
                $jumlah = $row->jumlah;
            }
            if($jumlah>0) {
                $newflag = 0;
            }else {
                $newflag = 1;
            }
            if($newflag==1){
                $sql="INSERT INTO jamaah(j_id,j_title,j_name,j_kelamin,j_tgl_lahir,j_addr,j_kota,j_telp,j_hp,j_no_passport,j_issuing_office,j_date_of_issue,j_hubungan_mahram,j_hubungan_mahram_ke,j_dok_pendukung_mahram_ke,j_tempat_lahir,j_mahram)";
                $sql = $sql . "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $this->log_message("about to save save patient $sql ");
                $query  = $this->db->query($sql,array($id,$title,$name,$sex,$age,$addr,$city,$phone,$phonecell,$passport,$issuing_office,$issue_date,$regist_mahram_rel,$regist_mahram_rel_ke,$regist_dokumen_pendukung,$regist_birth_place,$mahram));
            }else {
                $sql = " Update jamaah SET j_title = ?, ";
                $sql = $sql  . " j_name = ? ,";
                $sql = $sql  . " j_kelamin = ? ,";
                $sql = $sql  . " j_tgl_lahir = ? ,";
                $sql = $sql  . " j_addr = ? ,";
                $sql = $sql  . " j_kota = ? ,";
                $sql = $sql  . " j_telp = ? ,";
                $sql = $sql  . " j_hp = ?, ";
                $sql = $sql  . " j_no_passport = ?, ";
                $sql = $sql  . " j_issuing_office = ?,";
                $sql = $sql  . " j_date_of_issue = ? ,";
                $sql = $sql  . " j_hubungan_mahram = ?, ";
                $sql = $sql  . " j_hubungan_mahram_ke = ? ,";
                $sql = $sql  . " j_dok_pendukung_mahram_ke = ? ,";
                $sql = $sql  . " j_tempat_lahir=? ,";
                //j_mahram
                $sql = $sql  . " j_mahram=? ";
                //j_date_of_issue
                $sql = $sql  . " WHERE j_id=?";
                $query  = $this->db->query($sql,array($title,$name,$sex,$age,$addr,$city,$phone,$phonecell,$passport,$issuing_office,$issue_date,$regist_mahram_rel,$regist_mahram_rel_ke,$regist_dokumen_pendukung,$regist_birth_place,$mahram,$id));
            }
            $this->log_message($sql);
        }       
}

?>
