<?php

class Model_pasien extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'pasien';
		$this->_id = 'pas_id';
	}
        function get_gender($id) {
            $gender="";
            $sql = " SELECT patient_gender FROM patient WHERE patient_id=?";
            $this->log_message("sql get gender $sql  dgn id $id ");            
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $gender = $row->patient_gender;
            }
            return $gender;
        }
        function get_gender_by_labid($lab_id) {
            $gender="";
            $this->log_message("CALLED ");
            $sql = " SELECT patient_gender FROM patient INNER JOIN frontend ";
            $sql = $sql . " ON frontend.frontend_patient_id=patient.patient_id  WHERE frontend_lab_id=? ";
            
            $this->log_message("sql get gender $sql  dgn id $lab_id ");            
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $gender = $row->patient_gender;
            }
            return $gender;
        }
        function get_name_by_labid($lab_id) {
            $name="";
            $this->log_message("get name by lab id CALLED ");
            $sql = " SELECT (patient_title || ' ' || patient_name) as name FROM patient INNER JOIN frontend ";
            $sql = $sql . " ON frontend.frontend_patient_id=patient.patient_id  WHERE frontend_lab_id=? ";
            
            $this->log_message("sql get name $sql  dgn id $lab_id ");            
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $name = $row->name;
            }
            return $name;
        }        
        
        function savepatient($id,$title,$name,$sex,$age,$addr,$phone,$phonecell) {
            $this->log_message("lohg");
            $sqlcount = " SELECT COUNT(*) as jumlah FROM patient WHERE patient_id=? ";
            $this->log_message("about sqlcount  $sqlcount $id ");
            $query = $this->db->query($sqlcount,array($id));
            $this->log_message("after qry ");
            //$this->db->query($sqlcount);
            $jumlah=0;
            foreach($query->result() as $row) {
                $jumlah = $row->jumlah;
            }
            if($jumlah>0) {
                $newflag = 0;
            }else {
                $newflag = 1;
            }
            if($newflag==1){
                $sql="INSERT INTO patient(patient_id,patient_name,patient_address,patient_phone,patient_gender,patient_mobilephone,patient_dob,patient_title,patient_city)";
                $sql = $sql . "VALUES(?,?,?,?,?,?,?,?,?)";
                $this->log_message("about to save save patient $sql ");
                $query  = $this->db->query($sql,array($id,$name,$addr,$phone,$sex,$phonecell,$age,$title,NULL)); 
            }else {
                $sql = " Update PATIENT SET patient_name = ?, ";
                $sql = $sql  . " patient_address = ? ,";
                $sql = $sql  . " patient_phone = ? ,";
                $sql = $sql  . " patient_gender = ? ,";
                $sql = $sql  . " patient_mobilephone = ? ,";
                $sql = $sql  . " patient_dob = ? ,";
                $sql = $sql  . " patient_title = ? ,";
                $sql = $sql  . " patient_city = ? ";
                $sql = $sql  . " WHERE patient_id=?";
                $query  = $this->db->query($sql,array($name,$addr,$phone,$sex,$phonecell,$age,$title,NULL,$id)); 
            }
            $this->log_message($sql);
        }       

}

?>
