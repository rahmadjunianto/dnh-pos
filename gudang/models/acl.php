<?php

/**
 * Description of acl
 *
 * @author wijaz
 */
class Acl extends Common_Model {
//class Acl extends CI_Model {
	private $_err_msg;

	private function set_login($user_id, $user_login, $user_name = '') {
		$login_data = array(
			'is_logged_in' => true,
                        'users_id' => $user_id,
			'users_name' => $user_login,
			'users_real_id' => $user_name);

		$this->session->set_userdata($login_data);
	}

	private function unset_login() {
		$login_data = array(
			'is_logged_in' => false,
			'users_id' => '',
			'users_name' => '',
			'users_real_id' => '');
		$this->session->unset_userdata($login_data);
	}

	function get_err_msg() {
		return $this->_err_msg;
	}

	/**
	 * set login if valid
	 * @param type $pUser_id
	 * @param type $pPasswd
	 * @return int
	 * -1:user disabled
	 * 0:user not found / invalid user or password
	 * 1:valid
	 */
	function login($user_name, $raw_paswd) {
		if (strlen(trim($user_name)) == 0) {
			return -1;
		}
                //
		$hashed_passwd = md5($raw_paswd);
                $sql = "SELECT COUNT(*) as jumlah FROM users WHERE users_login=?  AND users_pwd=?";
                $this->log_message('User login id:'. $sql . " user $user_name dan pwd $hashed_passwd ");
		$query = $this->db->query( $sql,array($user_name,$hashed_passwd));
		foreach($query->result() as $row) {
                    $count = $row->jumlah;
                    $this->log_message("COUNT USER $count");
                }

                $sql = "SELECT *  FROM users WHERE users_login=?  AND users_pwd=?";
                $this->log_message('User login id:'. $sql . " user $user_name dan pwd $hashed_passwd ");
		$query = $this->db->query( $sql,array($user_name,$hashed_passwd));
		foreach($query->result() as $row) {
                    $id = $row->users_id ;
                    $name = $row->users_name ;
                    $login = $row->users_login ;
                    $this->log_message("COUNT USER $count");
                }

                if ($count > 0) {
                    $this->set_login($id,$login,$name);
                    return 1;

		} else { //user not found
			$this->_err_msg = 'User tidak valid atau password salah';
			$this->unset_login();
			return 0;
		}
	}

	public function logout() {
		$this->session->sess_destroy();
	}
	function is_logged_in() {
		return $this->session->userdata('is_logged_in');
                //return true;
	}
	function is_logged_in_void() {
		return $this->session->userdata('is_logged_in');
	}

	function get_users_id() {
		return $this->session->userdata('users_id');
	}

	function get_real_users_id() {
            //$this->log_message(" USER NAME  $this->session->userdata('users_name'");
            return $this->session->userdata('users_real_id');
	}

	function get_users_id_by_users_login($user_login) {
		$query = $this->db->from('users')->where('users_login', $user_login);
		if ($query->num_rows() > 0) {
			return $query->first_row()->users_id;
		} else {
			$this->_err_msg = 'User login tidak ditemukan';
			return false;
		}
	}

	function get_users_login() {
		$query = $this->db
			->from('users')
			->where('users_id', $this->get_users_id())
			->get();
		if ($query->num_rows() > 0) {
			return $query->first_row()->users_login;
		} else {
			$this->_err_msg = 'User login tidak ditemukan';
			return false;
		}
	}

	function get_users_nama() {
		$query = $this->db
			->from('users')
			->where('users_id', $this->get_users_id())
			->get();
		if ($query->num_rows() > 0) {
			$user_nama = $query->first_row()->users_nama;
			return $user_nama;
		} else {
			$this->logout();
			$this->show_error(); //error hacking activity
			return false;
		}
	}

	function get_users_nama_from_table($users_id) {
		$query = $this->db
			->from('users')
			->where('users_id', $users_id)
			->get();
		if ($query->num_rows() > 0) {
			$user_nama = $query->first_row()->users_nama;
			return $user_nama;
		}else{
			return false;
		}
	}
	function get_user_info($users_id){
		$query = $this->db
			->from('users')
			->where('users_id', $users_id)
			->get();
		if ($query->num_rows() > 0) {
			$user_nama = $query->first_row()->users_nama;
			return (string)$user_nama . "[" . (string) $users_id . "]";
		}else{
			return false;
		}
	}

	function get_db_cabang($db_hostname) {
		$retval = '';
		switch (strtolower($db_hostname)):
			case '127.0.0.1':$retval = "127.0.0.1";
				break;
			case 'localhost':$retval = "LOCALHOST";
				break;
			case '192.168.1.24':$retval = "SURABAYA-DHARMAWANGSA";
				break;
			case '192.168.5.201':$retval = "SURABAYA-DHARMO_PERMAI";
				break;
			case '192.168.6.2':$retval = "SURABAYA-MULYOSARI";
				break;
			case '192.168.15.3':$retval = "JOGJA-SUTOMO";
				break;
			case '192.168.16.3':$retval = "JOGJA-KALIURANG";
				break;
			case '192.168.14.21':$retval = "JKT-MAMPANG";
				break;
			case '192.168.18.251':$retval = "BANDUNG-KOPO";
				break;
			case '192.168.12.2':$retval = "JEMBER";
				break;
			case '192.168.19.3':$retval = "SOLO";
				break;
			case '192.168.17.2':$retval = "SIDOARJO-THAMRIN";
				break;
			case '192.168.20.2':$retval = "BANDUNG-TENGAH(A.YANI)";
				break;
			case '192.168.21.2':$retval = "GRESIK";
				break;
			case '192.168.23.3':$retval = "JOGJA-AMC";
				break;
			case '192.168.22.2':$retval = "MAKASSAR";
				break;
			case '192.168.24.1':$retval = "RS ASRI";
				break;
			default : $retval = $db_hostname;
				break;
		endswitch;
		if ($this->in_group(array(GROUP_IT_SUPER_USER))) {
			$this->load->database();
			$retval .= ' ['.$this->db->database.']';
		}
		return $retval;
	}

	function change_password($user_id, $oldPassword, $newPassword) {
		if (strlen(trim($newPassword)) == 0) {
			$this->_err_msg = 'Password tidak boleh kosong';
			return false;
		}

		$hashedOldPassword = md5($oldPassword);
		$hashedNewPassword = md5($newPassword);

		$query = $this->db
			->from('users')
			->where('users_id', $user_id)
			->get();
		if ($query->num_rows() > 0) {
			$row = $query->first_row();
			if ($row->users_webpass == $hashedOldPassword) {
				$this->db
					->set('users_webpass', $hashedNewPassword)
					->where('users_id', $user_id)
					->update('users');
				return true;
			} else {
				$this->_err_msg = 'Anda salah memasukan password lama';
				return false;
			}
		} else {
			$this->_err_msg = 'User tidak ditemukan';
			return false;
		}
	}

	function reset_password($user_id, $newPassword) {
		if (strlen(trim($user_id)) == 0) {
			$this->_err_msg = 'User tidak valid';
			return false;
		}

		$hashedPassword = md5($newPassword);

		$this->db
			->from('users')
			->set('users_webpass', $hashedPassword)
			->where('users_id', $user_id)
			->update();
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			$this->_err_msg = 'Tidak ada user yang terupdate';
			return false;
		}
	}

	function login_as($run_as_user_id) {
		$real_user_id = strtolower(trim($this->get_real_users_id()));

		if ($run_as_user_id != '') {
                        $sql = " SELECT * FROM users WHERE lower(trim(users_login))=?";
			$q = $this->db->query($sql,array($run_as_user_id));
                        foreach($q->result() as $row) {

				$user_name = $q->first_row()->users_name;
                                $this->set_login($row->users_id, $row->users_login, $row->users_name);
				$this->show_error("Anda sedang meng-override login $user_name");
                        }
		}
	}

	function exit_login_as() {
		$user_id = $this->get_real_users_id();
		$query = $this->db->from('users')->where('users_id', $user_id)->get();
		if ($query->num_rows() > 0) {
			$user_nama = $query->first_row()->users_nama;
			$this->set_login($user_id, $user_nama);
		}
	}
                function in_group($arrOfGroups) {
                    return true;
                }
	function in_group_void($arrOfGroups) {
		if (!isset($arrOfGroups)) {
			$this->_err_msg = 'Parameter kosong';
			return false;
		}

		$user_id = $this->get_users_id();
		$query = $this->db
			->from('groupusers gu')
			->where('gu.userid', $user_id)
			->get();

		$found = false;
		foreach ($query->result() as $key) {
			if (in_array($key->groupid, $arrOfGroups)) {
				$found = true;
			}
		}
		return $found;
	}

	function get_group_nama() {
		$user_id = $this->get_user_id();
		$query = $this->db
			->from('groupusers gu')
			->join('grup', 'grup.group_id = gu.groupid')
			->where('userid', $user_id)
			->get();
		if ($query->num_rows() > 0) {
			$retval = $query->first_row()->group_nama;
		} else {
			$retval = '';
		}
		return $retval;
	}

	function is_ajax_request() {
                        $x = (isset($_SERVER[ 'HTTP_X_REQUESTED_WITH' ]) && $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] == "XMLHttpRequest");
                        $this->log_message("AJAX : " . $x);
                        return $x;
                            //return 1;
	}

	private function show_error($message = null) {
		$login_url = '<br/>Klik <a href="' . site_url('auth/login') . '">disini</a> untuk login.<br/>' .
			'atau <a href="#" onclick="history.go(-1)">kembali</a> ke halaman sebelumnya';
		if ($message == null) {
			$message = 'Indikasi aktifitas kecurangan/hacking.
				Informasi Anda akan dicatat oleh sistem dan dilaporkan ke pihak IT.';
		}
		$message.=$login_url;
		show_error($message);
	}
	function set_flashdata($key,$val){
		$this->session->set_flashdata($key,$val);
	}
        private function show_user_info() {
                $this->get_users_id();
                $userdiv = " <div class='user_form'>";
                $userdiv = $userdiv  . "<div style='float:left;width:100px;'>ID User</div><div style='float:left;'><input id='user_form_id' type='text' readonly></div><div><p>&nbsp;</p></div>";
                $userdiv = $userdiv  . "<div style='float:left;width:100px;'>Nama User</div><div style='float:left;'><input id='user_form_name' type='text' readonly></div><div><p>&nbsp;</p></div>";
                $userdiv = $userdiv  . "<div style='float:left;width:100px;'>Password</div><div style='float:left;'><input id='user_form_pwd' type='password'></div><div><p>&nbsp;</p></div>";

        }
        public function form_access($formid) {
            $userid= $this->get_users_id();
            $sql = " SELECT gm_guid FROM groupmember WHERE gm_userid=?";
            $query = $this->db->query($sql,array($userid));
            //
            $retval =-1;
            return 1;
            foreach($query->result() as $row) {
                $sqlsec = " SELECT COUNT(*) AS jumlah FROM usersec WHERE us_form_id=? AND us_group_id=? ";
                $querysec = $this->db->query($sqlsec,array($formid,$row->gm_guid));
                foreach($querysec->result() as $rowsec) {
                    $cnt=$rowsec->jumlah;
                    if($cnt>0) {
                      $retval=1;
                      return $retval;
                    }
                }
            }
//            return $retval;
//            return $retval;
        }
        public function show_acl_warning() {
            $username = $this->get_real_users_id();
            echo "<head>";
            $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
            foreach ($this->html_headers->styles as $style) {
                    echo '<link href=" ' . $style .  '" rel="stylesheet" type="text/css" />';
            }
            foreach ($this->html_headers->scripts as $script) {
                echo '<script type="text/javascript" src="' . $script . '?>"></script>';
            }
            echo "</head>";
            $html = "<script>
                    function closewindow() {
                        window.close();
                    }
                    function showerr2()
                    {
                        showErrorAndExit(' User : <strong>" . $username . " </strong>  Tidak terdaftar dalam Access List Halaman ini');
                        //window.close();
                    }

                    </script>";
            echo $html;
            echo "<body onload=showerr2()></body>";
        }
}

?>