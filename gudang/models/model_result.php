<?php
/*
 * Januari 2014
 */

class Model_result extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                $this->load->library("class_public");         
	}
        function del_lab_id($lab_id) {
            $data=array();
            $status = -1;
            //fontend
            $doct_id="";
            $doct_name="";
            $patient_id ="";
            $patient_name ="";
            $sql = " SELECT frontend_doct_id,frontend_patient_id,(patient_title || ' ' || patient_name) as name,doct_name ";
            $sql = $sql . " FROM frontend INNER JOIN doctor ON doctor.doct_id=frontend.frontend_doct_id ";
            $sql = $sql . " INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql .  " WHERE frontend_lab_id=?";
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $doct_id = $row->frontend_doct_id;
                $doct_name = $row->doct_name;
                $patient_id = $row->frontend_patient_id;
                $patient_name = $row->name;                            
            }
            //insert into history;
            $user_id = $data['logged_in_user_name'] = $this->acl->get_users_id();
            $user_name = $this->acl->get_real_users_id();
            $sql = " SELECT * FROM result_detail WHERE rd_frontend_lab_id=? AND length(rd_pxcode)=5";
            $this->log_message(" DEL: SELECT RD : $sql dgn $lab_id");
            $query = $this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $pxcode = $row->rd_pxcode;
                $name = $row->rd_pxname;
                $price = $row->rd_price;
                $disc = $row->rd_disc;
                $net = $row->rd_net;
                $sql = " INSERT INTO delhistory(dh_lab_id,dh_pxcode,dh_pxname,dh_bruto,dh_disc,dh_net,dh_userid,dh_username,";
                $sql = $sql . "dh_patient_id,dh_patient_name,dh_doct_id,dh_doct_name)";
                $sql = $sql . "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                $this->log_message("DELETE $sql PARAM $lab_id,$pxcode,$name,$price,$disc,$net,$user_id,$user_name ");
                $this->db->query($sql,array($lab_id,$pxcode,$name,$price,$disc,$net,$user_id,$user_name,$patient_id,$patient_name,$doct_id,$doct_name));
            }
            //delete 
            $sql = " DELETE FROM result_detail WHERE rd_frontend_lab_id=?";
            $this->db->query($sql,array($lab_id));
            //electromedic
            $sql = " DELETE FROM em WHERE em_labid=?";
            $this->db->query($sql,array($lab_id));            
            //payment
            $sql = " DELETE FROM payment WHERE payment_labid=?";
            $this->db->query($sql,array($lab_id));                        
            //
            $sql = " DELETE FROM frontend WHERE frontend_lab_id=?";
            $this->db->query($sql,array($lab_id));                                    
            $status = 1;
            return $status;

        }
        //delete 
        function delpx($pxcode,$lab_id) {
            $pxcode=strtolower($pxcode);
            $sql = " DELETE  FROM result_detail WhERE  lower(rd_pxcode) LIKE '$pxcode%' and rd_frontend_lab_id=?";
            $this->log_message("DEL PX $sql nomer lab $lab_id nomer px $pxcode");
            $query = $this->db->query($sql,array($lab_id));
            return 0;
        }
        //
        function get_px_name($pxcode) {
            $retval ="";
            $pxname = "";
            $sql = " SELECT pc_pxcode,pc_pxname FROM pxcode WHERE lower(trim(pc_pxcode))=?";
            $this->log_message("SQL $sql dan pxcode $pxcode");
            $query = $this->db->query($sql,array($pxcode));
            foreach($query->result() as $row) {
                $pxname = $row->pc_pxname;
            }
            $retval = $pxname;
            return $retval;
        }
        function is_grouped_into_electromedic($pxcode,$lab_id) {
            $pxname = $this->get_px_name($pxcode);
            $first_char=strtolower(substr($pxcode,0,1));
            $this->log_message("first char $first_char");            
            $pxcode=strtoupper($pxcode);
            if($first_char=="r") {
                //rontgen
                $sql = " INSERT INTO em(em_labid,em_pxcode,em_pxname,em_type)VALUES(?,?,?,?)";
                $this->log_message("Rontgen $sql $lab_id, $pxcode , $pxname");
                $this->db->query($sql,array($lab_id,$pxcode,$pxname,1));
            }
            if($first_char=="g") {
                //rontgen
                $sql = " INSERT INTO em(em_labid,em_pxcode,em_pxname,em_type)VALUES(?,?,?,?)";
                $this->log_message("USG $sql $lab_id, $pxcode , $pxname");
                $this->db->query($sql,array($lab_id,$pxcode,$pxname,2));                
            }            
            if($first_char=="e") {
                //rontgen
                $sql = " INSERT INTO em(em_labid,em_pxcode,em_pxname,em_type)VALUES(?,?,?,?)";
                $this->log_message("ECG $sql $lab_id, $pxcode , $pxname");
                $this->db->query($sql,array($lab_id,$pxcode,$pxname,3));                
            }            
        }
        //
        function get_px_parent($pxcode) {
            $pxcode=strtolower($pxcode);
            $data = array();
            $first_char=strtolower(substr($pxcode,0,1));
            $sql = " SELECT *  FROM grouppx WHERE  lower(trim(gp_name))=?";
            $this->log_message("$sql dengan name $first_char");
            $query = $this->db->query($sql,array($first_char));
            foreach($query->result() as $row ) {
                $name = $row->gp_remarks;
                $order = $row->gp_order;
            }
            $data['parent']= $name;
            $data['order']= $order;
            return $data;
        }
        function save_result_perpx($pxcode,$lab_id,$price,$disc,$net) {
                $parent_info=array();
                
                $this->log_message("PX code $pxcode, dengan panjang " . strlen($pxcode));
                $rd_pc_order=100;
                $rd_parent="";
                if(strlen($pxcode)==5) {
                    $this->is_grouped_into_electromedic($pxcode,$lab_id);
                    $parent_info=$this->get_px_parent($pxcode);
                    $rd_pc_order=$parent_info['order'];
                    $rd_parent=$parent_info['parent'];
                }
                $pxcode=strtolower($pxcode);
                $sql = " SELECT * FROM pxcode WhERE  lower(pc_pxcode) LIKE '$pxcode%' ORDER BY pc_pxcode";
                $this->log_message("log message $sql");
                $query = $this->db->query($sql);
                $this->log_message("GET GENDER ");
                $CI =& get_instance();
                $this->modpasien = $CI->load->model('model_pasien');                       
                $gender = $CI->model_pasien->get_gender_by_labid($lab_id);
                $gender=strtoupper($gender);
                $this->log_message("GET GENDER $gender");
                foreach($query->result() as $row) {
                    //$retval=$row->labno;                    
                    $pc_pxcode= $row->pc_pxcode;
                    $pc_normal= $row->pc_normal;
                    $pc_min_normal=$row->pc_min_normal;
                    $pc_max_normal=$row->pc_max_normal;
                    
                    $pc_level  = $row->pc_level;
                    $pc_barcode   = $row->pc_barcode;
                    $this->log_message("GENDER $gender pxcode $pc_pxcode");
                    if($gender=="L") {
                        $pc_normal= $row->pc_normal_l;
                        $pc_min_normal=$row->pc_min_l;
                        $pc_max_normal=$row->pc_max_l;
                        if(trim($pc_normal)=="") {                            
                            $pc_normal= $row->pc_normal;
                            $pc_min_normal=$row->pc_min_normal;
                            $pc_max_normal=$row->pc_max_normal;                            
                        }
                    }elseif($gender=="P") {
                        
                        $pc_normal= $row->pc_normal_p;
                        $this->log_message("GENDER $gender normal P $pc_normal");
                        $pc_min_normal=$row->pc_min_p;
                        $pc_max_normal=$row->pc_max_p;
                        if(trim($pc_normal)=="") {                            
                            $pc_normal= $row->pc_normal;                            
                            $pc_min_normal=$row->pc_min_normal;
                            $pc_max_normal=$row->pc_max_normal;                            
                        }                        
                    }else {
                        $pc_normal= $row->pc_normal;
                        $pc_min_normal=$row->pc_min_normal;
                        $pc_max_normal=$row->pc_max_normal;                        
                    }
                    $this->log_message("GENDER $gender normal blank  $pc_normal min normal : $pc_min_normal ,max : $pc_max_normal ");
                    $pc_map_machinecode=$row->pc_map_machinecode;
                    $pc_map_machineid =$row->pc_map_machineid ;
                    $pc_digit=$row->pc_digit;
                    $pc_remarks=$row->pc_remarks;
                    $pc_pxname=$row->pc_pxname;
                    $pc_unit=$row->pc_unit;
                    $pc_order_child =$row->pc_order_child ;
                    $pc_order_parent=$row->pc_order_parent;
                    $pc_parent=$row->pc_parent;
                    $pc_catno =$row->pc_catno ;
                    $pc_catname =$row->pc_catname ;
                    $pc_price=$row->pc_price;
                    //normal value
                    $pc_min_l=$row->pc_min_l;
                    $pc_max_l=$row->pc_max_l;
                    $pc_min_p=$row->pc_min_p;
                    $pc_max_p=$row->pc_max_p;                    
                    $pc_normal_l=$row->pc_normal_l;
                    $pc_normal_p=$row->pc_normal_p;                    
                    //$pc_min_l,pc_min_p,$pc_max_l,$px_max_p,$pc_normal_l,$pc_normal_p;
                    if(strlen($pc_pxcode)==5) {
                        $pc_price=$price;
                        $pc_disc=$disc;
                        $pc_net=$net;
                    }
                    //
                    $sqldetail = " INSERT INTO result_detail (rd_frontend_lab_id,rd_pxcode,rd_value,rd_unit,rd_normal,rd_min_normal,";
                    $sqldetail =  $sqldetail . " rd_max_normal,rd_level,rd_pxname,rd_remarks,rd_barcode ,rd_map_machinecode,rd_map_machineid,";
                    $sqldetail =  $sqldetail . " rd_pc_order ,rd_ordered ,rd_parent,rd_catno ,rd_catname ,rd_price,rd_disc,rd_net";
                    $sqldetail =  $sqldetail . " ,rd_min_l,rd_min_p,rd_max_l,rd_max_p,rd_normal_l,rd_normal_p";
                    $sqldetail =  $sqldetail . " )VALUES(";
                    $sqldetail =  $sqldetail . " ?,?,?,?,?,?,";
                    $sqldetail =  $sqldetail . " ?,?,?,?,?,?,?,";
                    $sqldetail =  $sqldetail . " ?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $this->log_message(" $sqldetail $lab_id,$pc_pxcode,NULL,$pc_unit,$pc_normal,$pc_min_normal,$pc_max_normal,$pc_level,$pc_pxname,$pc_remarks,$pc_barcode,$pc_map_machinecode,$pc_map_machineid,NULL,NULL,NULL,$pc_catno,$pc_catname,$pc_price,$pc_min_l,pc_min_p,$pc_max_l,$pc_max_p,$pc_normal_l,$pc_normal_p");
                    $this->db->query($sqldetail,array($lab_id,$pc_pxcode,NULL,$pc_unit,$pc_normal,$pc_min_normal,$pc_max_normal,$pc_level,$pc_pxname,$pc_remarks,$pc_barcode,$pc_map_machinecode,$pc_map_machineid,$rd_pc_order,NULL,$rd_parent,$pc_catno,$pc_catname,$pc_price,$pc_disc,$pc_net,$pc_min_l,$pc_min_p,$pc_max_l,$pc_max_p,$pc_normal_l,$pc_normal_p));
                    $this->log_message("AFTER QUERY");
                }                            
        }
        function save_note($lab_id,$note) {
            $sql = " UPDATE  frontend SET frontend_lab_note=? WHERE frontend_lab_id=?";
            $this->db->query($sql,array($note,$lab_id));
            return 1;
        }
        function get_note($lab_id) {
            $sql = " SELECT frontend_lab_note as note FROM frontend WHERE frontend_lab_id=?";
            $query =$this->db->query($sql,array($lab_id));
            foreach($query->result() as $row) {
                $retval = $row->note;
            }
            return $retval;
        }        
}
?>
