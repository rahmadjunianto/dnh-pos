<?php
/*
 * Januari 2014
 */

class Model_tunjab extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                                $this->load->library("class_public");
	}
                function get_tunjab_standar() {                        
                        $retval = 0;
//                        $tunjabstandar = $this->class_public->getSettingGaji("tunjab standar");
//                        $this->log_message("tunjab standar: " . $tunjabstandar);
//                        $retval = $tunjabstandar;
                        return $retval;
                }
                
	function get_data_tunjab($idcabang) {//dipake
		$retval = array();
                                $sql = "SELECT * FROM cabang  where idcabang=?";
                                $query = $this->db->query($sql, array($idcabang));
		foreach ($query->result() as $row) {
//        dtunjabSpv3 = AvoidNUllNum(rs("tunjab_spv3"))
//        dtunjabSpv2 = AvoidNUllNum(rs("tunjab_spv2"))
//        dtunjabSpv1 = AvoidNUllNum(rs("tunjab_spv1"))
//        dtunjabwakacab = AvoidNUllNum(rs("tunjab_wakacab"))
//        dtunjabKacab = AvoidNUllNum(rs("tunjab_kacab"))                                            
			$retval[ ] = array(
				'spv3' => $row->tunjab_spv3,
				'spv2' => $row->tunjab_spv2,
				'spv1' => $row->tunjab_spv1,
				'wakacab' => $row->tunjab_wakacab,
				'kacab' => $row->tunjab_kacab
			);
		}
		return $retval;                                
	}
                //save std tunjab
                function save_tunjab_cabang($idcabang,$spv1,$spv2,$spv3,$wakacab,$kacab ) {                                        
                    //
                    $spv1 = $this->class_public-> AvoidNullNum($spv1);
                    $spv2 = $this->class_public->AvoidNullNum($spv2);
                    $spv3 = $this->class_public->AvoidNullNum($spv3);
                    $wakacab = $this->class_public->AvoidNullNum($wakacab);
                    $kacab = $this->class_public->AvoidNullNum($kacab);
                    $strSQL = " UPDATE cabang SET ";
                    $strSQL = $strSQL . " tunjab_spv1= ? , ";
                    $strSQL = $strSQL . " tunjab_spv2= ? ,";
                    $strSQL = $strSQL . " tunjab_spv3= ? ,";
                    $strSQL = $strSQL .  " tunjab_wakacab= ? ,";
                    $strSQL = $strSQL .  " tunjab_kacab= ?  ";
                    $strSQL = $strSQL  . " WHERE idcabang= ? ";
                    $query = $this->db->query($strSQL, array($spv1,$spv2,$spv3,$wakacab,$kacab,$idcabang));                    
                    return 0;
                }
                //end of save std tunjab
	function get_tunjab_tbr($idcabang) {//dipake
                                $this->log_message("ASDASD");
		$retval = array();
                                $sql = "SELECT * FROM cabang  where idcabang=?";
                                $query = $this->db->query($sql, array($idcabang));
		foreach ($query->result() as $row) {
			$retval[ ] = array(
                                                    'rad' => $row->tbr_radiografer,
                                                    'p_rad' => $row->tbr_pembantu_radiografer
			);
		}
		return $retval;
	}                
                //
                function calc_tunjab($sdmid) {
                    $this->load->model('model_grade');                    
                    $this->load->model('model_sdm');                    
                    $masakerja = $this->class_public->AvoidNullNum($this->model_grade->get_masa_kerja_diakui_pegawai($sdmid));                    
                    
                    $idcabang = $this->model_sdm->get_cabang_sdm_byid($sdmid);
                    $jabatan = strtolower(trim($this->model_grade->get_jabatan_pegawai($sdmid)));
                    $this->log_message("masa kerja $masakerja jabatan $jabatan cabang $idcabang");                    
                    //
                    $tbr=array();
                    $tbr = $this->get_tunjab_tbr($idcabang);
                    $struktural = array();
                    $struktural = $this->get_data_tunjab($idcabang);
                    $retval = 0;
                    switch(trim(strtolower($jabatan))){
                    case "supervisor 1":
                        $retval = $struktural[0]['spv1'];
                        break;
                    case "supervisor 2":
                        $retval = $struktural[0]['spv2'];
                        break;
                    case "supervisor 3":
                        $retval = $struktural[0]['spv3'];
                        break;
                    case "wakacab":
                        $retval = $struktural[0]['wakacab'];
                        break;                    
                    case "kacab":
                        $retval = $struktural[0]['kacab'];
                        break;                    
                    case "radiografer":
                        $retval = $tbr[0]['rad'];
                        break;                    
                    case "pembantu radiografer":
                        $this->log_message('prad');
                        $retval = $tbr[0]['p_rad'];
                        break;                    
                    } //end of switch
                    return $retval;                    
                }
                
}

?>
