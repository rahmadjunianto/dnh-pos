<?php
/*
 * Januari 2014
 */

class Model_sdm extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                                $this->load->library("class_public");
	}
        //get cabang
                function get_cabang_sdm_byid($idsdm='') {                                                
                        $retval = "";
                        $sql = "SELECT cab_id  FROM sdm WHERE sdm_id=?";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $retval = $row->cab_id;
                        }
                        return $retval;
                }
                //end of getcabang
                function get_pendidikan_sdm_byid($idsdm='') {                                                
                        $retval = "";
                        $sql = "SELECT sdm_pendidikanterakhir_diakui  FROM sdm WHERE sdm_id=?";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $retval = $row->sdm_pendidikanterakhir_diakui;
                        }
                        return $retval;
                }
                //funsional
                function get_tunjangan_fungsional($idsdm='') {                                                
                        $retval = "";
                        $sql = "SELECT sdm_fungsional  FROM sdm WHERE sdm_id=?";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $retval = $row->sdm_fungsional;
                        }
                        return $retval;
                }                
                //end of functional
                //
                function get_tunjangan_keluarga($sdmid) {
                        $retval = 0;
                        $statusnikah="";
                        $tanggungan=0;
                        $sql = "SELECT * FROM sdm WHERE sdm_id=? ";
                        $query = $this->db->query($sql, array($sdmid));
                        foreach ($query->result() as $row) {
                            $statusnikah = $row->sdm_statusnikah;
                            $tanggungan = $row->sdm_tanggungan;
                        }                        
                        if(strtolower(trim($statusnikah)) == "b" or strtolower(trim($statusnikah)) == "") {
                                $retval = 0;
                        }else {
                                if (strtolower(trim($statusnikah)) == "m" or strtolower(trim($statusnikah))== "j") {
                                    $retval = 10;
                                    switch($tanggungan) {
                                     case 0:
                                        $retval = $retval + 0;
                                        break;
                                    case 1:
                                        $retval = $retval + 2.5;
                                        break;
                                    case 2:
                                        $retval = $retval + 5;
                                    default:
                                        $retval = $retval + 5;
                                    }
                                }
                        } //end else
                        //view-simulasi-panel-right-tkeluarga-value
                        $this->load->model('model_grade');      
                        $this->log_message("tunkel $retval");
                        $gradegapok = $this->model_grade->get_grade_gapok_pegawai($sdmid);
                        $this->log_message("nominal : $gradegapok");
                        $nominal=0;
                        $nominal = $this->model_grade->get_grade_value_scalar($gradegapok);                        
                        $this->log_message("nominal : $nominal");
                        $retval = (float)($retval/100)* $nominal;
                        return $retval;                    
                }
                //status karyawan
                function get_status_karyawan($idsdm='') {                                                
                        $retval = "";
                        $status_text="";
                        $status = "";
                        $sql = " SELECT sdm_status FROM sdm WHERE sdm_id = ? ";
                        $query = $this->db->query($sql, array($idsdm));
                        foreach ($query->result() as $row) {
                            $status = (string)$row->sdm_status;
                            $this->log_message("STATUS : " . $status);
                            switch($status) {
                                case "1":
                                    $status_text = "Training";
                                    break;
                                case "2":
                                    $status_text = "Kontrak";
                                    break;
                                case "3":
                                    $status_text = "Tetap";
                                    break;
                                case "4":
                                    $status_text = "Resign";
                                    break;
                                case "5":
                                    $status_text = "Eksternal";
                                    break;                                
                            }
                        }
                        $retval = $status_text;
                         $this->log_message("STATUS : " . $retval);
                        return $retval;
                }                                
                //end of status kary
                //
	function get_data_sdm_struktural($idcabang) {//dipake
		$retval = array();
                                $sql = "SELECT * FROM cabang  where idcabang=?";
                                $query = $this->db->query($sql, array($idcabang));
		foreach ($query->result() as $row) {

			$retval[ ] = array(
				'spv3' => $row->sdm_spv3,
				'spv2' => $row->sdm_spv2,
				'spv1' => $row->sdm_spv1,
				'wakacab' => $row->sdm_wakacab,
				'kacab' => $row->sdm_kacab
			);
		}
		return $retval;                                
	}                
                //set pendidikan terakhir
                function set_pendidikan_terakhir_diakui($pendidikan,$sdmid) {
                    //$sql  = " UPDATE sdm SET sdm_pendidikanterakhir_diakui='" & cbopendidikan.Text & "' WHERE sdm_id='" & idpegawai & "'"
                    $strSQL =  "UPDATE sdm SET sdm_pendidikanterakhir_diakui=?  WHERE sdm_id= ? " ;
                    $query = $this->db->query($strSQL, array($pendidikan, $sdmid));
                    return 0;
                }
                //set fungsional
                function set_tunjangan_fungsional($idsdm='',$nominal) {                                                
                        $retval = "";
                        $sql = " UPDATE sdm SET sdm_fungsional = ?  WHERE sdm_id=?";
                        $query = $this->db->query($sql, array($nominal,$idsdm));
                        return 0;
                }                                
}

?>
