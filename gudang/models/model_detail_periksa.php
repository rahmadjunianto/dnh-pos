<?php

class Model_detail_periksa extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'detail_periksa';
		$this->_id = 'oid';
	}

	private function get_pxinfo($kodepx) {
		//
		$retval = array();
		$query = $this->db
			->select('gruppx, namapx', false)
			->from('harga')
			->where('kodepx', $kodepx)
			->get();
		foreach ($query->result() as $row) {
			$retval[ 'gruppx' ] = $row->gruppx;
			$retval[ 'namapx' ] = $row->namapx;
		}
		return $retval;
		//
	}

	private function get_labid_bycekid($cek_id) {
		$query = $this->db
			->select('lab_id')
			->from('pemeriksaan')
			->where('cek_id', $cek_id)
			->get();
		$lab_id = "";
		foreach ($query->result() as $row) {
			$lab_id = $row->lab_id;
		}
		return $lab_id;
	}

	private function insert_electromedic_xxx($tablename, $table_prefix, $cek_id, $lab_id, $test_id, $namapx) {
		//
		//$cek_id =  $this->db->Escape($cek_id);
		//$lab_id =  $this->db->Escape($lab_id);
		//$test_id = $this->db->Escape($test_id);
		log_message("error", "masuk fungsi radiologi");
		$sql = "insert into $tablename ($table_prefix" . "_cekid,";
		$sql = $sql . "$table_prefix" . "_kodepx,";
		$sql = $sql . "$table_prefix" . "_namapx,";
		$sql = $sql . "$table_prefix" . "_labid,";
		$sql = $sql . "$table_prefix" . "_cektgl)";
		$sql = $sql . "VALUES ( ";
		$sql = $sql . "$cek_id,$test_id," . (string) $this->db->Escape($namapx) . ",$lab_id,";
		$sql = $sql . "localtimestamp) ";
		$this->db->query($sql);
		//log_message("error","elektromedis " . $sql);
		//
		 log_message('error', 'Insert Electromedic ' . $sql);
		//
	}

	private function insert_electromedic($tablename, $table_prefix, $cek_id, $lab_id, $kodepx, $namapx) {
		$this->db
			->set($table_prefix . "_cekid", $cek_id)
			->set($table_prefix . "_labid", $lab_id)
			->set($table_prefix . "_kodepx", $kodepx)
			->set($table_prefix . "_namapx", $namapx)
			->set($table_prefix . "_cektgl", 'localtimestamp', false) //not escaped
			->from($tablename)
			->insert();
		$this->log_message($this->db->last_query());
	}

	/**/

	function add($obj_data) {
		$retval = $this->db
			->set($obj_data)
			->insert($this->_table_name);
		$this->log_message($this->db->last_query());

		if ($retval) { //insert berhasil
			$cek_id = $obj_data->cek_id;
			$kodepx = $obj_data->test_id;
			$lab_id = $this->get_labid_bycekid($cek_id);

			$pxinfo = $this->get_pxinfo($kodepx);
			$gruppx = $pxinfo[ 'gruppx' ];
			$namapx = $pxinfo[ 'namapx' ];

			switch ($gruppx):
				case "01":// lab
					break;
				case "02": //radiologi
					$this->insert_electromedic('radiologi', 'rad', $cek_id, $lab_id, $kodepx, $namapx);
					break;
				case "03": //insert usg
					$this->insert_electromedic("usg", "usg", $cek_id, $lab_id, $kodepx, $namapx);
					break;
				case "04": //insert treadmill
					$this->insert_electromedic("tm", "tm", $cek_id, $lab_id, $kodepx, $namapx);
					break;
				case "05":	//insert ecg
					$this->insert_electromedic("eec", "eec", $cek_id, $lab_id, $kodepx, $namapx);
					break;
				case "06":	//insert eeg
					$this->insert_electromedic("eeg", "eeg", $cek_id, $lab_id, $kodepx, $namapx);
					break;
			endswitch;
		}
		return $retval;
	}

	private function initial_insert_xxx($counter = 'NULL', $cek_id = 'NULL', $test_id = 'NULL') {
		$sql = "INSERT INTO detail_periksa(counter, cek_id, test_id)VALUES($counter, $cek_id, $test_id)";
		//
		$lab_id = $this->get_labid_bycekid($cek_id);
		$query = $this->db->query($sql);
		//
		$pxinfo = $this->get_pxinfo($test_id);
		$grouppx = $pxinfo[ 'gruppx' ];
		$pxname = $pxinfo[ 'namapx' ];
		log_message("error", " nama px " . $pxname . "dengan gruppx " . $grouppx);
		switch ($grouppx) {
			case "02":
				//insert radiologi
				//insert_electromedic($tablename,$table_prefix,$cek_id,$lab_id,$test_id,$namapx)
				log_message("error", "radiologi");
				$this->insert_electromedic("radiologi", "rad", $cek_id, $lab_id, $test_id, $pxname);
				break;
			case "03":
				//insert usg
				$this->insert_electromedic("usg", "usg", $cek_id, $lab_id, $test_id, $pxname);
				break;
			case "04":
				//insert treadmill
				$this->insert_electromedic("tm", "tm", $cek_id, $lab_id, $test_id, $namapx);
				break;
			case "05":
				//insert ecg
				$this->insert_electromedic("eec", "eec", $cek_id, $lab_id, $test_id, $namapx);
				break;
			case "06":
				//insert eeg
				$this->insert_electromedic("eeg", "eeg", $cek_id, $lab_id, $test_id, $namapx);
				break;
		}
		//
	}

}

//end of class
?>