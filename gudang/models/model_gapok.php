<?php

class Model_gapok extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
	}
                //function get_gapok
                
	function getgapok() {//dipake
//		$retval = array();
		$query = $this->db->select('SELECT mg_min as MIN,mg_max as MAX,mg_grade as Grade,mg_nominal as Nominal,mg_sma,mg_d1,mg_smak,mg_d3,mg_s1 ', false)
			->from('mastergapok')
                                                ->order_by("mg_min,mg_max,mg_grade")
			->get();

		$retval = array();
		foreach ($query->result() as $row) {
			$retval[ ] = array(
				'mg_min' => $row->min,
				'mg_max' => $row->max,
				'grade' => $row->grade,
				'mg_nominal' => $row->Nominal,
				'mg_sma' => $row->sma,
				'mg_smak' => $row->smak,
				'mg_d1' => $row->d1,
				'mg_d2' => $row->d2,
				'mg_d3' => $row->d3,
                                                                'mg_s1' => $row->s1,
                                                                'mg_s2' => $row->s2
			);
		}
		return $retval;
	}

}

?>
