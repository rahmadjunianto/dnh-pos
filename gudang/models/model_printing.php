<?php
/*
 * Januari 2014
 */

class Model_printing extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                $this->load->library("class_public");
	}
        function get_page_margin($print_id) {
            $data=array();
            $sql = " SELECT * from printing WHERE print_id=?";
            $query = $this->db->query($sql,array($print_id));
            foreach($query->result() as $row) {
                $data['top']=$row->print_top_margin;
                $data['left']=$row->print_left_margin;
                $data['right']=$row->print_right_margin;
                $data['bottom']=$row->print_bottom_margin;
            }
            return $data;
        }
        function get_table_header_style($print_id) {
            $data=array();
            $sql = " SELECT * from print_detail  WHERE print_id=? AND lower(trim(print_section))='header' AND lower(trim(print_detail_type))='table' ORDER BY print_order,print_table_order";
            $query = $this->db->query($sql,array($print_id));
            return $query->result();
        }
        //get_content_table_style
        function get_content_table_style($print_id) {
            $data=array();
            $sql = " SELECT * from print_detail  WHERE print_id=? AND lower(trim(print_section))='content' AND lower(trim(print_detail_type))='table' ORDER BY print_order,print_table_order";
            $query = $this->db->query($sql,array($print_id));
            return $query->result();
        }        
        function get_style_detail($print_id) {
            $data=array();
            $sql = " SELECT * from print_detail WHERE print_id=? AND lower(trim(print_detail_type))='item' ORDER BY print_order";
            $query = $this->db->query($sql,array($print_id));
            return $query->result();
        }
        function get_count_header_table($print_id) {
            $retval = 0;
            $sql=" SELECT COUNT(*) as jumlah FROM print_detail WHERE print_id=? AND lower(trim(print_section))='header' AND lower(trim(print_detail_type))='table'";
            $query = $this->db->query($sql,array($print_id));
            foreach($query->result() as $row) {
                $retval = $row->jumlah;
            }
            return $retval;
        }
        //
        function get_count_content_table($print_id) {
            $retval = 0;
            $sql=" SELECT COUNT(*) as jumlah FROM print_detail WHERE print_id=? AND lower(trim(print_section))='content' AND lower(trim(print_detail_type))='table'";
            $query = $this->db->query($sql,array($print_id));
            foreach($query->result() as $row) {
                $retval = $row->jumlah;
            }
            return $retval;
        }
        //
        function get_count_content_table_data($print_id) {
            $retval = 0;
            $sql=" SELECT COUNT(*) as jumlah FROM print_detail WHERE print_id=? AND lower(trim(print_section))='content_detail' AND lower(trim(print_detail_type))='table'";
            $query = $this->db->query($sql,array($print_id));
            foreach($query->result() as $row) {
                $retval = $row->jumlah;
            }
            return $retval;
        }        
        //        
        function get_content_header_table($print_id) {
            $retval = 0;
            $sql=" SELECT COUNT(*) as jumlah FROM print_detail WHERE print_id=? AND lower(trim(print_section))='content' AND lower(trim(print_detail_type))='table'";
            $query = $this->db->query($sql,array($print_id));
            foreach($query->result() as $row) {
                $retval = $row->jumlah;
            }
            return $retval;
        }
        //        
        function get_table_column_detail($print_id) {            
            $sql=" SELECT * FROM print_detail WHERE print_id=? AND lower(trim(print_section))='header' AND lower(trim(print_detail_type))='table' ORDER BY print_table_order ";
            $query = $this->db->query($sql,array($print_id));
            return $query->result();
        }
        //
        function get_table_column_detail_data($print_id) {            
            $sql=" SELECT * FROM print_detail WHERE print_id=? AND lower(trim(print_section))='content_detail' AND lower(trim(print_detail_type))='table'  ORDER BY print_table_order ";
            $query = $this->db->query($sql,array($print_id));
            return $query->result();
        }
        //
        function get_content_table_column_detail($print_id) {            
            $sql=" SELECT * FROM print_detail WHERE print_id=? AND lower(trim(print_section))='content' AND lower(trim(print_detail_type))='table' ORDER BY print_table_order ";
            $this->log_message($sql);
            $query = $this->db->query($sql,array($print_id));
            return $query->result();
        }        

}
?>
