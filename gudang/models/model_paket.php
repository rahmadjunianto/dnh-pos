<?php

/**
 * Description of model_paket
 * kumpulan fungsi untuk tabel Paket
 * @author RONI
 */
class Model_paket extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'paket';
		$this->_id = 'paket_id';
	}

	function add($arr_data) {
		$retval = $this->db
			->set($arr_data)
			->set($this->_id, "nextval('seq_paket')", false) //akan mereplace value di class bank
			->insert($this->_table_name);
		$this->log_message($this->db->last_query());
		if ($retval) {
			$query = $this->db->query("select currval('seq_paket') as new_id");
			$retval = $query->first_row()->new_id;
		}
		return $retval;
	}

	function selectAll($order_by = 'lower(paket_nama)') {
		$query = $this->db->select('*')
			->from($this->_table_name)
			->order_by($order_by)
			->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return array();
		}
	}

}
?>