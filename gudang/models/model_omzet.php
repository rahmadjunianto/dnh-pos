<?php
/*
 * Sep 2014
 */

class Model_omzet extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                //$this->load->library("class_public");
                $this->user_id = $this->acl->get_users_id();
	}
        function get_check_up_date($lab_id) {
            $sql = " SELECT date_name(to_date(frontend_date::text,'YYYY-MM-DD')) as date FROM frontend WHERE ";
            $sql = $sql . " frontend_lab_id=?";
            $query = $this->db->query($sql,array($lab_id));
            $date = "";
            foreach($query->result() as $row) {
                $date =$row->date;
            }
            return $date;
        }
        function get_omzet_by_group($groupname,$lab_id) {
            $groupname = strtolower($groupname);
            $retval = 0;
            $sql = "SELECT ";
            $sql = $sql . " coalesce(sum(result_detail.rd_net),0) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " WHERE  1=1 ";
            $sql = $sql . " AND rd_frontend_lab_id =?";
            $sql = $sql . " AND lower(substr(trim(rd_pxcode),1,1))=?";            
            $sql = $sql . " AND length(rd_pxcode)=5 ";
            $this->log_message("GROUP GET $sql dan lab id $lab_id group name $groupname");
            $query = $this->db->query($sql,array($lab_id,$groupname));
            foreach($query->result() as $row) {
                $retval = $row->net;
            }
            return $retval;
            //rd_frontend_lab_id 
        }
        function calc_omzet_detail($start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            //revisi tro_mktid 
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_mktid ";
            $sql = $sql . " WHERE  ";
            $sql = $sql . " to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=? ORDER BY tro_date";            
            $this->log_message("omzet info $sql  periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $retval;            
        }  
        //
        function get_percent_mkt($id) {
            $percent=0;
            $CI =& get_instance();
            $CI->load->model('model_users');            
            $this->modusers = $CI->model_users;
            $percent=$this->modusers->get_mkt_percent($id);
            return $percent;
        }
        function get_potongan() {
            $retval="";
            $sql=" SELECT * FROM settings WHERE lower(trim(varname))='potongan bbm'";
            $query=$this->db->query($sql);
            foreach($query->result() as $row) {
                $retval = $row->varvalue;
            }
            return $retval;
        }        
        function get_cl_name($id) {
            $sql=" SELECT cl_name from client WHERE cl_id=?";
            $retval="";
            $query=$this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $retval=$row->cl_name;
            }
            return $retval;
        }
        function calc_omzet_mkt_detail($id,$start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            $sql = $sql . " tro_mktid=? AND ";
            $sql = $sql . " to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=? ORDER BY tro_date";            
            $this->log_message("omzet info $sql id $id periode $start dan $finish");
            $query=  $this->db->query($sql,array($id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $retval;            
        }  
        function calc_omzet_supplier_detail($id,$start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            //$sql = $sql . " tro_mktid=? AND ";
            $sql = $sql . " trans_code IN ";
            $sql = $sql . " ( ";
            $sql = $sql . " SELECT distinct(trans_detail.trans_code) FROM trans_detail INNER JOIN transin ";
            $sql = $sql . " ON transin.tri_id=trans_detail.trans_id";
            $sql = $sql . " WHERE tri_supp_id=? ";
            $sql = $sql . " )";            
            $sql = $sql . " AND to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=? ORDER BY tro_date";            
            $this->log_message("omzet info $sql id $id periode $start dan $finish");
            $query=  $this->db->query($sql,array($id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $retval;            
        }          
        //
        function calc_omzet_counter_detail($id,$start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            //$sql = $sql . " tro_mktid=? AND ";
            $sql = $sql . " (tro_cust_id=?  or tro_cust_id IN (SELECT distinct(cl_id)  FROM client WHERE cl_reference=?) ) AND ";
            $sql = $sql . " to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=? ORDER BY tro_date";            
            $this->log_message("omzet info $sql id $id periode $start dan $finish");
            $query=  $this->db->query($sql,array($id,$id,$start,$finish));
            $counter = 1;            
            //
            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $retval;            
        }          
        function calc_omzet_product_detail_void($id,$start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            $sql = $sql . " trans_code=? AND ";
            $sql = $sql . " to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=? ORDER BY tro_date";            
            $this->log_message("omzet info $sql id $id periode $start dan $finish");
            $query=  $this->db->query($sql,array($id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $retval;            
        }          
        function calc_omzet_product_detail($id,$start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            //$sql = $sql . " tro_mktid=? AND ";
            $sql = $sql . " trans_code=? AND ";            
            $sql = $sql . " to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=? ORDER BY tro_date";            
            $this->log_message("omzet info $sql id $id periode $start dan $finish");
            $query=  $this->db->query($sql,array($id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $retval;            
        }                  
//net price
        function rl_netprice($tro_id) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            //$sql = $sql . " tro_mktid=? AND ";
            $sql = $sql . " tro_id=?  ";            
            //$this->log_message("omzet info $sql id $id periode $start dan $finish");
            $this->log_message("omzet info $sql id $tro_id ");
            $query=  $this->db->query($sql,array($tro_id));
            //id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $totalnetto;            
        }                          
        function rl_buyprice($tro_id) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            //$sql = $sql . " tro_mktid=? AND ";
            $sql = $sql . " tro_id=?  ";            
            //$this->log_message("omzet info $sql id $id periode $start dan $finish");
            $this->log_message("omzet info $sql id $tro_id ");
            $query=  $this->db->query($sql,array($tro_id));
            //id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $totalbuyprice=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                $totalbuyprice=$totalbuyprice + ($row->trans_buyprice * $row->trans_qty);
                //
            }
            //
            return $totalbuyprice;            
        }                                  
        
        function calc_omzet_supplier_detail_void($id,$start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $sql = "SELECT transout.*,trans_detail.*,cl_name as name,cl_address as addr,date_name(tro_date) as datename " ;
            $sql = $sql .  " ,tro_id as nomer_nota,users_name as marketing,tro_disc1 as disc1";
            $sql = $sql .  " ,tro_disc2 as disc2,tro_disc3 as disc3,tro_ppn as ppn ";
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql .  " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql .  " INNER JOIN users ON users.users_id=transout.tro_users_id ";
            $sql = $sql . " WHERE  ";
            $sql = $sql . " trans_code IN ";
            $sql = $sql . " ( ";
            $sql = $sql . " SELECT distinct(trans_detail.trans_code) FROM trans_detail INNER JOIN transin ";
            $sql = $sql . " ON transin.tri_id=trans_detail.trans_id";
            $sql = $sql . " WHERE tri_supp_id=? ";
            $sql = $sql . " )";
            $sql = $sql . " AND to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=? ORDER BY tro_date";            
            $this->log_message("omzet info $sql id $id periode $start dan $finish");
            $query=  $this->db->query($sql,array($id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            $currentid="";
            $_disc1=0;
            $_disc2=0;
            $_disc3=0;
            $_ppn=0;
            $_extra_disc=0;
            $_net_price=0;
            $_bruto_price=0;
            //
            //
            $totalqtyitem = 0; //$totalqtyitem + $row->trans_qty;
            $totalpriceitem = 0; //$totalpriceitem + $row->trans_price;
            $totaldiscitem = 0; //$totaldiscitem + $row->trans_disc;
            $totalitem = 0; //$totalitem + $row->trans_total;
            $totalbruto = 0; //$totalbruto + $_total_bruto;
            $totaldisc1 = 0; //$totaldisc1 + $_disc1;
            $totaldisc2 = 0; //$totaldisc2 + $_disc2;
            $totaldisc3 = 0; //$totaldisc3 + $_disc3;
            $totalppn = 0; //$totalppn + $ppn;
            $totalnetto = 0; //$totalnetto + $netto;
            $totalextradisc=0;
            foreach($query->result() as $row) {              
                //                                                                
                //d1/2/3/ppn/edisc hanya berlaku 1 per no nota
                if(!($currentid==$row->nomer_nota)) {
                    $totalbruto=($row->tro_totalprice);                    
                    $_disc1=$row->disc1;
                    $net1=$totalbruto - ($_disc1/100*$totalbruto);
                    $nominal_disc1 = ($_disc1/100*$totalbruto);                    
                    //
                    $_disc2=$row->disc2;
                    $net2=$net1 - ($_disc2/100*$net1);
                    $nominal_disc2 = ($_disc2/100*$net1);
                    $_disc3=$row->disc3;
                    $net3=$net2 - ($_disc3/100*$net2);
                    $nominal_disc3 = ($_disc3/100*$net2);
                    $_net_price=$net3;
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price - $_extra_disc;
                    $_bruto_price=$totalbruto;
                    $_ppn=$row->ppn;  
                    $ppn=0;
                    if($_ppn==1) {
                        $ppn=(0.1*$_net_price);
                    }
                    $_extra_disc=$row->tro_extra_disc;
                    $_net_price = $_net_price + $ppn;
                    $_total_bruto = $totalbruto;                    
                }else {
                    $_disc1=0;
                    $_disc2=0;
                    $_disc3=0;
                    $ppn=0;       
                    $_extra_disc=0;       
                    $_net_price=0;  
                    $_total_bruto=0;
                    $nominal_disc1=0;
                    $nominal_disc2=0;
                    $nominal_disc3=0;
                }
                //
		$retval[ ] = array( 
                        'counter' => $counter,
                        'datename' => $row->datename,
                        'nota' => $row->nomer_nota,
                        'marketing' => $row->marketing,
                        'outlet' => $row->name,
                        'addr' => $row->addr,
                        'prodname' => $row->trans_name,
                        'qtyitem' => $row->trans_qty,                        
                        'priceitem' => $row->trans_price,                        
                        'discitem' => $row->trans_disc,                    
                        'totalitem' => $row->trans_total,
                        'bruto' => $_total_bruto,
                        'disc1' => $nominal_disc1 . "($_disc1 %)",
                        'disc2' => $nominal_disc2 . "($_disc2 %)",
                        'disc3' => $nominal_disc3 . "($_disc3 %)",                        
                        'ppn' => $ppn,        
                        'extradisc'=>$_extra_disc,
                        'netto' => $_net_price
                    );
                $counter = $counter + 1; 
                $totalqtyitem = $totalqtyitem + $row->trans_qty;
                $totalpriceitem = $totalpriceitem + $row->trans_price;
                $totaldiscitem = $totaldiscitem + $row->trans_disc;
                $totalitem = $totalitem + $row->trans_total;
                $totalbruto = $totalbruto + $_total_bruto;
                $totaldisc1 = $totaldisc1 + $nominal_disc1;
                $totaldisc2 = $totaldisc2 + $nominal_disc2;
                $totaldisc3 = $totaldisc3 + $nominal_disc3;
                $totalppn = $totalppn + $ppn;
                $totalextradisc = $totalextradisc+ $_extra_disc;
                $totalnetto = $totalnetto + $_net_price;
                $currentid=$row->nomer_nota;
            }
            //
            $retval[ ] = array( 
                    'counter' => "",
                    'datename' => "",
                    'nota' => "",
                    'marketing' => "",
                    'outlet' => "",
                    'addr' => "TOTAL",
                    'prodname' => "",
                    'qtyitem' => $totalqtyitem,                        
                    'priceitem' => $totalpriceitem,                        
                    'discitem' => $totaldiscitem,                    
                    'totalitem' => $totalitem,
                    'bruto' => $totalbruto,
                    'disc1' => $totaldisc1,
                    'disc2' => $totaldisc2,
                    'disc3' => $totaldisc3,                        
                    'extradisc' => $totalextradisc,   
                    'ppn' => $totalppn,   
                    'netto' => $totalnetto
                );            
            //
            return $retval;            
        }                  
        //
        function calc_omzet_counter_detail_void($id,$start,$finish) {
            $CI =& get_instance();
            
            $data = array();
            $this->log_message("omzet info  id $id periode $start dan $finish");
            $sql = "SELECT trans_detail.*,date_name(tro_date) as datename,tro_cust_id " ;
            $sql = $sql .  " FROM trans_detail";
            $sql = $sql .  " INNER JOIN transout on trans_detail.trans_id=transout.tro_id";
            $sql = $sql . " WHERE  ";
            $sql = $sql . " (tro_cust_id=?  or tro_cust_id IN (SELECT distinct(cl_id)  FROM client WHERE cl_reference=?) ) AND ";
            $sql = $sql . " to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=?";
            
            $query=  $this->db->query($sql,array($id,$id,$start,$finish));
            $counter = 1;            

            $totalnetto=0;
            $totaldisc=0;
            $totalprice=0;
            $totalqty=0;
            $name = "";
            $idtour= "";
            $paket = "";
            $groupname = "";
            $share = 0;
            $margin = 0;
            $totalshare=0;
            $totalmargin=0;
            foreach($query->result() as $row) {              
                //                                                                
                $totalnetto = $totalnetto + $row->trans_total;
                $totalprice = $totalprice + $row->trans_price;
                $totalqty = $totalqty + $row->trans_qty;
                
                $percent=$this->get_percent_mkt($id);
                $margin = $row->trans_qty * ($row->trans_price - $row->trans_buyprice);
                //dipotong solar
                $potongan = $this->get_potongan() *$margin;
                $margin2 = $margin - $potongan;
                $share=($percent/100)* ($margin2);
                $totalshare = $totalshare + $share;
                $totalmargin=$totalmargin + $margin2;
                $client_name= $this->get_cl_name($row->tro_cust_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'name' => $client_name,
                        'datename' => $row->datename,
                        'prodname' => $row->trans_name,                        
                        'price' => $row->trans_price,                        
                        'qty' => $row->trans_qty,                    
                        'netto' => $row->trans_total,
                        'margin' => $margin2 . "($margin-$potongan)",
                        'share' => $share,
                        'buyprice' => $row->trans_buyprice
                    );
                $counter = $counter + 1;                
            }
            //
		$retval[ ] = array(
                        'counter' => "",
			'name' => "TOTAL",
                        'datename' => "",
                        'prodname' =>"",
                        'price' => "",
                        'qty' => $totalqty,                    
                        'netto' => $totalprice,
                        'margin' => $totalmargin,
                        'share' => $totalshare,
                        'buyprice' => ""                    
                    );
            
            return $retval;            
        }          
        //
        function calc_doc_omzet_detail($start,$finish,$doct_id) {
            $CI =& get_instance();
            $CI->load->model('model_pasien');            
            $this->modpasien = $CI->model_pasien;
            
            $data = array();
            $sql = "SELECT date_name(frontend_date) as date,frontend_lab_id as lab_id,patient_name, sum(result_detail.rd_price) as bruto,";
            $sql = $sql . " sum(result_detail.rd_disc) as disc,sum(result_detail.rd_net) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " INNER JOIN frontend ON frontend.frontend_lab_id=result_detail.rd_frontend_lab_id ";
            $sql = $sql . " INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql . " WHERE length(rd_pxcode)=5 AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " AND frontend_doct_id=? ";
            $sql = $sql . " GROUP BY frontend_lab_id,frontend_date,patient_name ORDER BY lab_id";
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish,$doct_id));
            $counter = 1;            
            $totalnetto =0;
            $hema = 0;
            $kimia = 0;
            $imun = 0;
            $faeces = 0;
            $urine = 0;
            $analisalain = 0;
            $mikro = 0;
            $infeksilain = 0;
            $radiologi = 0;
            $ekg = 0;
            $usg = 0;            
            $totalhema = 0;
            $totalkimia = 0;
            $totalimun = 0;
            $totalfaeces = 0;
            $totalurine = 0;
            $totalanalisalain = 0;
            $totalmikro = 0;
            $totalinfeksilain = 0;
            $totalradiologi = 0;
            $totalekg = 0;
            $totalusg = 0;               
            $grandtotal = 0;
            foreach($query->result() as $row) {              
                $name = $this->modpasien->get_name_by_labid($row->lab_id);
                //$name = "";
                $date = $this->get_check_up_date($row->lab_id);
                $hema = 0;
                $kimia = 0;
                $imun = 0;
                $faeces = 0;
                $urine = 0;
                $analisalain = 0;
                $mikro = 0;
                $infeksilain = 0;
                $radiologi = 0;
                $ekg = 0;
                $usg = 0;                
                
                $hema = $this->modomzet->get_omzet_by_group('H', $row->lab_id);                
                $kimia = $this->modomzet->get_omzet_by_group('K', $row->lab_id);
                $imun = $this->modomzet->get_omzet_by_group('I', $row->lab_id);
                $faeces = $this->modomzet->get_omzet_by_group('F', $row->lab_id);
                $urine = $this->modomzet->get_omzet_by_group('U', $row->lab_id);
                $analisalain = $this->modomzet->get_omzet_by_group('A', $row->lab_id);
                $mikro = $this->modomzet->get_omzet_by_group('M', $row->lab_id);
                $infeksilain = $this->modomzet->get_omzet_by_group('S', $row->lab_id);
                $radiologi = $this->modomzet->get_omzet_by_group('R', $row->lab_id);
                $ekg = $this->modomzet->get_omzet_by_group('E', $row->lab_id);
                $usg = $this->modomzet->get_omzet_by_group('U', $row->lab_id);
                $totalhema = $totalhema + $hema;
                $totalkimia = $totalkimia + $kimia;
                $totalimun = $totalimun + $imun;
                $totalfaeces = $totalfaeces + $faeces;
                $totalurine = $totalurine + $urine;
                $totalanalisalain = $totalanalisalain + $analisalain;
                $totalmikro = $totalmikro + $mikro;
                $totalinfeksilain = $totalinfeksilain + $infeksilain;
                $totalradiologi = $totalradiologi + $radiologi;
                $totalekg = $totalekg + $ekg;
                $totalusg = $totalusg + $usg;
                //                                                                
                $totalnetto = $hema + $kimia + $imun + $faeces + $urine + $analisalain + $mikro + $infeksilain +  $radiologi + $ekg + $usg;
                $grandtotal = $grandtotal + $totalnetto;
		$retval[ ] = array(
                        'counter' => $counter,
			'lab_id' => $row->lab_id,
                        'name' => $name,
                        'date' => $date,
                        'hema' => $hema,
                        'kimia' => $kimia,
			'imun' => $imun,
                        'faeces' => $faeces,
                        'urine' => $urine,
                        'analisalain' => $analisalain,
                        'mikro' => $mikro,
                        'infeksilain' => $infeksilain,
                        'radiologi' => $radiologi,
                        'ekg' => $ekg,
                        'usg' => $usg,
                        'netto' => $totalnetto
                    );
                $counter = $counter + 1;                
            }
            //
		$retval[ ] = array(
                        'counter' => "",
			'lab_id' => "TOTAL",
                        'name' => "",
                        'date' => "",
                        'hema' => $totalhema,
                        'kimia' => $totalkimia,
			'imun' => $totalimun,
                        'faeces' => $totalfaeces,
                        'urine' => $totalurine,
                        'analisalain' => $totalanalisalain,
                        'mikro' => $totalmikro,
                        'infeksilain' => $totalinfeksilain,
                        'radiologi' => $totalradiologi,
                        'ekg' => $totalekg,
                        'usg' => $totalusg,
                        'netto' => $grandtotal
                    );            
            //
            $percenthema= $this->get_percent_of_group('h');
            $percentkimia=$this->get_percent_of_group('k');
            $percentimun=$this->get_percent_of_group('i');
            $percentfaeces=$this->get_percent_of_group('f');
            $percenturine=$this->get_percent_of_group('u');
            $percentanalisalain=$this->get_percent_of_group('a');
            $percentmikro=$this->get_percent_of_group('m');
            $percentinfeksilain=$this->get_percent_of_group('s');
            $percentradiologi=$this->get_percent_of_group('r');
            $percentekg=$this->get_percent_of_group('e');
            $percentusg=$this->get_percent_of_group('g');
            //
            $totalhema=$percenthema*$totalhema;
            $totalkimia=$percentkimia*$totalkimia;
            $totalimun=$percentimun*$totalimun;
            $totalfaeces=$percentfaeces*$totalfaeces;
            $totalurine=$percenturine*$totalurine;
            $totalanalisalain=$percentanalisalain*$totalanalisalain;
            $totalmikro=$percentmikro*$totalmikro;
            $totalinfeksilain=$percentinfeksilain*$totalinfeksilain;
            $totalradiologi=$percentradiologi*$totalradiologi;
            $totalekg=$percentekg*$totalekg;
            $totalusg=$percentusg*$totalusg;
            //
            $totalpercent = $totalhema + $totalkimia + $totalimun + $totalfaeces + $totalurine + $totalanalisalain + $totalmikro + $totalinfeksilain +  $totalradiologi + $totalekg + $totalusg;
            //
            $retval[ ] = array(
                    'counter' => "",
                    'lab_id' => "% CN",
                    'name' => "",
                    'date' => "",
                    'hema' => $percenthema,
                    'kimia' => $percentkimia,
                    'imun' => $percentimun,
                    'faeces' => $percentfaeces,
                    'urine' => $percenturine,
                    'analisalain' => $percentanalisalain,
                    'mikro' => $percentmikro,
                    'infeksilain' => $percentinfeksilain,
                    'radiologi' => $percentradiologi,
                    'ekg' => $percentekg,
                    'usg' => $percentusg,
                    'netto' => ""
                );
            //
            $retval[ ] = array(
                    'counter' => "",
                    'lab_id' => "TOTAL CN",
                    'name' => "",
                    'date' => "",
                    'hema' => $totalhema,
                    'kimia' => $totalkimia,
                    'imun' => $totalimun,
                    'faeces' => $totalfaeces,
                    'urine' => $totalurine,
                    'analisalain' => $totalanalisalain,
                    'mikro' => $totalmikro,
                    'infeksilain' => $totalinfeksilain,
                    'radiologi' => $totalradiologi,
                    'ekg' => $totalekg,
                    'usg' => $totalusg,
                    'netto' => $totalpercent
                );                        
            //
            return $retval;                        
        }  
        //end of d  oct omzet
        function get_percent_of_group($group_code) {
            $percent=0;
            $group_code = strtolower($group_code);
            $sql = " SELECT gp_percent FROM grouppx WHERE lower(trim(gp_name))=?";
            $query = $this->db->query($sql,array($group_code));
            foreach($query->result() as $row) {
                $percent = $row->gp_percent;
            }
            return $percent;
        }
        //
        function calc_omzet_global($start,$finish) {
            $data = array();
            $sql = "SELECT frontend_lab_id as lab_id,patient_name, sum(result_detail.rd_price) as bruto,";
            $sql = $sql . " sum(result_detail.rd_disc) as disc,sum(result_detail.rd_net) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " INNER JOIN frontend ON frontend.frontend_lab_id=result_detail.rd_frontend_lab_id ";
            $sql = $sql . " INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql . " WHERE length(rd_pxcode)=5 AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " GROUP BY frontend_lab_id,patient_name ORDER BY lab_id";
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;
            $totalbruto = 0;
            $totaldisc =0;
            $totalnetto =0;
            foreach($query->result() as $row) {              
                $date = $this->get_check_up_date($row->lab_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'lab_id' => $row->lab_id,
                        'date' => $date,                    
			'name' => $row->patient_name,
                        'bruto' => $row->bruto,
                        'disc' => $row->disc,
                        'net' => $row->net,
		);                      
                $counter = $counter + 1;
                $totalbruto = $totalbruto + $row->bruto;
                $totaldisc = $totaldisc + $row->disc;
                $totalnetto = $totalnetto + $row->net;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'lab_id' =>"",
                    'date'=>"",
                    'name' => "TOTAL",
                    'bruto' => $totalbruto,
                    'disc' => $totaldisc,
                    'net' => $totalnetto
            ); 
            return $retval;
        }  
        
        //doc
        function patient_count_per_month($month,$year,$doct_id) {
            $sql = " SELECT COUNT(*) as jumlah FROM frontend ";
            $sql = $sql . " WHERE frontend_doct_id=? " ;                   
            $sql = $sql . " AND extract(month from frontend_date)=? AND extract(year from frontend_date)=? ";
            $this->log_message("patient doct $doct_id dengan $sql dan $month serta $year");
            $query = $this->db->query($sql,array($doct_id,$month,$year));
            $count = 0;
            foreach($query->result() as $row) {
                $count = $row->jumlah;
            }
            return $count;
        }
        
        //
        function patient_count($start,$finish,$doct_id) {
            $sql = " SELECT COUNT(*) as jumlah FROM frontend ";
            $sql = $sql . " WHERE frontend_doct_id=? " ;                   
            $sql = $sql . " AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $query = $this->db->query($sql,array($doct_id,$start,$finish));
            $count = 0;
            foreach($query->result() as $row) {
                $count = $row->jumlah;
            }
            return $count;
        }
        function calc_omzet_doct_global($start,$finish) {
            $data = array();            
            $sql = "SELECT doct_id as doct_id,doct_name, sum(result_detail.rd_price) as bruto,";
            $sql = $sql . " sum(result_detail.rd_disc) as disc,sum(result_detail.rd_net) as net ";
            $sql = $sql . "  ";
            $sql = $sql . " FROM result_detail ";
            $sql = $sql . " INNER JOIN frontend ON frontend.frontend_lab_id=result_detail.rd_frontend_lab_id ";
            $sql = $sql . " INNER JOIN doctor ON frontend.frontend_doct_id=doctor.doct_id ";
            $sql = $sql . " WHERE length(rd_pxcode)=5 AND to_date(frontend_date::text,'YYYY-MM-DD')>=? AND to_date(frontend_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " GROUP BY doct_id,doct_name ORDER BY doct_id";
            $this->log_message("omzet info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;
            $totalbruto = 0;
            $totaldisc =0;
            $totalnetto =0;
            $totalcount = 0;
            foreach($query->result() as $row) {                              
                $count = $this->patient_count($start,$finish,$row->doct_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'doct_id' => $row->doct_id,
                        'name' => $row->doct_name,
                        'count' => $count,
                        'bruto' => $row->bruto,
                        'disc' => $row->disc,
                        'net' => $row->net,
		);                      
                $counter = $counter + 1;
                $totalbruto = $totalbruto + $row->bruto;
                $totaldisc = $totaldisc + $row->disc;
                $totalnetto = $totalnetto + $row->net;
                $totalcount = $totalcount + $count;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'doct_id' => "",
                    'count' => $totalcount,
                    'name' => "TOTAL",
                    'bruto' => $totalbruto,
                    'disc' => $totaldisc,
                    'net' => $totalnetto
            );                                  
            return $retval;
        }          
}
?>
