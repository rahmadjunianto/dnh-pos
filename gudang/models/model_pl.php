<?php
/*
 * Januari 2014
 */

class Model_pl extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                $this->load->library("class_public");  
	}
        function show_person_list_by_paket($id) {
            $sql = " SELECT jamaah.*,tour_id  FROM jamaah ";
            $sql = $sql . " INNER JOIN tour ON tour.tour_j_id = jamaah.j_id ";
            $sql = $sql . " WHERE tour.tour_paket=?";
            $this->log_message("$sql");
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row) {
		$retval[ ] = array(
			'id' => $row->j_id,
			'name' => $row->j_name,
			'addr' => $row->j_addr,
			'city' => $row->j_kota,
			'phone' => $row->j_telp,
                        'phonecell' => $row->j_hp,
                        'tour_id' => $row->tour_id
		);                      
            }
            return $retval;
        }
       function get_name_by_id($id) {
            $name="";            
            $this->log_message("get name by id CALLED ");
            $sql = " SELECT (j_title || ' ' || j_name) as name FROM jamaah   ";
            $sql = $sql . "   WHERE j_id=? ";
            
            $this->log_message("sql get name $sql  dgn id $id ");            
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $name = $row->name;
            }
            return $name;
        }             
        function show_manifest_by_paket($id) {
            $CI =& get_instance();
            $this->modjamaah = $CI->load->model('model_jamaah');                    
            
            $id=$id . "";
            $sql = " SELECT jamaah.*,age_name(j_tgl_lahir) as age FROM jamaah ";
            $sql = $sql . " INNER JOIN tour ON tour.tour_j_id = jamaah.j_id ";
            $sql = $sql . " WHERE tour.tour_paket=?";
            $this->log_message("$sql");
            $query = $this->db->query($sql,array($id));
            $counter=0;
            $retval=array();
            foreach($query->result() as $row) {
                $this->log_message("ENTERING ROW..");
                $counter=$counter+1;
                $mahram_name="";
                $hubunganmahram="";
                if($row->j_id==$row->j_mahram) {
                    $mahram_name="";
                    $hubunganmahram="";
                }else {
                    $hubunganmahram=$row->j_hubungan_mahram;
                    $this->log_message("CARI NAMA..");
                    $mahram_name=$this->get_name_by_id($row->j_mahram);
                }
                $this->log_message("ENTERING ROW 2..$mahram_name");
		$retval[ ] = array(
			'no' => $counter,
                        'title' => $row->j_title,
			'name' => $row->j_name,
                        'bp' => $row->j_tempat_lahir,
			'bd' => $row->j_tgl_lahir,
			'age' => $row->age,
			'passno' => $row->j_no_passport,
                        'office' => $row->j_issuing_office,
                        'issuedate' => $row->j_date_of_issue,
                        'hubunganmahram' => $hubunganmahram,
                        'mahram' => $mahram_name
		);                      
            }
            return $retval;
        }
   
}
?>
