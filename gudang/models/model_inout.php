<?php
/*
 * Sep 2014
 */

class Model_inout extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                //$this->load->library("class_public");
                $this->user_id = $this->acl->get_users_id();
	}
        function get_client_name_by_tro_id($id) {
            $retval="";
            $sql=" SELECT cl_name from client inner join transout ON transout.tro_cust_id=client.cl_id ";
            $sql = $sql . " WHERE tro_id=?";
            $query=$this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $retval = $row->cl_name;
            }
            return $retval;
        }
        //
        function get_supp_name_by_trin_id($id) {
            $retval="";
            $sql=" SELECT supp_name from supplier inner join transin ON transin.tri_supp_id=supplier.supp_id ";
            $sql = $sql . " WHERE tri_id=?";
            $query=$this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $retval = $row->supp_name;
            }
            return $retval;
        }
        //
        function calc_in($start,$finish) {
            $data = array();
            $sql = "SELECT trans_detail.* ,date_name(trans_date) as transdate FROM trans_detail WHERE trans_type=1 ";
            $sql = $sql . " AND to_date(trans_date::text,'YYYY-MM-DD')>=? AND to_date(trans_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " ORDER BY trans_date,trans_id";
            $this->log_message("calc info info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;
            $totalbruto = 0;
            $totalqty =0;
            //$totalnetto =0;
            foreach($query->result() as $row) {
                //$date = $this->get_check_up_date($row->trans_id);
                $supp=$this->get_supp_name_by_trin_id($row->trans_id);
                $this->log_message("SUPP : $supp");
		$retval[ ] = array(
                        'counter' => $counter,
			'trans_id' => $row->trans_id,
                        'date' => $row->transdate,
			'name' => $row->trans_name,
                        'supp'=>$supp,
                        'total' => $row->trans_total,
                        'qty' => $row->trans_qty,
                        'price' => $row->trans_price,
		);
                $counter = $counter + 1;
                $totalbruto = $totalbruto + $row->trans_total;
                $totalqty = $totalqty + $row->trans_qty;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'trans_id' =>"",
                    'date'=>"",
                    'supp'=>"",
                    'name' => "TOTAL",
                    'total' => $totalbruto,
                    'qty' => $totalqty,
                    'price' => ""
            );
            return $retval;
        }
        //calc out
        function calc_out($start,$finish) {
            $data = array();
            $sql = "SELECT trans_detail.* ,date_name(trans_date) as transdate FROM trans_detail WHERE trans_type=2 ";
            $sql = $sql . " AND to_date(trans_date::text,'YYYY-MM-DD')>=? AND to_date(trans_date::text,'YYYY-MM-DD')<=?";
            $sql = $sql . " ORDER BY trans_date,trans_id ";
            $this->log_message("calc info info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish));
            $counter = 1;
            $totalbruto = 0;
            $totalqty =0;
            //$totalnetto =0;
            foreach($query->result() as $row) {
                //$date = $this->get_check_up_date($row->trans_id);
                $client=$this->get_client_name_by_tro_id($row->trans_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'trans_id' => $row->trans_id,
                        'date' => $row->transdate,
			'name' => $row->trans_name,
                        'total' => $row->trans_total,
                        'qty' => $row->trans_qty,
                        'price' => $row->trans_price,
                        'client'=>$client
		);
                $counter = $counter + 1;
                $totalbruto = $totalbruto + $row->trans_total;
                $totalqty = $totalqty + $row->trans_qty;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'trans_id' =>"",
                    'client' =>"",
                    'date'=>"",
                    'name' => "TOTAL",
                    'total' => $totalbruto,
                    'qty' => $totalqty,
                    'price' => ""
            );
            return $retval;
        }
        //in out per item
        function in_out_per_item($start,$finish,$id) {
            //$CI =& get_instance();
            //$CI->load->model('model_');
            //$this->modpasien = $CI->model_pasien;

            $data = array();
            $sql = "SELECT trans_detail.* ,date_name(trans_date) as transdate FROM trans_detail WHERE 1=1 ";
            $sql = $sql . " AND to_date(trans_date::text,'YYYY-MM-DD')>=? AND to_date(trans_date::text,'YYYY-MM-DD')<=? ";
            $sql = $sql . " AND trans_code=? ";
            $sql = $sql . " ORDER BY trans_date,trans_id ";
            $this->log_message("calc info info $sql periode $start dan $finish");
            $query=  $this->db->query($sql,array($start,$finish,$id));
            $counter = 1;
            $totalbruto = 0;
            $totalqty =0;
            $sumqty=$this->stock_before_date($id, $start);
		$retval[ ] = array(
                        'counter' => "",
                        'code' => "",
			'trans_id' => "",
                        'date' => "QTY sebelumnya",
			'name' => "",
                        'total' => "",
                        'tipe' => "",
                        'qty' => $sumqty,
                        'price' => "",
                        'sumqty' => $sumqty
		);
            //$totalnetto =0;
            foreach($query->result() as $row) {
                //$date = $this->get_check_up_date($row->trans_id);
                $tipetrans="";
                $type = $row->trans_type;
                if($type==3) {
                    $sumqty = $row->trans_qty;
                    $tipetrans="Opname";
                }elseif($type==1) { //masuk
                    $tipetrans="IN";
                    $sumqty = $sumqty + $row->trans_qty;
                }elseif($type==2) {
                    $tipetrans="OUT";
                    $sumqty = $sumqty - $row->trans_qty;
                }

		$retval[ ] = array(
                        'counter' => $counter,
                        'code' => $row->trans_code,
			'trans_id' => $row->trans_id,
                        'date' => $row->transdate,
			'name' => $row->trans_name,
                        'total' => $row->trans_total,
                        'tipe' => $tipetrans,
                        'qty' => $row->trans_qty,
                        'price' => $row->trans_price,
                        'sumqty' => $sumqty
		);
                $counter = $counter + 1;
                $totalbruto = $totalbruto + $row->trans_total;
                $totalqty = $totalqty + $row->trans_qty;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'trans_id' =>"",
                    'date'=>"TOTAL",
                    'code'=>"",
                    'name' => "",
                    'total' => $totalbruto,
                    'qty' => "",
                    'price' => "",
                    'tipe' => "",
                    'sumqty' => $sumqty
            );
            return $retval;
        }
        //
        function show_per_supplier($id) {
            //$CI =& get_instance();
            //$CI->load->model('model_');
            //$this->modpasien = $CI->model_pasien;

            $data = array();
            $sql = "SELECT prod_code,prod_name,prod_unit,prod_sellprice,prod_buyprice FROM products WHERE lower(prod_supplier) LIKE  '%" . strtolower($id) . "%'";
            $this->log_message("show_per_supplier info info $sql kode Supplier $id");
            $query=  $this->db->query($sql);
            $counter = 1;
            //$totalnetto =0;
            $sqldate=" SELECT to_date(localtimestamp::text,'YYYY-MM-DD') as nowdate";
            $qrydate= $this->db->query($sqldate);
            $todate="1970-01-01";
            foreach($qrydate->result() as $row) {
                $todate=$row->nowdate;
            }
            foreach($query->result() as $row) {
                $stock=$this->modinout->stock_per_date($row->prod_code,$todate);
                //$date = $this->get_check_up_date($row->trans_id);
		    $retval[ ] = array(
                'counter' => $counter,
                'code' => $row->prod_code,
			    'prod_name' => $row->prod_name,
                'prod_unit' => $row->prod_unit,
			    'prod_sellprice' => $row->prod_sellprice,
                'prod_buyprice' => $row->prod_buyprice,
                'stock' => $stock,
		    );
                $counter = $counter + 1;
            }
            return $retval;
        }
        function stock_per_date($code,$date) {
            $date="" . $date;
            $totalstock=0;
            $sql=" SELECT COUNT(*) as jumlah  FROM trans_detail WHERE trans_type=3 AND trans_code=? AND to_date(trans_date::text,'YYYY-MM-DD')<=to_date('$date'::text,'YYYY-MM-DD')";
            $this->log_message("COUNT untuk  :  $sql ");
            $query=  $this->db->query($sql,array($code));
            $total=0;
            foreach($query->result() as $row) {
                $total= $row->jumlah;
            }
            if($total>0) {
                //cari last stock opname
                $sql=" SELECT *  FROM trans_detail WHERE  trans_code=?  AND to_date(trans_date::text,'YYYY-MM-DD') <=to_date('$date'::text,'YYYY-MM-DD') AND trans_date >= (";
                $sql = $sql . " SELECT trans_date FROM trans_detail WHERE trans_type=3 AND trans_code=? AND to_date(trans_date::text,'YYYY-MM-DD')<=to_date('$date'::text,'YYYY-MM-DD') LIMIT 1 ";
                $sql =  $sql . " ) ";
                $sql = $sql . "  ORDER BY trans_date,trans_id ASC";
                $this->log_message($sql);
                $qrydetail=  $this->db->query($sql,array($code,$code));
                foreach($qrydetail->result() as $row) {
                    if($row->trans_type=="3") {
                        //
                        $totalstock=$row->trans_qty;
                    }else {
                        if($row->trans_type=="1") {
                            $totalstock= $totalstock + $row->trans_qty;
                        }
                        if($row->trans_type=="2") {
                            $totalstock= $totalstock - $row->trans_qty;
                        }
                    }
                }
                //$qrydetail=$this->db->query($sql,array($code));
            }else {
                $this->log_message("TOTAL kurang dari = nol");
                //hitung by transaksi dengan tidak pernah ada stock opname
                $sql=" SELECT *  FROM trans_detail WHERE  trans_code=?  AND to_date(trans_date::text,'YYYY-MM-DD')<=to_date(?::text,'YYYY-MM-DD')";
                $sql = $sql . "  ORDER BY trans_date ASC ";
                $this->log_message("SQL : $sql");
                $qrydetail=  $this->db->query($sql,array($code,$date));
                foreach($qrydetail->result() as $row) {
                    $this->log_message("QTY : " . $row->trans_qty);
                    if($row->trans_type==1) {
                        $totalstock= $totalstock + $row->trans_qty;
                    }
                    if($row->trans_type==2) {
                        $totalstock= $totalstock - $row->trans_qty;
                    }
                    //
                }
                //
            }
            return $totalstock;
        } //end of function stock_per_date
        //before date
        function stock_before_date($code,$date) {
            $date="" . $date;
            $totalstock=0;
            $sql=" SELECT COUNT(*) as jumlah  FROM trans_detail WHERE trans_type=3 AND trans_code=? AND to_date(trans_date::text,'YYYY-MM-DD')<to_date('$date'::text,'YYYY-MM-DD')";
            //$this->log_message("COUNT untuk  :  $sql ");
            $query=  $this->db->query($sql,array($code));
            $total=0;
            foreach($query->result() as $row) {
                $total= $row->jumlah;
            }
            if($total>0) {
                //cari last stock opname
                $sql=" SELECT *  FROM trans_detail WHERE  trans_code=?  AND to_date(trans_date::text,'YYYY-MM-DD') <to_date('$date'::text,'YYYY-MM-DD')  AND trans_date >= (";
                $sql = $sql . " SELECT trans_date FROM trans_detail WHERE trans_type=3 AND trans_code=? AND to_date(trans_date::text,'YYYY-MM-DD')<to_date('$date'::text,'YYYY-MM-DD') LIMIT 1 ";
                $sql =  $sql . " ) ";
                $sql = $sql . "  ORDER BY trans_date,trans_id ASC";
                $qrydetail=  $this->db->query($sql,array($code,$code));
                foreach($qrydetail->result() as $row) {
                    if($row->trans_type=="3") {
                        //
                        $totalstock=$row->trans_qty;
                    }else {
                        if($row->trans_type=="1") {
                            $totalstock= $totalstock + $row->trans_qty;
                        }
                        if($row->trans_type=="2") {
                            $totalstock= $totalstock - $row->trans_qty;
                        }
                    }
                }
                //$qrydetail=$this->db->query($sql,array($code));
            }else {
                $this->log_message("TOTAL kurang dari = nol");
                //hitung by transaksi dengan tidak pernah ada stock opname
                $sql=" SELECT *  FROM trans_detail WHERE  trans_code=?  AND to_date(trans_date::text,'YYYY-MM-DD')<to_date(?::text,'YYYY-MM-DD')";
                $sql = $sql . "  ORDER BY trans_date ASC ";
                $this->log_message("SQL : $sql");
                $qrydetail=  $this->db->query($sql,array($code,$date));
                foreach($qrydetail->result() as $row) {
                    $this->log_message("QTY : " . $row->trans_qty);
                    if($row->trans_type==1) {
                        $totalstock= $totalstock + $row->trans_qty;
                    }
                    if($row->trans_type==2) {
                        $totalstock= $totalstock - $row->trans_qty;
                    }
                    //
                }
                //
            }
            return $totalstock;
        } //end of function stock_bfore_date
}
?>
