<?php
/*
 * Januari 2014
 */

class Model_tulok extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                                $this->load->library("class_public");
	}
                function get_tulok_standar() {                        
                        $retval = 0;
                        $tulokstandar = $this->class_public->getSettingGaji("tulok standar");
                        $this->log_message("tulok standar: " . $tulokstandar);
                        $retval = $tulokstandar;
                        return $retval;
                }
                
	function get_data_tulok($idcabang) {//dipake
		$retval = array();
                                $tulokstandar = $this->get_tulok_standar();
                                $this->log_message("tulok standar: " . $tulokstandar);
                                $sql = "SELECT * FROM cabang  where idcabang=?";
                                $query = $this->db->query($sql, array($idcabang));
		foreach ($query->result() as $row) {
                                            
			$retval[ ] = array(
				'pu' => ($row->persentulokpu/100)*$tulokstandar,
				'karyawan' => ($row->persen_tulok/100)*$tulokstandar,
				'spv3' => $row->tulok_spv3,
				'spv2' => $row->tulok_spv2,
				'spv1' => $row->tulok_spv1,
				'wakacab' => $row->tulok_wakacab,
				'kacab' => $row->tulok_kacab,
                                                                'dokter' => $row->tulok_dokter
			);
		}
		return $retval;                                
	}
                //save std tulok
                function save_tulok_cabang($idcabang,$pu,$karyawan,$spv1,$spv2,$spv3,$wakacab,$kacab ) {                                        
                    //
                    $dtulokstandar =0 ;
                    $dtulokstandar = $this->class_public->getSettingGaji("tulok standar");
                    $this->log_message("tulok standar: " . $dtulokstandar);                                        
                    $dpersentulokpu=0;
                    $dpersentulokpu = ($pu / $dtulokstandar) * 100;
                    $dpersentulok = ($karyawan / $dtulokstandar) * 100;
                    
                    $strSQL = " UPDATE cabang SET ";
                    $strSQL = $strSQL . " persentulokpu= ? ,";
                    $strSQL = $strSQL .  " persen_tulok= ?,";
                    $strSQL = $strSQL . " tulok_spv1= ? , ";
                    $strSQL = $strSQL . " tulok_spv2= ? ,";
                    $strSQL = $strSQL . " tulok_spv3= ? ,";
                    $strSQL = $strSQL .  " tulok_wakacab= ? ,";
                    $strSQL = $strSQL .  " tulok_kacab= ?  ";
                    $strSQL = $strSQL  . " WHERE idcabang= ? ";
                    $query = $this->db->query($strSQL, array($dpersentulokpu, $dpersentulok, $spv1,$spv2,$spv3,$wakacab,$kacab,$idcabang));                    
                    return 0;
                }
                //end of save std tulok
}

?>
