<?php
/*
 * Januari 2014
 */

class Model_regist extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                $this->load->library("class_public");
	}
        //delete 
        function regist_info($tr_id){
            $data = array();            
            $sql = " SELECT users_name as mktname,date_name(localtimestamp) as jamsekarang,tro_id,tro_cust_id as id ,date_name(tro_date) as transdate,tro_users_id,  ";
            $sql = $sql .  "cl_name as name,cl_address as addr ,cl_kabupaten,cl_city as city   ";                                    
            $sql = $sql .  " FROM client INNER JOIN transout ON transout.tro_cust_id=client.cl_id";            
            $sql = $sql .  " INNER JOIN trans_detail ON trans_detail.trans_id=transout.tro_id";            
            $sql = $sql .  " INNER JOIN users ON transout.tro_mktid=users.users_id";            
            $sql = $sql . " Where transout.tro_id= ? ";
            $this->log_message($sql . " ");
            $query = $this->db->query($sql,array($tr_id));
            return $query->result();
        }      
//
        function regist_in_info($tr_id){
            $data = array();            
            $sql = " SELECT date_name(localtimestamp) as jamsekarang,tri_id,tri_supp_id as id ,date_name(tri_date) as transdate,tri_users_id,  ";
            $sql = $sql .  "supp_name as name,supp_address as addr  ";                                    
            $sql = $sql .  " FROM supplier  INNER JOIN transin ON transin.tri_supp_id=supplier.supp_id ";            
            $sql = $sql .  " INNER JOIN trans_detail ON trans_detail.trans_id=transin.tri_id ";            
            $sql = $sql . " Where transin.tri_id= ? ";
            $this->log_message($sql . " ");
            $query = $this->db->query($sql,array($tr_id));
            return $query->result();
        }      
//        
        function regist_full_info($tr_id){
            $data = array();            
            $sql = " SELECT patient_phone,patient_mobilephone,patient_id,frontend_lab_id as lab_id,patient_title || ' ' || patient_name as name,(coalesce(patient_address,'') || ' ' || coalesce(patient_city,'') ) as addr ,age_name(to_date(patient_dob::text,'YYYY-MM-DD')) as age,doct_name  ";
            $sql = $sql .  ",patient_gender as gender,date_name(to_date(frontend_date::text,'YYYY-MM-DD')) as labdate,frontend_res_del as delivery,frontend_res_finish_char as finish_state, (to_date(localtimestamp::text,'YYYY-MM-DD') ||' ' || to_char(localtimestamp,'HH24:MM')) as jamsekarang1 ,date_name(localtimestamp) as jamsekarang, (coalesce(patient_phone,'') || '/' || coalesce(patient_mobilephone,'') ) as phonenum  ";
            $sql = $sql .  ", to_date(patient_regdate::text,'YYYY-MM-DD') as regdate FROM frontend INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id";
            $sql = $sql .  " INNER JOIN doctor on doctor.doct_id=Frontend.frontend_doct_id ";
            $sql = $sql . " Where frontend_lab_id= ? ";
            $this->log_message($sql . " ");
            $paid = $this->lab_id_is_paid($tr_id);
            $retval=array();
            $query = $this->db->query($sql,array($tr_id));
            //
            foreach($query->result() as $row) {
                //                
		$retval[ ] = array(
			'patient_id' => $row->patient_id,
			'lab_id' => $row->lab_id,
			'name' => $row->name,
			'addr' => $row->addr,
			'age' => $row->age,
                        'doct_name' => $row->doct_name,
                        'gender' => $row->gender,
                        'phonenum' => $row->phonenum,
                        'lab_date' => $row->labdate,
                        'reg_date'=> $row->regdate,
                        'delivery'=> $row->delivery,
                        'complete_phone_num'=> $row->phonenum,
                        'finish_state'=> $row->finish_state,
                        'paid'=> $paid
		);      
            }
            return $retval;
        }        
        function search_regist_full_info($tp_id,$name){
            $CI =& get_instance();
            $CI->load->model('model_payment');            
            $this->mp = $CI->model_payment;
            
            $name = strtolower($name);
            $data = array();            
            $sql = " SELECT transout.tro_id as id,cl_id,'' || cl_name as name,cl_address || ' ' || coalesce(cl_city,'') as addr ";
            $sql = $sql .  ",cl_phone as phone,cl_sex as sex ,date_name(tro_date) as registdate ";
            $sql = $sql .  " FROM client ";
            $sql = $sql .  " INNER JOIN transout on transout.tro_cust_id=client.cl_id  ";
            $sql = $sql . " Where tro_id LIKE '$tp_id%'  OR lower(trim(cl_name)) LIKE '%$name%' ";
            $this->log_message("sql ; $sql ");
            $retval=array();
            $query = $this->db->query($sql,array($tp_id));
            //
            foreach($query->result() as $row) {
                //$outstanding=$payment['outstanding'];
                //$this->log_message($sql . " posisi lunas $spaid");                
                $data=$this->mp->tr_id_is_paid($row->id);
                if($data['paid']==1) {
                    $spaid="LUNAS";
                }else {
                    $spaid="BELUM LUNAS";
                }                 
		$retval[ ] = array(
			'name' => $row->name,
                        'cl_id' => $row->cl_id,			
			'id' => $row->id,
			'addr' => $row->addr,
                        'registdate' => $row->registdate,
                        'phone' => $row->phone,
                        'spaid' => $spaid,
                        'paid' => $data['paid'],
                        'sex' => $row->sex
		);      
            }
            return $retval;
        }   
        function search_regist_full_info2($tp_id,$name){
            $CI =& get_instance();
            $CI->load->model('model_payment');            
            $this->mp = $CI->model_payment;
            
            $name = strtolower($name);
            $data = array();            
            $sql = " SELECT transout.tro_id as id,cl_id,'' || cl_name as name,cl_address || ' ' || coalesce(cl_city,'') as addr ";
            $sql = $sql .  ",cl_phone as phone,cl_sex as sex ,date_name(tro_date) as registdate ";
            $sql = $sql .  " FROM client ";
            $sql = $sql .  " INNER JOIN transout on transout.tro_cust_id=client.cl_id  ";
            $sql = $sql . " Where tro_id LIKE 'TJ-$tp_id%'  OR lower(trim(cl_name)) LIKE '%$name%' ";
            $this->log_message("sql ; $sql ");
            $retval=array();
            $query = $this->db->query($sql,array($tp_id));
            //
            foreach($query->result() as $row) {
                //$outstanding=$payment['outstanding'];
                //$this->log_message($sql . " posisi lunas $spaid");                
                $data=$this->mp->tr_id_is_paid($row->id);
                if($data['paid']==1) {
                    $spaid="LUNAS";
                }else {
                    $spaid="BELUM LUNAS";
                }                 
		$retval[ ] = array(
			'name' => $row->name,
                        'cl_id' => $row->cl_id,			
			'id' => $row->id,
			'addr' => $row->addr,
                        'registdate' => $row->registdate,
                        'phone' => $row->phone,
                        'spaid' => $spaid,
                        'paid' => $data['paid'],
                        'sex' => $row->sex
		);      
            }
            return $retval;
        }  		

}
?>
