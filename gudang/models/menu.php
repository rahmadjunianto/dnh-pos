<?php

class Menu extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'menu';
		$this->_id = 'menu_name';
	}

	function get_top_menu($order_by = 'menu_level, menu_order') {
		$query = $this->db
			->from($this->_table_name)
			->where('(menu_order % 100)=0')
			->order_by($order_by)
			->get();
		return $query->result();
	}

	function get_child_menu() {
		$query = $this->db
			->from('menu')
			->where('(menu_order % 100)!=0')
			->order_by('menu_level, menu_order')
			->get();
		return $query->result();
	}

}