<?php
/*
 * Januari 2014
 */

class Model_payment extends Single_Model {
	function __construct() {
		parent::__construct();
		$this->_id = 'oid';
                $this->user_id = $this->acl->get_users_id();
	}
        function show_payments($trid) {
            $trid="" . $trid;
            $data = array();
            $sql = " SELECT payment_no,payment_trid as tr_id,payment_amount as amount ,date_name(to_date(payment_date::text,'YYYY-MM-DD')) as date  FROM payment INNER JOIN ";
            $sql = $sql . " transout ON transout.tro_id=payment.payment_trid ";
            $sql = $sql . " WHERE payment_trid=? ORDER BY payment_date";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            $retval = array();
            $data=array();
            $data=$this->tr_id_is_paid($trid);
            if($data['paid']==1) {
                $spaid="LUNAS";
            }else {
                $spaid="BELUM LUNAS";
            }
            $counter=0;
            foreach($query->result() as $row) {
                $counter=$counter+1;
		$retval[ ] = array(
			'pay_date' => $row->date,
                        'payment_no' => $row->payment_no,
			'amount' => $row->amount,
                        'paid' => $spaid,
                        'outstanding' => $data['outstanding'],
		);
            }
            if($counter==0) {
		$retval[ ] = array(
			'pay_date' => "",
                        'payment_no' => "",
			'amount' => 0,
                        'paid' => $spaid,
                        'outstanding' => $data['outstanding'],
		);
            }
            return $retval;
        }
        function get_payment_no() {
            $retval = "";
            $this->log_message("ENTERING GENERATE PAYMENT NO ");
            $sql= " SELECT get_next_value('seq_payment','P') as nextvalue";
            $query = $this->db->query($sql);
            foreach($query->result() as $row) {
                $retval = $row->nextvalue;
            }
            $this->log_message("PAYMENT NO $retval");
            return $retval;
        }
        function save_payment($trid,$amount,$type) {
            $this->log_message("ENTERIN FUNCTION SAVE PAYMENT");
            $no = $this->get_payment_no();
            $this->log_message("ENTERIN FUNCTION SAVE PAYMENT WIT no $no");
            $user_id = $this->user_id;
            $sql = " INSERT INTO payment(payment_trid,payment_amount,payment_type,payment_no,payment_userid)VALUES";
            $sql = $sql . "(?,?,?,?,?)";
            $this->log_message("SAVE PAYMENT $trid, no $no dengan amount $amount");
            $this->db->query($sql,array($trid,$amount,$type,$no,$user_id));
            return $no;
        }
        function save_payment_by_del($trid,$amount,$type,$code,$bruto,$disc,$net,$name) {
            $no = $this->get_payment_no();
            $user_id = $this->user_id;
            $sql = " INSERT INTO payment(payment_trid,payment_amount,payment_type,payment_no,payment_userid,";
            $sql = $sql . "payment_ref_code,payment_ref_bruto,payment_ref_disc,payment_ref_net,payment_ref_pxname)VALUES";
            //$sql = $sql . "payment_ref_code,payment_ref_bruto)VALUES(";
            $sql = $sql . "(?,?,?,?,?,?,?,?,?,?)";
            //$sql = $sql . "(?,?,?,?,?,?)";
            $this->log_message("SAVE PAYMENT $trid, no $no dengan amount $amount");
            $this->db->query($sql,array($trid,$amount,$type,$no,$user_id,$code,$bruto,$disc,$net,$name));
            //$this->db->query($sql,array($trid,$amount,$type,$no,$user_id,$code));
            return $no;
        }
        function tr_id_is_paid($trid) {
            $data = array();
            $sql = " SELECT tro_id,tro_cust_id,tro_date,tro_users_id,payment_trid as tr_id,payment_amount as amount ,payment_date as date  FROM payment INNER JOIN ";
            $sql = $sql . " transout ON transout.tro_id=payment.payment_trid ";
            //$sql = $sql . " INNER JOIN trans_detail ON trans_detail.trans_id=payment.payment_trid ";
            $sql = $sql . " WHERE payment_trid=? ";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $totalpaid = $totalpaid + $row->amount;
            }
            //
            $sql = " SELECT tro_ppn as ppn,tro_duedate,tro_extra_disc as extra_disc,tro_disc1,tro_disc2,tro_disc3,tro_netprice,tro_totalprice,tro_id,tro_cust_id,tro_date,tro_users_id,trans_price,trans_total,tro_totalprice,tro_netprice ";
            $sql = $sql . "  FROM transout INNER JOIN trans_detail  ";
            $sql = $sql . "  ON trans_detail.trans_id=transout.tro_id  ";
            $sql = $sql . "  WHERE trans_detail.trans_id=? ";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            $bruto = 0;
            $netto = 0;
            $netprice=0;
            $disc1=0;
            $disc2=0;
            $disc3=0;
            $ppn=0;
            $extra_disc=0;
            $duedate="";
            $total=0;
            $totalprice=0;
            foreach($query->result() as $row) {
                $bruto = $bruto + $row->trans_price;
                $netto = $netto + $row->trans_total;
                //$totalpaid = $totalpaid + $row->amount;
                $bruto = $row->tro_totalprice;
                $netto = $row->tro_netprice;
                $total = $row->trans_total;
                $totalprice = $totalprice + $total;
                //
                $disc1=$row->tro_disc1;
                $disc2=$row->tro_disc2;
                $disc3=$row->tro_disc3;
                //
                $duedate=$row->tro_duedate;
                $ppn=$row->ppn;
                $extra_disc=$row->extra_disc;
            }
            //$totalprice=$netto;
            //
            $netprice=$totalprice-(($disc1/100)*$totalprice);
            $netdisc1=(($disc1/100)*$totalprice);
            $netdisc2=(($disc2/100)*$netprice);
            $netprice=$netprice-(($disc2/100)*$netprice);
            $netdisc3=(($disc3/100)*$netprice);
            $netprice=$netprice-(($disc3/100)*$netprice);
            //
            $dppn=0;
            $netprice=$netprice-$extra_disc;
            if($ppn==1) {
                $dppn=(10/100)*$netprice;
            }
            $netprice=$netprice + $dppn;
            //$outstanding=($netprice)-($amount);

            //$disc = $bruto - $netto;
            //$outstanding = $netto - $totalpaid;
            $outstanding = $netprice  - $totalpaid;
            $this->log_message("OUTSTANDING $outstanding");
            $data['bruto']= $netto;
            $data['netto']= $netprice;
            //$data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
            /*
            $outstanding=0;
            $data = array();
            $sql = " SELECT payment_trid as tr_id,payment_amount as amount ,payment_date as date FROM payment  ";
            $sql = $sql . " ";
            $sql = $sql . " WHERE payment_trid=?";
            $this->log_message("payment info $sql dgn lab $trid ");
            $query=  $this->db->query($sql,array($trid));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $totalpaid = $totalpaid + $row->amount;
            }
            //debugger;
            $totalprice=0;
            $sql = " SELECT trans_total  FROM trans_detail ";
            $sql = $sql . " WHERE trans_id=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            foreach($query->result() as $row) {
                $totalprice = $totalprice + $row->trans_total;
            }

            $outstanding = $totalprice - $totalpaid;
            $paid=0;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid']=$paid;
            $data['outstanding']=$outstanding;
             *
             */
            return $data;
        }
        function get_payment_amount_per_no($trid,$no) {
            $no = strtolower($no);
            $data = array();
            $sql = " SELECT payment_trid as tr_id,payment_amount as amount ,payment_date as date  FROM payment INNER JOIN ";
            $sql = $sql . " transout ON transout.tro_id=payment.payment_trid ";
            $sql = $sql . " WHERE payment_trid=? AND lower(payment_no)=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid,$no));
            $totalpaid=0;
            foreach($query->result() as $row) {
                $totalpaid = $totalpaid + $row->amount;
            }
            //
            return $totalpaid;
        }
        function get_netto($trid) {
            $netto=0;
            $sql=" SELECT SUM(rd_net) as netto FROM result_detail WHERE rd_frontend_tr_id=? and length(rd_pxcode)=5";
            $q = $this->db->query($sql,array($trid));
            foreach($q->result() as $row) {
                $netto =$row->netto;
            }
            return $netto;
        }
        function get_payment_info($trid) {
            $data = array();
            $sql = " SELECT tro_id,tro_cust_id,tro_date,tro_users_id,payment_trid as tr_id,payment_amount as amount ,payment_date as date  FROM payment INNER JOIN ";
            $sql = $sql . " transout ON transout.tro_id=payment.payment_trid ";
            //$sql = $sql . " INNER JOIN trans_detail ON trans_detail.trans_id=payment.payment_trid ";
            $sql = $sql . " WHERE payment_trid=? ";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $totalpaid = $totalpaid + $row->amount;
            }
            //
            $sql = " SELECT tro_ppn as ppn,tro_duedate,tro_extra_disc as extra_disc,tro_disc1,tro_disc2,tro_disc3,tro_netprice,tro_totalprice,tro_id,tro_cust_id,tro_date,tro_users_id,trans_price,trans_total,tro_totalprice,tro_netprice ";
            $sql = $sql . "  FROM transout INNER JOIN trans_detail  ";
            $sql = $sql . "  ON trans_detail.trans_id=transout.tro_id  ";
            $sql = $sql . "  WHERE trans_detail.trans_id=? ";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            $bruto = 0;
            $netto = 0;
            $netprice=0;
            $disc1=0;
            $disc2=0;
            $disc3=0;
            $ppn=0;
            $extra_disc=0;
            $duedate="";
            $total=0;
            $totalprice=0;
            foreach($query->result() as $row) {
                $bruto = $bruto + $row->trans_price;
                $netto = $netto + $row->trans_total;
                //$totalpaid = $totalpaid + $row->amount;
                $bruto = $row->tro_totalprice;
                $netto = $row->tro_netprice;
                $total = $row->trans_total;
                $totalprice = $totalprice + $total;
                //
                $disc1=$row->tro_disc1;
                $disc2=$row->tro_disc2;
                $disc3=$row->tro_disc3;
                //
                $duedate=$row->tro_duedate;
                $ppn=$row->ppn;
                $extra_disc=$row->extra_disc;
            }
            //$totalprice=$netto;
            //
            $netprice=$totalprice-(($disc1/100)*$totalprice);
            $netdisc1=(($disc1/100)*$totalprice);
            $netdisc2=(($disc2/100)*$netprice);
            $netprice=$netprice-(($disc2/100)*$netprice);
            $netdisc3=(($disc3/100)*$netprice);
            $netprice=$netprice-(($disc3/100)*$netprice);
            //
            $dppn=0;
            $netprice=$netprice-$extra_disc;
            if($ppn==1) {
                $dppn=(10/100)*$netprice;
            }
            $netprice=$netprice + $dppn;
            //$outstanding=($netprice)-($amount);

            //$disc = $bruto - $netto;
            //$outstanding = $netto - $totalpaid;
            $outstanding = $netprice  - $totalpaid;
            $this->log_message("OUTSTANDING $outstanding");
            $data['bruto']= $totalprice;
            $data['netto']= $netprice;
            //$data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
        }
        function cash_in_per_user($filterdate,$filterdate2) {
            //
            $user_id= $this->user_id;
            $sql = " SELECT payment.*,(to_date(payment_date,'YYYY-MM-DD') ||' ' || to_char(payment_date,'HH24:MM')) as datepayment,frontend_tr_id as tr_id,patient_name FROM payment INNER JOIN ";
            $sql = $sql . " frontend ON frontend.frontend_tr_id=payment.payment_trid ";
            $sql = $sql . " INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql . " INNER JOIN users ON payment.payment_userid=users.users_id ";
            $sql = $sql . " WHERE to_date(payment_date,'YYYY-MM-DD')>=? AND payment_userid=? ";
            $sql = $sql . " AND to_date(payment_date,'YYYY-MM-DD')<=? ORDER BY payment_userid,frontend_tr_id ";
            $this->log_message("SQL CASH $sql   date $filterdate dengan user $user_id");
            $query = $this->db->query($sql,array($filterdate,$user_id,$filterdate2));
            $counter=1;
            $total =0;
            foreach($query->result() as $row) {
		$retval[ ] = array(
			'counter' => $counter,
                        'tr_id' => $row->tr_id,
                        'name' => $row->patient_name,
                        'payment_date' => $row->datepayment,
                        'payment_no' => $row->payment_no,
                        'amount' => $row->payment_amount
                        );
                $total = $total + $row->payment_amount;
                $counter = $counter+1;
            }
            $retval[ ] = array(
                    'counter' => "",
                    'tr_id' => " ",
                    'name' => " ",
                    'payment_date' => "",
                    'payment_no' => " TOTAL ",
                    'amount' => $total,
                    );
            //$this->log_message("affected $jumlah");
            $data['curPage']=1;
            $data['totalRecords']= $counter;
            $data['data']=$retval;
            return $data;
        }
        function cash_in($filterdate,$filterdate2) {
            $CI =& get_instance();
            $CI->load->model('model_users');
            $this->modusers = $CI->model_users;

            //$this->modusers = $this->load->model('model_users','modusers');
            $data=array();
            //$filterdate = "2014-09-10";
            //
            $sql = " SELECT payment.*,(to_date(payment_date::text,'YYYY-MM-DD') ||' ' || to_char(payment_date,'HH24:MM')) as datepayment ,tro_id as tr_id,cl_name as name FROM payment INNER JOIN ";
            $sql = $sql . " transout ON transout.tro_id=payment.payment_trid ";
            $sql = $sql . " INNER JOIN client ON client.cl_id=transout.tro_cust_id ";
            $sql = $sql . " INNER JOIN users ON payment.payment_userid=users.users_id ";
            $sql = $sql . " WHERE to_date(payment_date::text,'YYYY-MM-DD')>=? ";
            $sql = $sql . " AND to_date(payment_date::text,'YYYY-MM-DD')<=? ORDER BY payment_userid,tro_id ";
            $this->log_message("SQL CASH $sql   date $filterdate dan $filterdate2");
            $query = $this->db->query($sql,array($filterdate,$filterdate2));
            $counter=1;
            $total =0;
            $totaluser=0;
            $user_id="";
            $last_user_id="";
            $retval = array();
            foreach($query->result() as $row) {
                //
                $current_user_id = $row->payment_userid;
                //$name = $data=$this->modusers->get_users_name($last_user_id);
                if(!($current_user_id==$last_user_id) AND $counter>1) {
                    $name = $this->modusers->get_users_name($last_user_id);
                    $this->log_message("cash in NAMA $name");

                    $retval[ ] = array(
                            'counter' => "",
                            'tr_id' => "",
                            'name' => "",
                            'payment_date' => "TOTAL USER $name",
                            'payment_no' => "",
                            'amount' => $totaluser
                            );

                    $totaluser=0;
                }
		$retval[ ] = array(
			'counter' => $counter,
                        'tr_id' => $row->tr_id,
                        'name' => $row->name,
                        'payment_date' => $row->datepayment,
                        'payment_no' => $row->payment_no,
                        'amount' => $row->payment_amount
                        );
                $totaluser = $totaluser + $row->payment_amount;
                $total = $total + $row->payment_amount;
                $last_user_id = $row->payment_userid;
                $counter = $counter+1;

            }

            $name =$this->modusers->get_users_name($last_user_id);
            //break
            $retval[ ] = array(
                    'counter' => "",
                    'tr_id' => "",
                    'name' => "",
                    'payment_date' => "TOTAL USER $name",
                    'payment_no' => "",
                    'amount' => $totaluser
                    );
            $retval[ ] = array(
                    'counter' => "",
                    'tr_id' => " ",
                    'name' => " ",
                    'payment_date' => "TOTAL",
                    'payment_no' => " ",
                    'amount' => $total,
                    );
            //$this->log_message("affected $jumlah");

            $data['curPage']=1;
            $data['totalRecords']= $counter;
            $data['data']=$retval;
            return $data;
        }
        //outstanding
        function outstanding($filterdate,$filterdate2) {
            //$this->load->model('model_payment','mp');
            $retval=array();
            $start="2014-09-01";
            $finish="2014-12-31";
            $sql =" SELECT tro_id,cl_name as name FROM transout inner join client on transout.tro_cust_id=client.cl_id WHERE to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=?  ORDER BY tro_id ";
            $global_q = $this->db->query($sql,array($filterdate,$filterdate2));
            $total=0;
            $counter=0;
            $this->log_message($sql);
            foreach($global_q->result() as $row_global) {
                $this->log_message($row_global->tro_id);
                $trans_id=$row_global->tro_id;
                $arr_paid = $this->tr_id_is_paid($trans_id);
                //$row_global->trans_id);
                $paid=$arr_paid['paid'];
                $outstanding = $arr_paid['outstanding'];
                if($paid==0) {
                        $counter=$counter+1;
                        $name = $row_global->name;
                        $retval[ ] = array(
			'counter' => $counter,
                        'trans_id' => $row_global->tro_id,
                        'name' => $name,
                        'outstanding' => $outstanding
                        );
                        $total=$total+$outstanding;
                }
            }
            $retval[ ] = array(
            'counter' => "",
            'trans_id' => "",
            'name' => "TOTAL",
            'outstanding' => $total
            );
            $data=array();
            $data['curPage']=1;
            $data['totalRecords']= 1000;
            $data['data']=$retval;
            //$this->log_message("DATA : $data");
            //echo json_encode($data);
            return $data;
        }
}
?>
