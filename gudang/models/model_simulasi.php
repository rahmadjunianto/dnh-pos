<?php
/*
 * Januari 2014
 */

class Model_simulasi extends Single_Model {

	function __construct() {
		parent::__construct();
		$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                                $this->load->library("class_public");
	}
                function get_simulasi_standar() {                        
                        $retval = 0;
//                        $simulasistandar = $this->class_public->getSettingGaji("simulasi standar");
//                        $this->log_message("simulasi standar: " . $simulasistandar);
//                        $retval = $simulasistandar;
                        return $retval;
                }
                
	function get_data_simulasi($idcabang) {//dipake
		$retval = array();
                                $sql = "SELECT * FROM sdm where cab_id=?";
                                $query = $this->db->query($sql, array($idcabang));
		foreach ($query->result() as $row) {
			$retval[ ] = array(
				'idkaryawan' => $row->sdm_id,
				'nama' => $row->sdm_nama
			);
		}
		return $retval;                                
	}
                //save std simulasi
                function save_simulasi_cabang($idcabang,$spv1,$spv2,$spv3,$wakacab,$kacab ) {                                        
                    //
                    $spv1 = $this->class_public-> AvoidNullNum($spv1);
                    $spv2 = $this->class_public->AvoidNullNum($spv2);
                    $spv3 = $this->class_public->AvoidNullNum($spv3);
                    $wakacab = $this->class_public->AvoidNullNum($wakacab);
                    $kacab = $this->class_public->AvoidNullNum($kacab);
                    $strSQL = " UPDATE cabang SET ";
                    $strSQL = $strSQL . " simulasi_spv1= ? , ";
                    $strSQL = $strSQL . " simulasi_spv2= ? ,";
                    $strSQL = $strSQL . " simulasi_spv3= ? ,";
                    $strSQL = $strSQL .  " simulasi_wakacab= ? ,";
                    $strSQL = $strSQL .  " simulasi_kacab= ?  ";
                    $strSQL = $strSQL  . " WHERE idcabang= ? ";
                    $query = $this->db->query($strSQL, array($spv1,$spv2,$spv3,$wakacab,$kacab,$idcabang));                    
                    return 0;
                }
                //end of save std simulasi
}

?>
