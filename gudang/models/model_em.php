<?php
/*
 * Januari 2014
 */

class Model_em extends Single_Model {
	function __construct() {
		parent::__construct();
		//$this->_table_name = 'mastergapok';
		$this->_id = 'oid';
                $this->load->library("class_public");
	}
        //
        function get_em_type($type=1)  {
            //
            return 1;
        }
        function save_result_em($pxcode,$lab_id,$result,$kesan) {
            $pxcode = trim(strtolower($pxcode));

            $sql = " Update em  SET em_result=?,em_kesan=? WHERE lower(trim(em_pxcode))=? AND em_labid=?";
            $this->log_message("SQL EM : $sql lab ID $lab_id code $pxcode result $result");
            $this->db->query($sql,array($result,$kesan,$pxcode,$lab_id));
            return 1;
        }
        function get_result_em($pxcode,$lab_id) {
            $pxcode = trim(strtolower($pxcode));            
            $res ="";
            $sql = " SELECT * FROM em  WHERE lower(trim(em_pxcode))=? AND em_labid=?";
            $this->log_message("SQL EM : $sql lab ID $lab_id code $pxcode ");
            $query = $this->db->query($sql,array($pxcode,$lab_id));
            $data=array();
            foreach($query->result() as $row ) {                
                $data['kesan'] =  $row->em_kesan;
                $data['result'] =   $row->em_result;
            }
            //$this->log_message("RESULT EM $res");
            return $data;
        }        
                
}
?>
