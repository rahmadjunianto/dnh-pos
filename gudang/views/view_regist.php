<?php foreach ($this->html_headers->styles as $style): ?>
        <link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
<?php endforeach; ?>
<?php foreach ($this->html_headers->scripts as $script): ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php endforeach; ?>        
        <script type="text/javascript">
        <?php         
        echo 'var table_title = "' . $table_title  . '";';                     
        //echo 'var table_title = "PENJUALAN";';                     
        echo 'var table_id = "' . $table_id  . '";';
        echo 'var filter_htmlid = "filter_' . $grid_name  . '";';        
        echo 'var filter_select_htmlid = "select_col_' . $grid_name  . '";';
        echo 'var filter_panel_select_htmlid = "select_panel_col_' . $grid_name  . '";';
        ?>            
            var select_panel_col_html_def="<?php
            $str_select="<select style='width:200px;margin-left:10px;padding-right:100px;margin-right:10px;' id='select_panel_col_" . $grid_name  . "'>";
            $str_select = $str_select . "<option  selected value='0'>&nbsp;</option>";
            for($x=0;$x<count($panels);$x++)
            {
                $panel_id = $panels[$x]->mp_id;
                $panel_name = $panels[$x]->mp_name;                                
                $str_select = $str_select . "<option value=$panel_id>$panel_name</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";                    
            var select_col_html_def="<?php
            $str_select="<select id='select_col_" . $grid_name  . "'>";
            for($x=0;$x<count($table_detail);$x++)
            {
                $table_desc = $table_detail[$x]->tfd_coldesc;
                $table_order = $table_detail[$x]->tfd_order;
                if(trim($table_detail[$x]->tfd_colname)==trim($default_sort_col)) {
                    $str_select = $str_select . "<option  selected value=$table_order >$table_desc</option>";
                }else {
                    $str_select = $str_select . "<option value=$table_order>$table_desc</option>";
                }                
                //$str_select = $str_select . "<option value=$table_order>$table_desc</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";        
	var ht = $(".span-grid").innerHeight();		
        var wd = $(".span-grid").innerWidth();            
        var docwidth = $(document).width();
        //alert(wd);
        wd = 50/100 * (docwidth);
        
        //alert(wd);
        //alert(wd);
        var  newObj = { width: wd, height: 100, numberCell: true, minWidth: 10,
        title: table_title,
        filter_id : filter_htmlid,
        filter_select_htmlid : filter_select_htmlid,
        new_key_value:'',
        tableID : table_id,
        bottomVisible:false,
        resizable: true, columnBorders: true,
        selectionModel: { type: 'cell', mode: 'block' },
        editModel: { clicksToEdit:2, saveKey: 13 },
        hoverMode: 'cell',
    };  
            
        newObj.colModel=[];
        newObj.colModel[0] = { dataIndx: 0, editable: false, sortable: false, title: "", width: 30, align: "center", resizable: false, render: function (ui) {
            var rowData = ui.rowData, dataIndx = ui.dataIndx;
            var val = rowData[dataIndx];
            str = "";
            if (val) {
                str = "checked='checked'";
            }
            return "<input type='checkbox' " + str + " />";
        }, className: "checkboxColumn"
        }            
        </script>                    
<?php        
        echo '<script type="text/javascript">';
        echo 'var base_url = "' .   base_url() . '";';
        echo 'var site_url = "' . site_url() . '";';
        echo 'var default_sort_col = "' . $default_sort_col  . '";'; //< ?php echo $default_sort_col;
        echo 'var geturladdr = "' . $geturladdr  . '";'; //<?php echo $geturladdr ;
        //newrowurl
        echo 'var newrowurl= "' . $newrowurl  . '";'; //<?php echo $geturladdr ;
        echo 'var updateurl = "' . $updateurl  . '";'; // <?php echo $updateurl ;
        echo 'var drowurl = "' . $drowurl  . '";'; // <?php echo $updateurl ;
        echo 'var grid_name = "'   . $grid_name .  '";'; //#<?php echo $grid_name;
        echo 'var grid_name2 = "#'  . $grid_name . '";'; //<?php echo $grid_name;
        echo 'var keycolumnindex=' . $keycolumn_index  . ';'; // <?php echo $keycolumn_index
        echo '$grid={};';
        echo 'var sortcols =';
            $prefix = "[";
            $suffix = "]";
            $colcnt = $prefix;
            for($x=0;$x<count($table_detail);$x++)
            {
                if($x<count($table_detail)-1) {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "',";
                }else {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "'" . $suffix . ";";
                }
            }            
            echo $colcnt;        
            //var newObj = jQuery.noConflict();        
            for($x=0;$x<count($table_detail);$x++)
            {
                $tmp = $table_detail[$x]->tfd_editable;
                if($tmp==1) {
                    $editable="true";
                }else {
                    $editable="false";
                }
                echo 'newObj' . '.colModel['. ($x+1) . '] = { title: "' . $table_detail[$x]->tfd_coldesc . '",width:' . $table_detail[$x]->tfd_colwidth . ' ,dataIndx: "' . $table_detail[$x]->tfd_colname . '",editable:' . $editable  . '};';
            }            
        //echo 'newObj = newObj'  . ';';
        echo '</script>';        
     ?>  
    <div id="dialog-confirm"></div>        
    <div class="frameintab-pendaftaran" style="padding:1px 5px 20px 35px;"> 
        <!--
        <div style="float:left;width:150px;">Nama Marketing&nbsp;&nbsp;        
        </div>
        
        <div style="float:left;"><input type="text" id="mktsearch"/></div>
        <div style="float:left;" id="mktname">Marketing</div>
        <div style="clear:both;height:0px;">&nbsp;</div>
        <div>
        <div style="float:left;width:150px;">Skema Harga&nbsp;&nbsp;        
        </div>
        <div style="float:left;"><input type="text" id="scheme_id" value="000"/></div>
        <div style="float:left;" id="scheme_name">Harga Umum</div>
        <div style="clear:both;height:0px;">&nbsp;</div>
        -->
        <div>            
        <!--
        <div style=" background-color:#fed22f;height:50px;padding:0px;width:100px;">
            <div style="float:left">KIRI..</div>
            <div style="float:right">KANAN..</div>
        </div>        
        -->
            <div class="grid regist">
                    <div class="span-grid">
                        <div id="<?php echo $grid_name; ?>">                            
                        </div>
                    </div>                    
                    <!--bukan row, maka clear both, supaya div berikut muncul di bawahnya-->
                    <div style="height:0px;clear:both">&nbsp;</div>
                    <div >
                        <div class="panel" data-role="panel">
                            <div class="panel-header bg-darkRed fg-white" id="regist_sum_panel_header">
                                Data Pelanggan/Customer
                            </div>
                            <div class="panel-content">
                                <div id="panel_1" style="float:left;border:0px solid;width:850px">                                    
                                    <div style="float:left;">
                                        <table border="0">
                                            <tr> <td>ID Pelanggan</td><td><input type="text" id="regist_id_cust" value="<?php  echo($kode_cust_umum); ?>"</td>
                                                 <td width="180" colspan="2">&nbsp;</td>
                                                 <td>Bruto</td><td>:</td><td><label id="regist_bruto">B</label></td>
                                            </tr>
                                            <tr> 
                                                <td>Nama</td><td><input type="text" id="regist_name" value="UMUM">
                                                <td width="180" colspan="2">&nbsp;</td>
                                                <td>Disc(%)</td><td>:</td><td ><input type="text" id="regist_disc" value="0"></td>
                                            </tr>
                                            <tr> <td>Petugas Pengirim</td><td colspan=""><input type="text" id="regist_drivers">
                                                <td width="180" colspan="2">&nbsp;</td>
                                                <td>Nett</td><td>:</td><td ><input type="text" id="regist_nett" value="0"></td>                                        
                                            </tr>
                                        </table>                                
                                    </div>
                                    <div class="regist_two_col_container" style="float:left;">
                                        <div id="regist_payment" style="float:left;">
                                            <table border="0">
                                                <tr>
                                                    <td >Jenis Bayar</td>
                                                    <td>
                                                        <select id="regist_payment_type">
                                                            <option selected value="1">Tunai</option>
                                                            <option value="2">Tempo</option>
                                                            <option value="3">Kartu Debit</option>
                                                            <option value="4">Kartu Kredit</option>
                                                        </select>                                                
                                                    </td>
                                                    <td><div class="duedateclass">Jatuh Tempo(Hari)</div></td>
                                                    <td>
                                                        <div class="duedateclass"><input type="text" id="duedate"></div>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <!---->
                                                </tr>
                                                <tr>
                                                    <td>Bayar Sekarang</td>
                                                    <td><input type="text" class="payment_textbox" id="regist_payment_amount"></td>
                                                    <td>&nbsp;</td>
                                                    <td>Uang Pelanggan</td>
                                                    <td><input type="text" class="payment_textbox" id="regist_payment_cust_cash"></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>Kurang Bayar</td>
                                                    <td><input type="text" class="payment_textbox" id="regist_payment_outstanding"></td>
                                                    <td>&nbsp;</td>
                                                    <td>Kembalian</td>
                                                    <td><input type="text" class="payment_textbox" id="regist_payment_cust_return"></td>
                                                    <td>
                                                        <button id="save_trans">Simpan</button>
                                                        <input type="hidden" id="trans_id"/> <input type="hidden" id="payment_id"/>
                                                        <button id="print_trans">Cetak</button>
                                                        <button id="print_trans2">Cetak #</button>                                                        
                                                    </td>
                                                </tr>                                                
                                            </table>                                        
                                        </div>                                                   
                                    </div>                                
                                </div>
                                <div id="panel_2" style="float:left;border:0px solid">
                                    <div style="float:right;font-size:60px">
                                        000.00,-
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

            </div>
    </div>
        <div id="fappletx" ><?php echo($applet); ?></div>        
<script type="text/javascript" src="/pos/asset2/pos/js/regist.js"></script>
<script type="text/javascript" src="/pos/asset2/pos/js/regist-ui.js"></script>