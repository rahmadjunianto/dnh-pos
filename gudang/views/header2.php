<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
		var site_url = "<?php echo site_url(); ?>";
		var source_page = "<?php echo $source_page; ?>";
    </script>
	<?php foreach ($this->html_headers->scripts as $script): ?>
		<script type="text/javascript" src="<?php echo $script; ?>"></script>
	<?php endforeach; ?>
	<?php foreach ($this->html_headers->styles as $style): ?>
		<link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
	<?php endforeach; ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
		<?php echo (isset($this->html_headers->title)) ? $this->html_headers->title : "Parahita System"; ?>
    </title>
</head>
<body>
 <div id="maincontainer">
 </div>

</body>

