<?php foreach ($this->html_headers->styles as $style): ?>
        <link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
<?php endforeach; ?>
<?php foreach ($this->html_headers->scripts as $script): ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>

<?php endforeach; ?>        
<script type="text/javascript">
<?php
    echo "var em_type=$table_id;";
    echo "var keycolumn_index=$keycolumn_index;";
    echo "var keycolumn='$keycolumn';";
?>
</script>        
    <div class="frameintab-pendaftaran"> 
            <div class="grid_regist">
                <div class="row">                    
                    <div class="span-grid-left">                        
                        <div class="panel" data-role="panel">
                            <div class="panel-header bg-darkRed fg-white">
                                Daftar Pasien
                            </div>
                            <div class="panel-content">  
                                <div class="left_search_table">
                                    <div class="left_search_title">
                                        <div class="left_search_heading">&nbsp;</div>
                                        <div class="left_search_row">
                                            <div class="left_search_cell1">
                                                Tanggal
                                            </div>
                                            <div class="left_search_cell2">
                                                <div class="input-control text" data-role="datepicker" data-week-start="1">
                                                    <input id="result_filter_date" type="text" readonly="readonly">
                                                    <button class="btn-date"></button>
                                                    <div class="calendar calendar-dropdown" data-start-mode="month" style="position: absolute; max-width: 260px; z-index: 1000; top: 100%; left: 0px; display: none;"><table class="bordered"><tbody><tr class="calendar-header"><td class="text-center"><a class="btn-previous-year" href="#"><i class="icon-previous"></i></a></td><td class="text-center"><a class="btn-previous-month" href="#"><i class="icon-arrow-left-4"></i></a></td><td colspan="3" class="text-center"><a class="btn-select-month" href="#">August 2014</a></td><td class="text-center"><a class="btn-next-month" href="#"><i class="icon-arrow-right-4"></i></a></td><td class="text-center"><a class="btn-next-year" href="#"><i class="icon-next"></i></a></td></tr><tr class="calendar-subheader"><td class="text-center day-of-week">Mo</td><td class="text-center day-of-week">Tu</td><td class="text-center day-of-week">We</td><td class="text-center day-of-week">Th</td><td class="text-center day-of-week">Fr</td><td class="text-center day-of-week">Sa</td><td class="text-center day-of-week">Su</td></tr><tr><td class="empty"><small class="other-day"></small></td><td class="empty"><small class="other-day"></small></td><td class="empty"><small class="other-day"></small></td><td class="empty"><small class="other-day"></small></td><td class="text-center day"><a href="#">1</a></td><td class="text-center day"><a href="#">2</a></td><td class="text-center day"><a href="#">3</a></td></tr><tr><td class="text-center day"><a href="#">4</a></td><td class="text-center day"><a href="#">5</a></td><td class="text-center day"><a href="#">6</a></td><td class="text-center day"><a href="#">7</a></td><td class="text-center day"><a href="#">8</a></td><td class="text-center day"><a href="#">9</a></td><td class="text-center day"><a href="#">10</a></td></tr><tr><td class="text-center day"><a href="#">11</a></td><td class="text-center day today"><a href="#" class="selected">12</a></td><td class="text-center day"><a href="#">13</a></td><td class="text-center day"><a href="#">14</a></td><td class="text-center day"><a href="#">15</a></td><td class="text-center day"><a href="#">16</a></td><td class="text-center day"><a href="#">17</a></td></tr><tr><td class="text-center day"><a href="#">18</a></td><td class="text-center day"><a href="#">19</a></td><td class="text-center day"><a href="#">20</a></td><td class="text-center day"><a href="#">21</a></td><td class="text-center day"><a href="#">22</a></td><td class="text-center day"><a href="#">23</a></td><td class="text-center day"><a href="#">24</a></td></tr><tr><td class="text-center day"><a href="#">25</a></td><td class="text-center day"><a href="#">26</a></td><td class="text-center day"><a href="#">27</a></td><td class="text-center day"><a href="#">28</a></td><td class="text-center day"><a href="#">29</a></td><td class="text-center day"><a href="#">30</a></td><td class="text-center day"><a href="#">31</a></td></tr></tbody></table></div>
                                                </div>                                                       
                                            </div>
                                        </div>
                                        <div class="left_search_row">
                                            <div class="left_search_cell1">
                                                 No Lab
                                            </div>
                                            <div class="left_search_cell2">
                                               <input type="text"/>
                                            </div>
                                        </div>                                        
                                        <div class="left_search_cell1">
                                            &nbsp;
                                        </div>                                        
                                        <div class="left_search_cell2">
                                            <button id="result_filter" class="button success">Cari</button>
                                        </div>                                                                                
                                    </div>
                                </div>                                                            


                                <div>&nbsp;</div>                    
                                <div id="container_left_search">
                                    <table id="result_table" class="table striped bordered hovered">
                                    <thead>
                                    <tr>
                                        <th class="text-left" style="width:5px !important">No</th>
                                        <th class="text-left">No Lab</th>
                                        <th class="text-left">Nama</th>
                                        <th class="text-left">Kode PX</th>
                                        <th class="text-left">Nama PX</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="selected"><td>News</td><td class="right">0:00:01</td><td class="right">0,1 Mb</td></tr>
                                    <tr class="selected"><td>Music</td><td class="right">0:00:01</td><td class="right">0,1 Mb</td></tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="span-grid-right-result">
                        <div class="right-container-em">
                            <div class='result_header_col1' >Nama</div><div class='result_header_col2'>:</div><div class='result_header_col3'><label id='result_header_name'>Nama Orang</label></div><div class='result_header_col4'>No Lab/No MR</div><div class='result_header_col5'>:</div><div class='result_header_col6'><label id='result_header_nomr'>Nomer MR</label></div><div style="clear:left">&nbsp;</div>
                            <div class='result_header_col1'>Alamat</div><div class='result_header_col2'>:</div><div class='result_header_col3'><label id='result_header_addr'>Alamat Orang</label></div><div class='result_header_col4'>Pengirim</div><div class='result_header_col5'>:</div><div class='result_header_col6'><label id='result_header_doctname'>Dokter Pengirim</label></div><div style="clear:left">&nbsp;</div>
                            <div class='result_header_col1'>Kode PX</div><div class='result_header_col2'>:</div><div class='result_header_col3'><label id='result_header_pxcode'>Alamat Orang</label></div><div class='result_header_col4'>Nama PX</div><div class='result_header_col5'>:</div><div class='result_header_col6'><label id='result_header_pxname'>Dokter Pengirim</label></div><div style="clear:left">&nbsp;</div>
                            <div class='result_header_col1'><label id="result_em_lab_id"></label></div>
                            <div class='result_header_col1'><a href='' id='result_em_print_per_lab_id'>Print</a></div><div class='result_header_col2'>&nbsp;</div><div class='result_header_col3'>&nbsp;</div><div class='result_header_col4'>&nbsp;</div><div class='result_header_col5'>&nbsp;</div><div class='result_header_col6'>&nbsp;</div><p>&nbsp;</p>
                        </div>    
                            <div class="result_em">                        
                                 HASIL
                                <div class='result_em_container' style='border:solid 1px;min-height:300px'>
                                    <textarea class="result_em_text" id="result_em">
                                    </textarea>
                                </div>           
                                 KESAN
                                <div style='border:solid 1px;min-height:40px;width:450px;'>
                                    <textarea class='result_em_kesan' id="result_em_kesan">
                                    </textarea>
                                </div>
                                 <div style='height:2px;'>&nbsp;</div>
                                <button id="result_em_save">Save</button>
                                <button id="result_em_print" class="button success">Print</button>                             
                            </div>                        
                    </div>                    
                    
                </div>
            </div>
    </div>        
<script type="text/javascript" src="/pos/asset2/pos/js/em.js"></script>
