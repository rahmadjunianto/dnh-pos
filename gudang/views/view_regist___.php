       
    <div class="frameintab-pendaftaran" style="padding-top:1px;">     
            <div class="grid regist">
                <div class="row">                    
                        <div class="panel" data-role="panel" style="height:auto;width:800px !important;">
                            <div class="panel-header bg-darkRed fg-white" id="regist_sum_panel_header">
                                Pendaftaran/Biodata Jamaah
                            </div> 
                            <div class="panel-content">
                                <div class="regist_table">
                                    <div class="regist_row">
                                        <div class="regist_cell1">ID Jamaah</div>
                                        <div class="regist_cell3"><input type="text" id="regist_id_jamaah"></div>
                                        <div class="regist_clearer">&nbsp;</div>                                    
                                        <div class="regist_cell1">Nama</div>
                                        <div class="regist_cell2">
                                                <select id="regist_title">
                                                <option selected value="Mr.">Mr.</option>
                                                <option value="Mrs.">Mrs.</option>
                                                <option value="Mrs.">Ms.</option>
                                                <option value="Tn.">Tn.</option>
                                                <option value="Ny.">Ny.</option>
                                                <option value="Sdra.">Sdra.</option>
                                                <option value="Sdri.">Sdri.</option>
                                            </select>
                                        </div>    
                                        <div class="regist_cell3">
                                            <input type="text" id="regist_name">
                                        </div>
                                        <div class="regist_clearer">&nbsp;</div>
                                    </div>                                    
                                    <div class="regist_cell1">Jenis Kelamin</div>
                                    <div class="regist_cell3">  <select id="regist_sex">
                                                <option selected value="L">L</option>
                                                <option value="P">P</option>
                                            </select>    
                                    </div>      
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">Tempat Lahir</div>
                                    <div class="regist_cell2"><input type="text" id="regist_birth_place"></input></div>
                                    <div class="regist_clearer">&nbsp;</div>                                    
                                    <div class="regist_cell1">Tgl Lahir</div>
                                    <div class="regist_cell2"><input type="text" id="regist_birth_date"></input></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">Alamat</div><div class="regist_cell2"><input type="text" style="width:300px;" id="regist_addr"></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">Kota</div><div class="regist_cell2"><input type="text" id="regist_city"></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">Telp</div><div class="regist_cell2"><input type="text" id="regist_phone"></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">No HP</div><div class="regist_cell2"><input type="text" id="regist_phonecell"></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">No Passport</div><div class="regist_cell2"><input type="text" id="regist_passport_no"></div>
                                    <div class="regist_clearer">&nbsp;</div>                                    
                                    <div class="regist_cell1">Issuing Office</div><div class="regist_cell2"><input style="width:300px;" type="text" id="regist_issuing_office"></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">Issue Date</div>
                                    <div class="regist_cell2"><input type="text" id="regist_issue_date"></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div style="float:left;">Hubungan Mahram</div>
                                        <div style="float:left;margin-left:10px;margin-top:2px;">
                                            <input style="width:120px;" type="text" id="regist_mahram_rel" value="-">
                                        </div>
                                        <!--
                                        <div style="width:20px;float:left">
                                            Ke
                                        </div>
                                        <div style="width:20px;float:left">
                                            <input style="width:40px;" type="text" id="regist_mahram_rel_ke" value="-">
                                        </div>
                                        -->
                                        <div style="margin-left:30px;width:20px;float:left;">
                                            Dari
                                        </div>                                    
                                        <div style="float:left;margin-left:10px;margin-top:2px;">
                                            <input style="width:150px;" type="text" id="regist_mahram" value="-">
                                        </div>                                    
                                        <div style="float:left;">
                                            <label style="height:10px" id="regist_mahram_name">Nama Mahram</label>
                                        </div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div style="float:left;">Dokumen Pendukung Mahram</div>
                                        <div class="regist_cell2" style="margin-left:20px;"><input type="text" id="regist_dokumen_pendukung"></div>
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">Paket Tour</div>
                                    <div style="float:left">
                                        <select id="paket_type">
                                            <option selected value="quard">QUARD</option>
                                            <option  value="triple">TRIPLE</option>
                                            <option  value="double">DOUBLE</option>
                                            <option  value="other">-</option>
                                        </select>
                                    </div>
                                    <div class="regist_cell2"><input style="margin-left:10px;"  type="text" id="regist_paket"></div>
                                    <div class="regist_cell3"><label style="height:10px;width:500px;" id="regist_paket_name">Nama Paket</label></div>                                    
                                    <div class="regist_clearer">&nbsp;</div>
                                    <div class="regist_cell1">Group Jamaah</div><div class="regist_cell2"><input type="text" id="regist_group"></div><div class="regist_cell3"><label style="height:10px" id="regist_group_name">Nama Group Jamaah</label></div>
                                    <div style="height:2px;clear:both;border-bottom:1px solid grey;">&nbsp;</div>
                                    <div>
                                        <div style="float:left;">
                                            <div class="regist_cell1">Harga</div><div class="regist_cell2"><input value="0" type="text" id="regist_price"></div>
                                            <div class="regist_clearer">&nbsp;</div>
                                            <div class="regist_cell1">Disc</div><div class="regist_cell2"><input type="text" id="regist_disc"></div>
                                            <div class="regist_clearer">&nbsp;</div>
                                            <div class="regist_cell1">Net</div><div class="regist_cell2"><input value="0" type="text" id="regist_net"></div>                                            
                                        </div>
                                        <div style="float:left;">
                                            <div class="regist_cell1" style="width:90%;margin-left:50px;margin-top:10px;">
                                                    <button id="regist_save_data" class="button success" style="margin-left:30px;">Simpan</button>
                                                    <button id="regist_clear_data" class="button warning">Clear Data</button>
                                            </div>                                                                    
                                        </div>
                                    </div>

                            </div>                       
                        </div>

                    
                </div>
            </div>
    </div>
<script type="text/javascript" src="/pos/asset2/pos/js/regist-ui.js"></script>
<script type="text/javascript" src="/pos/asset2/pos/js/regist.js"></script>