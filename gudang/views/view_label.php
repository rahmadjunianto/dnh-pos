<?php foreach ($this->html_headers->styles as $style): ?>
        <link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
<?php endforeach; ?>
<?php foreach ($this->html_headers->scripts as $script): ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php endforeach; ?>        
    <div class="frameintab-pendaftaran"> 
            <div class="grid_regist">
                <div class="row">                    
                    <div class="span-grid-left" style="width:40% !important;">                        
                        <div class="panel" data-role="panel">
                            <div class="panel-header bg-darkRed fg-white">
                                Cetak Label
                            </div>
                            <div class="panel-content">  
                                <div class="left_search_table">
                                    <div class="left_search_title">
                                        <div class="left_search_heading">&nbsp;</div>
                                        <div class="left_search_row">
                                            <div class="left_search_cell1">
                                                 No Lab
                                            </div>
                                            <div class="left_search_cell2">
                                               <input type="text" id="payment_lab_id"/>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>                                                            
                                
                                <div>&nbsp;</div>                    
                                <div id="container_left_search">
                                    <table id="payment_table" class="table striped bordered hovered">
                                    <thead>
                                    <tr>
                                        <th class="text-left">No</th>
                                        <th class="text-left">Tanggal Bayar</th>
                                        <th class="text-left">No Nota</th>
                                        <th class="text-left">Jumlah Bayar</th>
                                        <th class="text-left">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                                </div>
                                <div class="payment_frame">
                                    <div class="col1">Status Pelunasan</div><div class="col2"><label class="pay_state">STATE</label></div>
                                    <div class="col1">Kurang Bayar</div><div class="col2"><label class="pay_outstanding">STATE 0</label></div>
                                    <p><hr></p>
                                    <div class="pay">
                                        <div class="col1">Jenis Bayar</div>
                                        <div class="col2">
                                            <select class="pay_type">
                                                            <option selected="" value="1">Tunai</option>
                                                            <option value="2">Kartu Debit</option>
                                                            <option value="3">Kartu Kredit</option>
                                             </select>
                                        </div>    
                                        <p>&nbsp;</p>
                                        <div class="col1">Jumlah Bayar</div>
                                        <div class="col2"><input class="pay_amount" type="text"></div>
                                        <div class="col3">
                                            <button class="pay_save_payment">Simpan</button>
                                            <input type="hidden" class="pay_no">
                                            <button class="pay_print">Print</button>
                                        </div>                                                                                
                                    </div>                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="span-grid-right-result" style ="float:left;width:58% !important " >
                        <div  id="<?php echo $grid_name; ?>" >                            
                        </div>
                    </div>                                        
                </div>
            </div>
    </div>        
        <script type="text/javascript">
        <?php         echo 'var table_title = "' . $table_title  . '";';                     
        echo 'debugger;';
        echo 'var table_id = "' . $table_id  . '";';
        echo 'var filter_htmlid = "filter_' . $grid_name  . '";';        
        echo 'var filter_select_htmlid = "select_col_' . $grid_name  . '";';
        ?>            
            var select_col_html_def="<?php
            $str_select="<select id='select_col_" . $grid_name  . "'>";
            for($x=0;$x<count($table_detail);$x++)
            {
                $table_desc = $table_detail[$x]->tfd_coldesc;
                $table_order = $table_detail[$x]->tfd_order;
                if(trim($table_detail[$x]->tfd_colname)==trim($default_sort_col)) {
                    $str_select = $str_select . "<option  selected value=$table_order >$table_desc</option>";
                }else {
                    $str_select = $str_select . "<option value=$table_order>$table_desc</option>";
                }                
                //$str_select = $str_select . "<option value=$table_order>$table_desc</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";   
	var ht = $(".span-grid").innerHeight();
        var wd = $(".span-grid").innerWidth();      
        //var docwidth = $(document).width();
        //wd = wd/100 * (docwidth);        
        debugger;
        var  newObj = { width:wd, height: 540, numberCell: true, minWidth: 10,
        title: table_title,
        filter_id : filter_htmlid,
        filter_select_htmlid : filter_select_htmlid,
        new_key_value:'',
        tableID : table_id,
        _lab_id : '',
        bottomVisible:false,
        resizable: true, columnBorders: true,
        selectionModel: { type: 'row', mode: 'block' },
        editModel: { clicksToEdit:2, saveKey: 13 },
        hoverMode: 'cell',
    };  
            
        newObj.colModel=[];
        </script>                    
<?php        
        echo '<script type="text/javascript">';
        echo 'var base_url = "' .   base_url() . '";';
        echo 'var site_url = "' . site_url() . '";';
        echo 'var default_sort_col = "' . $default_sort_col  . '";'; //< ?php echo $default_sort_col;
        echo 'var geturladdr = "' . $geturladdr  . '";'; //<?php echo $geturladdr ;
        //newrowurl
        echo 'var newrowurl= "' . $newrowurl  . '";'; //<?php echo $geturladdr ;
        echo 'var updateurl = "' . $updateurl  . '";'; // <?php echo $updateurl ;
        echo 'var drowurl = "' . $drowurl  . '";'; // <?php echo $updateurl ;
        echo 'var grid_name = "'   . $grid_name .  '";'; //#<?php echo $grid_name;
        echo 'var grid_name2 = "#'  . $grid_name . '";'; //<?php echo $grid_name;
        echo 'var keycolumnindex=' . $keycolumn_index  . ';'; // <?php echo $keycolumn_index
        echo '$grid={};';
        echo 'var sortcols =';
            $prefix = "[";
            $suffix = "]";
            $colcnt = $prefix;
            for($x=0;$x<count($table_detail);$x++)
            {
                if($x<count($table_detail)-1) {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "',";
                }else {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "'" . $suffix . ";";
                }
            }            
            echo $colcnt;        
            for($x=0;$x<count($table_detail);$x++)
            {
                $tmp = $table_detail[$x]->tfd_editable;
                if($tmp==1) {
                    $editable="true";
                }else {
                    $editable="false";
                }
                echo 'newObj' . '.colModel['. ($x) . '] = { title: "' . $table_detail[$x]->tfd_coldesc . '",width:' . $table_detail[$x]->tfd_colwidth . ' ,dataIndx: "' . $table_detail[$x]->tfd_colname . '",editable:' . $editable  . '};';
            }            
        //echo 'newObj = newObj'  . ';';
        echo '</script>';       
?>        
<script type="text/javascript" src="/pos/asset2/pos/js/payment.js"></script>
