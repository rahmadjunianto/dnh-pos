<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
	var site_url = "<?php echo site_url(); ?>";
        var menu_attr_url = base_url + "<?php echo $menu_attr_url; ?>";
    </script>
	<?php foreach ($this->html_headers->styles as $style): ?>
		<link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
	<?php endforeach; ?>
<?php foreach ($this->html_headers->scripts as $script): ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>

<?php endforeach; ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset2/logging/jQuery.clientSideLogging.js"></script>

    <title>
		<?php echo (isset($this->html_headers->title)) ? $this->html_headers->title : "pos"; ?>
    </title>

</head>
<body class="metro">
    <header class="bg-dark" data-load="header.html">
    <div class="navigation-bar">
       <div class="navigation-bar-content container">
           <a href="/" class="element"><span class="icon-grid-view"></span>pos<sup>1.0</sup></a>
           <span class="element-divider"></span>
           <a class="element1 pull-menu" href="#"></a>
           <ul class="element-menu">
               <?php
                for($x=0;$x<count($menu);$x++) {
                    if($menu[$x]->menu_hierarchy==0) {
                        echo "<li>";
                        echo '<a class="dropdown-toggle" href="#">' . $menu[$x]->menu_name .  '</a>';
                        echo '<ul class="dropdown-menu " data-role="dropdown">';
                        for($y=0;$y<count($menu);$y++) {
                            if($menu[$x]->menu_id==$menu[$y]->menu_parent) {
                                if($menu[$y]->menu_name=="separator") {
                                    echo '<div class="divider"></div>';
                                }else {
                                    echo "<li>";
                                    if($menu[$y]->menu_haschild==1) {
                                        //$class='class="dropdown-menu "';
                                        //}  else {
                                        //$class='';
                                        echo '<a class="dropdown-toggle" href="#">' . $menu[$y]->menu_name .  '</a>';
                                        echo '<ul class="dropdown-menu " data-role="dropdown">';
                                    }  else {
                                        echo '<a class="menu_element" href="#" id="' . $menu[$y]->menu_htmlid . '">' . $menu[$y]->menu_name . '</a>';
                                    }
                                    for($z=0;$z<count($menu);$z++) {
                                        if($menu[$y]->menu_id==$menu[$z]->menu_parent) {
                                            if($menu[$z]->menu_name=="separator") {
                                                echo '<div class="divider"></div>';
                                            }else {
                                                echo "<li>";
                                                echo '<a class="menu_element" href="#" id="' . $menu[$z]->menu_htmlid . '">' . $menu[$z]->menu_name . '</a>';
                                                echo "</li>";
                                            }
                                        }
                                    }
                                    if($menu[$y]->menu_haschild==1) {
                                        echo '</ul>';
                                    }
                                    echo "</li>";
                                }
                            }
                        }
                        echo '</ul>';
                        echo "</li>";
                    }
                }
               ?>
           </ul>
           <div class="no-tablet-portrait no-phone">
               <a title="logout" href="/pos/login/logout" class="element place-right"><span class="icon-box-add">Logout</span></a>
               <span class="element-divider place-right"></span>

               <div class="element place-right" title="User Name"><span class="icon-star"></span> <span class="user_name">
                   <?php
                            //<!-- HTML stuff -->
                           echo $users_name;
                   ?>
                   </span></div>
               <!--<span class="element-divider place-right"></span>-->
               <!--<div class="element place-right" title="GitHub Forks"><span class="icon-share-2"></span> <span class="github-forks">0</span></div>-->
           </div>
       </div>
   </div>
    <div style='float:left;background-color:red;width:100%'>
        <?php
        if(strlen($header_info)>0) {
            echo "<blink><marquee>$header_info</marquee></blink>";
        }else {
            echo "";
        }
        ?>
    </div>
   </header>
    &nbsp;
    <!--<div class='user_form'>kosong</div>-->