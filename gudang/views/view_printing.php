<html>
	<head>
                <?php
                    $top=$page_margin['top'];
                    $left=$page_margin['left'];
                    $right=$page_margin['right'];
                    $bottom=$page_margin['bottom'];
                    $page_margin = "margin: $top $right $bottom $left";
                ?>
		<style>
			/* Define page size. Requires print-area adjustment! */
                    @page { <?php echo $page_margin; 
                            //echo "font-family: verdana,arial,sans-serif;";
                            //echo "font-size:11px;";
                    ?> }
                    <?php
                        //$style_id = "#" . trim($style_detail[0]->print_style_id);
                        //echo $style_id . " {  }"
                        //echo count($style_detail);
                        
                        for($x=0;$x<count($style_detail);$x++) {
                            $style_id = $style_detail[$x]->print_style_id;
                            $style_attr = $style_detail[$x]->print_attr;
                            $style_attr_value = $style_detail[$x]->print_attr_value;
                            $style_tag =  "#" . trim($style_id);
                            $style_tag =  $style_tag . " { $style_attr : $style_attr_value; } \r\n";
                            echo $style_tag;
                            //echo $style_tag;
                            //echo "<tag>$style_detail[$x]['print_style_id']</tag>";
                        }
                        //style_header_table
                        for($x=0;$x<count($style_header_table);$x++) {
                            $style_id = $style_header_table[$x]->print_style_id;
                            $style_attr = $style_header_table[$x]->print_attr;
                            $style_attr_value = $style_header_table[$x]->print_attr_value;
                            $style_tag =  "." . trim($style_id);
                            $style_tag =  $style_tag . " { $style_attr : $style_attr_value; } \r\n";
                            echo $style_tag;
                            //echo $style_tag;
                            //echo "<tag>$style_detail[$x]['print_style_id']</tag>";
                        }     
                        //content_table : $style_content_table
                        for($x=0;$x<count($style_content_table);$x++) {
                            $style_id = $style_content_table[$x]->print_style_id;
                            $style_attr = $style_content_table[$x]->print_attr;
                            $style_attr_value = $style_content_table[$x]->print_attr_value;
                            $style_tag =  "." . trim($style_id);
                            $style_tag =  $style_tag . " { $style_attr : $style_attr_value; } \r\n";
                            echo $style_tag;
                            //echo $style_tag;
                            //echo "<tag>$style_detail[$x]['print_style_id']</tag>";
                        }                             
                    ?>
		</style>
	</head>
	<!--<body onload="window.print();window.focus();" >-->
        <body>
            <div id="header">
                <?php
                    if($count_header_table>0) {
                        echo "<table>\r\n";                        
                        echo "<tr>\r\n";
                        $last_row=1;
                        for($x=0;$x<$count_header_table;$x++) {
                            //                            
                            $row = $header_table_column_detail[$x]->print_table_row_num;
                            if($last_row!=$row) {
                                if($last_row>1) {
                                    echo "</tr>\r\n";
                                }
                                echo "<tr>\r\n";
                            }
                            $td_class  = $header_table_column_detail[$x]->print_style_id;
                            $td_value  = $header_table_column_detail[$x]->print_table_col_desc;
                            
                            if(substr($td_value,0,1)=="$") {
                                $varname = str_replace("$","",$td_value);
                                echo "<td $td_class> $header[$varname] </td>";
                            }else {
                                echo "<td $td_class> $td_value </td>";
                            }

                            $last_row = $row;
                            
                        }
                        echo "</tr>\r\n";
                        echo "</table>";
                    }
                ?>
                
            </div>
            <div id="content" style='border:1px transparent'>
     
                <?php
                
                    if($count_content_table>0) {
                        echo "<table width=100%>\r\n";                        
                        echo "<tr><td colspan=$count_content_table><div style='height:0px;width:100%;border:solid 0.2px'>&nbsp;</div></td></tr>";
                        echo "<tr>\r\n";
                        $last_row=1;
                        for($x=0;$x<$count_content_table;$x++) {
                            //
                            
                            $row = $content_table_column_detail[$x]->print_table_row_num;
                            //echo " ROW $row dan last row $last_row<br>";
                            if($last_row!=$row) {
                                if($last_row>1) {
                                    echo "</tr>\r\n";
                                }
                                echo "<tr>\r\n";
                            }
                            $td_class  = $content_table_column_detail[$x]->print_style_id;
                            $td_value  = $content_table_column_detail[$x]->print_table_col_desc;
                            
                            if(substr($td_value,0,1)=="$") {
                                $varname = str_replace("$","",$td_value);
                                echo "<td class='" . $td_class . "'> $content[$varname] </td>";
                            }else {
                                echo "<td class='" . $td_class . "'> $td_value </td>";
                            }
                            $last_row = $row;                            
                        }
                        echo "</tr>\r\n";
                        echo "<tr><td colspan=$count_content_table><div style='height:0px;width:100%;border:solid 0.2px'>&nbsp;</div></td></tr>";
                        //data
                        for($yy=0;$yy<count($content['data']['data']);$yy++) {
                            echo "<tr>\r\n";                        
                            for($x=0;$x<$count_content_table_data;$x++) {
                                //                                                        
                                $td_class  = $content_table_data_column_detail[$x]->print_style_id;
                                $td_value  = $content_table_data_column_detail[$x]->print_table_col_desc;
                                $varname = str_replace("$","",$td_value);
                                echo "<td class='" . $td_class . "'>" . $content['data']['data'][$yy][$varname] . "</td>";                                
                                //$last_row = $row;                            
                            }
                            echo "</tr>\r\n";                                                 
                            echo "<tr><td colspan=$count_content_table><div style='height:0px;width:100%;border:0.2px dashed;'>&nbsp;</div></td></tr>";
                        }
                        echo "</table>";
                    }

                ?>
            </div>
	</body>                
</html>
