<html>
<head>
  <style>
    @page { margin: 180px 50px; }
    #header {
        position: fixed; 
        left: 0px; 
        top: -20px; right: 0px; height: 180px; 
        text-align: center; 
    }
    #footer { 
        position: fixed; 
        left: 50px; bottom: -180px; right: 0px; height: 150px; 
        
    }
    #content {
            position:relative;
            /*adjust ketinggian content di sini*/
            top:153px;
            border:1px solid;
    }
    .container_header_result_detail {
        border : 1px solid;
        top :10px;        
        
        /*position:fixed;*/
    }
    .header_result_detail {
    
        
        //page-break-after: always;   

    }
    /*
    .spacer1 {
	height:10px;
    }*/
    .hrd_col1 {
        width:150px;
    }
    .hrd_col2 {
        width:120px;
    }    
    .hrd_col3 {
        width:150px;
    }    
    .hrd_col4 {
        width:50px;
    }    
    .b_hrd_col1 {
        width:150px;
    }
    .b_hrd_col2 {
        width:120px;        
    }    
    .b_hrd_col3 {
        width:150px;
    }    
    .b_hrd_col4 {
        width:50px;
    }   
    
    /*#footer .page:after { content: counter(page, upper-roman); }*/
        body {
                margin:     0;
                padding:    0;
                margin-top:0;
        }
        .table_container{
            border: 0px solid;
            width:500px;
        }
        .inner_header_top{
            border:1px solid;
            height:0px;
        }
        .inner_header_bottom{
            border:1px solid;
            height:0px;
        }


        /* Printable area */
        #print-area {
                position:   relative;
                top:        0cm;
                left:       0cm;
                width:      17cm;                
                /*height:     27.6cm;*/
                font-size:  12px;
                font-family: Helvetica, serif;
        }
        .ligther{font-weight: lighter;}
        .strong{font-weight: bolder;}
        .caps_font{text-transform:uppercase;}
        .font_0{font-size: 10px;}
        .font_1{font-size: 11px;}
        .font_2{font-size: 12px;}
        .centered{text-align: center;}
        .align_right{text-align: right;}
        /*table*/
  </style>


</head>
<body>

  <div id="header">
      <?php
        $name =  $pxinfo[0]->name;
        $lab_id =  $pxinfo[0]->lab_id;
        $age = $pxinfo[0]->age;
        $patientid = $pxinfo[0]->patient_id;
        $addr = $pxinfo[0]->addr;
        $doctname = $pxinfo[0]->doct_name;
        $gender = $pxinfo[0]->gender;
        $phonenum = $pxinfo[0]->phonenum;
        $regdate = $pxinfo[0]->regdate;
        //$regdate = $pxinfo[0]->regdate;
      ?>
      <div>
        <table id="tbl_header" >                                                        
            <tr>  
                <td class="res_col1">No REG</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $patientid; ?></td>
                <td class="res_col4">NO LAB</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $lab_id; ?></td>
            </tr>                                                                                                                
            <tr>  
                <td class="res_col1">NAMA</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $name; ?></td>
                <td class="res_col4">TANGGAL REG</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $regdate; ?></td>
            </tr>                                                                                                                
            <tr >  
                <td class="res_col1">ALAMAT</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $addr; ?></td>
                <td class="res_col4">JENIS KELAMIN</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $gender; ?></td>
            </tr>                     
            <tr>  
                <td class="res_col1">DOKTER</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $doctname; ?></td>
                <td class="res_col4">UMUR</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $age; ?></td>
            </tr>                                                                             
            <tr>  
                <td class="res_col1">NO TELP/HP</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $phonenum; ?></td>
                <td class="res_col4">&nbsp;</td>
                <td class="res_col5"></td>
                <td class="res_col6">&nbsp;</td>
            </tr>                                                                                                                                     
        </table>
    </div>
<!--    <div class="spacer1">&nbsp;</div>-->
    <div class="container_header_result_detail">
        
        <table class="header_result_detail">                                                        
            <tr>
                <td class="hrd_col1">JENIS PEMERIKSAAN</td>
                <td class="hrd_col2">Hasil</td>
                <td class="hrd_col3">NILAI RUJUKAN</td>
                <td class="hrd_col4">SATUAN</td>
            </tr>
        </table>
    
    </div>  
      
  </div>
  <div id="footer">
    <p class="page"> </p>
  </div>
  <div id="content">
                                                    <table border="0" style="width: 100%; border-bottom:1px solid #ccc" >
                                                        </tr>
                                                        <?php
$pxname="";
$pxvalue="";
$pxunit="";                                                               
$pxnormal="";

                                                                    for($x=0;$x<count($listpx);$x++) {
                                                                        $row = $x+1;
                                                                        //echo "$row";
                                                                        $pxname = $listpx[$x]['pxname'];
                                                                        if($pxname=="") {
                                                                            $pxname="&nbsp;";
                                                                        }
                                                                        $pxvalue = $listpx[$x]['pxvalue'];
                                                                        if($pxvalue=="") {
                                                                            $pxvalue="&nbsp;";
                                                                        }                                                                        
                                                                        $pxnormal = $listpx[$x]['pxnormal'];
                                                                        if($pxnormal=="") {
                                                                            $pxnormal="&nbsp;";
                                                                        }                                                                                   
                                                                        $pxunit = $listpx[$x]['pxunit'];
                                                                        if($pxunit=="") {
                                                                            $pxunit="&nbsp;";
                                                                        }                                                                                    
                                                                        $str_row="";
                                                                        $str_row = "<tr><td class='b_hrd_col1'>$pxname</td><td class='b_hrd_col2'>$pxvalue</td><td class='b_hrd_col3'>$pxnormal</td><td class='b_hrd_col4'>$pxunit</td></tr>";
                                                                        echo $str_row;
                                                                    }
                                                        ?>
                                                    </table>    

  </div>
</body>
</html>
