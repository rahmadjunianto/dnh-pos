
    <div class="frameintab-pendaftaran"> 
            <div class="grid_regist">
                <div class="row">                    
                    <div class="span-grid-left">                        
                        <div class="panel" data-role="panel">
                            <div class="panel-header bg-darkRed fg-white">
                                Daftar Pasien
                            </div>
                            <div class="panel-content">  
                                <div class="left_search_table">
                                    <div class="left_search_title">
                                        <div class="left_search_heading">&nbsp;</div>
                                        <div class="left_search_row">
                                            <div class="left_search_cell1">
                                                Tanggal
                                            </div>
                                            <div class="left_search_cell2">
                                                <div class="input-control text" data-role="datepicker" data-week-start="1">
                                                    <input id="result_filter_date" type="text" readonly="readonly">
                                                    <button class="btn-date"></button>
                                                    <div class="calendar calendar-dropdown" data-start-mode="month" style="position: absolute; max-width: 260px; z-index: 1000; top: 100%; left: 0px; display: none;"><table class="bordered"><tbody><tr class="calendar-header"><td class="text-center"><a class="btn-previous-year" href="#"><i class="icon-previous"></i></a></td><td class="text-center"><a class="btn-previous-month" href="#"><i class="icon-arrow-left-4"></i></a></td><td colspan="3" class="text-center"><a class="btn-select-month" href="#">August 2014</a></td><td class="text-center"><a class="btn-next-month" href="#"><i class="icon-arrow-right-4"></i></a></td><td class="text-center"><a class="btn-next-year" href="#"><i class="icon-next"></i></a></td></tr><tr class="calendar-subheader"><td class="text-center day-of-week">Mo</td><td class="text-center day-of-week">Tu</td><td class="text-center day-of-week">We</td><td class="text-center day-of-week">Th</td><td class="text-center day-of-week">Fr</td><td class="text-center day-of-week">Sa</td><td class="text-center day-of-week">Su</td></tr><tr><td class="empty"><small class="other-day"></small></td><td class="empty"><small class="other-day"></small></td><td class="empty"><small class="other-day"></small></td><td class="empty"><small class="other-day"></small></td><td class="text-center day"><a href="#">1</a></td><td class="text-center day"><a href="#">2</a></td><td class="text-center day"><a href="#">3</a></td></tr><tr><td class="text-center day"><a href="#">4</a></td><td class="text-center day"><a href="#">5</a></td><td class="text-center day"><a href="#">6</a></td><td class="text-center day"><a href="#">7</a></td><td class="text-center day"><a href="#">8</a></td><td class="text-center day"><a href="#">9</a></td><td class="text-center day"><a href="#">10</a></td></tr><tr><td class="text-center day"><a href="#">11</a></td><td class="text-center day today"><a href="#" class="selected">12</a></td><td class="text-center day"><a href="#">13</a></td><td class="text-center day"><a href="#">14</a></td><td class="text-center day"><a href="#">15</a></td><td class="text-center day"><a href="#">16</a></td><td class="text-center day"><a href="#">17</a></td></tr><tr><td class="text-center day"><a href="#">18</a></td><td class="text-center day"><a href="#">19</a></td><td class="text-center day"><a href="#">20</a></td><td class="text-center day"><a href="#">21</a></td><td class="text-center day"><a href="#">22</a></td><td class="text-center day"><a href="#">23</a></td><td class="text-center day"><a href="#">24</a></td></tr><tr><td class="text-center day"><a href="#">25</a></td><td class="text-center day"><a href="#">26</a></td><td class="text-center day"><a href="#">27</a></td><td class="text-center day"><a href="#">28</a></td><td class="text-center day"><a href="#">29</a></td><td class="text-center day"><a href="#">30</a></td><td class="text-center day"><a href="#">31</a></td></tr></tbody></table></div>
                                                </div>                                                       
                                            </div>
                                        </div>
                                        <div class="left_search_row">
                                            <div class="left_search_cell1">
                                                 No Lab
                                            </div>
                                            <div class="left_search_cell2">
                                               <input type="text"/>
                                            </div>
                                        </div>                                        
                                        <div class="left_search_cell1">
                                            &nbsp;
                                        </div>                                        
                                        <div class="left_search_cell2">
                                            <button id="result_filter" class="button success">Cari</button>
                                        </div>                                                                                
                                    </div>
                                </div>                                                            
                                <div>&nbsp;</div>                    
                                <div id="container_left_search">
                                    <table id="result_table" class="table striped bordered hovered">
                                    <thead>
                                    <tr>
                                        <th class="text-left">No</th>
                                        <th class="text-left">No Lab</th>
                                        <th class="text-left">Nama</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="span-grid-right-result">
                        <div id="<?php echo $grid_name; ?>">                            
                        </div>
                    </div>                    
                    
                </div>
            </div>
    </div>        
        <script type="text/javascript">
        <?php         echo 'var table_title = "' . $table_title  . '";';                     
        echo 'debugger;';
        echo 'var table_id = "' . $table_id  . '";';
        echo 'var filter_htmlid = "filter_' . $grid_name  . '";';        
        echo 'var filter_select_htmlid = "select_col_' . $grid_name  . '";';
        ?>            
            var select_col_html_def="<?php
            $str_select="<select id='select_col_" . $grid_name  . "'>";
            for($x=0;$x<count($table_detail);$x++)
            {
                $table_desc = $table_detail[$x]->tfd_coldesc;
                $table_order = $table_detail[$x]->tfd_order;
                if(trim($table_detail[$x]->tfd_colname)==trim($default_sort_col)) {
                    $str_select = $str_select . "<option  selected value=$table_order >$table_desc</option>";
                }else {
                    $str_select = $str_select . "<option value=$table_order>$table_desc</option>";
                }                
                //$str_select = $str_select . "<option value=$table_order>$table_desc</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";   
	var ht = $(".span-grid").innerHeight();
        var wd = $(".span-grid").innerWidth();      
        //var docwidth = $(document).width();
        //wd = wd/100 * (docwidth);        
        debugger;
        var  newObj = { width:wd, height: 540, numberCell: true, minWidth: 10,
        title: table_title,
        filter_id : filter_htmlid,
        filter_select_htmlid : filter_select_htmlid,
        new_key_value:'',
        tableID : table_id,
        _lab_id : '',
        bottomVisible:false,
        resizable: true, columnBorders: true,
        selectionModel: { type: 'cell', mode: 'block' },
        editModel: { clicksToEdit:2, saveKey: 13 },
        hoverMode: 'cell',
    };  
            
        newObj.colModel=[];
        </script>                    
<?php        
        echo '<script type="text/javascript">';
        echo 'var base_url = "' .   base_url() . '";';
        echo 'var site_url = "' . site_url() . '";';
        echo 'var default_sort_col = "' . $default_sort_col  . '";'; //< ?php echo $default_sort_col;
        echo 'var geturladdr = "' . $geturladdr  . '";'; //<?php echo $geturladdr ;
        //newrowurl
        echo 'var newrowurl= "' . $newrowurl  . '";'; //<?php echo $geturladdr ;
        echo 'var updateurl = "' . $updateurl  . '";'; // <?php echo $updateurl ;
        echo 'var drowurl = "' . $drowurl  . '";'; // <?php echo $updateurl ;
        echo 'var grid_name = "'   . $grid_name .  '";'; //#<?php echo $grid_name;
        echo 'var grid_name2 = "#'  . $grid_name . '";'; //<?php echo $grid_name;
        echo 'var keycolumnindex=' . $keycolumn_index  . ';'; // <?php echo $keycolumn_index
        echo '$grid={};';
        echo 'var sortcols =';
            $prefix = "[";
            $suffix = "]";
            $colcnt = $prefix;
            for($x=0;$x<count($table_detail);$x++)
            {
                if($x<count($table_detail)-1) {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "',";
                }else {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "'" . $suffix . ";";
                }
            }            
            echo $colcnt;        
            for($x=0;$x<count($table_detail);$x++)
            {
                $tmp = $table_detail[$x]->tfd_editable;
                if($tmp==1) {
                    $editable="true";
                }else {
                    $editable="false";
                }
                echo 'newObj' . '.colModel['. ($x) . '] = { title: "' . $table_detail[$x]->tfd_coldesc . '",width:' . $table_detail[$x]->tfd_colwidth . ' ,dataIndx: "' . $table_detail[$x]->tfd_colname . '",editable:' . $editable  . '};';
            }            
        //echo 'newObj = newObj'  . ';';
        echo '</script>';       
?>        
<script type="text/javascript" src="/pos/asset2/pos/js/daily.js"></script>
