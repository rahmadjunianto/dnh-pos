<html>
	<head>
		<style>
			/* Define page size. Requires print-area adjustment! */
			body {
				margin:     0;
				padding:    0;
                                margin-top:0;
			}
                        .table_container{
                            border: 0px solid;
                            width:500px;
                        }
                        .inner_header_top{
                            border:0.2px solid;
                            height:0px;
                        }
                        .inner_header_bottom{
                            border:0.1px solid;
                            height:0px;
                        }
                        
                         .col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col2{                             
                             width:200px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col3{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col4{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         /*payment*/
                         .payment_col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col2{                             
                             width:120px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col3{ 
                            width:10px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col4{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                                                
                         /*si*/
                         .sign_col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col2{                             
                             width:120px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col3{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col4{ 
                            width:60px;                            
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                                                
                         .bio_col1 {
                             width:60px;                             
                         }
                         .bio_col2 {
                             width:1px;
                         }        
                         .bio_col3 {
                             width:100px;
                             text-align:left;
                         }
                         .bio_col4 {
                             width:60px;
                         }                                 
                         .bio_col5 {
                             width:5px;
                         }
                         .bio_col6 {
                             width:100px;
                             text-align:left;
                         }        
			/* Printable area */
			#print-area {
				position:   relative;
				top:        0cm;
				left:       0cm;
				width:      17cm;
				/*height:     27.6cm;*/
				
				font-size:  12px;
				font-family: Helvetica, serif;
			}
			.ligther{font-weight: lighter;}
			.strong{font-weight: bolder;}
			.caps_font{text-transform:uppercase;}
			.font_0{font-size: 10px;}
			.font_1{font-size: 11px;}
			.font_2{font-size: 12px;}
			.centered{text-align: center;}
			.align_right{text-align: right;}
                        /*table*/
                    .detail_px {
                            font-family: verdana,arial,sans-serif;
                            font-size:11px;
                            color:#333333;
                            border-width: 0.1px;
                            border-color: #666666;
                            border-collapse: collapse;
                    }
                    #tbl_header {
                        font-family: verdana,arial,sans-serif;
                        font-size: 11px;
                        color: #333333;                        
                    }

		</style>
		<script type="text/javascript">
			window.onkeydown=function(o){
				o= o||event;
				if(o.keyCode == 27){ //escape char
					window.open('','_parent','');
					window.close();
				}}
		</script>
	</head>
	<!--<body onload="window.print();window.focus();" >-->
            <body>
		<div id="print-area"> 
			<div id="header">                                                    
                                                    <table id="tbl_header">
                                                        <!--
                                                        <tr><td colspan="6"><center><h1>NOTA PEMBAYARAN</h1> </center></td></tr>
                                                    <tr><td colspan="6"><center><strong>No NOTA : <?php echo $payment_no;  ?> </strong></center></td></tr>
                                                        -->
                                                        <tr>
                                                            <td class="bio_col1">Nama Pelanggan</td><td class="bio_col2">:</td><td class="bio_col3"><?php echo $pxinfo[0]->name;?></td>
                                                            <td class="bio_col4">ID Pelanggan</td><td class="bio_col5">:</td><td class="bio_col6"><?php echo $pxinfo[0]->patient_id;?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bio_col1">Umur/Jenis Kelamin</td><td class="bio_col2">:</td><td class="bio_col3"><?php echo $pxinfo[0]->age . "/" .  $pxinfo[0]->gender ;?></td>
                                                            <td class="bio_col4">No Lab</td><td class="bio_col5">:</td><td class="bio_col6"><?php echo $pxinfo[0]->lab_id;?></td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td class="bio_col1">Alamat</td><td class="bio_col2">:</td><td class="bio_col2"><?php echo $pxinfo[0]->addr;?></td>
                                                            <td class="bio_col4">Tanggal</td><td class="bio_col5">:</td><td class="bio_col6"><?php echo $pxinfo[0]->labdate;?></td>
                                                        </tr>                                                                                                                
                                                        <tr>
                                                            <td class="bio_col1">Dokter</td><td class="bio_col2">:</td><td class="bio_col3"><?php echo $pxinfo[0]->doct_name;?></td>
                                                            <td class="bio_col4">&nbsp;</td><td class="bio_col5">&nbsp;</td><td class="bio_col6">&nbsp;</td>
                                                        </tr>                                                                                                                                                                        
                                                    </table>
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                        <table class="detail_px">                                                                                                                
                                                                <tr><td class="col1">No</td ><td class="col2">Nama Pemeriksaan</td><td class="col3">Harga</td><td class="col4">Disc</td><td class="col5">Net</td></tr>                  
                                                        </table>        
                                                        <div class="inner_header_bottom">&nbsp;</div>
                                                    </div>
                        </div>
                        <div id="content">                            
                                                    <table class="detail_px">                                                        
                                                        <?php
                                                        //
                                                        $delivery = $pxinfo[0]->delivery;
                                                        $finish = $pxinfo[0]->finish_state;
                                                        $jamsekarang = $pxinfo[0]->jamsekarang;
                                                        //
                                                        if($delivery=="1") {
                                                            $delivery_state="Di Ambil Sendiri";
                                                        }elseif($delivery=="2") {
                                                            $delivery_state="Di Antar ke Dokter";
                                                        }elseif($delivery=="3") {
                                                            $delivery_state="Di Antar ke Kantor";
                                                        }else {
                                                            $delivery_state="-";
                                                        }
                                                        
                                                        //
                                                                $str_row="";
                                                                $totalprice = 0;
                                                                $totaldisc = 0;
                                                                $totalnet = 0;
                                                                for($x=0;$x<count($listpx);$x++) {                                                                        
                                                                    $row = $x+1;
                                                                    $pxname=$listpx[$x]->rd_pxname;
                                                                    $pxprice = $listpx[$x]->rd_price;
                                                                    $pxdisc = $listpx[$x]->rd_disc;
                                                                    $pxnet = $listpx[$x]->rd_net;
                                                                    $totalprice = $totalprice + $pxprice;
                                                                    $totaldisc = $totaldisc + $pxdisc;
                                                                    $totalnet = $totalnet + $pxnet;
                                                                    
                                                                    $str_row = "<tr><td class='col1'>$row</td><td class='col2'>$pxname</td><td class='col3'>$pxprice</td><td class='col4'>$pxdisc</td><td class='col5'>$pxnet</td>";                                                                        
                                                                    echo $str_row;
                                                                }
                                                        ?>
                                                    </table>
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                        <table class="detail_px">                                                                                                                
                                                            <?php
                                                                $str_row = "<tr><td class='col1'>&nbsp;</td><td class='col2'>TOTAL</td><td class='col3'>$totalprice</td><td class='col4'>$totaldisc</td><td class='col5'>$totalnet</td>";
                                                                echo $str_row;
                                                            ?>    
                                                        </table>        
                                                        <div class="inner_header_bottom">&nbsp;</div>
                                                    </div>
                            
                                                    <table class="detail_px">                                                        
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">PEMBAYARAN</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo $amount;?></strong></td>
                                                        </tr>
                                                        <?php
                                                        if($outstanding>0) {
                                                        ?>    
                                                        
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2"><strong>Kurang Bayar</strong></td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo $outstanding;?></strong></td>
                                                        </tr>                                                        
                                                        <?php
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">Status Pelunasan</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4">
                                                            <?php
                                                                if($paid==1) {
                                                                    echo "<strong>LUNAS</strong>";
                                                                }else {
                                                                    echo "<strong>BELUM LUNAS</strong>";
                                                                }
                                                            ?>                                                            
                                                            </td>
                                                        </tr>                                                                                                                
                                                    </table>    
                                                    <!--Tipe Pengambilan-->
                                                    
                                                    <table class="detail_px">                                                        
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">PENGAMBILAN HASIL</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2"><strong><?php echo $delivery_state?></strong></td>
                                                            <td class="payment_col3">&nbsp;</td>
                                                            <td class="payment_col4">&nbsp;</td>
                                                        </tr>                                                        
                                                    </table>    
                                                    
                                                    <!--Janji Hasil-->                                                    
                                                    <table class="detail_px">                                                        
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">HASIL SELESAI</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2"><strong><?php echo $finish?></strong></td>
                                                            <td class="payment_col3">&nbsp;</td>
                                                            <td class="payment_col4">&nbsp;</td>
                                                        </tr>                                                        
                                                    </table>                                                        
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                    </div>
                        </div>
                        <div id="footer">
                                                    <table class="detail_px">                                                        
                                                        <tr>
                                                            <td class="nb_col1"><strong>NB :</strong></td>
                                                            <td class="nb_col2">&nbsp;</td>
                                                            <td class="nb_col3"><strong>-</strong>
                                                            <td class="nb_col4"><strong>Nota harus di bawa pada saat Pengambilan Hasil</strong></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td class="nb_col1"><strong>&nbsp;</strong></td>
                                                            <td class="nb_col2">&nbsp;</td>
                                                            <td class="nb_col3"><strong>-</strong>
                                                            <td class="nb_col4"><strong>Hasil harap diambil paling lambat 1 (Satu) Minggu setelah tanggal janji hasil</strong></td>                                                            
                                                        </tr>                                                        
                                                        <tr>
                                                            <td class="nb_col1"><strong>&nbsp;</strong></td>
                                                            <td class="nb_col2">&nbsp;</td>
                                                            <td class="nb_col3"><strong>-</strong>
                                                            <td class="nb_col4"><strong>Pembatalan tidak bisa dilakukan bila pemeriksaan sudah dilakukan</strong></td>                                                            
                                                        </tr>                                                                                                                
                                                        <tr>
                                                            <td class="nb_col1"><strong>&nbsp;</strong></td>
                                                            <td class="nb_col2">&nbsp;</td>
                                                            <td class="nb_col3"><strong>-</strong>
                                                            <td class="nb_col4"><strong>Pembatalan pemeriksaan yang belum dilakukan sebelum lewat bulan </strong></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td class="nb_col1"><strong>&nbsp;</strong></td>
                                                            <td class="nb_col2">&nbsp;</td>
                                                            <td class="nb_col3"><strong></strong>
                                                            <td class="nb_col4"><strong>dikenakan biaya 12 % dan 25 % jika sudah lewat Bulan</strong></td>                                                            
                                                        </tr>                                                        
                                                    </table>    
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <table class="detail_px">                                                        
                                                        <tr>
                                                            <td class="sign_col1"><strong></strong></td>
                                                            <td class="sign_col2">&nbsp;</td>
                                                            <td class="sign_col3">&nbsp;</td>
                                                            <td class="sign_col4"><strong>Cirebon, <?php echo $jamsekarang?> </strong>
                                                            <td class="sign_col5"><strong></strong></td>                                                            
                                                        </tr>
                                                    </table>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <!--
                                                    <table class="detail_px">                                                        
                                                        <tr>
                                                            <td class="sign_col1"><strong></strong></td>                                                            
                                                            <td class="sign_col2"><strong>Terima Kasih Sudah melakukan Pemeriksaan di Lab. Klinik "PARAMEDIKA" </strong>
                                                            <td class="sign_col3"><strong></strong></td>
                                                            <td class="sign_col4"><strong></strong></td>
                                                        </tr>
                                                    </table>                                                    
                                                    -->
                        </div>                                   
		</div>
	</body>                
</html>
