<script type="text/javascript">
$(function () {
    //$("#search_master_criteria").click()    
    col_to_search="1";
    value_to_search="";    
    //
    <?php echo "newObj" . $grid_name ;?> .pqFilter = {
        search: function () {
            var txt = $("input.pq-filter-txt").val().toUpperCase(),
            colIndx = $("select#pq-filter-select-column").val();
            //DM = newObjgrid_doctor.dataModel;
            //DM.filterIndx = "1";
            //DM.filterValue = txt;
            //debugger;
            //newObjgrid_doctor.refreshDataAndView;
            
            $(grid_name2).pqGrid("refreshDataAndView");
        }
    } //end of filter
    <?php echo "newObj" . $grid_name ;?>.dataModel = {
        location: "remote",
        sorting: "remote",
        paging: "remote",
        dataType: "JSON",
        method: "POST",
        curPage: 1,        
        rPP: 20,
        sortIndx: default_sort_col,
        sortDir: "up",
        rPPOptions: [1, 10, 20, 30, 40, 50, 100, 500, 1000],
        filterIndx:"2",
        filterValue:"",
        getUrl: function () {
            //debugger;
            if(value_to_search=="") {
                value_to_search="#";
            }
            search_ctrl_name ="#search_val_critera" + grid_name;
            var txt = $(search_ctrl_name).val(),
            colIndx = $("select#pq-filter-select-column").val();
            debugger;
            //alert(table_id);
            //alert(site_url + geturladdr + this.curPage + "/" +
            //    this.rPP + "/" + this.sortIndx + "/" + this.sortDir + "/" + col_to_search + "/" + value_to_search + "/" + table_id);
            
            var sortDir = (this.sortDir == "up") ? "asc" : "desc";
            //alert(site_url + geturladdr + this.curPage + "/" + this.rPP + "/" + this.sortIndx + "/" + sortDir + "/" + col_to_search + "/" + value_to_search + "/" + table_id)
            var sort = sortcols;
            //data: "cur_page=" + this.curPage + "&records_per_page=" +
            //    this.rPP + "&sortBy=" + sort[this.sortIndx] + "&dir=" + sortDir
            return { url: site_url + geturladdr , 
                    data : { 
                        curpage : this.curPage , 
                        rpp : this.rPP  ,
                        sortidx :  this.sortIndx, 
                        sortdir :  sortDir ,
                        colsearch : this.filterIndx,
                        valsearch :  txt,
                        table_id :  table_id 
                       }
                }
        },
       getData: function (dataJSON) {
            //var data=               
            //debugger;
            return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
        }        
    };    
       <?php echo "newObj" . $grid_name ;?>.render = function (evt, obj) {
        var $toolbar = $("<div class='pq-grid-toolbar pq-grid-toolbar-search'></div>").appendTo($(".pq-grid-top", this)); 
        $("<span>Filter</span>").appendTo($toolbar);
        $("<input type='text' id='filter_" + grid_name + "' class='pq-filter-txt'/>").appendTo($toolbar).keyup(function (evt) {
            if (evt.keyCode == 13) {
                <?php echo "newObj" . $grid_name ;?>.pqFilter.search();
            }
        });
        $("<select id='pq-filter-select-column'>\
        <option value='0'>Ship Country</option>\
        <option value='1'>Customer Name</option>\
        </select>").appendTo($toolbar).change(function () {
            <?php echo "newObj" . $grid_name ;?>.pqFilter.search();
        });
        $("<span class='pq-separator'></span>").appendTo($toolbar);
 
    };    
    //here we have to set dataIndx explicitly because it's a new column.    
    <?php echo "newObj" . $grid_name ;?>.cellEditKeyDown= function(evt,ui) {                
        var keyCode = evt.keyCode,
            rowIndxPage = ui.rowIndxPage,
            colIndx = ui.colIndx;
        if (keyCode == 13) {
            //debugger;
            var dt = ui.$cell.text();                       
            <?php echo "newObj" . $grid_name ;?>.saveEditCell;
            debugger;
            //var doct_id= ui.dataModel.data[rowIndxPage].doct_id;
            objdata = ui.dataModel.data[rowIndxPage]
            var arrtmp = $.map(objdata, function(el) { return el; });
            key_value = arrtmp[keycolumnindex];
            //doct_id = get_col_key();
            var url = updateurl ;
            //alert(url);
            //debugger;
            updaters(url,dt,key_value,table_id,colIndx);
        }
    }

    $(grid_name2).pqGrid(<?php echo "newObj" . $grid_name ;?>);
    namatext ="#search_val_criteria" +  grid_name;
    //alert(namatext);
    $(namatext).keydown(function( event ) {        
        if ( event.which == 13 ) {
           alert("OK " + grid_name );
           <?php echo "newObj" . $grid_name ;?>.pqFilter.search();
        }
        //xTriggered++;
    });      
});

</script>