<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php
        $topmargin=((($offset-1)*68)-13);
    ?>

    <style>
    /*@page { margin: -10px 10px; }*/
    /* -10 + 70*/
    /* offset 70*/
    @page { margin: <?php echo $topmargin;?>px 20px; }
    
    #header {
        position: fixed; 
        left: 0px; 
        top: 10px; 
        right: 0px; 
        height: 180px; 
        text-align: center; 
        
    }    
    #footer { 
        position: fixed; 
        left: 30px;
        bottom: -180px; 
        right: 0px; 
        height: 150px;        
        //border:1px solid;
        width:500px;
        font-size:12px;
    }
    #footer_content {
        
        height:100px;
        width:400px;
        margin-left:100px;
    }
    #container_header_result_detail { 
        position:fixed;
        border : 1px solid;
        top :138px;     
        
    }    
    #content {
            position:relative;
            /*adjust ketinggian content di sini*/
            top:165px;
            border:1px solid;
    }
    .header_result_detail {            
        //page-break-after: always;
        top:-200px;
        
    }
    .spec_body {
            font-size:9px;
            font-family: Helvetica, serif;
        
    }
    /*
    .spacer1 {
	height:10px;
    }*/
    .hrd_col1 {
        width:200px;
    }
    .hrd_col2 {
        width:190px;
        text-align:left;
    }    
    .hrd_col3 {
        width:220px;
    }    
    .hrd_col4 {
        width:50px;
    }    
    .b_hrd_col1 {
        width:180px;
    }
    .b_hrd_col2 {
        width:170px;        
    }    
    .b_hrd_col3 {
        width:200px;
    }    
    .b_hrd_col4 {
        width:90px;
    }       
    /*#footer .page:after { content: counter(page, upper-roman); }*/
    body {
        margin:     0;
        padding:    0;
        margin-top:0;
        
    }
    .table_container{
        border: 0px solid;
        width:500px;
    }
    .inner_header_top{
        border:1px solid;
        height:0px;
    }
    .inner_header_bottom{
        border:1px solid;
        height:0px;
    }
    .inner_separator{
        border:0.1px dashed;
        height:0px;
    }    
    .res_col1 {
        width:125px;
	padding-left:-9px;
    }
    .res_col2 {
        width:125px;
        padding-left:4px; 
    }    
    .res_col3 {
        width:125px;                
	padding-left:7px;
    }
    .res_col4 {
        width:125px;
	padding-left:3px;
    }
    
    .res_col5 {
        width:125px;
	padding-left:3px;
    }
    .res_col6 {
        width:125px;
	padding-left:3px;
    }
    
    
    /* Printable area */
    #print-area {
            position:   relative;
            top:        0cm;
            left:       0cm;
            /*height:     27.6cm;*/
            font-size:  9px;
            
    }
    .ligther{font-weight: lighter;}
    .strong{font-weight: bolder;}
    .caps_font{text-transform:uppercase;}
    .font_0{font-size: 10px;}
    .font_1{font-size: 11px;}
    .font_2{font-size: 12px;}
    .centered{text-align: center;}
    .align_right{text-align: right;}
        /*table*/
  </style>
</head>
<body class='spec_body'>
 
  <div id="header">
      <?php
        $name =  $pxinfo[0]->name;
	//15 karakter
        $name = substr($name,0,15);
        $lab_id =  $pxinfo[0]->lab_id;
        $age = $pxinfo[0]->age;
        $patientid = $pxinfo[0]->patient_id;
        $addr = $pxinfo[0]->addr;
        $doctname = $pxinfo[0]->doct_name;
        $labdate = $pxinfo[0]->labdate;
        //$regdate = $pxinfo[0]->regdate;
        $command="sudo /home/id/bcode/bcodegen.py $lab_id 15";
        //echo "$command";
        exec($command);        
        $imgurl = "/var/www/pos/barcode/" . $lab_id . ".jpg";
      ?>
 
      <div>
        <table id="tbl_header" width="100%" >                                                        
            <tr>  
                <td class="res_col1"><?php echo $lab_id . "/" . $name; ?></td>
                <td class="res_col2"><?php echo $lab_id . "/" . $name; ?></td>
                <td class="res_col3"><?php echo $lab_id . "/" . $name; ?></td>
                <td class="res_col4"><?php echo $lab_id . "/" . $name; ?></td>
                <td class="res_col5"><?php echo $lab_id . "/" . $name; ?></td>
                <td class="res_col6"><?php echo $lab_id . "/" . $name; ?></td>
            </tr>                                                                                                                
            <tr>  
                <td class="res_col1"><?php echo "<img src='$imgurl'>" ?></td>
                <td class="res_col2"><?php echo "<img src='$imgurl'>" ?></td>
                <td class="res_col3"><?php echo "<img src='$imgurl'>" ?></td>
                <td class="res_col4"><?php echo "<img src='$imgurl'>" ?></td>
                <td class="res_col5"><?php echo "<img src='$imgurl'>" ?></td>
                <td class="res_col6"><?php echo "<img src='$imgurl'>" ?></td>
            </tr>                                                                                                                
            <tr>  
                <td class="res_col1"><?php echo $labdate; ?></td>
                <td class="res_col2"><?php echo $labdate; ?></td>
                <td class="res_col3"><?php echo $labdate; ?></td>
                <td class="res_col4"><?php echo $labdate; ?></td>
                <td class="res_col5"><?php echo $labdate; ?></td>                
                <td class="res_col6"><?php echo $labdate; ?></td>                
            </tr>                                                                                                                
        </table>
    </div>
<!--    <div class="spacer1">&nbsp;</div>-->
</body>
</html>
