<?php foreach ($this->html_headers->styles as $style): ?>
        <link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
<?php endforeach; ?>

<?php foreach ($this->html_headers->scripts as $script): ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php endforeach; ?>        
        <script type="text/javascript">
        <?php         echo 'var table_title = "' . $table_title  . '";';                     
        echo 'var table_id = "' . $table_id  . '";';
        echo 'var filter_htmlid = "filter_' . $grid_name  . '";';        
        echo 'var filter_select_htmlid = "select_col_' . $grid_name  . '";';
        echo 'var filter_panel_select_htmlid = "select_panel_col_' . $grid_name  . '";';
        ?>            
            var select_panel_col_html_def="<?php
            $str_select="<select style='width:200px;margin-left:10px;padding-right:100px;margin-right:10px;' id='select_panel_col_" . $grid_name  . "'>";
            $str_select = $str_select . "<option  selected value='0'>&nbsp;</option>";
            for($x=0;$x<count($panels);$x++)
            {
                $panel_id = $panels[$x]->mp_id;
                $panel_name = $panels[$x]->mp_name;                                
                $str_select = $str_select . "<option value=$panel_id>$panel_name</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";                    
            var select_col_html_def="<?php
            $str_select="<select id='select_col_" . $grid_name  . "'>";
            for($x=0;$x<count($table_detail);$x++)
            {
                $table_desc = $table_detail[$x]->tfd_coldesc;
                $table_order = $table_detail[$x]->tfd_order;
                if(trim($table_detail[$x]->tfd_colname)==trim($default_sort_col)) {
                    $str_select = $str_select . "<option  selected value=$table_order >$table_desc</option>";
                }else {
                    $str_select = $str_select . "<option value=$table_order>$table_desc</option>";
                }                
                //$str_select = $str_select . "<option value=$table_order>$table_desc</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";        
	var ht = $(".span-grid").innerHeight();		
        var wd = $(".span-grid").innerWidth();            
        var docwidth = $(document).width();
        //alert(wd);
        wd = 50/100 * (docwidth);
        
        //alert(wd);
        //alert(wd);
        var  newObj = { width: wd, height: 100, numberCell: true, minWidth: 10,
        title: table_title,
        filter_id : filter_htmlid,
        filter_select_htmlid : filter_select_htmlid,
        new_key_value:'',
        tableID : table_id,
        bottomVisible:false,
        resizable: true, columnBorders: true,
        selectionModel: { type: 'cell', mode: 'block' },
        editModel: { clicksToEdit:2, saveKey: 13 },
        hoverMode: 'cell',
    };  
            
        newObj.colModel=[];
        newObj.colModel[0] = { dataIndx: 0, editable: false, sortable: false, title: "", width: 30, align: "center", resizable: false, render: function (ui) {
            var rowData = ui.rowData, dataIndx = ui.dataIndx;
            var val = rowData[dataIndx];
            str = "";
            if (val) {
                str = "checked='checked'";
            }
            return "<input type='checkbox' " + str + " />";
        }, className: "checkboxColumn"
        }            
        </script>                    
<?php        
        echo '<script type="text/javascript">';
        echo 'var base_url = "' .   base_url() . '";';
        echo 'var site_url = "' . site_url() . '";';
        echo 'var default_sort_col = "' . $default_sort_col  . '";'; //< ?php echo $default_sort_col;
        echo 'var geturladdr = "' . $geturladdr  . '";'; //<?php echo $geturladdr ;
        //newrowurl
        echo 'var newrowurl= "' . $newrowurl  . '";'; //<?php echo $geturladdr ;
        echo 'var updateurl = "' . $updateurl  . '";'; // <?php echo $updateurl ;
        echo 'var drowurl = "' . $drowurl  . '";'; // <?php echo $updateurl ;
        echo 'var grid_name = "'   . $grid_name .  '";'; //#<?php echo $grid_name;
        echo 'var grid_name2 = "#'  . $grid_name . '";'; //<?php echo $grid_name;
        echo 'var keycolumnindex=' . $keycolumn_index  . ';'; // <?php echo $keycolumn_index
        echo '$grid={};';
        echo 'var sortcols =';
            $prefix = "[";
            $suffix = "]";
            $colcnt = $prefix;
            for($x=0;$x<count($table_detail);$x++)
            {
                if($x<count($table_detail)-1) {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "',";
                }else {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "'" . $suffix . ";";
                }
            }            
            echo $colcnt;        
        //var newObj = jQuery.noConflict();
//        echo 'newObj' . '.colModel=[];';
//        echo 'newObj.colModel[0] = { dataIndx: 0, editable: false, sortable: false, title: "", width: 30, align: "center", resizable: false, render: function (ui) {';
//        echo '  var rowData = ui.rowData, dataIndx = ui.dataIndx;';
//        echo '        var val = rowData[dataIndx];';
//        echo '        str = "";';
//        echo '        if (val) {';
//        echo '            str = "' . 'checked=' . "'checked';" . '"';
//        echo '        }';
//        echo '        return "' . "<input type='checkbox'" .  ' + str +  />"';
//        echo '    }, className: "checkboxColumn"';
//        echo '    };';        
        
            for($x=0;$x<count($table_detail);$x++)
            {
                $tmp = $table_detail[$x]->tfd_editable;
                if($tmp==1) {
                    $editable="true";
                }else {
                    $editable="false";
                }
                echo 'newObj' . '.colModel['. ($x+1) . '] = { title: "' . $table_detail[$x]->tfd_coldesc . '",width:' . $table_detail[$x]->tfd_colwidth . ' ,dataIndx: "' . $table_detail[$x]->tfd_colname . '",editable:' . $editable  . '};';
            }            
        //echo 'newObj = newObj'  . ';';
        echo '</script>';        
     ?>  
    <div class="frameintab-pendaftaran"> 
        <!--
        <div style="background-color:#993;padding:20px;">
            <marquee><strong>
            WARNING ! : MODUL PENDAFTARAN EXPIRED PADA 31 Desember 2014
                </strong>
            </marquee>
        </div>
        -->
            <div class="grid regist">
                    <div class="span-grid">
                        <div id="<?php echo $grid_name; ?>">                            
                        </div>
                    </div>                    
                    <!--bukan row, maka clear both, supaya div berikut muncul di bawahnya-->
                    <div style="clear:both">&nbsp;</div>
                    <div >
                        <div class="panel" data-role="panel">
                            <div class="panel-header bg-darkRed fg-white" id="regist_sum_panel_header">
                                Data Supplier
                            </div>
                            <div class="panel-content">
                                <table>
                                    <tr> <td>ID Supplier</td><td><input type="text" id="regist_id_supp"><td>&nbsp;</td></tr>
                                    <tr> <td>Nama</td><td colspan="2"><input type="text" id="regist_supp_name"></tr>
                                    
                                </table>                                
                                <div class="regist_two_col_container">
                                    <div id="regist_payment">
                                        <table border="0">
                                            <tr>
                                                <td><button id="save_trans_in">Simpan</button></td>
                                                <td><input type="hidden" id="trans_in_id"/> <input type="hidden" id="payment_id"/><button id="print_trans_in">Cetak</button>&nbsp;<!--<button id="print_surat_jalan">Surat Jalan</button>--></td>
                                            </tr>
                                        </table>
                                        
                                    </div>               
                                    
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
    </div>
<script type="text/javascript" src="/pos/asset2/pos/js/regist-in.js"></script>
<script type="text/javascript" src="/pos/asset2/pos/js/regist-in-ui.js"></script>
