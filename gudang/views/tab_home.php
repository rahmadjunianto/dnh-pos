    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
		var site_url = "<?php echo site_url(); ?>";
    </script>
	<?php foreach ($this->html_headers->scripts as $script): ?>
		<script type="text/javascript" src="<?php echo $script; ?>"></script>
	<?php endforeach; ?>
	<?php foreach ($this->html_headers->styles as $style): ?>
		<link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
	<?php endforeach; ?>

<div class="frameintab">
          <!--grid regist-->
          <div id="grid_regist">              
          </div> 
          <!--end of grid regist-->
      </div>
<table width="610" id="nasdaq_market_table" cellspacing="1" cellpadding="0" style="display:none;">
<tr><th>Company Name</th><th>Symbol</th><th>Chart</th><th>Price</th><th>Change</th><th>% Change</th><th>Volume</th></tr>
<tr class="ts0"><td align="left">Sirius Xm Radio Inc. </td><td>SIRI</td><td></td><td align="left">2.70</td><td align="right">+0.01</td><td align="right">+0.37%</td><td align="right">81,250,806</td></tr>
<tr class="ts1"><td align="left">Intel</td><td>INTC</td><td></td><td align="left">20.26</td><td align="right">+0.07</td><td align="right">+0.35%</td><td align="right">69,433,790</td></tr>
<tr class="ts0"><td align="left">Microsoft</td><td>MSFT</td><td></td><td align="left">26.74</td><td align="right">+0.22</td><td align="right">+0.83%</td><td align="right">57,179,237</td></tr>
<tr class="ts1"><td align="left">Research in Motion Limited </td><td>RIMM</td><td></td><td align="left">9.58</td><td align="right">+0.38</td><td align="right">+4.13%</td><td align="right">42,529,737</td></tr>
<tr class="ts0"><td align="left">Cisco Systems </td><td>CSCO</td><td></td><td align="left">18.31</td><td align="right">+0.32</td><td align="right">+1.78%</td><td align="right">40,616,346</td></tr>
<tr class="ts1"><td align="left">Yahoo! Inc. </td><td>YHOO</td><td></td><td align="left">18.36</td><td align="right">+0.50</td><td align="right">+2.80%</td><td align="right">32,997,005</td></tr>
<tr class="ts0"><td align="left">Dell</td><td>DELL</td><td></td><td align="left">9.12</td><td align="right">+0.26</td><td align="right">+2.95%</td><td align="right">29,800,460</td></tr>
<tr class="ts1"><td align="left">Apple</td><td>AAPL</td><td></td><td align="left">566.06</td><td align="right">+38.38</td><td align="right">+7.27%</td><td align="right">29,398,995</td></tr>
<tr class="ts0"><td align="left">Oracle </td><td>ORCL</td><td></td><td align="left">30.14</td><td align="right">+0.14</td><td align="right">+0.47%</td><td align="right">22,551,075</td></tr>
<tr class="ts1"><td align="left">Applied Materials </td><td>AMAT</td><td></td><td align="left">10.35</td><td align="right">+0.20</td><td align="right">+1.97%</td><td align="right">15,542,402</td></tr>
<tr class="ts0"><td align="left">Brocade Communications Systems </td><td>BRCD</td><td></td><td align="left">5.56</td><td align="right">+0.30</td><td align="right">+5.70%</td><td align="right">14,598,474</td></tr>
<tr class="ts1"><td align="left">News </td><td>NWSA</td><td></td><td align="left">23.96</td><td align="right">+0.67</td><td align="right">+2.88%</td><td align="right">14,096,772</td></tr>
<tr class="ts0"><td align="left">Dynavax Technologies </td><td>DVAX</td><td></td><td align="left">2.64</td><td align="right">+0.20</td><td align="right">+8.20%</td><td align="right">12,758,155</td></tr>
<tr class="ts1"><td align="left">Qualcomm Incorporated </td><td>QCOM</td><td></td><td align="left">62.12</td><td align="right">+0.19</td><td align="right">+0.31%</td><td align="right">12,169,272</td></tr>
<tr class="ts0"><td align="left">Activision Blizzard </td><td>ATVI</td><td></td><td align="left">11.23</td><td align="right">+0.18</td><td align="right">+1.62%</td><td align="right">11,940,358</td></tr>
<tr class="ts1"><td align="left">Huntington Bancshares</td><td>HBAN</td><td></td><td align="left">6.11</td><td align="right">+0.10</td><td align="right">+1.66%</td><td align="right">11,448,001</td></tr>
<tr class="ts0"><td align="left">Ftb</td><td>FITB</td><td></td><td align="left">14.47</td><td align="right">+0.37</td><td align="right">+2.62%</td><td align="right">11,313,550</td></tr>
<tr class="ts1"><td align="left">Comcast </td><td>CMCSA</td><td></td><td align="left">36.01</td><td align="right">+0.56</td><td align="right">+1.58%</td><td align="right">10,694,461</td></tr>
<tr class="ts0"><td align="left">Marvell</td><td>MRVL</td><td></td><td align="left">7.70</td><td align="right">+0.30</td><td align="right">+4.06%</td><td align="right">10,262,907</td></tr>
<tr class="ts1"><td align="left">Arena</td><td>ARNA</td><td></td><td align="left">8.98</td><td align="right">+0.48</td><td align="right">+5.65%</td><td align="right">10,182,512</td></tr>
<tr class="ts0"><td align="left">Nvidia </td><td>NVDA</td><td></td><td align="left">11.70</td><td align="right">+0.32</td><td align="right">+2.77%</td><td align="right">10,103,195</td></tr>
<tr class="ts1"><td align="left">Staples </td><td>SPLS</td><td></td><td align="left">12.20</td><td align="right">+0.47</td><td align="right">+4.01%</td><td align="right">9,588,436</td></tr>
<tr class="ts0"><td align="left">Seagate Technology </td><td>STX</td><td></td><td align="left">27.12</td><td align="right">+0.01</td><td align="right">+0.02%</td><td align="right">9,254,477</td></tr>
<tr class="ts1"><td align="left">Gilead Sciences </td><td>GILD</td><td></td><td align="left">74.82</td><td align="right">+0.42</td><td align="right">+0.56%</td><td align="right">9,165,000</td></tr>
<tr class="ts0"><td align="left">Ebay Inc. </td><td>EBAY</td><td></td><td align="left">47.95</td><td align="right">+0.69</td><td align="right">+1.46%</td><td align="right">8,928,825</td></tr>
<tr class="ts1"><td align="left">Sina </td><td>SINA</td><td></td><td align="left">48.66</td><td align="right">+3.59</td><td align="right">+7.97%</td><td align="right">8,158,039</td></tr>
<tr class="ts0"><td align="left">Express Scripts </td><td>ESRX</td><td></td><td align="left">52.16</td><td align="right">+0.03</td><td align="right">+0.06%</td><td align="right">8,051,891</td></tr>
<tr class="ts1"><td align="left">American Capital Agency </td><td>AGNC</td><td></td><td align="left">31.12</td><td align="right">+0.13</td><td align="right">+0.42%</td><td align="right">8,008,496</td></tr>
<tr class="ts0"><td align="left">Broadcom </td><td>BRCM</td><td></td><td align="left">31.25</td><td align="right">+0.78</td><td align="right">+2.56%</td><td align="right">7,879,259</td></tr>
<tr class="ts1"><td align="left">Clearwire </td><td>CLWR</td><td></td><td align="left">2.21</td><td align="right">+0.02</td><td align="right">+0.91%</td><td align="right">7,690,019</td></tr>
<tr class="ts0"><td align="left">Green Mountain Coffee Roasters </td><td>GMCR</td><td></td><td align="left">27.29</td><td align="right">+2.74</td><td align="right">+11.16%</td><td align="right">7,680,811</td></tr>
<tr class="ts1"><td align="left">Riverbed Technology </td><td>RVBD</td><td></td><td align="left">17.26</td><td align="right">+0.78</td><td align="right">+4.73%</td><td align="right">7,659,634</td></tr>
<tr class="ts0"><td align="left">Mylan Inc. </td><td>MYL</td><td></td><td align="left">25.96</td><td align="right">+0.42</td><td align="right">+1.64%</td><td align="right">6,555,432</td></tr>
<tr class="ts1"><td align="left">Symantec </td><td>SYMC</td><td></td><td align="left">18.19</td><td align="right">+0.22</td><td align="right">+1.22%</td><td align="right">6,376,310</td></tr>
<tr class="ts0"><td align="left">Starbucks </td><td>SBUX</td><td></td><td align="left">49.76</td><td align="right">+1.00</td><td align="right">+2.05%</td><td align="right">6,335,188</td></tr>
<tr class="ts1"><td align="left">Netapp </td><td>NTAP</td><td></td><td align="left">30.88</td><td align="right">+0.62</td><td align="right">+2.05%</td><td align="right">6,270,638</td></tr>
<tr class="ts0"><td align="left">Melco Crown Entertainment Limited</td><td>MPEL</td><td></td><td align="left">14.50</td><td align="right">+0.67</td><td align="right">+4.84%</td><td align="right">6,024,739</td></tr>
<tr class="ts1"><td align="left">Skyworks Solutions </td><td>SWKS</td><td></td><td align="left">20.66</td><td align="right">+0.84</td><td align="right">+4.23%</td><td align="right">6,012,725</td></tr>
<tr class="ts0"><td align="left">Nii Holdings </td><td>NIHD</td><td></td><td align="left">5.06</td><td align="right">-0.17</td><td align="right">-3.25%</td><td align="right">5,978,970</td></tr>
<tr class="ts1"><td align="left">Nuance Communications</td><td>NUAN</td><td></td><td align="left">21.58</td><td align="right">+1.23</td><td align="right">+6.04%</td><td align="right">5,767,351</td></tr>
<tr class="ts0"><td align="left">Sandisk</td><td>SNDK</td><td></td><td align="left">39.30</td><td align="right">-0.16</td><td align="right">-0.41%</td><td align="right">5,738,722</td></tr>
<tr class="ts1"><td align="left">Urban Outfitters </td><td>URBN</td><td></td><td align="left">37.07</td><td align="right">+2.02</td><td align="right">+5.76%</td><td align="right">5,386,582</td></tr>
<tr class="ts0"><td align="left">Aruba Networks </td><td>ARUN</td><td></td><td align="left">18.53</td><td align="right">-0.30</td><td align="right">-1.59%</td><td align="right">5,281,506</td></tr>
<tr class="ts1"><td align="left">Dryships</td><td>DRYS</td><td></td><td align="left">1.68</td><td align="right">-0.02</td><td align="right">-1.18%</td><td align="right">5,243,539</td></tr>
<tr class="ts0"><td align="left">Cirrus Logic</td><td>CRUS</td><td></td><td align="left">30.84</td><td align="right">+2.57</td><td align="right">+9.09%</td><td align="right">4,990,237</td></tr>
<tr class="ts1"><td align="left">Baidu</td><td>BIDU</td><td></td><td align="left">92.41</td><td align="right">-0.27</td><td align="right">-0.29%</td><td align="right">4,989,128</td></tr>
<tr class="ts0"><td align="left">Dollar Tree </td><td>DLTR</td><td></td><td align="left">40.44</td><td align="right">+1.62</td><td align="right">+4.17%</td><td align="right">4,760,360</td></tr>
<tr class="ts1"><td align="left">Atmel </td><td>ATML</td><td></td><td align="left">4.73</td><td align="right">+0.29</td><td align="right">+6.54%</td><td align="right">4,731,647</td></tr>
<tr class="ts0"><td align="left">The Directv Grp. - Cmn Stk</td><td>DTV</td><td></td><td align="left">49.09</td><td align="right">+0.10</td><td align="right">+0.20%</td><td align="right">4,725,443</td></tr>
<tr class="ts1"><td align="left">Virgin Media Inc. </td><td>VMED</td><td></td><td align="left">33.63</td><td align="right">+1.12</td><td align="right">+3.45%</td><td align="right">4,601,339</td></tr>
<tr class="ts0"><td align="left">On Semiconductor </td><td>ONNN</td><td></td><td align="left">5.93</td><td align="right">+0.11</td><td align="right">+1.89%</td><td align="right">4,566,807</td></tr>
<tr class="ts1"><td align="left">Hudson City Bancorp</td><td>HCBK</td><td></td><td align="left">8.04</td><td align="right">+0.09</td><td align="right">+1.07%</td><td align="right">4,285,937</td></tr>
<tr class="ts0"><td align="left">Amarin</td><td>AMRN</td><td></td><td align="left">10.62</td><td align="right">-0.22</td><td align="right">-2.03%</td><td align="right">4,284,533</td></tr>
<tr class="ts1"><td align="left">Dish Network </td><td>DISH</td><td></td><td align="left">35.02</td><td align="right">-0.53</td><td align="right">-1.49%</td><td align="right">4,212,883</td></tr>
<tr class="ts0"><td align="left">Allscripts-misys Healthcare Solutions</td><td>MDRX</td><td></td><td align="left">12.26</td><td align="right">+0.10</td><td align="right">+0.82%</td><td align="right">4,139,064</td></tr>
<tr class="ts1"><td align="left">Hologic </td><td>HOLX</td><td></td><td align="left">19.45</td><td align="right">-0.05</td><td align="right">-0.26%</td><td align="right">3,940,160</td></tr>
<tr class="ts0"><td align="left">Acacia</td><td>ACTG</td><td></td><td align="left">20.42</td><td align="right">-3.02</td><td align="right">-12.88%</td><td align="right">3,912,198</td></tr>
<tr class="ts1"><td align="left">Biomimetic Therapeutics </td><td>BMTI</td><td></td><td align="left">7.30</td><td align="right">+3.15</td><td align="right">+75.90%</td><td align="right">3,838,551</td></tr>
<tr class="ts0"><td align="left">Amgen Inc. </td><td>AMGN</td><td></td><td align="left">85.39</td><td align="right">+0.85</td><td align="right">+1.01%</td><td align="right">3,796,311</td></tr>
<tr class="ts1"><td align="left">Verisign </td><td>VRSN</td><td></td><td align="left">40.62</td><td align="right">-0.30</td><td align="right">-0.74%</td><td align="right">3,760,446</td></tr>
<tr class="ts0"><td align="left">Warner Chilcott Limited </td><td>WCRX</td><td></td><td align="left">11.99</td><td align="right">+0.01</td><td align="right">+0.08%</td><td align="right">3,731,682</td></tr>
<tr class="ts1"><td align="left">Leap Wireless</td><td>LEAP</td><td></td><td align="left">6.30</td><td align="right">+0.33</td><td align="right">+5.53%</td><td align="right">3,709,778</td></tr>
<tr class="ts0"><td align="left">Ca Inc. </td><td>CA</td><td></td><td align="left">21.98</td><td align="right">+0.17</td><td align="right">+0.78%</td><td align="right">3,688,920</td></tr>
<tr class="ts1"><td align="left">Jds Uniphase </td><td>JDSU</td><td></td><td align="left">11.06</td><td align="right">+0.26</td><td align="right">+2.36%</td><td align="right">3,637,923</td></tr>
<tr class="ts0"><td align="left">Amazon.com </td><td>AMZN</td><td></td><td align="left">229.62</td><td align="right">+4.39</td><td align="right">+1.95%</td><td align="right">3,593,171</td></tr>
<tr class="ts1"><td align="left">Vivus </td><td>VVUS</td><td></td><td align="left">10.28</td><td align="right">-0.05</td><td align="right">-0.48%</td><td align="right">3,532,629</td></tr>
<tr class="ts0"><td align="left">Celgene</td><td>CELG</td><td></td><td align="left">75.17</td><td align="right">+0.84</td><td align="right">+1.13%</td><td align="right">3,510,318</td></tr>
<tr class="ts1"><td align="left">Autodesk </td><td>ADSK</td><td></td><td align="left">31.31</td><td align="right">-0.17</td><td align="right">-0.54%</td><td align="right">3,467,705</td></tr>
<tr class="ts0"><td align="left">Ross Stores</td><td>ROST</td><td></td><td align="left">55.53</td><td align="right">+1.79</td><td align="right">+3.33%</td><td align="right">3,378,375</td></tr>
<tr class="ts1"><td align="left">Netflix </td><td>NFLX</td><td></td><td align="left">81.38</td><td align="right">+0.48</td><td align="right">+0.59%</td><td align="right">3,325,462</td></tr>
<tr class="ts0"><td align="left">Altera</td><td>ALTR</td><td></td><td align="left">31.10</td><td align="right">+0.65</td><td align="right">+2.13%</td><td align="right">3,311,009</td></tr>
<tr class="ts1"><td align="left">E*trade Financial </td><td>ETFC</td><td></td><td align="left">8.18</td><td align="right">+0.30</td><td align="right">+3.81%</td><td align="right">3,305,654</td></tr>
<tr class="ts0"><td align="left">Compuware </td><td>CPWR</td><td></td><td align="left">8.49</td><td align="right">+0.04</td><td align="right">+0.47%</td><td align="right">3,273,667</td></tr>
<tr class="ts1"><td align="left">Ocular Sciences</td><td>OCLR</td><td></td><td align="left">1.54</td><td align="right">+0.25</td><td align="right">+19.38%</td><td align="right">3,253,864</td></tr>
<tr class="ts0"><td align="left">Bgc Partners </td><td>BGCP</td><td></td><td align="left">3.66</td><td align="right">+0.33</td><td align="right">+9.91%</td><td align="right">3,234,387</td></tr>
<tr class="ts1"><td align="left">Prospect Energy</td><td>PSEC</td><td></td><td align="left">10.61</td><td align="right">+0.24</td><td align="right">+2.31%</td><td align="right">3,219,966</td></tr>
<tr class="ts0"><td align="left">Flextronics</td><td>FLEX</td><td></td><td align="left">5.80</td><td align="right">+0.26</td><td align="right">+4.69%</td><td align="right">3,171,138</td></tr>
<tr class="ts1"><td align="left">Comcast</td><td>CMCSK</td><td></td><td align="left">35.07</td><td align="right">+0.58</td><td align="right">+1.68%</td><td align="right">3,151,089</td></tr>
<tr class="ts0"><td align="left">Take Two</td><td>TTWO</td><td></td><td align="left">12.36</td><td align="right">+0.51</td><td align="right">+4.26%</td><td align="right">3,001,856</td></tr>
<tr class="ts1"><td align="left">Citrix</td><td>CTXS</td><td></td><td align="left">61.17</td><td align="right">+1.96</td><td align="right">+3.31%</td><td align="right">2,990,489</td></tr>
<tr class="ts0"><td align="left">Acadia</td><td>ACAD</td><td></td><td align="left">2.26</td><td align="right">+0.35</td><td align="right">+18.32%</td><td align="right">2,986,958</td></tr>
<tr class="ts1"><td align="left">Lam Research </td><td>LRCX</td><td></td><td align="left">34.69</td><td align="right">+0.17</td><td align="right">+0.49%</td><td align="right">2,886,352</td></tr>
<tr class="ts0"><td align="left">Costco Wholesale </td><td>COST</td><td></td><td align="left">96.57</td><td align="right">+0.88</td><td align="right">+0.92%</td><td align="right">2,877,424</td></tr>
<tr class="ts1"><td align="left">Arm Holdings Plc Ads</td><td>ARMH</td><td></td><td align="left">35.72</td><td align="right">+1.30</td><td align="right">+3.78%</td><td align="right">2,822,111</td></tr>
<tr class="ts0"><td align="left">Avanir Pharmaceuticals </td><td>AVNR</td><td></td><td align="left">2.60</td><td align="right">+0.37</td><td align="right">+16.37%</td><td align="right">2,800,294</td></tr>
</table>
                
<script type="text/javascript" src="/sms/asset2/pos/js/regist3.js"></script>