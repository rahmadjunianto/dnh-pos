<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style>
    @page { margin: 160px 40px; }
    
    #header {
        position: fixed; 
        left: 0px; 
        top: 10px; 
        right: 0px; 
        height: 180px; 
        text-align: center; 
        
    }    
    #footer { 
        position: fixed; 
        left: 30px;
        bottom: -180px; 
        right: 0px; 
        height: 150px;        
        //border:1px solid;
        width:500px;
        font-size:12px;
    }
    #footer_content {
        
        height:100px;
        width:400px;
        margin-left:100px;
    }
    #container_header_result_detail { 
        position:fixed;
        border : 1px solid;
        top :138px;     
        
    }    
    #content {
            position:relative;
            /*adjust ketinggian content di sini*/
            top:165px;
            border:1px solid;
            height:615px;
            
    }
    .header_result_detail {            
        //page-break-after: always;
        top:-200px;
        
    }
    .spec_body {
            font-size:14px;
            font-family: Helvetica, serif;
        
    }
    /*
    .spacer1 {
	height:10px;
    }*/
    .hrd_col1 {
        width:190px;
    }
    .hrd_col2 {
        width:190px;
        text-align:left;
        padding-left:30px;
    }    
    .hrd_col3 {
        width:220px;
        padding-left:-10px;
    }    
    .hrd_col4 {
        width:50px;
        padding-left:-10px;
    }    
    .b_hrd_col1 {
        width:180px;
    }
    .b_hrd_col2 {
        width:170px;  
        padding-left:30px;
    }    
    .b_hrd_col3 {
        width:200px;
    }    
    .b_hrd_col4 {
        width:90px;
    }       
    /*#footer .page:after { content: counter(page, upper-roman); }*/
    body {
        margin:     0;
        padding:    0;
        margin-top:0;
        
    }
    .table_container{
        border: 0px solid;
        width:500px;
    }
    .inner_header_top{
        border:1px solid;
        height:0px;
    }
    .inner_header_bottom{
        border:1px solid;
        height:0px;
    }
    .inner_separator{
        border:0.1px dashed;
        height:0px;
    }    
    .res_col1 {
        width:80px;
    }
    .res_col2 {
        width:5px;
        padding-left:10px;
    }    
    .res_col3 {
        width:160px;
        
    }
    .res_col4 {
        width:100px;
    }
    
    .res_col5 {
        width:5px;
    }
    .res_col6 {
        width:100px;
    }
    
    
    /* Printable area */
    #print-area {
            position:   relative;
            top:        0cm;
            left:       0cm;
            /*height:     27.6cm;*/
            font-size:  14px;
            
    }
    .ligther{font-weight: lighter;}
    .strong{font-weight: bolder;}
    .caps_font{text-transform:uppercase;}
    .font_0{font-size: 10px;}
    .font_1{font-size: 11px;}
    .font_2{font-size: 12px;}
    .centered{text-align: center;}
    .align_right{text-align: right;}
        /*table*/
  </style>
</head>
<body class='spec_body'>
 
  <div id="header">
      <?php
        $name =  $pxinfo[0]->name;
        $lab_id =  $pxinfo[0]->lab_id;
        $age = $pxinfo[0]->age;
        $patientid = $pxinfo[0]->patient_id;
        $addr = $pxinfo[0]->addr;
        $doctname = $pxinfo[0]->doct_name;
        $gender = $pxinfo[0]->gender;
        $phonenum = $pxinfo[0]->phonenum;
        $labdate = $pxinfo[0]->labdate;
        //$regdate = $pxinfo[0]->regdate;
      ?>
 
      <div>
        <table id="tbl_header" width="100%" >                                                        
            <tr>  
                <td class="res_col1">NO REG</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $patientid; ?></td>
                <td class="res_col4">NO LAB</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $lab_id; ?></td>
            </tr>                                                                                                                
            <tr>  
                <td class="res_col1">NAMA</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $name; ?></td>
                <td class="res_col4">TANGGAL</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $labdate; ?></td>
            </tr>                                                                                                                
            <tr >  
                <td class="res_col1">ALAMAT</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $addr; ?></td>
                <td class="res_col4">JENIS KELAMIN</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $gender; ?></td>
            </tr>                     
            <tr>  
                <td class="res_col1">DOKTER</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $doctname; ?></td>
                <td class="res_col4">UMUR</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $age; ?></td>
            </tr>                                                                             
        </table>
    </div>
<!--    <div class="spacer1">&nbsp;</div>-->
  </div> <!--end of header-->
    <div id="container_header_result_detail">
        <table class="header_result_detail">
            <tr>
                <td class="hrd_col1">JENIS PEMERIKSAAN</td>
                <td class="hrd_col2">HASIL</td>
                <td class="hrd_col3">NILAI RUJUKAN</td>
                <td class="hrd_col4">SATUAN</td>
            </tr>
        </table>
    </div> 
  
  <div id="content">
                                                    <table border="0" style="width: 100%; border-bottom:1px solid #ccc" >
                                                        </tr>
                                                        <?php
$pxname="";
$pxvalue="";
$pxunit="";                                                               
$pxnormal="";
$counter_title=0;
$last_group_title="";
$lab_note = $note;
//$listpx[0]['note'];
                                                                    for($x=0;$x<count($listpx);$x++) {
                                                                        $parent= $listpx[$x]['pxparent'];
                                                                        $pxorder= $listpx[$x]['pxorder'];
                                                                        //                                                                        
                                                                        $group_title= $parent;
                                                                        
                                                                        $row = $x+1;
                                                                        //echo "$row";
                                                                        $pxname = $listpx[$x]['pxname'];
                                                                        if($pxname=="") {
                                                                            $pxname="&nbsp;";
                                                                        }
                                                                        $pxvalue = $listpx[$x]['pxvalue'];
                                                                        if($pxvalue=="") {
                                                                            $pxvalue="&nbsp;";
                                                                        }                                                                        
                                                                        $pxnormal = $listpx[$x]['pxnormal'];
                                                                        if($pxnormal=="") {
                                                                            $pxnormal="&nbsp;";
                                                                        }                                                                                   
                                                                        $pxunit = $listpx[$x]['pxunit'];
                                                                        if($pxunit=="") {
                                                                            $pxunit="&nbsp;";
                                                                        }                                                                                    
                                                                        $str_row="";
                                                                        if($last_group_title!=$group_title)  {
                                                                            if(($last_group_title=="")) {
                                                                                $str_row = "";
                                                                                $str_row = $str_row . "<tr><td class='b_hrd_col1' colspan='2'><strong>$group_title</strong></tr>";
                                                                                $str_row = $str_row  . "<tr><td class='b_hrd_col1'>$pxname</td><td class='b_hrd_col2'>$pxvalue</td><td class='b_hrd_col3'>$pxnormal</td><td class='b_hrd_col4'>$pxunit</td></tr>";                                                                                
                                                                            }else {
                                                                                $str_row = "<tr><td colspan='4'><div class='inner_separator'></div></td></tr>";
                                                                                $str_row = $str_row . "<tr><td class='b_hrd_col1' colspan='2'><strong>$group_title</strong></tr>";
                                                                                $str_row = $str_row  . "<tr><td class='b_hrd_col1'>$pxname</td><td class='b_hrd_col2'>$pxvalue</td><td class='b_hrd_col3'>$pxnormal</td><td class='b_hrd_col4'>$pxunit</td></tr>";
                                                                            }    
                                                                        }else {                                                                             
                                                                            $str_row = "<tr><td class='b_hrd_col1'>$pxname</td><td class='b_hrd_col2'>$pxvalue</td><td class='b_hrd_col3'>$pxnormal</td><td class='b_hrd_col4'>$pxunit</td></tr>";
                                                                        }
                                                                        echo $str_row;
                                                                        $last_group_title=$group_title;
                                                                    }
                                                        ?>
                                                    </table>    

  </div>
<div id="footer">
    <div id="footer_content">
        Catatan : <?php echo "<pre>". $lab_note . "</pre>";?>
    </div>
    
</div>    
 
</body>
</html>
