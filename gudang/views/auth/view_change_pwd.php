<div id="div_judul" class="alert alert-success" >
	<div id="judul_form"><?php echo $this->html_headers->title; ?></div>
	<div id="sub_judul_form"><?php echo $this->html_headers->description; ?></div>
</div>
<div id="div_data">
	<form id="frm_data" >
		<table id="tbl_data">
			<tr>
				<td>Nama User</td>
				<td>:</td>
				<td><input type="text" id="user_nama" value="<?php echo $user_nama; ?>" readonly="true"/></td>
			</tr>
			<tr>
				<td>Password Lama</td>
				<td>:</td>
				<td><input type="password" id="pwd_lama" value="" tabindex="1"/></td>
			</tr>
			<tr>
				<td>Password Baru</td>
				<td>:</td>
				<td><input type="password" id="pwd_baru1" value="" tabindex="2"/></td>
			</tr>
			<tr>
				<td>Ulangi Password Baru</td>
				<td>:&nbsp;</td>
				<td><input type="password" id="pwd_baru2" value="" tabindex="3"/></td>
			</tr>
			<tr>
				<td><input type="button" id="btn_ok" value="SIMPAN" tabindex="4" class="btn btn-primary"/></td>
				<td></td>
				<td><input type="reset" id="btn_reset" value="RESET" tabindex="5" class="btn"/></td>
			</tr>
		</table>
	</form>
</div>
<style>

</style>
<script type="text/javascript">
//	var source_page='<?php echo $source_page;?>';
	$(document).ready(function(){
		debugger;
		$('#btn_ok').bind('click',simpan);
	});
	function simpan(){
		var pwd_lama=$('#pwd_lama').val();
		var pwd_baru1=$('#pwd_baru1').val();
		var pwd_baru2=$('#pwd_baru2').val();
		if ($.trim(pwd_baru1) =='' || pwd_baru1.length <4){
			showError('password baru tidak boleh kosong dan minimal 4 karakter');
			return;
		}
		if (pwd_baru1 != pwd_baru2){
			showError('password baru yang pertama & kedua harus sama');
			return;
		}
		$.ajax({	
			url: source_page+'/simpan',
			type:'POST',
			dataType:'json',
			data:{ pwd_lama: pwd_lama, 
				pwd_baru : pwd_baru1
			},
			success:function(data){
				if (data.status=='ok'){
					showSukses('Ganti Password SUKSES');
				}
				else{
					showError(data.err_msg);
				}
			},
			error: function(xhr,ajaxOptions,thrownError){
				ajaxError(xhr,ajaxOptions,thrownError);
			}
		});
	}
	
</script>