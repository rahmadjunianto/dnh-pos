<div id="div_judul" class="alert alert-success">
	<div id="judul_form"><?php echo $this->html_headers->title; ?></div>
	<div id="sub_judul_form"><?php echo $this->html_headers->description; ?></div>
</div>
<div id="div_data">
	<form id="frm_data">
		<table id="tbl_grid">
		</table>
		<table id="tbl_data">
			<tr>
				<td>ID</td>
				<td>:&nbsp;	</td>
				<td><input type="text" id="user_id" value="" readonly="true"/></td>
			</tr>
			<tr>
				<td>Nama User</td>
				<td>:</td>
				<td><input type="text" id="user_nama" value="" readonly="true"/></td>
			</tr>
			<tr>
				<td>Password Baru</td>
				<td>:</td>
				<td><input type="password" id="pwd_baru" value="" tabindex="1"/></td>
			</tr>
			<tr>
				<td>Ulangi Password Baru</td>
				<td>:</td>
				<td><input type="password" id="pwd_baru_re" value="" tabindex="2"/></td>
			</tr>
			<tr>
				<td><input type="button" id="btn_ok" value="SIMPAN" class="btn btn-primary"/></td>
				<td></td>
				<td><input type="reset" id="btn_reset" value="RESET" class="btn "/></td>
			</tr>
		</table>
	</form>
</div>
<style>
	#tbl_grid thead{
		background-color: #A8A88D;
	}
	#tbl_grid thead,#tbl_grid tbody{
		max-height: 250px;
		overflow-y: auto;
		display: block;
	}
	.col_id{
		width: 50px;
	}
	.col_nama{
		width: 350px;
	}
	.col_username{
		width: 50px;
	}
</style>
<script type="text/javascript">
	var gridNode;
	$(document).ready(function(){
		debugger;
		gridNode=$('#tbl_grid');
		loadGrid();
		$('#btn_ok').bind('click',simpan);
	});
	function setGridHeader(){
		var header={
			col_id:'ID',
			col_nama:'Nama',
			col_username:'Username'
		}
		grid.setHeader(gridNode, header,'btn-success');
		grid.setStyle(gridNode, "table table-condensed table-bordered");
		grid.rowSelectedClass='ui-state-highlight';
		grid.onRowClick=loadDetailData;
	}
	function loadDetailData(rowNode){
		$('#user_id').val( $(rowNode).children('.col_id').html());
		$('#user_nama').val( $(rowNode).children('.col_nama').html());
		$('#pwd_baru').val('');
		$('#pwd_baru_re').val('');
	}
	function loadGrid(){
		setGridHeader();
		gridNode.children('tbody').empty();
		showLoading();
		$.ajax({	
			url: source_page+'/load_grid',
			type:'POST',
			dataType:'json',
			data:{ 
			},
			success:function(data){
				
				if (data.status=='ok'){
					$.each(data.rs, function(i,val){
						var row={
							col_id: val.id,
							col_nama: val.nama,
							col_username: val.username
						};
						grid.addRow(gridNode, row);
					});
				}
				else{
					showError(data.err_msg);
				}
				hideLoading();
			},
			error: function(xhr,ajaxOptions,thrownError){
				ajaxError(xhr,ajaxOptions,thrownError);
			}
		});
		
	}
	function simpan(){
		var user_id=$('#user_id').val();
		var pwd_baru=$('#pwd_baru').val();
		var pwd_baru_re=$('#pwd_baru_re').val();
		
		if ($.trim(pwd_baru) =='' || pwd_baru.length <4){
			showError('password baru tidak boleh kosong dan minimal 4 karakter');
			return;
		}
		if (pwd_baru != pwd_baru_re ){
			showError('password baru yang pertama dan yang kedua harus sama');
			return;
		}
		var tombol=confirm("Apakah data sudah benar ?");
		if (! tombol) return false;
		$.ajax({	
			url: source_page+'/simpan',
			type:'POST',
			dataType:'json',
			data:{ user_id: user_id, 
				pwd_baru : pwd_baru
			},
			success:function(data){
				if (data.status=='ok'){
					showSukses('Ganti Password SUKSES');
				}
				else{
					showError(data.err_msg);
				}
			},
			error: function(xhr,ajaxOptions,thrownError){
				ajaxError(xhr,ajaxOptions,thrownError);
			}
		});
	}
	
</script>