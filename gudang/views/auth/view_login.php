<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<script type="text/javascript">
			var base_url = '<?php echo base_url(); ?>';
			var site_url = '<?php echo site_url(); ?>';
		</script>
		<?php foreach ($this->html_headers->scripts as $script): ?>
			<script type="text/javascript" src="<?php echo $script; ?>"></script>
		<?php endforeach; ?>
		<?php foreach ($this->html_headers->styles as $style): ?>
			<link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
		<?php endforeach; ?>


		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			<?php echo (isset($this->html_headers->title)) ? $this->html_headers->title : "Login "; ?>
		</title>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#user_name").select();
				$("#user_name").keydown(user_name_keydown);
				$("#user_pwd").keydown(user_pwd_keydown);
				$("#loginsubmit").keydown(loginsubmit_keydown);
				$("#loginsubmit").click(submit_user);
			});
			function user_name_focus(){
				setTimeout( function(){
					try{
						var d = document.getElementById('user_name');
						d.value = '';
						d.focus();
					}
					catch(e){}
				}, 100);
			}

			function loginsubmit_keydown(event) {
				var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
				if (key==13){
					submit_user();
				}
			}
			function user_name_keydown(event) {
				var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
				if (key==13){
					$("#user_pwd").select();
				}
			}
			function user_pwd_keydown(event) {
				var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
				if (key==13){
					submit_user();
				}
			}

			//submit user :
			//Verify User and Password
			function submit_user(){
				//	var redir=false;
                                debugger;
				var username=$("#user_name").val();
				var pass=$("#user_pwd").val();
				$.ajax( {
					url:site_url + 'login/verify',
					type:"POST",
					dataType:"json",
					data:{
						user:username,
						passwd:pass
					},
					success:function(data) {
                                                debugger;
						if(data.status=="ok")
						{
                                                        debugger;
							$("#outer_login").toggle('puff',{},5000);
							$(location).attr('href',data.redirurl);
						}else {
							//$("#outer_login").toggle('shake',{},100);
							$("#user_name").select();
							$("#user_name").val("");
							$("#user_pwd").val("");
                                                        $("#user_info").text("user/password salah!");
							//showError(data.err_msg, user_name_focus);
						}

					}
				});
				//	return redir;
				//return false untuk tidak berpindah ke halaman submit
			}
		</script>
		<style>
			#box_username {
				position:absolute;
				top:80px;
				left:100px;
			}
			#box_passwd {
				position:absolute;
				top:105px;
				left:100px;
			}
			#box_submit {
				position:absolute;
				top:130px;
				left:280px;
			}
			#user_pwd {
				position:absolute;
				top:0px;
				left:70px;
			}
			#user_name {
				position:absolute;
				top:0px;
				left:70px;
			}
			#label_notification {
				position:absolute;
				top:20px;
				left:70px;
			}
			.flat_button {
				color:#050;
				font: bold 120% 'trebuchet ms',helvetica,sans-serif;
				background-color:#fed;
				border:1px solid;
				border-color: #696 #363 #363 #696;
				cursor:pointer;
			}

			.centered{
				display:block;
				/*set the div in the center of the screen*/
				position:absolute;
				top:40%;
				left:35%;
				width:350px;
			}
			.outer_login{
				float:left;
				background:#544681;
				width:400px;
				height:200px;
			}
			.inner_login{
				float:left;
				background:#FFF repeat;
				margin-top:40px;
				margin-left:2px;
				margin-right:2px;
				width:396px;
				height:158px
			}
			.input_login {
				display:block;
			}
			.input_label {
				float:left;
			}
		</style>
	</head>
	<body class="metro">
                     <div style="width:100%;height:100%;padding:20px;">
                        <div class="carousel" id="carousel2" style="width: 100%;">
                            <div class="slide" style="display: block; left: 0px;">
                                <img style="width:100%;height:100%;" src="/pos/asset2/pos/images/splash1.jpg" class="cover1">
                            </div>
                            <div class="slide" style="display: block; left: 0px;">
                                <img style="width:100%;height:100%;" src="/pos/asset2/pos/images/splash2.jpg" class="cover1">
                            </div>
                            <div class="slide" style="display: block; left: 0px;">
                                <img style="width:100%;height:100%;" src="/pos/asset2/pos/images/splash3.jpg" class="cover1">
                            </div>
                            <div class="slide" style="display: block; left: 0px;">
                                <img style="width:100%;height:100%;" src="/pos/asset2/pos/images/splash4.jpg" class="cover1">
                            </div>
                            <div class="slide" style="display: block; left: 0px;">
                                <img style="width:100%;height:100%;" src="/pos/asset2/pos/images/splash5.jpg" class="cover1">
                            </div>
                            <div class="slide" style="display: block; left: 0px;">
                                <img style="width:100%;height:100%;" src="/pos/asset2/pos/images/splash6.jpg" class="cover1">
                            </div>
                        <div class="markers square" style="right: 10px; left: auto;"><ul><li class="active"><a href="javascript:void(0)" data-num="0"></a></li><li class=""><a href="javascript:void(0)" data-num="1"></a></li><li class=""><a href="javascript:void(0)" data-num="2"></a></li></ul></div></div>
                        <script>
                            $(function(){
                                height  = $(window).height();
                                height = height-80;
                                $("#carousel2").carousel({
                                    height: height,
                                    effect: 'slowdown',
                                    markers: {
                                        show: true,
                                        type: 'square',
                                        position: 'bottom-right'
                                    }
                                });
                            })
                        </script>
                    </div>
            <!--end of carousel-->
		<div class="centered">
			<div id="outer_login" class="outer_login">
				<div class="inner_login">
					<div id="box_username" class="input_login">
						Nama User <input style="margin-left:10px;" type="text" id="user_name"/>
					</div>
					<div id="box_passwd" class="input_login">
						Password <input style="margin-left:10px;" id="user_pwd" type="password" name="user_pwd"/>
					</div>

					<div style="padding-top:20px" id="box_passwd" class="input_login">
                                            <label id="user_info">&nbsp;</label>
					</div>

					<div style="left:260px;padding-top:1px" id="box_submit" class="input_login">
						<input  class="flat_button" id="loginsubmit" type="button" value=" Login ">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
