<div id="div_judul" class="alert alert-success">
	<div id="judul_form"><?php echo $this->html_headers->title; ?></div>
	<div id="sub_judul_form"><?php echo $this->html_headers->description; ?></div>
</div>
<div id="div_data">
	<form id="frm_data">
		<table id="tbl_grid">
		</table>
		<table id="tbl_data">
			<tr>
				<td>ID</td>
				<td>:&nbsp;	</td>
				<td><input type="text" id="user_id" value="" readonly="true"/></td>
			</tr>
			<tr>
				<td>Nama User</td>
				<td>:</td>
				<td><input type="text" id="user_nama" value="" readonly="true"/></td>
			</tr>
			<tr>
				<td><input type="button" id="btn_ok" value="Run As" class="btn btn-danger"/></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</form>
</div>
<style>
	#tbl_grid thead,#tbl_grid tbody{
		max-height: 300px;
		overflow-y: auto;
		display: block;
	}
	.col_id{
		width: 50px;
	}
	.col_nama{
		width: 250px;
	}
	.col_username{
		width: 50px;
	}
</style>
<script type="text/javascript">
	var gridNode;
//	var source_page="<?php echo $source_page; ?>";
	$(document).ready(function(){
		debugger;
		gridNode=$('#tbl_grid');
		loadGrid();
		$('#btn_ok').bind('click',simpan);
		
	});
	
	function loadGrid(){
		var header={
			col_id:'ID',
			col_nama:'Nama',
			col_username:'Username'
		}
		grid.setHeader(gridNode, header,'btn-success');
		grid.setStyle(gridNode, "table table-condensed table-bordered");
		grid.rowSelectedClass='ui-state-highlight';
		grid.onRowClick=loadDetailData;
		
		gridNode.children('tbody').empty();
		showLoading();
		$.ajax({	
			url: source_page+'/load_grid',
			type:'POST',
			dataType:'json',
			async:true,
			data:{ 
			},
			success:function(data){
				if (data.status=='ok'){
					$.each(data.rs, function(i,val){
						var row={
							col_id: val.id,
							col_nama: val.nama,
							col_username: val.username
						};
						grid.addRow(gridNode, row);
					});
				}
				else{
					showError(data.err_msg);
				}
				hideLoading();
			},
			error: function(xhr,ajaxOptions,thrownError){
				ajaxError(xhr,ajaxOptions,thrownError);
			}
		});
		
	}
	function simpan(){
		var user_id=$('#user_id').val();
		var user_nama=$('#user_nama').val();
		
		var tombol=confirm("Yakin tha ?");
		if (! tombol) return false;
		$.ajax({	
			url: source_page+'/simpan',
			type:'POST',
			dataType:'json',
			data:{ user_id: user_id},
			success:function(data){
				if (data.status=='ok'){
					showSukses('Saat ini anda login sebagai '+user_nama+
						'\nHalaman akan di redirect ke menu awal', function(){
						window.location=site_url;
					});
					
				}
				else{
					showError(data.err_msg);
				}
			},
			error: function(xhr,ajaxOptions,thrownError){
				ajaxError(xhr,ajaxOptions,thrownError);
			}
		});
	}
	function loadDetailData(rowNode){
		$('#user_id').val($(rowNode).children('.col_id').html());
		$('#user_nama').val($(rowNode).children('.col_nama').html());
	};
</script>