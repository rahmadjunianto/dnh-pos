<html>
<head>
  <style>
    @page { margin: 180px 50px; }
    #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after { content: counter(page, upper-roman); }
  </style>

</head>
<body>

  <div id="header">
                                                    <table id="tbl_header" >
                                                        <tr><td colspan="4"><h1>Rontgen</center></td></tr>
                                                        <tr>    <td>Nama/Umur</td><td>:</td><td><?php echo $pxinfo[0]->name . "/" . $pxinfo[0]->age;?></td>                                                        
                                                                <td>No Lab/No MR</td><td>:</td><td><?php echo $pxinfo[0]->lab_id . "/" . $pxinfo[0]->patient_id;?></td>
                                                        </tr>                                                                                                                
                                                        <tr>
                                                            <td>Alamat</td><td>:</td><td><?php echo $pxinfo[0]->addr;?></td>                
                                                            <td>Dokter</td><td>:</td><td><?php echo $pxinfo[0]->doct_name;?></td>
                                                        </tr>
                                                    </table>
  </div>
  <div id="footer">
    <p class="page">Page </p>
  </div>
  <div id="content">
      <?php echo $result;?>
  </div>
</body>
</html>