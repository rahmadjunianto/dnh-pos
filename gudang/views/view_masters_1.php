
    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
        var site_url = "<?php echo site_url(); ?>";
        var table_id = "<?php echo $table_id;?>";
        var table_title = "<?php echo $table_title;?>";
        var default_sort_col = "<?php echo $default_sort_col;?>";
        var geturladdr = "<?php echo $geturladdr ;?>";
        var updateurl = "<?php echo $updateurl ;?>";
        var grid_name = "#<?php echo $grid_name;?>";
        var grid_name2 = "<?php echo $grid_name;?>";
        var keycolumnindex=<?php echo $keycolumn_index;?>;
        var sortcols = <?php
            $prefix = "[";
            $suffix = "]";
            $colcnt = $prefix;
            for($x=0;$x<count($table_detail);$x++)
            {
                if($x<count($table_detail)-1) {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "',";
                }else {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "'" . $suffix . ";";
                }
            }            
            echo $colcnt;
        ?>
        newObj = { width: 1200, height: 450, numberCell: true, minWidth: 10,
            title: table_title,
            bottomVisible:true,
            resizable: true, columnBorders: true,
            selectionModel: { type: 'cell', mode: 'block' },
            editModel: { clicksToEdit:2, saveKey: 13 },
            hoverMode: 'cell',
        };            
        newObj.colModel=[];
        <?php
            for($x=0;$x<count($table_detail);$x++)
            {
                $tmp = $table_detail[$x]->tfd_editable;
                if($tmp==1) {
                    $editable="true";
                }else {
                    $editable="false";
                }                    
                echo 'newObj.colModel['. $x . '] = { title: "' . $table_detail[$x]->tfd_coldesc . '",width:' . $table_detail[$x]->tfd_colwidth . ' ,dataIndx: "' . $table_detail[$x]->tfd_colname . '",editable:' . $editable  . '};';
            }            
        ?>            
    </script>
    <div class="frameintab">
          <!--grid regist-->          
          <div class="minisearch">
                <select id="search_col_criteria" class="minisearch-input-text">
                    <?php
                        for($x=0;$x<count($table_detail);$x++)
                        {
                            $table_desc = $table_detail[$x]->tfd_coldesc;
                            $table_order = $table_detail[$x]->tfd_order;                            
                            echo "<option value=$table_order>$table_desc</option>";
                        }
                        //echo "AD";                        
                    ?>
                </select>
              <input id="search_val_criteria" class="minisearch-input-text-value" type="text" placeholder="Search...">
          </div>          
          <div id="<?php echo $grid_name;?>">              
          </div> 
          <!--end of grid regist-->
    </div>                
<script type="text/javascript" src="/sms/asset2/pos/js/masters.js"></script>
<script type="text/javascript" src="/sms/asset2/pos/js/updaters.js"></script>
