<html><head>
		<style>
			/* Define page size. Requires print-area adjustment! */
                        @page { margin:-25px 0px 0px 80px; }
			body {
				margin:     0;
				padding:    0;
                                margin-top:40px;
			}
                        #header {
                            margin:0px 1px;
                        }
                        #content {
                        }
                        .table_container{
                            border: 0px solid;

                        }
                        .inner_header_top{
                            border:0.2px solid;
                            height:0px;
                        }
                        .inner_header_bottom{
                            border:0.1px solid;
                            height:0px;
                        }

                         .col1{
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }
                         .col2{
                             width:190px;
                             /*border: 1px solid #CCC;*/
                         }
                         .col3{
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         .col4{
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         .col5{
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         /*payment*/
                         .payment_col1{
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }
                         .payment_col2{
                             width:120px;
                             /*border: 1px solid #CCC;*/
                         }
                         .payment_col3{
                            width:10px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         .payment_col4{
                            width:120px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         .payment_col5{
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         /*si*/
                         .sign_col1{
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }
                         .sign_col2{
                             width:120px;
                             /*border: 1px solid #CCC;*/
                         }
                         .sign_col3{
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         .sign_col4{
                            width:60px;
                             /*border: 1px solid #CCC;*/
                         }
                         .sign_col5{
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }
                         .bio_col1 {
                             width:100px;
                             valign:top;
                         }
                         .bio_col2 {
                             width:1px;
                             valign:top;
                         }
                         .bio_col3 {
                             width:150px;
                             text-align:left;
                             valign:top;
                         }
                         .bio_col4 {
                             width:55px;
                             valign:top;
                         }
                         .bio_col5 {
                             width:5px;
                             valign:top;
                         }
                         .bio_col6 {
                             width:100px;
                             text-align:left;
                             valign:top;
                         }
			/* Printable area */
			#print-area {
				position:   relative;
				top:        0cm;
				left:       0cm;
				/*height:     27.6cm;*/
				font-size:  10px;
				font-family: Helvetica, serif;
                                width:17cm;
			}
			.ligther{font-weight: lighter;}
			.strong{font-weight: bolder;}
			.caps_font{text-transform:uppercase;}
			.font_0{font-size: 10px;}
			.font_1{font-size: 11px;}
			.font_2{font-size: 12px;}
			.centered{text-align: center;}
			.align_right{text-align: right;}
                        /*table*/
                    .detail_px {
                            font-family: verdana,arial,sans-serif;
                            font-size:10px;
                            color:#333333;
                            border-width: 0.1px;
                            border-color: #666666;
                            border-collapse: collapse;
                    }
                    #tbl_header {
                        font-family: verdana,arial,sans-serif;
                        font-size: 10px;
                        color: #333333;
                    }
		</style>
		<script type="text/javascript">
			window.onkeydown=function(o){
				o= o||event;
				if(o.keyCode == 27){ //escape char
					window.open('','_parent','');
					window.close();
				}}
		</script>
	</head><body>
		<div id="print-area">
			<div id="header">
                                                    <table id="tbl_header">

                                                        <tr><td colspan="6"><center><h1>Faktur Penjualan</h1> </center></td></tr>
                                                    <tr><td colspan="6"><center><strong>No Nota : <?php echo $payment_no;  ?> </strong></center></td></tr>

                                                        <tr>
                                                            <td class="bio_col1">Nama Pelanggan</td><td class="bio_col2">:</td><td class="bio_col3"><?php echo $pxinfo[0]->name . "(" . $pxinfo[0]->id . ")";?></td>
                                                            <td class="bio_col4">No Order</td><td class="bio_col5">:</td><td class="bio_col6"><?php echo $pxinfo[0]->tro_id;?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bio_col1" valign="top">Alamat</td><td class="bio_col2" valign="top">:</td><td class="bio_col2"><?php echo $pxinfo[0]->addr;?></td>
                                                            <td class="bio_col4" valign="top">Tanggal</td><td class="bio_col5" valign="top">:</td><td class="bio_col6" valign="top"><?php echo $pxinfo[0]->transdate;?></td>
                                                        </tr>
                                                    </table>
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                        <table class="detail_px">
                                                                <tr><td class="col1">No</td ><td class="col2">Nama Barang</td><td class="col3">Harga</td><td class="col4">Disc</td><td class="col4">Qty</td><td class="col5">Total</td></tr>
                                                        </table>
                                                        <div class="inner_header_bottom">&nbsp;</div>
                                                    </div>
                        </div>
                    <?php
                        $user_name = $logged_in_user_name;
                        $extra_disc=0;
                        $mktid="";
                    ?>
                        <div id="content">
                                                    <table class="detail_px">
                                                        <?php
                                                        //$thistime = date("H:i:s");
                                                        date_default_timezone_set('Asia/Jakarta');
                                                        $thistime = date('H:i:s', time());
                                                        $jamsekarang = $pxinfo[0]->jamsekarang;
                                                        //
                                                        //
                                                                $str_row="";
                                                                $totalprice = 0;
                                                                $totaldisc = 0;
                                                                $totalnet = 0;
                                                                for($x=0;$x<count($listpx);$x++) {
                                                                    $row = $x+1;
                                                                    $name=$listpx[$x]->trans_name;
                                                                    $price = $listpx[$x]->trans_price;
                                                                    $disc = $listpx[$x]->trans_disc;
                                                                    $qty = $listpx[$x]->trans_qty;
                                                                    $total = $listpx[$x]->trans_total;
                                                                    $totalprice = $totalprice + $total;
                                                                    $str_row = "<tr><td class='col1'>$row</td><td class='col2'>$name</td><td class='col3'>$price</td><td class='col4'>$disc</td><td class='col4'>$qty</td><td class='col5'>$total</td>";
                                                                    echo $str_row;
                                                                    $disc1=$listpx[$x]->tro_disc1;
                                                                    $disc2=$listpx[$x]->tro_disc2;
                                                                    $disc3=$listpx[$x]->tro_disc3;
                                                                    $payment_type=$listpx[$x]->tro_payment_type;
                                                                    $duedate=$listpx[$x]->duedate;
                                                                    $ppn=$listpx[$x]->ppn;
                                                                    $extra_disc=$listpx[$x]->extra_disc;
                                                                }

                                                        ?>
                                                    </table>
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                        <table class="detail_px">
                                                            <?php
                                                                $str_row = "<tr><td class='col1'>&nbsp;</td><td class='col2'>TOTAL</td><td class='col3'>&nbsp;</td><td class='col4'>&nbsp;</td><td class='col4'>&nbsp;</td><td class='col5'>$totalprice</td>";
                                                                echo $str_row;
                                                                $netprice=$totalprice-(($disc1/100)*$totalprice);
                                                                $netdisc1=(($disc1/100)*$totalprice);
                                                                $netdisc2=(($disc2/100)*$netprice);
                                                                $netprice=$netprice-(($disc2/100)*$netprice);
                                                                $netdisc3=(($disc3/100)*$netprice);
                                                                $netprice=$netprice-(($disc3/100)*$netprice);

                                                            ?>
                                                        </table>
                                                        <div class="inner_header_bottom">&nbsp;</div>
                                                    </div>

                                                    <table class="detail_px">
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">Discount 1</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo $netdisc1;?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">Discount 2</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo $netdisc2;?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">Discount 3 </td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo $netdisc3;?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">Tot. Discount</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo $totalprice-$netprice;?></strong></td>
                                                        </tr>
                                                        <?php
                                                            if($extra_disc>0) {
                                                        ?>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">Extra Disc</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo $extra_disc;?></strong></td>
                                                        </tr>

                                                        <?php

                                                            }
                                                            $dppn=0;
                                                            $netprice=$netprice-$extra_disc;
                                                            if($ppn==1) {
                                                                $dppn=(10/100)*$netprice;
                                                               ?>
                                                                <tr>
                                                                    <td class="payment_col1">&nbsp;</td>
                                                                    <td class="payment_col2">PPN</td>
                                                                    <td class="payment_col3">:</td>
                                                                    <td class="payment_col4"><strong><?php echo $dppn;?></strong></td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        ?>
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">TOTAL BAYAR</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo ($netprice + $dppn);?></strong></td>
                                                        </tr>
                                                        <?php
                                                            $outstanding=($netprice)-($amount);
                                                            //echo "OUT : $outstanding";
                                                        if($payment_type==2) {
                                                        ?>

                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2"><strong>Jatuh Tempo</strong></td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4"><strong><?php echo ($duedate);?></strong></td>
                                                        </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                        <!--
                                                        <tr>
                                                            <td class="payment_col1">&nbsp;</td>
                                                            <td class="payment_col2">Status Pelunasan</td>
                                                            <td class="payment_col3">:</td>
                                                            <td class="payment_col4">
                                                            <?php
                                                                if($outstanding<=0) {
                                                                    echo "<strong>LUNAS</strong>";
                                                                }else {
                                                                    echo "<strong>BELUM LUNAS</strong>";
                                                                }
                                                            ?>
                                                            </td>
                                                        </tr>
                                                        -->
                                                    </table>
                                                    <!--Tipe Pengambilan-->

                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                    </div>
                        </div>
                        <div id="footer">

                                                    <table class="detail_px">
                                                        <tr>
                                                            <td class="sign_col1"><strong></strong></td>
                                                            <td class="sign_col2">&nbsp;</td>
                                                            <td class="sign_col3">&nbsp;</td>
                                                            <td class="sign_col4"><strong>Yogyakarta, <?php echo $jamsekarang . " " .  $thistime;?> </strong>
                                                            <td class="sign_col5"><strong></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="sign_col1"><strong></strong></td>
                                                            <td class="sign_col2">&nbsp;</td>
                                                            <td class="sign_col3">&nbsp;</td>
                                                            <td class="sign_col4"><center><strong><?php echo $user_name?> </strong></center></td>
                                                            <td class="sign_col5"><strong></strong></td>
                                                        </tr>
                                                    </table>
								<br/>
                                                                <center>
                                                                    <strong>Terima Kasih atas kepercayaan anda </strong>
                                                                </center>


                        </div>
		</div>
	</body></html>
