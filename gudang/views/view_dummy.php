<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
	var site_url = "<?php echo site_url(); ?>";		
        var menu_attr_url = base_url + "<?php echo $menu_attr_url; ?>";		
    </script>
	<?php foreach ($this->html_headers->styles as $style): ?>
		<link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" />
	<?php endforeach; ?>
<?php foreach ($this->html_headers->scripts as $script): ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php endforeach; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
		<?php echo (isset($this->html_headers->title)) ? $this->html_headers->title : "pos"; ?>
    </title>
</head>
<body class="metro">
        Metro class

            <div class="grid fluid">
                <div class="row">
                    <div class="span6">
                        <div class="carousel" id="carousel2" style="width: 100%; height: 200px;">
                            <div class="slide" style="display: block; left: 0px;">
                                <img src="images/1.jpg" class="cover1">
                            </div>

                            <div class="slide" style="left: 0px; display: none;">
                                <img src="images/2.jpg" class="cover1">
                            </div>

                            <div class="slide" style="left: 0px; display: none;">
                                <img src="images/3.jpg" class="cover1">
                            </div>
                        <div class="markers square" style="right: 10px; left: auto;"><ul><li class="active"><a href="javascript:void(0)" data-num="0"></a></li><li class=""><a href="javascript:void(0)" data-num="1"></a></li><li class=""><a href="javascript:void(0)" data-num="2"></a></li></ul></div></div>
                        <script>
                            $(function(){
                                $("#carousel2").carousel({
                                    height: 200,
                                    effect: 'fade',
                                    markers: {
                                        show: true,
                                        type: 'square',
                                        position: 'bottom-right'
                                    }
                                });
                            })
                        </script>
                    </div>
                </div>
            </div>

</body>
</html>