<html>
	<head>
		<style>
			/* Define page size. Requires print-area adjustment! */
                        @page { margin: 0px 0px; }
			body {
				margin:     0;
				padding:    0;
                                margin-top:10px;                                
                                margin-left:60px;                                
			}
                        #header {
                            margin:0px 10px;
                        }
                        #content {
                            margin:0px 10px;
                        }
                        .table_container{
                            border: 0px solid;
                            
                        }
                        .inner_header_top{
                            border:0.2px solid;
                            height:0px;
                        }
                        .inner_header_bottom{
                            border:0.1px solid;
                            height:0px;
                        }
                        
                         .col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col2{                             
                             width:200px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col3{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col4{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         /*payment*/
                         .payment_col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col2{                             
                             width:120px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col3{ 
                            width:10px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col4{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                                                
                         /*si*/
                         .sign_col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col2{                             
                             width:120px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col3{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col4{ 
                            width:60px;                            
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                                                
                         .bio_col1 {
                             width:80px;                    
                             valign:top;
                         }
                         .bio_col2 {
                             width:1px;
                             valign:top;
                         }        
                         .bio_col3 {
                             width:150px;
                             text-align:left;
                             valign:top;
                         }
                         .bio_col4 {
                             width:55px;
                             valign:top;
                         }                                 
                         .bio_col5 {
                             width:5px;
                             valign:top;
                         }
                         .bio_col6 {
                             width:100px;
                             text-align:left;
                             valign:top;
                         }        
			/* Printable area */
			#print-area {
				position:   relative;
				top:        0cm;
				left:       0cm;
				width:      17cm;
				/*height:     27.6cm;*/
				
				font-size:  12px;
				font-family: Helvetica, serif;
                                width:14.8cm;
			}
			.ligther{font-weight: lighter;}
			.strong{font-weight: bolder;}
			.caps_font{text-transform:uppercase;}
			.font_0{font-size: 10px;}
			.font_1{font-size: 11px;}
			.font_2{font-size: 12px;}
			.centered{text-align: center;}
			.align_right{text-align: right;}
                        /*table*/
                    .detail_px {
                            font-family: verdana,arial,sans-serif;
                            font-size:11px;
                            color:#333333;
                            border-width: 0.1px;
                            border-color: #666666;
                            border-collapse: collapse;
                    }
                    #tbl_header {
                        font-family: verdana,arial,sans-serif;
                        font-size: 11px;
                        color: #333333;                        
                    }

		</style>
		<script type="text/javascript">
			window.onkeydown=function(o){
				o= o||event;
				if(o.keyCode == 27){ //escape char
					window.open('','_parent','');
					window.close();
				}}
		</script>
	</head>
	<!--<body onload="window.print();window.focus();" >-->
            <body>
		<div id="print-area"> 
			<div id="header">                                                    
                                                    <table id="tbl_header">
                                     
                                                        <tr><td colspan="6"><center><h1>Order Pembelian</h1> </center></td></tr>
                                                    <tr><td colspan="6"><center><strong>No  : <?php echo $pxinfo[0]->po_id;  ?> </strong></center></td></tr>
                                     
                                                        <tr>
                                                            <td class="bio_col1">Nama Supplier</td><td class="bio_col2">:</td><td class="bio_col3"><?php echo $pxinfo[0]->name . "(" . $pxinfo[0]->id . ")";?></td>
                                                            <td class="bio_col4">No Order</td><td class="bio_col5">:</td><td class="bio_col6"><?php echo $pxinfo[0]->po_id;?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bio_col1" valign="top">Alamat</td><td class="bio_col2" valign="top">:</td><td class="bio_col2"><?php echo $pxinfo[0]->addr;?></td>
                                                            <td class="bio_col4" valign="top">Tanggal</td><td class="bio_col5" valign="top">:</td><td class="bio_col6" valign="top"><?php echo $pxinfo[0]->podate;?></td>
                                                        </tr>                                                                                                                
                                                    </table>
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                        <table class="detail_px">                                                                                                                
                                                                <tr><td class="col1">No</td ><td class="col2">Nama Barang</td><td class="col3">Qty</td><td class="col4">&nbsp;</td><td class="col5">&nbsp;</td></tr>                  
                                                        </table>        
                                                        <div class="inner_header_bottom">&nbsp;</div>
                                                    </div>
                        </div>
                    <?php
                        $user_name = $logged_in_user_name;
                    ?>
                        <div id="content">                            
                                                    <table class="detail_px">                                                        
                                                        <?php
                                                        //$thistime = date("H:i:s");
                                                        date_default_timezone_set('UTC');
                                                        $thistime = date('H:i:s', time());
                                                        $jamsekarang = $pxinfo[0]->jamsekarang;
                                                        //                                                        
                                                        //
                                                                $str_row="";
                                                                $totalprice = 0;
                                                                $totaldisc = 0;
                                                                $totalnet = 0;
                                                                for($x=0;$x<count($listpx);$x++) {                                                                        
                                                                    $row = $x+1;
                                                                    $name=$listpx[$x]->po_name;
                                                                    $price = "";
                                                                    $qty = $listpx[$x]->po_qty;
                                                                    $total = ""; //$listpx[$x]->trans_total;
                                                                    $totalprice = ""; //$totalprice + $total;
                                                                    $str_row = "<tr><td class='col1'>$row</td><td class='col2'>$name</td><td class='col3'>$price</td><td class='col4'>$qty</td><td class='col5'>&nbsp;<td>";                                                                        
                                                                    echo $str_row;
                                                                }
                                                        ?>
                                                    </table>
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                        <table class="detail_px">                                                                                                                
                                                            <?php
                                                                $str_row = "<tr><td class='col1'>&nbsp;</td><td class='col2'>TOTAL</td><td class='col3'>&nbsp;</td><td class='col4'>&nbsp;</td><td class='col5'>$totalprice</td>";
                                                                echo $str_row;
                                                            ?>    
                                                        </table>        
                                                        <div class="inner_header_bottom">&nbsp;</div>
                                                    </div>                            
                                                    <!--Tipe Pengambilan-->                                                    
                                                    <div class="table_container">
                                                        <div class="inner_header_top">&nbsp;</div>
                                                    </div>
                        </div>
                        <div id="footer">

                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <table class="detail_px">                                                        
                                                        <tr>
                                                            <td class="sign_col1"><strong></strong></td>
                                                            <td class="sign_col2">&nbsp;</td>
                                                            <td class="sign_col3">&nbsp;</td>
                                                            <td class="sign_col4"><strong>Cirebon, <?php echo $jamsekarang . " " .  $thistime;?> </strong>
                                                            <td class="sign_col5"><strong></strong></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="sign_col1"><strong></strong></td>
                                                            <td class="sign_col2">&nbsp;</td>
                                                            <td class="sign_col3">&nbsp;</td>
                                                            <td class="sign_col4"><center><strong><?php echo $user_name?> </strong></center></td>
                                                            <td class="sign_col5"><strong></strong></td>                                                            
                                                        </tr>                                                        
                                                    </table>
                                                    <p>&nbsp;</p>
                                                    


                                                    
                        </div>                                   
		</div>
	</body>                
</html>
