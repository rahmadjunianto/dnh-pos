<html>
	<head>
		<style>
			/* Define page size. Requires print-area adjustment! */
                        @page { margin: 0px 0px; }
			body {
				margin:     0;
				padding:    0;
                                margin-top:0;                                
			}
                        #header {
                            margin:50px 10px;
                        }
                        #content {
                            margin:0px 10px;
                        }
                        .table_container{
                            border: 0px solid;
                            margin-left:10px;
                        }
                        .inner_header_top{
                            /*border:0.2px solid;*/
                            height:0px;
                        }
                        .inner_header_bottom{
                            /*border:0.1px solid;*/
                            height:0px;
                        }
                        
                         .col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col2{                             
                             width:200px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col3{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col4{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         /*payment*/
                         .payment_col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col2{                             
                             width:120px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col3{ 
                            width:10px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col4{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .payment_col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                                                
                         /*si*/
                         .sign_col1{ 
                             width:30px;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col2{                             
                             width:120px;                             
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col3{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col4{ 
                            width:60px;                            
                             /*border: 1px solid #CCC;*/
                         }                        
                         .sign_col5{ 
                            width:80px;
                            text-align:right;
                             /*border: 1px solid #CCC;*/
                         }                                                
                         .bio_col1 {
                             width:100px;                    
                             valign:top;
                         }
                         .bio_col2 {
                             width:1px;
                             valign:top;
                         }        
                         .bio_col3 {
                             width:300px;                             
                             valign:top;
                             
                         }
                         .bio_col4 {
                             width:55px;
                             valign:top;
                         }                                 
                         .bio_col5 {
                             width:5px;
                             valign:top;
                         }
                         .bio_col6 {
                             width:100px;
                             text-align:left;
                             valign:top;
                         }        
			/* Printable area */
			#print-area {
				position:   relative;
				top:        0cm;
				left:       0cm;
				width:      17cm;
				/*height:     27.6cm;*/
				
				font-size:  12px;
				font-family: Helvetica, serif;
                                width:14.8cm;
			}
			.ligther{font-weight: lighter;}
			.strong{font-weight: bolder;}
			.caps_font{text-transform:uppercase;}
			.font_0{font-size: 10px;}
			.font_1{font-size: 11px;}
			.font_2{font-size: 12px;}
			.centered{text-align: center;}
			.align_right{text-align: right;}
                        /*table*/
                    .detail_px {
                            font-family: verdana,arial,sans-serif;
                            font-size:11px;
                            color:#333333;
                            border-width: 0.1px;
                            border-color: #666666;
                            border-collapse: collapse;
                    }
                    #tbl_header {
                        font-family: verdana,arial,sans-serif;
                        font-size: 11px;
                        color: #333333;                        
                    }

		</style>
		<script type="text/javascript">
			window.onkeydown=function(o){
				o= o||event;
				if(o.keyCode == 27){ //escape char
					window.open('','_parent','');
					window.close();
				}}
		</script>
	</head>
	<!--<body onload="window.print();window.focus();" >-->
            <body>
		<div id="print-area">
			<div id="header" style="text-align: center;">                              
                            <center>
                                <h2>RETUR PEMERIKSAAN</h2>
                                No NOTA : <?php echo $payment_no;  ?>
                                </table>
                            </center>    


                        </div>
                    
                    <?php
                        $user_name = $logged_in_user_name;
                        $patient_name = $pxinfo[0]->name;
                        $jamsekarang = $pxinfo[0]->jamsekarang;
                        
                    ?>
                        <div id="content">                            
                            <table>
                                <tr>
                                    <td class="bio_col1">Sudah Terima Dari</td><td class="bio_col2">:</td><td class="bio_col3">Laboratorium Paramedika</td>
                                </tr>
                                <tr>
                                    <td class="bio_col1">Uang Sejumlah</td><td class="bio_col2">:</td><td halign="left" class="bio_col3"><?php echo $amount;?></td>
                                </tr>
                                <tr>
                                    <td class="bio_col1">Terbilang</td><td class="bio_col2">:</td><td halign="left" class="bio_col3"><?php echo $terbilang;?></td>
                                </tr>                                                                
                               <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>                                                                                                                                
                               <tr>
                                    <td colspan="3">Untuk Pengembalian pemeriksaan <?php echo $pxname?></td>
                                </tr>                                                                                                
                            </table>                            
                        </div>
                        <div>    <p>&nbsp;</p></div>
                        <table class="detail_px">                                                        
                            <tr>
                                <td class="sign_col1"><strong></strong></td>
                                <td class="sign_col2">&nbsp;</td>
                                <td class="sign_col3">&nbsp;</td>
                                <td class="sign_col4"><strong>Cirebon, <?php echo $jamsekarang?> </strong>
                                <td class="sign_col5"><strong></strong></td>                                                            
                            </tr>
                            <tr>
                                <td colspan="5">&nbsp;<p>&nbsp;</p></td>
                            </tr>
                            <tr>
                                <td class="sign_col1"><strong></strong></td>
                                <td class="sign_col2">&nbsp;</td>
                                <td class="sign_col3">&nbsp;</td>
                                <td class="sign_col4"><center><strong><?php echo $patient_name?> </strong></center></td>
                                <td class="sign_col5"><strong></strong></td>                                                            
                            </tr>                                                        
                        </table>
              
              </div>                                   

	</body>                
</html>
