        <script type="text/javascript">
        <?php         echo 'var table_title = "' . $table_title  . '";';                     
        echo 'var table_id = "' . $table_id  . '";';
        echo 'var filter_htmlid = "filter_' . $grid_name  . '";';        
        echo 'var filter_select_htmlid = "select_col_' . $grid_name  . '";';
        echo 'var filter_panel_select_htmlid = "select_panel_col_' . $grid_name  . '";';
        echo 'var cash_out_coa_code = "' . $cash_out_coa_code  . '";';
        echo 'var cash_out_coa_name = "' . $cash_out_coa_name . '";';
        echo 'var journal_type = "' . $journal_type . '";';
        ?>            
            var select_panel_col_html_def="<?php
            $str_select="<select style='width:200px;margin-left:10px;padding-right:100px;margin-right:10px;' id='select_panel_col_" . $grid_name  . "'>";
            $str_select = $str_select . "<option  selected value='0'>&nbsp;</option>";
            for($x=0;$x<count($panels);$x++)
            {
                $panel_id = $panels[$x]->mp_id;
                $panel_name = $panels[$x]->mp_name;                                
                $str_select = $str_select . "<option value=$panel_id>$panel_name</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";                    
            var select_col_html_def="<?php
            $str_select="<select id='select_col_" . $grid_name  . "'>";
            for($x=0;$x<count($table_detail);$x++)
            {
                $table_desc = $table_detail[$x]->tfd_coldesc;
                $table_order = $table_detail[$x]->tfd_order;
                if(trim($table_detail[$x]->tfd_colname)==trim($default_sort_col)) {
                    $str_select = $str_select . "<option  selected value=$table_order >$table_desc</option>";
                }else {
                    $str_select = $str_select . "<option value=$table_order>$table_desc</option>";
                }                
                //$str_select = $str_select . "<option value=$table_order>$table_desc</option>";
            }
                $str_select = $str_select . "</select>";
                echo $str_select;
        ?>";        
	var ht = $(".span-grid").innerHeight();		
        var wd = $(".span-grid").innerWidth();            
        var docwidth = $(document).width();
        //alert(wd);
        wd = 50/100 * (docwidth);
        
        //alert(wd);
        //alert(wd);
        var  newObj = { width: wd, height: 450, numberCell: true, minWidth: 10,
        title: table_title,
        filter_id : filter_htmlid,
        filter_select_htmlid : filter_select_htmlid,
        new_key_value:'',
        tableID : table_id,
        bottomVisible:true,
        resizable: true, columnBorders: true,
        selectionModel: { type: 'cell', mode: 'block' },
        editModel: { clicksToEdit:2, saveKey: 13 },
        hoverMode: 'cell',
    };  
            
        newObj.colModel=[];
        newObj.colModel[0] = { dataIndx: 0, editable: false, sortable: false, title: "", width: 30, align: "center", resizable: false, render: function (ui) {
            var rowData = ui.rowData, dataIndx = ui.dataIndx;
            var val = rowData[dataIndx];
            str = "";
            if (val) {
                str = "checked='checked'";
            }
            return "<input type='checkbox' " + str + " />";
        }, className: "checkboxColumn"
        }            
        </script>           
        
<?php        
        echo '<script type="text/javascript">';
        echo 'var base_url = "' .   base_url() . '";';
        echo 'var site_url = "' . site_url() . '";';
        echo 'var default_sort_col = "' . $default_sort_col  . '";'; //< ?php echo $default_sort_col;
        echo 'var geturladdr = "' . $geturladdr  . '";'; //<?php echo $geturladdr ;
        //newrowurl
        echo 'var newrowurl= "' . $newrowurl  . '";'; //<?php echo $geturladdr ;
        echo 'var updateurl = "' . $updateurl  . '";'; // <?php echo $updateurl ;
        echo 'var drowurl = "' . $drowurl  . '";'; // <?php echo $updateurl ;
        echo 'var grid_name = "'   . $grid_name .  '";'; //#<?php echo $grid_name;
        echo 'var grid_name2 = "#'  . $grid_name . '";'; //<?php echo $grid_name;
        echo 'var keycolumnindex=' . $keycolumn_index  . ';'; // <?php echo $keycolumn_index
        echo '$grid={};';
        echo 'var sortcols =';
            $prefix = "[";
            $suffix = "]";
            $colcnt = $prefix;
            for($x=0;$x<count($table_detail);$x++)
            {
                if($x<count($table_detail)-1) {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "',";
                }else {
                    $colcnt = $colcnt . "'" . $table_detail[$x]->tfd_colname . "'" . $suffix . ";";
                }
            }            
            echo $colcnt;        
        //var newObj = jQuery.noConflict();
//        echo 'newObj' . '.colModel=[];';
//        echo 'newObj.colModel[0] = { dataIndx: 0, editable: false, sortable: false, title: "", width: 30, align: "center", resizable: false, render: function (ui) {';
//        echo '  var rowData = ui.rowData, dataIndx = ui.dataIndx;';
//        echo '        var val = rowData[dataIndx];';
//        echo '        str = "";';
//        echo '        if (val) {';
//        echo '            str = "' . 'checked=' . "'checked';" . '"';
//        echo '        }';
//        echo '        return "' . "<input type='checkbox'" .  ' + str +  />"';
//        echo '    }, className: "checkboxColumn"';
//        echo '    };';        
        
            for($x=0;$x<count($table_detail);$x++)
            {
                $tmp = $table_detail[$x]->tfd_editable;
                if($tmp==1) {
                    $editable="true";
                }else {
                    $editable="false";
                }
                echo 'newObj' . '.colModel['. ($x+1) . '] = { title: "' . $table_detail[$x]->tfd_coldesc . '",width:' . $table_detail[$x]->tfd_colwidth . ' ,dataIndx: "' . $table_detail[$x]->tfd_colname . '",editable:' . $editable  . '};';
            }            
        //echo 'newObj = newObj'  . ';';
        echo '</script>';        
     ?>  
        
    <div class="frameintab-pendaftaran" style="padding-top:1px;">     
            <div class="grid regist">
                <div class="row">  
                        <div class="span-grid">
                            <div id="<?php echo $grid_name; ?>">                            
                            </div>
                            <div style="height:2px;clear:both;"></div>
                            <div style="float:right;">
                                <button id="cashout_save">Simpan</button>
                            </div>
                        </div>                    
                </div>
            </div>
    </div>
<script type="text/javascript" src="/pos/asset2/pos/js/cashout-ui.js"></script>
<script type="text/javascript" src="/pos/asset2/pos/js/cashout.js"></script>