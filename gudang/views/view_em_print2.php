<html>
<head>
  <style>
    @page { margin: 160px 40px; }
    
    #header {
        position: fixed; 
        left: 0px; 
        top: -20px; 
        right: 0px; 
        height: 180px; 
        text-align: center; 
        
    }    
    #footer { 
        position: fixed; 
        left: 50px;
        bottom: -180px; 
        right: 0px; 
        height: 150px;         
    }
    #container_header_result_detail { 
        position:fixed;
        border : 1px solid;
        top :138px;     
        
    }    
    #content {      
            /*adjust ketinggian content di sini*/
            top:165px;
            border:1px solid;
            height:500px;
    }
    #content_header {
        text-align:left;
        margin-left:10px;
    }
    .header_result_detail {            
        //page-break-after: always;
        top:-200px;
        
    }
    .spec_body {
            font-size:14px;
            font-family:  Arial,Helvetica, sans-serif;
        
    }
    /*
    .spacer1 {
	height:10px;
    }*/
    .hrd_col1 {
        width:165px;
    }
    .hrd_col2 {
        width:170px;
        text-align:left;
    }    
    .hrd_col3 {
        width:220px;
    }    
    .hrd_col4 {
        width:90px;
    }    
    .b_hrd_col1 {
        width:120px;
        valign:top;
    }
    .b_hrd_col2 {
        width:500px;        
        valign:top;
    }    
    .b_hrd_col3 {
        width:200px;
        valign:top;
    }    
    .b_hrd_col4 {
        width:50px;
        valign:top;
    }       
    /*#footer .page:after { content: counter(page, upper-roman); }*/
    body {
        margin:     0;
        padding:    0;
        margin-top:0;
        
    }
    .table_container{
        border: 0px solid;
        width:500px;
    }
    .inner_header_top{
        border:1px solid;
        height:0px;
    }
    .inner_header_bottom{
        border:1px solid;
        height:0px;
    }
    .spacer_dashed{
        border:0.2px dashed;
        height:0px;
    }    
    .inner_separator{
        border:0.1px dashed;
        height:0px;
    }    
    .res_col1 {
        width:80px;
    }
    .res_col2 {
        width:5px;
        
    }    
    .res_col3 {
        width:210px;        
    }
    .res_col4 {
        width:100px;
    }
    
    .res_col5 {
        width:5px;
    }
    .res_col6 {
        width:100px;
    }
    
    
    /* Printable area */
    #print-area {
            position:   relative;
            top:        0cm;
            left:       0cm;
            /*height:     27.6cm;*/
            font-size:  14px;
            
    }
    .ligther{font-weight: lighter;}
    .strong{font-weight: bolder;}
    .caps_font{text-transform:uppercase;}
    .font_0{font-size: 10px;}
    .font_1{font-size: 11px;}
    .font_2{font-size: 12px;}
    .centered{text-align: center;}
    .align_right{text-align: right;}
        /*table*/
  </style>
</head>
<body class='spec_body'>
 
  <div id="header">
      <?php
        $name =  $pxinfo[0]['name'];
        $lab_id =  $pxinfo[0]['lab_id'];
        $age = $pxinfo[0]['age'];      
        $patientid = $pxinfo[0]['patient_id'];      

        $addr = $pxinfo[0]['addr'];
        $doctname = $pxinfo[0]['doct_name'];
        $gender = $pxinfo[0]['gender'];
        $phonenum = $pxinfo[0]['phonenum'];
        $labdate = $pxinfo[0]['lab_date'];

        //$regdate = $pxinfo[0]->regdate;
      ?>
      <div>
        <table id="tbl_header" >                                                        
            <tr>  
                <td class="res_col1">No REG</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $patientid; ?></td>
                <td class="res_col4">NO LAB</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $lab_id; ?></td>
            </tr>                                                                                                                
            <tr>  
                <td class="res_col1">NAMA</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $name; ?></td>
                <td class="res_col4">TANGGAL PERIKSA</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $labdate; ?></td>
            </tr>                                                                                                                
            <tr >  
                <td class="res_col1">ALAMAT</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $addr; ?></td>
                <td class="res_col4">JENIS KELAMIN</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $gender; ?></td>
            </tr>                     
            <tr>  
                <td class="res_col1">DOKTER</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $doctname; ?></td>
                <td class="res_col4">UMUR</td>
                <td class="res_col5">:</td>
                <td class="res_col6"><?php echo $age; ?></td>
            </tr>                                                                             
            <tr>  
                <td class="res_col1">NO TELP/HP</td>
                <td class="res_col2">:</td>
                <td class="res_col3"><?php echo $phonenum; ?></td>
                <td class="res_col4">&nbsp;</td>
                <td class="res_col5"></td>
                <td class="res_col6">&nbsp;</td>
            </tr>                                                                                                                                     
        </table>
    </div>
      
<!--    <div class="spacer1">&nbsp;</div>-->
  <div id="content">      
      <div id="content_header">
          <?php
            if($type==1) {
                $title = "X-RAY Foto";
            }elseif($type==2) {
                $title = "USG";
            }else {
                $title = "EKG/Elektromedis";
            }
            echo "<strong>$title</strong>"
          ?>
      
        <div><p>&nbsp;</p></div>
        <div>Teman Sejawat yang terhormat, </div>
        <div><p>&nbsp;</p></div>
      </div>
      <div>
        <table border="0" style="margin-left:10px;width: 100%; border-bottom:1px solid #ccc" >
        <?php
                    for($x=0;$x<count($listpx);$x++) {
                        
                        $row = $x+1;
                        //echo "$row";
                        $pxname = $listpx[$x]['pxname'];
                        if($pxname=="") {
                            $pxname="&nbsp;";
                        }
                        
                        $pxvalue = $listpx[$x]['pxvalue'];
                        if($pxvalue=="") {
                            $pxvalue="&nbsp;";
                        }                                                                        
                        
                        //$pxvalue = str_replace($pxvalue," ","&nbsp;");
                        
                        //$pxvalue = str_replace($pxvalue,chr(13),"");
                        //$pxvalue = str_replace($pxvalue,chr(10),"<p>&nbsp;</p>");
                        $pxvalue= str_replace ( "\r\n","<br>",$pxvalue);
                        $pxvalue= str_replace ( "\r","<br>",$pxvalue);
                        $pxvalue= str_replace ( "\0","<br>",$pxvalue);
                        //$pxvalue = "&nbsp;&nbsp;&nbsp;&nbsp;" . $pxvalue;
                        $pxkesan = $listpx[$x]['pxkesan'];
                        if($pxkesan=="") {
                            $pxkesan="&nbsp;";
                        }                                                                                     
                        //$pxkesan = "&nbsp;&nbsp;&nbsp;&nbsp;" . $pxkesan;

                        $str_row="";
                        $str_row = "<tr><td class='b_hrd_col1'>$pxname</td></tr>";
                        echo $str_row;
                        $str_row = "<tr><td class='b_hrd_col1' valign=top>&nbsp;&nbsp;&nbsp;HASIL</td><td class='b_hrd_col2'><pre>$pxvalue</pre></td></tr>";
                        echo $str_row;
                        $str_row = "<tr><td class='b_hrd_col1'>&nbsp;&nbsp;&nbsp;KESAN</td><td class='b_hrd_col2'><pre>$pxkesan</pre></td></tr>";
                        echo $str_row;
                    }
        ?>
        </table>    
      </div>
  </div>
<div id="footer">
    <p class="page"> </p>
  </div>    
</body>
</html>
