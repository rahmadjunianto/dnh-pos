<?php
/**
 */
class Printing extends Secured_Controller {
	function __construct() {
            parent::__construct();
            $this->data_head['source_page']=site_url('pos');                
            $this->user_name = $this->acl->get_real_users_id();;            
            $this->load->model('model_public','public');    
            $this->load->model('model_printing','modprinting');    
            $this->load->model('model_omzet','modomzet');
            $this->load->model('model_inout','modinout');            
            $this->load->model('model_users','modusers');            
            $this->load->model('model_rl','modrl');            
            //$this->default_group_allowed=array();
	}
        //
        function get_doct_name($doct_id) {
            $retval = "";
            $sql = " SELECT doct_name FROM doctor WHERE doct_id=?";
            $query = $this->db->query($sql,array($doct_id));
            foreach($query->result() as $row) {
                $retval = $row->doct_name;
            }
            return $retval;
        }
        
        function get_product_name($doct_id) {
            $retval = "";
            $sql = " SELECT prod_name FROM products WHERE prod_code=?";
            $query = $this->db->query($sql,array($doct_id));
            foreach($query->result() as $row) {
                $retval = $row->prod_name;
            }
            return $retval;
        }        
	function printdoc($id,$date1,$date2,$doct_id="") {            
            if($id==1) {
                $formid="21";
            }
            if($id==2) {
                $formid="22";
            }
            if($id==3) {
                $formid="51";
            }
            if($id==4) {
                $formid="53";
            }
            if($id==5) {
                $formid="52";
            }
            if($id==6) {
                $formid="54";
            }
            /*    
            $form_access = $this->acl->form_access($formid);
            if($form_access==-1) {
                $this->acl->show_acl_warning();
                return;
            } 
             * 
             */           
            $data=array();
            $this->load->library('dompdf_gen');
            $print_id=$id;
            $this->log_message("ID Print $print_id");
            $this->html_headers->title = "Title Printing";            
            $filterdate = $date1;
            $filterdate2 = $date2;
            //
            $docname="PRINTING_doc_name.pdf";
            $data['page_margin'] = $this->modprinting->get_page_margin($print_id);
            $this->log_message($data['page_margin']['top']);
            $data['style_detail'] = $this->modprinting->get_style_detail($print_id);
            $data['style_header_table'] = $this->modprinting->get_table_header_style($print_id);
            $data['style_content_table'] = $this->modprinting->get_content_table_style($print_id);
            $data['count_header_table'] = $this->modprinting->get_count_header_table($print_id);
            $data['count_content_table'] = $this->modprinting->get_count_content_table($print_id);
            $data['count_content_table_data'] = $this->modprinting->get_count_content_table_data($print_id);
            $data['header_table_column_detail'] = $this->modprinting->get_table_column_detail($print_id);
            $data['content_table_column_detail'] = $this->modprinting->get_content_table_column_detail($print_id);
            $data['content_table_data_column_detail'] = $this->modprinting->get_table_column_detail_data($print_id);
            //
            $data['name'] = "CONTOH NAMA";
            $data['header']['logged_in_user_name'] = $this->user_name;
            if($id==6) {
                $doct_name = $this->get_doct_name($doct_id);
                $data['header']['tanggal'] = " " . $doct_name . "  ($doct_id), Tanggal " . $date1 . " s/d " . $date2;
            }else {
                $data['header']['tanggal'] = $date1 . " s/d " . $date2;
            }
            //$data['content']['data'] = $tanggal;
            $this->load->model('model_payment','modpayment');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            //
            //$filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate2=="") {
                $filterdate2="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate2");
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            //
            //$data['content']['data'] = $this->modpayment->cash_in_per_user($tanggal);
            if($id=="1503192") {
                $data['content']['data'] = $this->modpayment->cash_in($filterdate,$filterdate2);
            }            
            if($id=="1503193") {
                $data['content']['data']['data'] = $this->modomzet->calc_omzet_detail($filterdate,$filterdate2);
            }
            if($id=="1503194") {
                $data['content']['data'] = $this->modpayment->outstanding($filterdate,$filterdate2);
            }
            if($id=="1503195") {
                $data['content']['data']['data'] = $this->modinout->calc_in($filterdate,$filterdate2);
            }
            if($id=="1503196") {
                $data['content']['data']['data'] = $this->modinout->calc_out($filterdate,$filterdate2);
            }
            if($id=="1503197") {
                $doct_name = $this->get_product_name($doct_id);
                $data['header']['tanggal'] = " " . $doct_name . "  ($doct_id), Tanggal " . $date1 . " s/d " . $date2;                
                $data['content']['data']['data'] = $this->modinout->in_out_per_item($filterdate,$filterdate2,$doct_id);
            }            
            if($id=="1503198") {
                
                $mktname = $this->modusers->get_users_name($doct_id);
                $data['header']['tanggal'] = $doct_id .  "(" . ($mktname) . ")". ", Tanggal " . $date1 . " s/d " . $date2;                                
                $data['content']['data']['data'] = $this->modomzet->calc_omzet_mkt_detail($doct_id,$filterdate,$filterdate2);
                
            }            
            if($id=="1504199") {

            	//$old_limit = ini_set("memory_limit", "1000M");
                $this->log_message("MODEL RL");
                $data['content']['data']['data']= $this->modrl->calc_rl($filterdate,$filterdate2);
            }            

            //
            if($id==2) {
                $data['content']['data'] = $this->modpayment->cash_in($filterdate,$filterdate2);
            }
            if($id==1) {
                $data['content']['data'] = $this->modpayment->cash_in_per_user($filterdate,$filterdate2);
            }            
            if($id==3) {
                $data['content']['data']['data'] = $this->modomzet->calc_omzet_global($filterdate,$filterdate2);
            }
            if($id==4) {
                $data['content']['data']['data'] = $this->modomzet->calc_omzet_doct_global($filterdate,$filterdate2);
            }            
            if($id==5) {
                $data['content']['data']['data'] = $this->modomzet->calc_omzet_detail($filterdate,$filterdate2);
                //echo json_encode($data['content']['data']['data']);
            }                        
            if($id==6) {
                $data['content']['data']['data'] = $this->modomzet->calc_doc_omzet_detail($filterdate,$filterdate2,$doct_id);
                //echo json_encode($data['content']['data']['data']);
            }                                    
            //$data['header_table_row_detail'] = $this->modprinting->get_table_column_detail($print_id);
            //$sql = " SELECT * FROM pxcode WHERE pc_pxcode like 'K01%' ";
            //$qry = $this->db->query($sql);
            //$data['content'] = $qry ->result();
            
            $html = $this->load->view('view_printing', $data, true);            
            //array(0,0,595.28,841.89) //a4
            //echo $html;
            
            if($id==5 or $id==6) {
                $this->dompdf->set_paper(array(0,0,595.28,841.89), "landscape" ); // 12" x 12"
            }else {
                $this->dompdf->set_paper(array(0,0,595.28,841.89), "portrait" ); // 12" x 12"
            }
            if($id=="1503198") {
                $this->dompdf->set_paper(array(0,0,595.28,841.89), "landscape" ); // 12" x 12"
            }
            if($id=="1503193") {
                $this->dompdf->set_paper(array(0,0,595.28,841.89), "landscape" ); // 12" x 12"
            }            
            //$this->dompdf->set_paper(array(0,0,595.28,841.89), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();      
            $this->dompdf->get_canvas()->get_page_count().
            $font = Font_Metrics::get_font("helvetica", "bold");
            $font2 = Font_Metrics::get_font("helvetica", "normal");
            $canvas = $this->dompdf->get_canvas();
            $canvas->page_text(50, 820, "Halaman {PAGE_NUM} dari {PAGE_COUNT} Halaman", $font, 10, array(0,0,0));
            $this->dompdf->stream($docname,array('Attachment'=>0));            
            
             
            //$count= $this->$dompdf->get_canvas()->get_page_count();
            /**/
	}
        
}
?>
