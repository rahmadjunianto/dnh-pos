<?php
/**
 */
class Panels extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
                $this->load->model('model_menu','modmenu');
                $this->load->model('model_panel','modpanel');
		//$this->default_group_allowed=array();
	}
	function indexdata($index=45) {

                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                
                
                $this->html_headers->title = "Tambah/Hapus Px";
                $data=array();          
                $data['users_name']=$this->acl->get_real_users_id();
                $grid_name = "grid_modify_" .  $this->modmenu->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->modmenu->get_table_title($index);
                $data['default_sort_col']= $this->modmenu->get_default_sort_column($index);
                $data['geturladdr']= $this->modmenu->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->modmenu->get_key_column_index($index);
                $data['updateurl']= $this->modmenu->get_update_url_address($index);
                $data['drowurl']= $this->modmenu->get_delete_url_address($index);
                $data['newrowurl']= $this->modmenu->get_newrowurl_address($index);
                $data['latest_lab_id']="";
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->modmenu->get_table_info($index);
                $data['header_info']=$this->header_info;              
                $data['menu'] = $this->modmenu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";                
                $this->load->view('init-view', $data);                
		$this->load->view('view_panel',$data);
                //$this->load->view('script_contracts');
	}//
        //
        function data() {
            //          
            $this->log_message("Data Called");
            //$cur_page=((int)$cur_page)-1;
            $idx=45;
            $panel_id = $this->input->get_post('panel_id');

            $col = $this->input->get_post('colsearch');
            $this->log_message("COLUMN SEARCH $col untuk id panel $panel_id");
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            
            //$lab_id='140800048';
            $selfilter = $this->modmenu->get_selection_filter_sql($idx);
            $tablename = $this->modmenu->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->modmenu->get_column_name($idx,$col);
            $coltype = $this->modmenu->get_column_type($idx,$col);
            $this->log_message("COLUMN NAME $colname value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }                
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where $selfilter AND  dp_mpid =? ";
            $this->log_message("SQL COUNT $sqlcount  dengan panel $panel_id");
            $query_count = $this->db->query($sqlcount,array($panel_id));
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }           
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }
            $this->log_message("current page $cur_page jumlah $jumlah");
            $selected_column = $this->modmenu->get_selected_column($idx);
            $this->log_message("SQL DATA KOLOM $selected_column ");
            $sql=" SELECT $selected_column  from $tablename  $where $selfilter AND dp_mpid=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql ");
            $retval = array();            
            //
            $query  = $this->db->query($sql,array($panel_id));
            $data = $query->result();   
            $this->log_message("affected $jumlah");
            //$retval['curPage']=$cur_page;
            $retval['curPage']=1;
            $retval['totalRecords']= $jumlah;
            //$retval['totalRecords']= 100;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        //
        function delpx() {
            $this->log_message("del PX");
            $data=array();
            $data['status']=1;
            $panel_id = $this->input->get_post('panel_id');
            $code = $this->input->get_post('code');
            //$code = str_replace('""','',$code);
            $code= strtolower($code);
            $this->log_message("del kode $code");
            if(!($code=="")) {
                $this->modpanel->delpx($code,$panel_id);
            }
            echo json_encode($data);
        }        
        
        function addpx() {
            $data=array();
            $data['status']=1;
            $panel_id = $this->input->get_post('panel_id');
            $code = $this->input->get_post('code');
            $this->log_message("kode $code dan panel $panel_id");            
            $this->modpanel->addpx($code,$panel_id);
            echo json_encode($data);
        }
        //
        function show_detail_panel() {
            $data=array();
            $id=$this->$this->input->get_post('panel_id');
            $this->modpanel->detail_panel($id);
            echo json_encode($data);
        }
        function panel_info() {
            $this->log_message("PANEL INFO ");
            $data=array();
            
            $id = $this->input->get_post('panel_id');
            $this->log_message("PANEL INFO  2 $id");
            $data['panel_info']= $this->modpanel->panel_info($id);
            echo json_encode($data);
        }        
        function show_panels() {
            $data=array();
            
            $data['listpanel']=$this->modpanel->get_panel();
            echo json_encode($data);                    
        }

}
?>