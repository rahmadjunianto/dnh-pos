<?php

/**
 
 */
class Diamond extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('diamond');
		//$this->default_group_allowed=array();
	}
	function index() {
		$this->html_headers->title = "Diamond";
		$this->html_headers->description="Halaman setelah Anda login";
                //$this->html_headers->styles[ ] = base_url() . "js/ui/css/redmond/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                //$this->html_headers->styles[ ] = base_url() . "css/payroll1.css";
                //$this->html_headers->styles[ ] = base_url() . "css/simulasi.css";
                //$this->html_headers->styles[ ] = base_url() . "js/grid/css/ui.jqgrid.css";
                //$this->html_headers->styles[ ] = base_url() . "js/grid/css/jquery.searchFilter.css";                                
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/jquery-1.11.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "js/ui/js/jquery-ui.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "js/sms.js";
                //$this->html_headers->scripts [ ] = base_url() . "js/ui/js/ui.tabs.closable.min.js";
                                //'assets/js/ext/ext-all.j            
                                //$data['header_info']=$this->header_info;              
                $data['header_info']=$this->header_info;                                                                   //
		$this->load->view('init-view', $this->data_head);
	}
                function identifygammu() {
                    $output = array();
                    $data = array();
                    $retvalarr = array();
                    $retval = -1;
                    $value = 0;
                    exec("/usr/bin/sudo /home/id/php-script/identify", $output, $value);
                    $retval = $value;
                    $data['retval'] =  $retval;
                    foreach ( $output as $o) {
                        $retvalarr[ ] = array(
                                'ret_text' => $o
                        );                        
                    }
                    $this->log_message("OK ");
                    $data['ret_text']=$retvalarr;
                    echo json_encode($data);
                }
                //
                function checknet() {
                    $nohp = $this->input->get_post('nohp');
                    $output = array();
                    $data = array();
                    $retvalarr = array();
                    $retval = -1;
                    $value = 0;
                    exec("/usr/bin/sudo /home/id/php-script/testsms $nohp", $output, $value);
                    $retval = $value;
                    $data['retval'] =  $retval;
                    foreach ( $output as $o) {
                        $retvalarr[ ] = array(
                                'ret_text' => $o
                        );                        
                    }
                    $this->log_message("OK ");
                    $data['ret_text']=$retvalarr;
                    echo json_encode($data);
                }
                //stopsms
                function stopsms() {
                    $output = array();
                    $data = array();
                    $retvalarr = array();
                    $retval = -1;
                    $value = 0;
                    exec("/usr/bin/sudo /home/id/php-script/stopgammu", $output, $value);
                    $retval = $value;
                    $data['retval'] =  $retval;
                    foreach ( $output as $o) {
                        $retvalarr[ ] = array(
                                'ret_text' => $o
                        );                        
                    }
                    $this->log_message("OK ");
                    $data['ret_text']=$retvalarr;
                    echo json_encode($data);
                }
                //start sms
                function startsms() {
                    $output = array();
                    $data = array();
                    $retvalarr = array();
                    $retval = -1;
                    $value = 0;
                    exec("/usr/bin/sudo /home/id/php-script/startgammu", $output, $value);
                    $retval = $value;
                    $data['retval'] =  $retval;
                    foreach ( $output as $o) {
                        $retvalarr[ ] = array(
                                'ret_text' => $o
                        );                        
                    }
                    $this->log_message("OK ");
                    $data['ret_text']=$retvalarr;
                    echo json_encode($data);
                }                
}
?>