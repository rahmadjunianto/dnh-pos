<?php
/**
 */
class Rptinout extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
                $this->user_id = $this->acl->get_users_id();
                $this->load->model('model_inout','modinout');
	}
        function show_in($index=1) {
                if($index==1) {
                    $formid="51";
                }else {
                    $formid="53";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                //
                $this->html_headers->title = "Barang Keluar-Masuk";

                $this->load->model('model_menu','menu');
                $data=array();
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $this->html_headers->title = "LAPORAN BARANG MASUK";
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);
		$this->load->view('view_product_in',$data);
        }
        //
        //out
        function show_out($index=1) {
                if($index==1) {
                    $formid="51";
                }else {
                    $formid="53";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }

                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                $this->html_headers->title = "Barang Keluar";

                $this->load->model('model_menu','menu');
                $data=array();
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $this->html_headers->title = "LAPORAN BARANG KELUAR";
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);
		$this->load->view('view_product_out',$data);
        }
        //show detail
        function sc($index=1) {
                if($index==1) {
                    $formid="52";
                }else {
                    $formid="54";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                $this->html_headers->title = "Omzet Detail";

                $this->load->model('model_menu','menu');
                $data=array();
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $this->html_headers->title = "Kartu Stok";
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);
		$this->load->view('view_kartu_stock',$data);
        }
        //end of show detail
        //
        function calc_in() {
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.01.2015";
            }
            //
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $data['list'] = $this->modinout->calc_in($filterdate,$filterdate2);
            echo json_encode($data);
        }
        //
        function calc_out() {
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.01.2015";
            }
            //
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $data['list'] = $this->modinout->calc_out($filterdate,$filterdate2);
            echo json_encode($data);
        }
        //
        //ksttyu stock
        function calc_per_item() {
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            $prod_id = $this->input->get_post('id');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.12.2014";
            }

            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate doct_id $prod_id");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $data['list'] = $this->modinout->in_out_per_item($filterdate,$filterdate2,$prod_id);
            echo json_encode($data);
        }
        //end of kartu stock
        function show_out_per_item_supp($index=1) {
            if($index==1) {
                $formid="52";
            }else {
                $formid="54";
            }
            $form_access = $this->acl->form_access($formid);
            if($form_access==-1) {
                $this->acl->show_acl_warning();
                return;
            }
            $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
            //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
            //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
            //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
            $this->html_headers->title = "Omzet Detail";

            $this->load->model('model_menu','menu');
            $data=array();
            //
            $data['menu'] = $this->menu->get_menu();
            $data['menu_attr_url']="pos/get_menu_by_id/";
            $data['users_name']=$this->acl->get_real_users_id();
            $this->html_headers->title = "Barang Per Supplier";
            //
            $this->log_message(" USERS NAME " . $data['users_name'] );
            $data['header_info']=$this->header_info;
            $this->load->view('init-view', $data);
    $this->load->view('view_show_out_per_item_supp',$data);
    }

    function calc_per_supplier() {
        $data = array();
        $supp_id = $this->input->get_post('id');
        //$filterdate = "01.01.2014";
        // $this->log_message("filter date $filterdate doct_id $supp_id");
        // $arrdate = explode(".",$filterdate);
        // $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
        // $arrdate = explode(".",$filterdate2);
        // $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
        $data['list'] = $this->modinout->show_per_supplier($supp_id);
        echo json_encode($data);
    }
}
?>