<?php
/**
 */
class Buyprice extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}
	/*fungsi untuk update harga per bulan di detaail transaksi*/
        function get_current_buy_price($code) {
            $retval=0;
            $sql=" SELECT * FROM products WHERE prod_code=?";
            $qry = $this->db->query($sql,array($code));
            foreach($qry->result() as $row) {
                $retval=$row->prod_buyprice;
            }
            return $retval;
        }
        function update_buy_price($month=0,$year=2016) {
            $sql=" SELECT COUNT(distinct(prod_code)) as jumlah FROM products";
            $query=$this->db->query($sql);
            foreach($query->result() as $row) {
                echo "Total $row->jumlah <br>";
            } //endfor

            $sql=" SELECT distinct(prod_code) as code FROM products";
            $query=$this->db->query($sql);
            foreach($query->result() as $row) {
                echo "code $row->code <br>";
                $code=$row->code;
                //
                $qryalldata=" SELECT trans_id,trans_date,trans_buyprice,trans_type,trans_code FROM trans_detail ";
                $qryalldata= $qryalldata . " WHERE extract(month from trans_date)=$month  ";
                $qryalldata= $qryalldata . " AND extract(year from trans_date)=$year  ";
                $qryalldata= $qryalldata . " AND trans_code='$code'";
                $qryalldata= $qryalldata . " ORDER BY trans_date";
                $qry = $this->db->query($qryalldata);
                echo "<table border=1>";
                echo "<tr><td>No</td><td>ID</td><td>Date</td><td>Kode</td><td>BuyPrice</td><td>Type</td><td>REF</td><td>Total</td><td>SQL</td></tr>";
                $counter=1;
                foreach($qry->result() as $rowdata) {
                    $id=$rowdata->trans_id;
                    $date=$rowdata->trans_date;
                    $bp=$rowdata->trans_buyprice;
                    $type=$rowdata->trans_type;
                    $code2=$rowdata->trans_code;
                    $refprice=$this->get_current_buy_price($code2);
                    $strupdate=$this->update_price($id,$code2,$month,$year,$refprice);
                    echo "<tr><td>$counter</td><td>$id</td><td>$date</td><td>$code2</td><td>$bp</td><td>$type</td><td>$refprice</td>$strupdate</tr>";
                    $counter=$counter+1;
                } //end for
                echo "</table>";
                echo " SELESAI UPDATE..!";
            } //end for
        }
        function update_price($id,$code,$month,$year,$refprice) {
            if($refprice=="") {
                $refprice="0";
            }
            $sqlcount=" SELECT COUNT(*) as jumlah FROM trans_detail WHERE trans_id='$id'";
            $sqlcount= $sqlcount . " AND extract(month from trans_date)=$month";
            $sqlcount = $sqlcount . " AND extract(year from trans_date)=$year  ";
            $sqlcount = $sqlcount . " AND trans_code='$code'";
            $qrycount=$this->db->query($sqlcount);
            $total="0";
            foreach($qrycount->result() as $rowcount) {
                $total=$rowcount->jumlah;
            }
            $sql= "update trans_detail set trans_buyprice=$refprice WHERE trans_id='$id'";
            $sql= $sql . " AND extract(month from trans_date)=$month";
            $sql = $sql . " AND extract(year from trans_date)=$year  ";
            $sql = $sql . " AND trans_code='$code'";
            $this->db->query($sql);
            $str="<td>$total</td><td>$sql</td>";
            return $str;
        }
        //

	//end of fungsi update harga
	function indexdata($index=1) {
                $index="" . $index;
                $form_access = $this->acl->form_access($index);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
		//$this->html_headers->title = "pos";
		//$this->html_headers->description="pos";
                //$this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                /* for testing only*/
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                /*end of for test only*/
                $data=array();
                $data['users_name']=$this->acl->get_real_users_id();
                $grid_name = "grid_" .  $this->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->get_table_title($index);
                $this->html_headers->title = $data['table_title'];
                $data['default_sort_col']= $this->get_default_sort_column($index);
                $data['geturladdr']= $this->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->get_key_column_index($index);
                $data['updateurl']= $this->get_update_url_address($index);
                $data['drowurl']= $this->get_delete_url_address($index);
                $data['newrowurl']= $this->get_newrowurl_address($index);
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->get_table_info($index);
                $data['menu'] = $this->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);
		$this->load->view('view_masters',$data);
                //$this->load->view('script_masters');
	}//
        //update url : tf_updateurl
        function get_update_url_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
            $this->log_message("update url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_updateurl;
            }
            $this->log_message("update url " . $retval);
            return $retval;
        }
        //delete url
        function get_delete_url_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_delurl  FROM tableforms WHERE tf_code=? ";
            $this->log_message("del  url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_delurl;
            }
            $this->log_message("delurl url " . $retval);
            return $retval;
        }
        //data url : tf_dataurl_addr
        function get_dataurl_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_dataurl_addr;
            }
            return $retval;
        }
        //
        function get_newrowurl_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_newrow_url FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_newrow_url;
            }
            return $retval;
        }
        //sorting column
        function get_default_sort_column($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_default_sortcol;
            }
            return $retval;
        }

        //table title
        function get_table_title($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_table_title FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_table_title;
            }
            return $retval;
        }
        //
        function get_table_info($tableindex) {
            $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order";
            $this->log_message("Log table $tableindex  dan $sql ");
            $query  = $this->db->query($sql,array($tableindex));
            $retval = $query->result();
            return $retval;
        }
        //get table name
        function get_table_name($tableindex) {
            //return "master";
            $tableindex=$tableindex . "";
            $retval = "";
            $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_tablename;
            }
            return $retval;
        }
        //get column name
        function get_column_name($tableindex,$colorder) {
            //return "master";
            $retval = "";
            $tableindex=$tableindex . "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";

            $query  = $this->db->query($sql,array($tableindex,$colorder));
            foreach($query->result() as $row) {
                $retval=$row->tfd_colname;
            }
            $this->log_message("Name SQL : $sql " . " code " . $tableindex . " order $colorder dan retval $retval");
            return $retval;
        }
        //col type
        function get_column_type($tableindex,$colorder) {
            //return "master";
            $tableindex=$tableindex . "";
            $retval = 0;
            $sql = " SELECT tableforms_detail.tfd_coldatatype  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            //$this->log_message("Type SQL : $sql " . " code " . $tableindex . " order $colorder");
            $query  = $this->db->query($sql,array($tableindex,$colorder));
            foreach($query->result() as $row) {
                $retval=$row->tfd_coldatatype;
            }
            $this->log_message("Name SQL : $sql " . " code " . $tableindex . " order $colorder dan retval $retval");
            return (int)$retval;
        }
        //get key_column
        //
        function get_key_column($tableid) {
            //return "master";
            $retval = "";
            $sql = " SELECT tf_keycolumn   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn;
            }
            return $retval;
        }
        function get_key_column_index($tableid) {
            //return "master";
            $retval = 0;
            $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn_index;
            }
            return $retval;
        }
        //
        //get sequence name
        function get_table_seq_name($tableid) {
            $retval = 0;
            $sql = " SELECT tf_seq_name   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_seq_name;
            }
            return $retval;
        }
        //
        //get sequence name
        function get_table_code($tableid) {
            $retval = 0;
            $sql = " SELECT tf_id   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_id;
            }
            return $retval;
        }
        //function selected column
        function get_selected_column($tableindex) {
            //return "master";
            $x=1;
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? ORDER BY tfd_order";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                if($x==1) {
                    $retval= $retval . $row->tfd_colname ;
                }else {
                    $retval= $retval . "," . $row->tfd_colname ;
                }
                $x++;
            }
            $this->log_message("SELECTED COL : " . $retval);
            return $retval;
        }
        //end of selected col
        //function data($cur_page=1,$records_per_page=20,$sortBy='doct_name',$dir='asc',$col='1',$value) {
        //function data($cur_page=1,$rpp=20,$sortBy='doct_name',$dir='asc',$col='1',$value='',$idx) {
        function get_selection_filter_sql($tableindex) {
            $tableindex=$tableindex . "";
            $retval = "";
            $sql = " SELECT tf_selection_filter   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_selection_filter;
            }
            return $retval;
        }
        function data() {
            //
            //$cur_page=((int)$cur_page)-1;
            $idx = $this->input->get_post('table_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            $tablename = $this->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->get_column_name($idx,$col);
            $coltype = $this->get_column_type($idx,$col);
            $selfilter = $this->get_selection_filter_sql($idx);
            $this->log_message("COL NAME $colname value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(distinct(prod_code)) as jumlah from $tablename $where  $selfilter ";
            $this->log_message("SQL COUNT: " . $sqlcount);
            $query_count = $this->db->query($sqlcount);
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }
            $this->log_message("jumlah $jumlah, current page $cur_page dari $sqlcount");
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }
            if($cur_page==0) {
                $cur_page=1;
            }
            $selected_column = $this->get_selected_column($idx);
            //$sql=" SELECT distinct(prod_code) as prod_code,prod_buyprice,prod$selected_column  from $tablename  $where $selfilter ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $sql = " SELECT distinct(prod_code),prod_name,prod_buyprice from products $where $selfilter ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql");
            $retval = array();
            //
            $query  = $this->db->query($sql);
            $data = $query->result();
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        //delete
        function drow() {
            $tableindex = $this->input->get_post('table_id');
            $keydata = $this->input->get_post('keydata');
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("DELETE  FROM $table_name dengan key $keydata");
            $sql=" DELETE FROM $table_name WHERE $keycolumn=?";
            $this->log_message("DEL sql $sql ");
            $query  = $this->db->query($sql,array($keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);
        }
        //
        function drow_dummy() {
            $status = -1;
            $retval['status']=$status;
            $retval['info']="no record deleted";
            echo json_encode($retval);
        }
        function update(){
            $tableindex = $this->input->get_post('table_id');
            $colindex = $this->input->get_post('colindex');
            $newdata = $this->input->get_post('newdata');
            $keydata = $this->input->get_post('keydata');
            $tableindex=$tableindex . "";
            $colname = $this->get_column_name($tableindex,$colindex);

            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("newdata : " . $newdata . " key $keydata keycolumn $keycolumn tableind $tableindex column $colindex dan colname $colname");
            $sql=" Update $table_name SET $colname=? WHERE $keycolumn=?";
            $this->log_message("update sql $sql ");
            $query  = $this->db->query($sql,array($newdata,$keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);
        }
        //function add
        ///masters/addrow_pxcode
        function addrow_dummy(){
            $retval = "";
            //sd;
            $status = -1;
            $retval['status']=$status;
            $retval['newkey']="";
            echo json_encode($retval);
        }
        function is_use_counter($index) {
            $retval=0;
            $sql= " SELECT * FROM tableforms WHERE tf_code=? ";
            $qry = $this->db->query($sql,array($index));
            foreach($qry->result() as $row) {
                if($row->tf_usecounter==1) {
                    $retval=1;
                }
            }
            return $retval;
        }
        //
        function addrow(){
            $retval = "";
            $tableindex = $this->input->get_post('table_id');
            //$colindex = $this->input->get_post('colindex');
            //$colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $seq_name = $this->get_table_seq_name($tableindex);
            $table_name = $this->get_table_name($tableindex);
            $table_code = $this->get_table_code($tableindex);
            $user_counter = $this->is_use_counter($tableindex);
            //$this->log_message("newdata : " . $newdata . " key $keydata");
            $sql = " SELECT get_next_value('" . $seq_name . "','" . $table_code . "') as nextvalue;";
            $this->log_message("log sql " . $sql);
            $query  = $this->db->query($sql);
            foreach($query->result() as $row) {
                $nextvalue = $row->nextvalue;
            }
            $sql=" INSERT INTO $table_name($keycolumn)VALUES(?)";
            $this->log_message("INSERT sql $sql ");
            $query  = $this->db->query($sql,array($nextvalue));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            $retval['newkey']=$nextvalue;
            echo json_encode($retval);
        }
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }
}
?>
