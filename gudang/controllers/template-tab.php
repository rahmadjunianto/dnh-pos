<?php

/**
 
 */
class pos extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}
	function index() {
		$this->html_headers->title = "pos";
		$this->html_headers->description="pos";
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/jquery-1.11.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
		$this->load->view('init-view', $this->data_head);
	}            
}
?>