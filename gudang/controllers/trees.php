<?php
/**
 */
class Trees extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}
	function index() {
                $index="1";
                $formid="56";
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                //
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/acitree/css/aciTree.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/acitree/js/jquery.aciPlugin.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/acitree/js/jquery.aciTree.min.js";
                $this->html_headers->title = "Master Akun";
                $data=array();
                $data['users_name']=$this->acl->get_real_users_id();
                $grid_name = "grid_" .  $this->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->get_table_title($index);
                $data['default_sort_col']= $this->get_default_sort_column($index);
                $data['geturladdr']= $this->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->get_key_column_index($index);
                $data['updateurl']= $this->get_update_url_address($index);
                $data['drowurl']= $this->get_delete_url_address($index);
                $data['newrowurl']= $this->get_newrowurl_address($index);
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->get_table_info($index);
                $data['header_info']=$this->header_info;
                $data['menu'] = $this->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $this->load->view('init-view', $data);
		$this->load->view('view_trees',$data);
                //$this->load->view('script_contracts');
	}//
        //update url : tf_updateurl
        function get_update_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
            $this->log_message("update url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_updateurl;
            }
            $this->log_message("update url " . $retval);
            return $retval;
        }
        //delete url
        function get_delete_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_delurl  FROM tableforms WHERE tf_code=? ";
            $this->log_message("del  url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_delurl;
            }
            $this->log_message("delurl url " . $retval);
            return $retval;
        }
        //data url : tf_dataurl_addr
        function get_dataurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_dataurl_addr;
            }
            return $retval;
        }
        //
        function get_newrowurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_newrow_url FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_newrow_url;
            }
            return $retval;
        }
        //sorting column
        function get_default_sort_column($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_default_sortcol;
            }
            return $retval;
        }

        //table title
        function get_table_title($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_table_title FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_table_title;
            }
            return $retval;
        }
        //
        function get_table_info($tableindex) {
            $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order";
            $this->log_message("Log table $tableindex  dan $sql ");
            $query  = $this->db->query($sql,array($tableindex));
            $retval = $query->result();
            return $retval;
        }
        //get table name
        function get_table_name($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_tablename;
            }
            return $retval;
        }
        //get column name
        function get_column_name($tableindex,$colorder) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder));
            foreach($query->result() as $row) {
                $retval=$row->tfd_colname;
            }
            return $retval;
        }
        //col type
        function get_column_type($tableindex,$colorder) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tableforms_detail.tfd_coldatatype  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder));
            foreach($query->result() as $row) {
                $retval=$row->tfd_coldatatype;
            }
            return (int)$retval;
        }
        //get key_column
        //
        function get_key_column($tableid) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tf_keycolumn   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn;
            }
            return $retval;
        }
        function get_key_column_index($tableid) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn_index;
            }
            return $retval;
        }
        //
        //get sequence name
        function get_table_seq_name($tableid) {
            $retval = 0;
            $sql = " SELECT tf_seq_name   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_seq_name;
            }
            return $retval;
        }
        //
        //get sequence name
        function get_table_code($tableid) {
            $retval = 0;
            $sql = " SELECT tf_code   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_code;
            }
            return $retval;
        }
        //function selected column
        function get_selected_column($tableindex) {
            //return "contract";
            $x=1;
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? ORDER BY tfd_order";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                if($x==1) {
                    $retval= $retval . $row->tfd_colname ;
                }else {
                    $retval= $retval . "," . $row->tfd_colname ;
                }
                $x++;
            }
            $this->log_message("SELECTED COL : " . $retval);
            return $retval;
        }
        //end of selected col
        //function data($cur_page=1,$records_per_page=20,$sortBy='doct_name',$dir='asc',$col='1',$value) {
        //function data($cur_page=1,$rpp=20,$sortBy='doct_name',$dir='asc',$col='1',$value='',$idx) {
        function data() {
            //
            //$cur_page=((int)$cur_page)-1;
            $idx = $this->input->get_post('table_id');
            //lab_id
            $lab_id = $this->input->get_post('lab_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            $tablename = $this->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->get_column_name($idx,$col);
            $coltype = $this->get_column_type($idx,$col);
            $this->log_message("COL NAME $col value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where AND  rd_frontend_lab_id=? ";
            $this->log_message("current page $cur_page");
            $query_count = $this->db->query($sqlcount,array($lab_id));
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }
            $selected_column = $this->get_selected_column($idx);
            $sql=" SELECT $selected_column  from $tablename  $where AND rd_frontend_lab_id=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql dan no lab $lab_id");
            $retval = array();
            //
            $query  = $this->db->query($sql,array($lab_id));
            $data = $query->result();
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        //delete
        function drow() {
            $tableindex = $this->input->get_post('table_id');
            $keydata = $this->input->get_post('keydata');
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("DELETE  FROM $table_name dengan key $keydata");
            $sql=" DELETE FROM $table_name WHERE $keycolumn=?";
            $this->log_message("DEL sql $sql ");
            $query  = $this->db->query($sql,array($keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);
        }
        function update(){
            $tableindex = $this->input->get_post('table_id');
            $colindex = $this->input->get_post('colindex');
            $newdata = $this->input->get_post('newdata');
            $keydata = $this->input->get_post('keydata');
            $colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            $lab_id = $this->input->get_post('lab_id');
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("newdata : " . $newdata . " key $keydata");
            $sql=" Update $table_name SET $colname=? WHERE $keycolumn=? AND rd_frontend_lab_id=?";
            $this->log_message("update sql $sql ");
            $query  = $this->db->query($sql,array($newdata,$keydata,$lab_id));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);
        }
        //function add

        function addrow(){
            $retval = "";
            $tableindex = $this->input->get_post('table_id');
            //$colindex = $this->input->get_post('colindex');
            //$colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $seq_name = $this->get_table_seq_name($tableindex);
            $table_name = $this->get_table_name($tableindex);
            $table_code = $this->get_table_code($tableindex);
            //$this->log_message("newdata : " . $newdata . " key $keydata");
            $sql = " SELECT get_next_value('" . $seq_name . "','" . $table_code . "') as nextvalue;";
            $this->log_message("log sql " . $sql);
            $query  = $this->db->query($sql);
            foreach($query->result() as $row) {
                $nextvalue = $row->nextvalue;
            }
            $sql=" INSERT INTO $table_name($keycolumn)VALUES(?)";
            $this->log_message("INSERT sql $sql ");
            $query  = $this->db->query($sql,array($nextvalue));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            $retval['newkey']=$nextvalue;
            echo json_encode($retval);
        }
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }
        //
        //function regist info
        function regist_info($lab_id){
            $data = array();
            $sql = " SELECT patient_id,frontend_lab_id as lab_id,patient_name as name,(nullif(patient_address,'') || ' ' || nullif(patient_city,'') ) as addr ,patient_age_string as age,doct_name  ";
            $sql = $sql .  " FROM frontend INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql .  " INNER JOIN doctor on doctor.doct_id=Frontend.frontend_doct_id ";
            $sql = $sql . " Where frontend_lab_id= ? ";
            $this->log_message($sql . " ");
            $query = $this->db->query($sql,array($lab_id));
            return $query->result();
        }
        function get_payment_info($lab_id) {
            $data = array();
            $sql = " SELECT payment_labid as lab_id,payment_amount as amount ,payment_date as date,frontend_totalgross as bruto,frontend_totalnett as netto FROM payment INNER JOIN ";
            $sql = $sql . " Frontend ON Frontend.frontend_lab_id=payment.payment_labid ";
            $sql = $sql . " WHERE payment_labid=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($lab_id));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $bruto = $row->bruto;
                $netto = $row->netto;
                $totalpaid = $totalpaid + $row->amount;
            }
            $disc = $bruto - $netto;
            $outstanding = $netto - $totalpaid;
            $data['bruto']= $bruto;
            $data['netto']= $netto;
            $data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
        }
        //
	function result_print($labid) {
            $this->load->library('dompdf_gen');
            $this->html_headers->title = "Nota Pendaftaran";
            $data=array();
            $sql = " SELECT rd_pxcode,rd_pxname,rd_value,rd_unit,rd_normal FROM result_detail WHERE  rd_frontend_lab_id=? ORDER BY rd_pxcode";
            $query = $this->db->query($sql,array($labid));
            $this->log_message("sql : $sql dan no lab $labid");
            $data['listpx'] = $query->result();
            $data['pxinfo'] = $this->regist_info($labid);
            $retval = $this->get_payment_info($labid);
            $data['bruto']= $retval['bruto'];
            $data['netto']= $retval['netto'];
            $data['disc']= $retval['disc'];
            $data['amount']= $retval['amount'];
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $html = $this->load->view('view_result_print', $data, true);
           //echo $html;

            $this->dompdf->set_paper(array(0,0,419.53,595.28), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->get_canvas()->get_page_count().
            $font = Font_Metrics::get_font("helvetica", "bold");
            $canvas = $this->dompdf->get_canvas();
            $canvas->page_text(100, 500, "Halaman {PAGE_NUM} dari {PAGE_COUNT} Halaman", $font, 10, array(0,0,0));
            //$this->dompdf->page_script('
              // $pdf is the variable containing a reference to the canvas object provided by dompdf
//              $pdf->line(10,730,800,730,array(0,0,0),1);
  //          ');
            $this->dompdf->stream("welcome.pdf",array('Attachment'=>0));
            //$count= $this->$dompdf->get_canvas()->get_page_count();
            /**/
	}
        function get_children($node_id="-1") {
            $this->log_message("node id $node_id");
            $pxlength = strlen($node_id);
            $data=array();
            $retval=array();
            if($node_id=="-1") {
                $sql=" SELECT * FROM account_group ORDER BY ag_order ";
                $query = $this->db->query($sql,array($node_id));
                foreach($query->result() as $row) {
                    $id = $row->ag_number;
                    $name = $row->ag_name;
                    $retval[ ] = array(
                            'id' => $id,
                            'label' => $name,
                            'inode'=>true,
                            'open'=>false
                    );

                }
                $data['child']= $retval;
                //echo json_encode($data);
            }else {
                $nextlength = $pxlength+2;
                $sql= " SELECT coa_code,coa_name FROM coa WHERE coa_code like '$node_id%' AND length(coa_code)=$nextlength ORDER BY coa_code ";
                //$this->log_message("$sql dengan name $name");
                $query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			$retval[ ] = array(
				'id' => $row->coa_code,
				'label' => $row->coa_code . "-" . $row->coa_name,
                                'inode'=>true,
                                'open'=>false
			);
		}
                $data['child']= $retval;
            }

            echo json_encode($data['child']);
        } //end of children list
        //del root
        function delcoa() {
            $data = array();
            $parent = $this->input->get_post('parent');
            $sql = " DELETE FROM coa WHERE lower(coa_code) LIKE '" . strtolower($parent). "%'";
            $this->log_message("DEL $sql");
            $this->db->query($sql);
            $data['status'] = "$parent ";
            echo json_encode($data);
        }
        function is_new_group_root($parent) {
            $retval = 0;
            $sql = " SELECT COUNT(*) as jumlah FROM coa  WHERE lower(substr(coa_code,1,1))=?";
            $parent = strtolower($parent);
            $query = $this->db->query($sql,array($parent));
            foreach($query->result() as $row) {
                $count = $row->jumlah;
                $retval = $count;
            }
            $this->log_message("root grup check : $sql dengan parent $parent");
            if($retval>0) {
                return 0;
            }else {
                return 1;
            }
            //
        }
        function new_child() {
            $parent = $this->input->get_post('parent');
            $name = $this->input->get_post('name');
            $node_id=$parent;
            $this->log_message("NODE ID $parent");
            //if($node_id>0 and $node_id<=10) {
            if($node_id>0) {
                //main parent
                $this->log_message("node id $parent dan name $name");
                $codelength=strlen($parent);
                $nextlength=$codelength+2;
                //
                $sql= " SELECT coalesce(max(to_number(substr(coa_code,length(coa_code)-1,2),'99'))+1,0) as num  FROM coa WHERE length(coa_code)=$nextlength AND coa_code like '$parent%'";
                $this->log_message("$sql dengan name $name  dan parent $parent");
                //$query = $this->db->query($sql,array(strtolower($name)));
                $query = $this->db->query($sql);
                $prefix="";
		foreach ($query->result() as $row) {
                    $maxnum = $row->num;
                    if($maxnum==0) {
                        $maxnum=1;
                    }
                    $this->log_message("MAX NUM $maxnum");
                    if(strlen($maxnum)==1) {
                        $prefix="0" . $maxnum;
                        $newnum = $prefix;
                        $this->log_message("New num $newnum");
                    }else {
                        if($maxnum>99) {
                            $newnum="";
                        }else {
                            $this->log_message("PREFIX $prefix");
                            if($prefix<=99) {
                                if(strlen($prefix)==1) {
                                    $prefix = "0" . $maxnum;
                                }else {
                                    $prefix = $maxnum;
                                }
                            }
                            $newnum= $prefix;
                        }
                    }
		}
            }elseif($node_id<0) {
                //initial view , no need to add
                return;
            }
            /*
            else {
                //new child
            }*/
            //
            $data=array();
            $newcode =  $parent . $newnum;
            $level = 2;
            $retval = $newcode;
            if($newnum=="") {
                $data['child']= $retval;
            }else {
                $sql = " INSERT INTO coa(coa_code,coa_name)VALUES(?,?)";
                $this->log_message(" SQL CODE $sql dengan $newcode ");
                $this->db->query($sql,array($newcode,$name));
                $data['child']= $retval;

            }
        }
        function new_child1() {
            $parent = $this->input->get_post('parent');
            $pxname = $this->input->get_post('name');
            $node_id=$parent;
            $this->log_message("node id $node_id name $pxname");
            $pxlength = strlen($node_id);
            $name = "";
            $newcode = "";
            $sql=" SELECT ag_name FROM account_group WHERE ag_number=? ORDER BY ag_order ";
            $query = $this->db->query($sql,array($node_id));
            foreach($query->result() as $row) {
                $name = $row->ag_name;
            }
            //
            $data=array();
            //echo "ASDA";
            //$root = $this->input->get_post('root');
            if($this->is_new_group_root($name)==1 or strlen($node_id)<=2) {
            //if(strlen($pxname)==1) {
                $this->log_message("pendek $name");
                //$name = "";
                if($this->is_new_group_root($name)==1) {
                    $this->log_message("root group ");
                    $newcode = strtoupper($name) . "0101";
                    $data['child']= $newcode;
                    $level=1;
                    $sql = " INSERT INTO pxcode(pc_pxcode,pc_level,pc_pxname)VALUES(?,?,?)";
                    $this->log_message(" SQL CODE $sql dengan $newcode dan $level");
                    $this->db->query($sql,array($newcode,$level,$pxname));
                    echo json_encode($data['child']);
                    return;
                }
                $this->log_message("$sql dengan node $node_id");
                $sql= " SELECT substr(pc_pxcode,1,1) as prefix1, to_number(substr(pc_pxcode,length(pc_pxcode)-3,2),'99') as num ,pc_pxcode  FROM pxcode WHERE lower(substr(pc_pxcode,1,1))=? AND length(pc_pxcode)=5 ;";
                $this->log_message("$sql dengan name $name");
                $query = $this->db->query($sql,array(strtolower($name)));
		foreach ($query->result() as $row) {
                    $prefix = $row->num;
                    $prefix1 = $row->prefix1;
		}
                //
                $sprefix="";
                $sql= " SELECT max(to_number(substr(pc_pxcode,length(pc_pxcode)-1,2),'999'))+1 as num  FROM pxcode WHERE lower(substr(pc_pxcode,1,1))=?  AND length(pc_pxcode)=5 ";
                $this->log_message("$sql dengan name $name dengan prefix $prefix");
                $query = $this->db->query($sql,array(strtolower($name)));
		foreach ($query->result() as $row) {
                    $maxnum = $row->num;
                    $this->log_message("MAX NUM $maxnum");
                    if(strlen($maxnum)==1) {
                        if(strlen($prefix)==1) {
                            $prefix="0" . $prefix;
                            $sprefix=$prefix;
                        }else {
                            $sprefix=$prefix;
                        }
                        $newnum="0" . $maxnum;
                    }else {
                        if($maxnum>99) {
                            $newnum="01";
                            $prefix = $prefix +1 ;
                            $this->log_message("PREFIX $prefix");
                            if($prefix<=99) {
                                if(strlen($prefix)==1) {
                                    $sprefix = "0" . $prefix;
                                }else {
                                    $sprefix = $prefix;
                                }
                            }
                        }else {
                            $this->log_message("PREFIX $prefix");
                            if($prefix<=99) {
                                if(strlen($prefix)==1) {
                                    $sprefix = "0" . $prefix;
                                }else {
                                    $sprefix = $prefix;
                                }
                            }
                            $newnum= $maxnum;
                        }
                    }
		}
                $this->log_message("Prefix 1 $prefix1 , Prefix : $sprefix new num : $newnum");
                $newcode =  $prefix1 . $sprefix . $newnum;
                $level = 2;
                $retval = $newcode;
                $sql = " INSERT INTO pxcode(pc_pxcode,pc_level,pc_pxname)VALUES(?,?,?)";
                $this->log_message(" SQL CODE $sql dengan $newcode dan $level");
                $this->db->query($sql,array($newcode,$level,$pxname));
                $data['child']= $retval;
            }else {
                $this->log_message("panjang ");
                $pxlength = strlen($node_id);
                $maxlength = $pxlength  +2 ;
                //$sql= " SELECT pc_pxcode,pc_pxname FROM pxcode WHERE lower(pc_pxcode) LIKE  '" .  strtolower($node_id)  . "%' AND length(pc_pxcode)=$maxlength  ORDER BY pc_pxcode";
                $sql= " SELECT substr(pc_pxcode,1,length(pc_pxcode)-2) as prefix1, to_number(substr(pc_pxcode,length(pc_pxcode)-3,2),'99') as num ,pc_pxcode  FROM pxcode WHERE lower(pc_pxcode) LIKE  '" .  strtolower($node_id)  . "%'  AND length(pc_pxcode)=$maxlength;";
                $this->log_message("$sql dengan name $node_id");
                $query = $this->db->query($sql);
                $prefix="01";
                $prefix1 = substr($node_id,0,$pxlength);
                $this->log_message("PREFIX 1 $prefix1");
		foreach ($query->result() as $row) {

                    $prefix = $row->num;
                    $prefix1 = $row->prefix1;
                    $this->log_message("PREFIX $prefix1 DAN  $prefix");
		}
                $this->log_message("PREFIX $prefix1 DAN  $prefix");
                //
                $sprefix="";
                $sql= " SELECT max(to_number(substr(pc_pxcode,length(pc_pxcode)-1,2),'999'))+1 as num  FROM pxcode WHERE lower(pc_pxcode) LIKE  '" .  strtolower($node_id)  . "%'  AND length(pc_pxcode)=$maxlength;";
                //$this->log_message("$sql dengan name $name");
                $query = $this->db->query($sql);

		foreach ($query->result() as $row) {

                    $maxnum = $row->num;
                    $newnum="";
                    $this->log_message("MAXNUM : $maxnum");
                    if(strlen($maxnum)==1) {
                        $newnum="0" . $maxnum;
                    }else {
                        if($maxnum>99) {
                            $newnum="01";
                            $prefix = $prefix +1 ;
                            $this->log_message("PREFIX $prefix");
                            if($prefix<=99) {
                                if(strlen($prefix)==1) {
                                    $sprefix = "0" . $prefix;
                                }else {
                                    $sprefix = $prefix;
                                }
                            }
                        }else {

                            if($prefix<=99) {
                                //sampai angka 08..09
                                if($maxnum=="") {
                                    $maxnum= "01";
                                }
                                $sprefix = $maxnum;
                                $this->log_message("Kurang dari 99 PREFIX $sprefix dengan prefix1 $prefix1");
                            }
                        }
                    }
		}
                $newcode =  $prefix1 . $sprefix . $newnum;
                $level = 2;
                $retval = $newcode;
                $sql = " INSERT INTO pxcode(pc_pxcode,pc_level,pc_pxname)VALUES(?,?,?)";
                $this->log_message(" SQL CODE $sql dengan $newcode dan $level");
                $this->db->query($sql,array($newcode,$level,$pxname));
                $data['child']= $retval;
            }
            echo json_encode($data['child']);
        }        //end of get children
}
?>