<?php
/**
 */
class Doctor extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}
	function index() {
		$this->html_headers->title = "pos";
		$this->html_headers->description="pos";
                //$this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                //$this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                //jquery/ui/jquery-ui-1.11.0
                //$this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap-responsive.css";
                //$this->html_headers->styles[ ] = base_url() . "asset2/metro/css/iconFont.css";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/jquery-1.11.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $data['users_name']=$this->acl->get_real_users_id();
                $data['header_info']=$this->header_info;              
		$this->load->view('view_doctor',$data);
	}            
        function data($cur_page=1,$records_per_page=20,$sortBy='doct_name',$dir='asc',$col='doct_name',$value='') {
            //          
            //$cur_page=((int)$cur_page)-1;
            $where = " WHERE 1=1 AND ($col like '%$value%')";
            $sqlcount=" SELECT count(*) as jumlah from doctor $where ";
            $this->log_message("current page $cur_page");
            $query_count = $this->db->query($sqlcount);
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }           
            $retnum = floor($jumlah/$records_per_page);
            $modulo = $jumlah % $records_per_page;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $records_per_page;
            }                        
            $sql=" SELECT doct_id,doct_name, doct_address,doct_phone,doct_mobilephone from doctor $where ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1) ";            
            $this->log_message("SQL DATA $sql");
            $retval = array();            
            //
            $query  = $this->db->query($sql);
            $data = $query->result();   
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        function update($colindex){
            if($colindex==1) {
                $colname = "doct_name";
            }elseif($colindex==2) {
                $colname = "doct_address";
            }elseif($colindex==3) {
                $colname = "doct_phone";
            }elseif($colindex==4) {
                $colname = "doct_mobilephone";
            }
            
            $newdata = $this->input->get_post('newdata');
            $keydata = $this->input->get_post('keydata');            
            $this->log_message("newdata : " . $newdata . " key $keydata");
            $sql=" Update doctor SET $colname=? WHERE doct_id=?";
            $this->log_message("update sql $sql");
            $query  = $this->db->query($sql,array($newdata,$keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);            
        }
}
?>