<?php
/**
 */
class Regist extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
                $this->load->model('model_panel','modpanel');
                $this->load->model('model_jamaah','modjamaah');
                $this->load->model('model_regist','modregist');
                $this->load->model('model_result','modres');
		//$this->default_group_allowed=array();
	}
	function index() {
                $formid="55";
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                /*end of for test only*/
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";

                $this->html_headers->title = "Registrasi Jamaah";
                $index="01";
                $data=array();          
                $data['users_name']=$this->acl->get_real_users_id();

                $grid_name = "regist-table"; // .  $this->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->get_table_title($index);
                $data['default_sort_col']= $this->get_default_sort_column($index);
                $data['geturladdr']= $this->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->get_key_column_index($index);
                $data['updateurl']= $this->get_update_url_address($index);
                $data['drowurl']= $this->get_delete_url_address($index);
                $data['newrowurl']= $this->get_newrowurl_address($index);
                $data['panels']=$this->modpanel->get_panel();
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->get_table_info($index);
                $data['menu'] = $this->get_menu();
                $data['header_info']=$this->header_info;              
                $data['menu_attr_url']="pos/get_menu_by_id/";                
                $this->load->view('init-view', $data);               
		$this->load->view('view_regist',$data);
                //$this->load->view('script_masters');
	}//
        function del_lab_id() {
           $data=array();
           $pt_id = $this->input->get_post('pt_id');            
           $this->log_message("about to del");
           $status = $this->modres->del_lab_id($pt_id);
           $this->log_message("after del $pt_id");
           $data['status']=$status;
           echo json_encode($data);
        }
        function is_valid_doct() {
            $doct_id = $this->input->get_post('doct_id');            
            $sql = " SELECT COUNT(*) as jumlah from doctor WHERE doct_id=?";
            $query = $this->db->query($sql,array($doct_id));
            $retval =0;
            foreach($query->result() as $row ) {
                $retval = $row->jumlah;
            }
            if ($retval>0) {
                $valid=1;
            }else {
                $valid=-1;
            }
            $data=array();
            $data['valid']=$valid;
            echo json_encode($data);
        }
        //update url : tf_updateurl
        function get_update_url_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
            $this->log_message("update url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_updateurl;
            }
            $this->log_message("update url " . $retval);
            return $retval;
        }                
        //delete url
        function get_delete_url_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_delurl  FROM tableforms WHERE tf_code=? ";
            $this->log_message("del  url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_delurl;
            }
            $this->log_message("delurl url " . $retval);
            return $retval;
        }                        
        //data url : tf_dataurl_addr
        function get_dataurl_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_dataurl_addr;
            }
            return $retval;
        }        
        //
        function get_newrowurl_address($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_newrow_url FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_newrow_url;
            }
            return $retval;
        }                
        //sorting column
        function get_default_sort_column($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_default_sortcol;
            }
            return $retval;
        }
        
        //table title
        function get_table_title($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_table_title FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_table_title;
            }
            return $retval;
        }
        //
        function get_table_info($tableindex) {
            $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order";
            $this->log_message("Log table $tableindex  dan $sql ");
            $query  = $this->db->query($sql,array($tableindex)); 
            $retval = $query->result();
            return $retval;
        }
        //get table name
        function get_table_name($tableindex) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_tablename;
            }
            return $retval;
        } 
        //get column name
        function get_column_name($tableindex,$colorder) {
            //return "master";
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_colname;
            }
            return $retval;
        }         
        //col type
        function get_column_type($tableindex,$colorder) {
            //return "master";
            $retval = 0;
            $sql = " SELECT tableforms_detail.tfd_coldatatype  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_coldatatype;
            }
            return (int)$retval;
        }         
        //get key_column
        //
        function get_key_column($tableid) {
            //return "master";
            $retval = "";
            $sql = " SELECT tf_keycolumn   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn;
            }
            return $retval;
        }         
        function get_key_column_index($tableid) {
            //return "master";
            $retval = 0;
            $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn_index;
            }
            return $retval;
        }              
        //
        //get sequence name
        function get_table_seq_name($tableid) {
            $retval = 0;
            $sql = " SELECT tf_seq_name   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_seq_name;
            }
            return $retval;            
        }
        //
        //get sequence name
        function get_table_code($tableid) {
            $retval = 0;
            $sql = " SELECT tf_code   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_code;
            }
            return $retval;            
        }        
        //function selected column
        function get_selected_column($tableindex) {
            //return "master";
            $x=1;
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? ORDER BY tfd_order";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                if($x==1) {
                    $retval= $retval . $row->tfd_colname ;
                }else {
                    $retval= $retval . "," . $row->tfd_colname ;
                }
                $x++;
            }
            $this->log_message("SELECTED COL : " . $retval);
            return $retval;
        }         
        //end of selected col
        //function data($cur_page=1,$records_per_page=20,$sortBy='doct_name',$dir='asc',$col='1',$value) {
        //function data($cur_page=1,$rpp=20,$sortBy='doct_name',$dir='asc',$col='1',$value='',$idx) {            
        function data() {
            //          
            //$cur_page=((int)$cur_page)-1;
            $idx = $this->input->get_post('table_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            $tablename = $this->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->get_column_name($idx,$col);
            $coltype = $this->get_column_type($idx,$col);
            $this->log_message("COL NAME $col value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }                
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where ";
            $this->log_message("current page $cur_page");
            $query_count = $this->db->query($sqlcount);
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }           
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }    
            $selected_column = $this->get_selected_column($idx);
            $sql=" SELECT $selected_column  from $tablename  $where ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql");
            $retval = array();            
            //
            $query  = $this->db->query($sql);
            $data = $query->result();   
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        //delete
        function drow() {
            $tableindex = $this->input->get_post('table_id');            
            $keydata = $this->input->get_post('keydata');            
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("DELETE  FROM $table_name dengan key $keydata");
            $sql=" DELETE FROM $table_name WHERE $keycolumn=?";
            $this->log_message("DEL sql $sql ");
            $query  = $this->db->query($sql,array($keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);                        
        }
        function update(){
            $tableindex = $this->input->get_post('table_id');
            $colindex = $this->input->get_post('colindex');
            $newdata = $this->input->get_post('newdata');
            $keydata = $this->input->get_post('keydata');            
            $colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("newdata : " . $newdata . " key $keydata");
            $sql=" Update $table_name SET $colname=? WHERE $keycolumn=?";
            $this->log_message("update sql $sql ");
            $query  = $this->db->query($sql,array($newdata,$keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);            
        }
        //function add
        
        function addrow(){
            $retval = "";
            $tableindex = $this->input->get_post('table_id');
            //$colindex = $this->input->get_post('colindex');
            //$colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $seq_name = $this->get_table_seq_name($tableindex);
            $table_name = $this->get_table_name($tableindex);
            $table_code = $this->get_table_code($tableindex);
            //$this->log_message("newdata : " . $newdata . " key $keydata");
            $sql = " SELECT get_next_value('" . $seq_name . "','" . $table_code . "') as nextvalue;";
            $this->log_message("log sql " . $sql);
            $query  = $this->db->query($sql);
            foreach($query->result() as $row) {
                $nextvalue = $row->nextvalue;
            }
            $sql=" INSERT INTO $table_name($keycolumn)VALUES(?)";
            $this->log_message("INSERT sql $sql ");
            $query  = $this->db->query($sql,array($nextvalue));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            $retval['newkey']=$nextvalue;
            echo json_encode($retval);            
        }       
        function addpx() {
            $data=array();
            $data['status']=1;
            $pt_id = $this->input->get_post('pt_id');
            $code = $this->input->get_post('code');
            $disc = $this->input->get_post('disc');
            $price = $this->input->get_post('price');
            $net = $this->input->get_post('net');
            $this->log_message("kode $code");
            $this->load->model('model_result','modres');
            $this->modres->save_result_perpx($code,$pt_id,$price,$disc,$net);
            echo json_encode($data);
        }
        function delpx() {
            $data=array();
            $data['status']=1;
            $pt_id = $this->input->get_post('pt_id');
            $code = $this->input->get_post('code');
            //$code = str_replace('""','',$code);
            $code= strtolower($code);
            $this->log_message("kode $code");
            if(!($code=="")) {
                $this->load->model('model_result','modres');
                $this->load->model('model_payment','mp');
                $sql = " SELECT rd_pxname,rd_pxcode,rd_price,rd_disc,rd_net FROM result_detail ";
                $sql = $sql . " WHERE lower(trim(rd_pxcode))=?  AND rd_frontend_lab_id=? ";
                $this->log_message("$sql dan pxcode $code and lab id $pt_id");
                $query=$this->db->query($sql,array($code,$pt_id));
                foreach($query->result() as $row) {
                    $this->log_message("masuk query ");
                    $amount = 0 - $row->rd_net;
                    $type=1;
                    $code = $row->rd_pxcode;
                    $bruto = $row->rd_price;
                    $disc = $row->rd_disc;
                    $net = $row->rd_net;
                    $pxname = $row->rd_pxname;
                    $this->mp->save_payment_by_del($pt_id,$amount,$type,$code,$bruto,$disc,$net,$pxname);
                }
                $this->modres->delpx($code,$pt_id);
            }
            echo json_encode($data);
        }        
        function saveregist() {
            //
            $this->log_message("code save regist");
            $id = $this->input->get_post('id');
            $title = $this->input->get_post('title');
            $name = $this->input->get_post('name');
            $sex = $this->input->get_post('sex');
            $age= $this->input->get_post('age');                        
            $arrdate = explode(".",$age);
            $dob = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $addr = $this->input->get_post('addr');
            $city = $this->input->get_post('city');   
            $regist_birth_place= $this->input->get_post('regist_birth_place');   
            $this->log_message("birth place : $regist_birth_place");
            //
            $phone = $this->input->get_post('phone');  
            $phonecell = $this->input->get_post('phonecell');
            $passport = $this->input->get_post('passport');
            $issuing_office = $this->input->get_post('issuing_office');
            $issue_date = $this->input->get_post('issue_date');
            $mahram = $this->input->get_post('mahram');
            $paket = $this->input->get_post('paket');
            $paket_name = $this->input->get_post('paket_name');
            $group = $this->input->get_post('group');
            $groupname = $this->input->get_post('group_name');
            //
            $netto = $this->input->get_post('netto');
            $bruto = $this->input->get_post('bruto');
            $disc = $this->input->get_post('disc');
            $regist_dokumen_pendukung=$this->input->get_post('regist_dokumen_pendukung');
            $regist_mahram_rel=$this->input->get_post('regist_mahram_rel');
            $regist_mahram_rel_ke=$this->input->get_post('regist_mahram_rel_ke');
            
            if($mahram=="-" or $mahram=="") {
                $mahram=$id;
            }
            $this->log_message("MAHRAMNYA $mahram");
            //
            //$arrdate = explode(".",$issue_date);
            //$issue_date = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            //jamaah
            $this->log_message("about to save  jamaah data $id,$name,$sex,$age,$addr,$phone,$phonecell");
            $this->modjamaah->savejamaah($id,$title,$name,$sex,$dob,$addr,$city,$phone,$phonecell,$passport,$issuing_office,$issue_date,$regist_mahram_rel,$regist_mahram_rel_ke,$regist_dokumen_pendukung,$regist_birth_place,$mahram);
            //$this->log_message("about to save lab id");
            $tour_id = $this->newlabID();
            //$this->log_message("detailina");
            $retval=0;
            $retval = $this->newtrans($tour_id,$id,$paket,$group,$mahram,$groupname);
            $retval_detail = $this->detailtrans($tour_id,$paket,$bruto,$disc,$netto,$paket_name);
            //detail            
            //$this->mp->save_payment($pt_id,$amount,$type);
            
            $data=array();
            $data['tour_id']="1";            
            $data['status']=$retval;
            $data['status_detail']=$retval_detail;
            echo json_encode($data);

        }
        function savepayment($labid,$amount,$type) {           
            $strSQL = " INSERT INTO payment(payment_labid,payment_amount,payment_type)VALUES";
            $strSQL = $strSQL . "(?,?,?)";            
            $this->log_message("payment $strSQL dengan type $type");
            $this->db->query($strSQL,array($labid,$amount,$type));
            return 0;
        }
        function detailtrans($id,$paket,$bruto,$disc,$netto,$paket_name) {
            $sql = " INSERT INTO tour_detail (tour_id,tour_item,tour_price,tour_disc,tour_net,tour_itemname)";
            $sql = $sql . "VALUES (";
            $sql = $sql . " ?,?,?,?,?,?)";
            $this->log_message("$sql $id,$paket,$paket,$bruto,$netto");
            $this->db->query($sql,array($id,$paket,$bruto,$disc,$netto,$paket_name));
            return 1;            
        }
        function newtrans($id,$j_id,$paket,$group,$mahram,$group_name) {
            $sql = " INSERT INTO tour (tour_id,tour_j_id,tour_mahram,tour_group_jamaah,tour_paket,tour_group_name)";
            $sql = $sql . "VALUES (";
            $sql = $sql . " ?,?,?,?,?,?)";
            $this->log_message("$sql $id,$j_id,$paket,$group,$mahram");
            $this->db->query($sql,array($id,$j_id,$mahram,$group,$paket,$group_name));
            return 1;
        }
        function newlabID() {
            $sql = "SELECT build_tour_id(1) as tour_id";
            $query  = $this->db->query($sql); 
            foreach($query->result() as $row) {
                $retval=$row->tour_id;
            }
            $this->log_message("return lab id $retval");
            return $retval;
            
        }
        function search_regist(){
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            //$filterdate = "2014-08-12";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $sql = " SELECT frontend_lab_id as lab_id,jamaah_name as name FROM frontend INNER JOIN jamaah ON jamaah.jamaah_id=frontend.frontend_jamaah_id ";
            $sql = $sql . " Where to_date(frontend_date::text,'YYYY-MM-DD')=to_date('$filterdate::text','YYYY-MM-DD') ";
            $this->log_message($sql . " filter date $filterdate ");
            $query = $this->db->query($sql);
            $data['listjamaah']=$query->result();
            $data['status']=1;
            foreach($query->result() as $row) {
                $this->log_message("$row->name ");
            }
            echo json_encode($data);
        }
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();                    
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }                   
        //mahram
        function get_mahram($j_id) {
            $this->log_message("GET MAHRAM");
            $this->load->model('model_jamaah','mj');
            $name="";
            $name=$this->mj->get_name_by_id($j_id);
            return $name;
        }
        //data pasien
        function get_ac_jamaah() {
            $data= array();
            $name = $this->input->get_post('name');
            $this->log_message("pat name $name");
            $sql = " SELECT j_id as id,j_name as name,j_title as title,j_addr as addr,j_telp as phone,j_hp  as phonecell,j_kelamin as sex,extract(day from j_tgl_lahir)::text || '.' || extract(month from j_tgl_lahir)::text ||  '.' || extract(year from j_tgl_lahir)::text as age,";            
            $sql = $sql . " ' ' as desc,";
            $sql = $sql . " j_kota as city,j_no_passport as passport,j_issuing_office as issue_off,j_date_of_issue as issue_date,j_mahram as idmahram ";
            $sql = $sql . " FROM jamaah WHERE lower(j_name) LIKE '%$name%' ";
            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            foreach($query->result() as $row) {
                $this->log_message("ENTERING ROW..");
                $mahram_name="";
                $mahram_name=$this->get_mahram($row->idmahram);
		$retval[ ] = array(
			'id' => $row->id,
                        'name' => $row->name,
			'title' => $row->title,
                        'addr' => $row->addr,
                        'phone' => $row->phone,
                        'phonecell' => $row->phonecell,
                        'sex' => $row->sex,
                        'age' => $row->age,
                        'city' => $row->city,
                        'passport' => $row->passport,
                        'issue_off' => $row->issue_off,
                        'issue_date' => $row->issue_date,  
                        'mahram_id' => $row->idmahram,  
                        'mahram_name' => $mahram_name
		);                   
            }
            //$data['jamaah']=$query->result();
            $data['jamaah']=$retval;
            echo json_encode($data);
        }
        //
        function get_ac_regist() {
            $data= array();
            $name = $this->input->get_post('code');
            $this->log_message("pat name $name");
            $sql = " SELECT tour.tour_id as tour_id,date_name(mp_startdate) as sdate,date_name(mp_enddate) as fdate,j_id as id,j_name as name,j_title as title,j_addr as addr,j_telp as phone,j_hp  as phonecell,j_kelamin as sex,extract(day from j_tgl_lahir)::text || '.' || extract(month from j_tgl_lahir)::text ||  '.' || extract(year from j_tgl_lahir)::text as age,";            
            $sql = $sql . " ' ' as desc,";
            $sql = $sql . " j_kota as city,j_no_passport as passport,j_issuing_office as issue_off,j_date_of_issue as issue_date,j_mahram as idmahram ";
            $sql = $sql . " FROM jamaah INNER JOIN tour ON tour.tour_j_id=jamaah.j_id ";
            $sql = $sql . " INNER JOIN tour_detail ON tour_detail.tour_id=tour.tour_id ";
            $sql = $sql . " INNER JOIN master_paket ON tour_detail.tour_item=master_paket.mp_id ";
            $sql = $sql . " WHERE lower(j_name) LIKE '%$name%' ";
            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            foreach($query->result() as $row) {
                $this->log_message("ENTERING ROW..");
                $mahram_name="";
                $mahram_name=$this->get_mahram($row->idmahram);
		$retval[ ] = array(
                        'tour_id' => $row->tour_id,
			'id' => $row->id,
                        'name' => $row->name,
			'title' => $row->title,
                        'addr' => $row->addr,
                        'phone' => $row->phone,
                        'phonecell' => $row->phonecell,
                        'sex' => $row->sex,
                        'age' => $row->age,
                        'city' => $row->city,
                        'passport' => $row->passport,
                        'issue_off' => $row->issue_off,
                        'issue_date' => $row->issue_date,  
                        'mahram_id' => $row->idmahram,  
                        'mahram_name' => $mahram_name,
                        'sdate' => $row->sdate,
                        'fdate' => $row->fdate
		);                   
            }
            //$data['jamaah']=$query->result();
            $data['jamaah']=$retval;
            echo json_encode($data);
        }
        //search regist
        
        
        function get_ac_paket() {
            $data= array();
            $name = $this->input->get_post('name');
            $type = $this->input->get_post('type');
            //
            
            $this->log_message("pat name $name");
            $sqlprice = "0 as price";
            if($type=="quard") {
                $sqlprice= "mp_quad_price as price ";
            }elseif($type=="triple") {
                $sqlprice= "mp_triple_price as price ";
            }elseif($type=="double") {
                $sqlprice= "mp_double_price as price ";
            }else {
                $sqlprice= "0 as price ";
            }        
            $sql = " SELECT mp_id as id,mp_name as name,date_name(mp_startdate) as start,date_name(mp_enddate) as finish ";
            $sql = $sql . ",mp_month_name as mnth, ' ' as desc ," . $sqlprice . " FROM master_paket WHERE lower(mp_name) LIKE '%$name%' ";
            //
            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            $data['paket']=$query->result();
            echo json_encode($data);
        }        
        //group jamaah
        function get_ac_group_jamaah() {
            $data= array();
            $name = $this->input->get_post('name');
            $this->log_message("pat name $name");
            $sql = " SELECT gj_id as id,gj_name as name,gj_address as addr,gj_contact_name as contact ";
            $sql = $sql . " FROM group_jamaah WHERE lower(gj_name) LIKE '%$name%' ";
            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            $data['group_jamaah']=$query->result();
            echo json_encode($data);
        }        
        //
        //get next pat ID/MR
        function next_pat_id() {
            $retval = "";
            $sql = "select get_next_value('seq_jamaah','2') as next_jamaah_id";
            $query = $this->db->query($sql);
            foreach($query->result() as $row) {
                $retval = $row->next_jamaah_id;
            }
            $data = array ();
            $data['next_pat_id'] = $retval;
            echo json_encode($data);
        }
        //data doct
        function get_ac_doct() {
            $data= array();
            $name = $this->input->get_post('name');
            $this->log_message("pat name $name");
            $sql = " SELECT doct_id as doctid,doct_name  as name,doct_address as addr,doct_phone as phone,doct_mobilephone  as phonecell,";
            $sql = $sql . " ' ' as desc FROM doctor WHERE lower(doct_name) LIKE '%$name%' ";
            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            $data['doct']=$query->result();
            echo json_encode($data);
        }        
// static $PAPER_SIZES = array("4a0" => array(0,0,4767.87,6740.79),
//                              "2a0" => array(0,0,3370.39,4767.87),
//                              "a0" => array(0,0,2383.94,3370.39),
//                              "a1" => array(0,0,1683.78,2383.94),
//                              "a2" => array(0,0,1190.55,1683.78),
//                              "a3" => array(0,0,841.89,1190.55),
//                              "a4" => array(0,0,595.28,841.89),
//                              "a5" => array(0,0,419.53,595.28),
//                              "a6" => array(0,0,297.64,419.53),
//                              "a7" => array(0,0,209.76,297.64),
//                              "a8" => array(0,0,147.40,209.76),
//                              "a9" => array(0,0,104.88,147.40),
//                              "a10" => array(0,0,73.70,104.88),
//                              "b0" => array(0,0,2834.65,4008.19),
//                              "b1" => array(0,0,2004.09,2834.65),
//                              "b2" => array(0,0,1417.32,2004.09),
//                              "b3" => array(0,0,1000.63,1417.32),
//                              "b4" => array(0,0,708.66,1000.63),
//                              "b5" => array(0,0,498.90,708.66),
//                              "b6" => array(0,0,354.33,498.90),
//                              "b7" => array(0,0,249.45,354.33),
//                              "b8" => array(0,0,175.75,249.45),
//                              "b9" => array(0,0,124.72,175.75),
//                              "b10" => array(0,0,87.87,124.72),
//                              "c0" => array(0,0,2599.37,3676.54),
//                              "c1" => array(0,0,1836.85,2599.37),
//                              "c2" => array(0,0,1298.27,1836.85),
//                              "c3" => array(0,0,918.43,1298.27),
//                              "c4" => array(0,0,649.13,918.43),
//                              "c5" => array(0,0,459.21,649.13),
//                              "c6" => array(0,0,323.15,459.21),
//                              "c7" => array(0,0,229.61,323.15),
//                              "c8" => array(0,0,161.57,229.61),
//                              "c9" => array(0,0,113.39,161.57),
//                              "c10" => array(0,0,79.37,113.39),
//                              "ra0" => array(0,0,2437.80,3458.27),
//                              "ra1" => array(0,0,1729.13,2437.80),
//                              "ra2" => array(0,0,1218.90,1729.13),
//                              "ra3" => array(0,0,864.57,1218.90),
//                              "ra4" => array(0,0,609.45,864.57),
//                              "sra0" => array(0,0,2551.18,3628.35),
//                              "sra1" => array(0,0,1814.17,2551.18),
//                              "sra2" => array(0,0,1275.59,1814.17),
//                              "sra3" => array(0,0,907.09,1275.59),
//                              "sra4" => array(0,0,637.80,907.09),
//                              "letter" => array(0,0,612.00,792.00),
//                              "legal" => array(0,0,612.00,1008.00),
//                              "ledger" => array(0,0,1224.00, 792.00),
//                              "tabloid" => array(0,0,792.00, 1224.00),
//                              "executive" => array(0,0,521.86,756.00),
//                              "folio" => array(0,0,612.00,936.00),
//                              "commerical #10 envelope" => array(0,0,684.00,297.00),
//                              "catalog #10 1/2 envelope" => array(0,0,648.00,864.00),
//                              "8.5x11" => array(0,0,612.00,792.00),
//                              "8.5x14" => array(0,0,612.00,1008.0),
//                              "11x17"  => array(0,0,792.00, 1224.00));        
        //
	function regist_full_info($labid) {
            $this->load->model('model_payment','mp');
            $sql = " SELECT rd_pxcode,rd_pxname,rd_price FROM result_detail WHERE length(rd_pxcode)=5 AND rd_frontend_lab_id=? ORDER BY rd_pxcode";
            $query = $this->db->query($sql,array($labid));            
            $data['listpx'] = $query->result();
            $data['pxinfo'] = $this->modregist->regist_info($labid);
            $retval = $this->mp->get_payment_info($labid);
            $data['bruto']= $retval['bruto'];
            $data['netto']= $retval['netto'];
            $data['disc']= $retval['disc'];
            $data['amount']= $retval['amount'];
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            echo json_encode($data);
	}        
	function search_regist_info() {
            $tr_id = $this->input->get_post('tr_id');
            $name = $this->input->get_post('tr_id');
            $this->load->model('model_regist','mr');
            $data=array();
            $data= $this->mr->search_regist_full_info($tr_id,$name);            
            echo json_encode($data);
        }
        function regist_nota($labid,$payment_no) {
            $this->load->library('dompdf_gen');
            $this->load->model('model_payment','mp');
            $this->html_headers->title = "Nota Pendaftaran";
            $data=array();
            $sql = " SELECT rd_pxcode,rd_pxname,rd_price,rd_disc,rd_net FROM result_detail WHERE length(rd_pxcode)=5 AND rd_frontend_lab_id=? ORDER BY rd_pxcode";
            $query = $this->db->query($sql,array($labid));            
            $data['listpx'] = $query->result();
            $data['pxinfo'] = $this->modregist->regist_info($labid);
            $retval = $this->mp->get_payment_info($labid);
            $data['bruto']= $retval['bruto'];
            $data['netto']= $retval['netto'];
            $data['payment_no']= $payment_no;
            $data['disc']= $retval['disc'];
            $data['amount']= $this->mp->get_payment_amount_per_no($labid,$payment_no);
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $data['logged_in_user_name'] = $this->acl->get_real_users_id();
            $html = $this->load->view('view_regist_nota', $data, true);
          //  echo $html;
 // A5                  
            $this->dompdf->set_paper(array(0,0,419.53,595.28), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("nota" . $labid . ".pdf",array('Attachment'=>0));            
            
	}
        //function regist info


}
?>