<?php
/**
 */
class Logging extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('simentaris');
		//$this->default_group_allowed=array();
	}
        function std() {
                $index="2";
                $index="" . $index;

		//$this->html_headers->title = "simentaris";
		//$this->html_headers->description="simentaris";
                //$this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                /* for testing only*/
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/simentaris/css/simentaris.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/simentaris/js/simentaris-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/simentaris/js/pages.js";
                /*end of for test only*/
                
                $data=array();          
                
                $data['users_name']=$this->acl->get_real_users_id();
                
                
                $data['table_id']=$index;                
                //$data['table_title']=$this->get_table_title($index);
                $this->html_headers->title = "TEST";
                
                $data['menu'] = $this->get_menu();
                $data['menu_attr_url']="simentaris/get_menu_by_id/";    
                $data['header_info']=$this->header_info;              
                $this->load->view('init-view', $data);

                //$this->load->view('script_masters');

        }
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();                    
        }        
        function log() {
            $type = $this->input->get_post('type');            
            $what = $this->input->get_post('data');            
            $this->log_message("CLIENT->$type: " . json_encode($what) );
        }
	function index2() {
                $form_access = $this->acl->form_access("32");
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/simentaris/css/simentaris.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/simentaris/js/simentaris-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/simentaris/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->title = "Display Harian";
                $this->load->model('model_menu','menu');
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="simentaris/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $data['header_info']=$this->header_info;      
                
                $this->load->view('init-view', $data);                
                $this->load->view('view_admin');
                
	}//
        function downserver() {
            $command="sudo /home/id/administrative/shutdown";
            exec($command);
            $data=array();
            $data['retval']=1;
            echo json_encode($data);
        }
        function rebootserver() {
            $command="sudo /home/id/administrative/reboot";
            exec($command);
            $data=array();
            $data['retval']=1;
            echo json_encode($data);            
        }
}
?>