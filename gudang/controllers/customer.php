<?php
/**
 */
class Customer extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}        
        //
        function get_ac_cust() {
            $data= array();
            $name = $this->input->get_post('name');
            $this->log_message("pat name $name");
            $sql = " SELECT cl_id as id,cl_name as name,cl_address as addr ";
            $sql = $sql . " ,cl_phone as phone,cl_phonecell as phonecell ,'' as desc FROM  client WHERE lower(cl_name) LIKE '%$name%' ";
            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            $data['cust']=$query->result();
            echo json_encode($data);
        }
        function get_ac_price_schema() {
            $data= array();
            $name = $this->input->get_post('name');
            $this->log_message("pat name $name");
            $sql = " SELECT distinct(cl_id) as id,cl_name as name,cl_address as addr,'' as phone,'' phonecell,'' as desc ";
            $sql = $sql . "  FROM  client INNER JOIN products ON products.prod_schema=client.cl_id  WHERE lower(cl_name) LIKE '%$name%' ";
            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            $data['cust']=$query->result();
            echo json_encode($data);            
        }
}
?>