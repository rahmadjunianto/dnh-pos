<?php
/**
 */
class Usersec extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
                $this->load->model('model_menu','modmenu');
                $this->load->model('model_users','modusers');
                $this->load->model('model_form','modform');
		//$this->default_group_allowed=array();
	}
	function index() {
                $index="16080233";
                //$form_access = $this->acl->form_access($index);
                $form_access = $this->acl->form_access('51');
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }                
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/goedang/css/goedang.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/goedang/js/goedang-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/goedang/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                
                
                $this->html_headers->title = "Sekuritas Halaman";
                $data=array();          
                $data['users_name']=$this->acl->get_real_users_id();
                $grid_name = "grid_modify_" .  $this->modmenu->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->modmenu->get_table_title($index);
                $data['default_sort_col']= $this->modmenu->get_default_sort_column($index);
                $data['geturladdr']= $this->modmenu->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->modmenu->get_key_column_index($index);
                $data['updateurl']= $this->modmenu->get_update_url_address($index);
                $data['drowurl']= $this->modmenu->get_delete_url_address($index);
                $data['newrowurl']= $this->modmenu->get_newrowurl_address($index);
                $data['latest_lab_id']="";
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->modmenu->get_table_info($index);
                $data['header_info']=$this->header_info;              
                $data['menu'] = $this->modmenu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";                
                $this->load->view('init-view', $data);                
		$this->load->view('view_usersec',$data);
                //$this->load->view('script_contracts');
	}//
        //
        
        function data() {
            //          
            $this->log_message("Data USER SEC Called");
            //$cur_page=((int)$cur_page)-1;
            $idx="16080233";
            $gu_id = $this->input->get_post('form_id');

            $col = $this->input->get_post('colsearch');
            $this->log_message("COLUMN SEARCH $col untuk id panel $gu_id");
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            //
            //$lab_id='140800048';
            $selfilter = $this->modmenu->get_selection_filter_sql($idx);
            $tablename = $this->modmenu->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->modmenu->get_column_name($idx,$col);
            $coltype = $this->modmenu->get_column_type($idx,$col);
            $this->log_message("COLUMN NAME $colname value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }                
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%') ";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%') ";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where $selfilter AND  us_form_id =? ";
            $this->log_message("SQL COUNT $sqlcount  dengan panel $gu_id");
            $query_count = $this->db->query($sqlcount,array($gu_id));
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }           
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }
            $this->log_message("current page $cur_page jumlah $jumlah");
            $selected_column = $this->modmenu->get_selected_column($idx);
            $this->log_message("SQL DATA KOLOM $selected_column ");
            //$sql=" SELECT $selected_column  from $tablename  $where $selfilter AND us_form_id=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $sql=" SELECT $selected_column  from $tablename  $where $selfilter AND us_form_id=? ORDER BY $sortBy $dir LIMIT $limit ";
            $this->log_message("SQL DATA $sql ");
            $retval = array();            
            //
            $query  = $this->db->query($sql,array($gu_id));
            $data = $query->result();   
            $this->log_message("affected $jumlah");
            //$retval['curPage']=$cur_page;
            $retval['curPage']=1;
            $retval['totalRecords']= $jumlah;
            //$retval['totalRecords']= 100;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        //
        function drow() {
            $this->log_message("DELETE  ROW");
            $formid = $this->input->get_post('form_id');            
            $gu_id = $this->input->get_post('code');            
            //$keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            //$table_name = $this->get_table_name($tableindex);
            $this->log_message("DELETE  FROM usersec dengan key $gu_id dan $formid");
            $sql=" DELETE FROM usersec  WHERE us_form_id=? AND us_group_id=? ";
            $this->log_message("DEL sql $sql ");
            $query  = $this->db->query($sql,array($formid,$gu_id));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);                        
        }        
        
        //
        function addgroup() {
            $data=array();
            $data['status']=1;
            $gu_id = $this->input->get_post('gu_id');
            $id = $this->input->get_post('form_id');
            $this->log_message("kode $id dan panel $gu_id");            
            $this->modusers->add_group_to_sectable($gu_id,$id);
            echo json_encode($data);
        }
        //
        function show_detail_panel() {
            $data=array();
            $id=$this->$this->input->get_post('gu_id');
            $this->modpanel->detail_panel($id);
            echo json_encode($data);
        }
        function panel_info() {
            $this->log_message("PANEL INFO ");
            $data=array();
            
            $id = $this->input->get_post('gu_id');
            $this->log_message("PANEL INFO  2 $id");
            $data['panel_info']= $this->modpanel->panel_info($id);
            echo json_encode($data);
        }        
        function show_forms() {
            $data=array();
            //
            $data['listpanel']=$this->modform->show_forms();
            echo json_encode($data);                    
        }
        function search_users() {
            $data=array();
            //
            //
            $name = $this->input->get_post('code');
            //$name="suaid";
            $this->log_message("LOG USER..$name");
            $data['listuser']=$this->modusers->search_users($name);
            echo json_encode($data);                                
        }
        function changepwd() {
            $status=1;
            $data=array();
            $user = $this->input->get_post('user');
            $pwd = $this->input->get_post('pwd');
            $pwd = md5($pwd);
            $sql = " update users SET users_pwd=? WHERE users_id=?";
            $this->log_message("sql log $sql dengan user $user dan pwd $pwd");
            $this->db->query($sql,array($pwd,$user));
            $data['status']=$status;
            echo json_encode($data);
        }
        function wallbreak($id) {
            $index="50";
            $form_access = $this->acl->form_access($index);
            if($form_access==-1) {
                $this->acl->show_acl_warning();
                return;
            }                
            $this->acl->login_as($id);
        }        
}
?>
