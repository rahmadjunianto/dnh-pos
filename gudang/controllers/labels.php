<?php
/**
 */
class Labels extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
                $this->user_id = $this->acl->get_users_id();
	}
        
	function indexdata($index=1) {

                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->title = "Cetak Label";
                $this->load->model('model_menu','menu');
                $data=array();          
                $grid_name = "grid_" .  $this->menu->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->menu->get_table_title($index);
                $data['default_sort_col']= $this->menu->get_default_sort_column($index);
                $data['geturladdr']= $this->menu->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->menu->get_key_column_index($index);
                $data['updateurl']= $this->menu->get_update_url_address($index);
                $data['drowurl']= $this->menu->get_delete_url_address($index);
                $data['newrowurl']= $this->menu->get_newrowurl_address($index);
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->menu->get_table_info($index);

                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;              
                $this->load->view('init-view', $data);                
		$this->load->view('view_label',$data);
                
                //$this->load->view('script_contracts');
	}//
        //show data
        function data() {
            //          
            //$cur_page=((int)$cur_page)-1;
            $this->load->model('model_menu','menu');
            $idx = $this->input->get_post('table_id');
            $lab_id = $this->input->get_post('lab_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            $tablename = $this->menu->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->menu->get_column_name($idx,$col);
            $coltype = $this->menu->get_column_type($idx,$col);
            $selfilter = $this->menu->get_selection_filter_sql($idx);
            $this->log_message("COL NAME $col value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }                
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where  $selfilter AND  rd_frontend_lab_id=? "; 
            $this->log_message("current page $cur_page");
            $query_count = $this->db->query($sqlcount,$lab_id);
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }           
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }    
            $selected_column = $this->menu->get_selected_column($idx);
            $sql=" SELECT $selected_column  from $tablename  $where $selfilter AND rd_frontend_lab_id=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql");
            $retval = array();            
            //
            $query  = $this->db->query($sql,$lab_id);
            $data = $query->result();   
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }

        //end of show data
        //update url : tf_updateurl        
        function get_payment_info($lab_id) {
            $data = array();
            $sql = " SELECT payment_labid as lab_id,payment_amount as amount ,payment_date as date,frontend_totalgross as bruto,frontend_totalnett as netto FROM payment INNER JOIN ";
            $sql = $sql . " Frontend ON Frontend.frontend_lab_id=payment.payment_labid ";
            $sql = $sql . " WHERE payment_labid=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($lab_id));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $bruto = $row->bruto;
                $netto = $row->netto;
                $totalpaid = $totalpaid + $row->amount;
            }
            $disc = $bruto - $netto;
            $outstanding = $netto - $totalpaid;
            $data['bruto']= $bruto;
            $data['netto']= $netto;
            $data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
        }
        //show payments
	function show_payments() {
            $lab_id = $this->input->get_post('lab_id');
            $this->load->model('model_payment','mp');
            $data=array();            
            $data= $this->mp->show_payments($lab_id);
            echo json_encode($data);
        }
        //
        function save_payment() {
            $lab_id = $this->input->get_post('lab_id');
            $amount = $this->input->get_post('amount');
            $type = $this->input->get_post('type');
            $this->load->model('model_payment','mp');
            
            $data=array();            
            $data= $this->mp->save_payment($lab_id,$amount,$type);
            echo json_encode($data);            
        }
        function cash_in() {
            $this->load->model('model_users','modusers');
            $this->load->model('model_payment','payment');
            $filterdate = $this->input->get_post('filterdate');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            
            $data = $this->payment->cash_in($filterdate);
            echo json_encode($data);
        }
        function cash_in_per_user() {
            $this->load->model('model_users','modusers');
            $this->load->model('model_payment','payment');
            $filterdate = $this->input->get_post('filterdate');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $data = $this->payment->cash_in_per_user($filterdate);
            echo json_encode($data);            
            
        }
        function show_all_cash($index) {
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->title = "Display Harian";
                $this->load->model('model_menu','menu');
                $data=array();          
                $grid_name = "grid_" .  $this->menu->get_table_name($index);
                $data['table_id']=$index;
                                
                $data['default_sort_col']= 1;
                if($index==1) {
                    $data['table_title']="PENERIMAAN KAS PER USER (" . $this->acl->get_real_users_id() . ")";
                    $data['geturladdr']= "/payment/cash_in_per_user";                    
                }else {
                    $data['table_title']="PENERIMAAN KAS KESELURUHAN";
                    $data['geturladdr']= "/payment/cash_in";
                }
                $this->html_headers->title = $data['table_title'];
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = 1;
                $data['updateurl']= "";
                $data['drowurl']= "";
                $data['newrowurl']= "";
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->menu->get_table_info(21);
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $this->load->view('init-view', $data);                
		$this->load->view('view_cash_in',$data);            
        }

        //
        function kwitansi($labid,$payment_no) {
            $this->load->library('dompdf_gen');
            $this->load->model('model_payment','mp');
            $this->load->model('model_regist','modreg');
            $this->load->model('model_public','modpub');
            $this->html_headers->title = "Kwitansi";
            $data=array();
            //
            $sql = " SELECT rd_pxcode,rd_pxname,rd_price,rd_disc,rd_net FROM result_detail WHERE length(rd_pxcode)=5 AND rd_frontend_lab_id=? ORDER BY rd_pxcode";
            $query = $this->db->query($sql,array($labid));            
            $data['listpx'] = $query->result();
            $sql = " SELECT * FROM payment WHERE payment_labid=? AND payment_no=?";
            $this->log_message("KWITANSI $sql $labid no $payment_no");
            $query = $this->db->query($sql,array($labid,$payment_no));
            foreach($query->result() as $row) {
                $data['payment_no']= $row->payment_no;
                $data['amount']= $row->payment_amount;
                $this->log_message("AMOUNT " . $row->payment_amount);
                $data['terbilang']= ucwords($this->modpub->terbilang($row->payment_amount)) . " Rupiah";
            }            
            //
            $this->log_message("KWITANSI");
            $data['pxinfo'] = $this->modreg->regist_info($labid);
            $this->log_message("KWITANSI 2");
            $retval = $this->mp->get_payment_info($labid);                                    
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $data['logged_in_user_name'] = $this->acl->get_real_users_id();
            $html = $this->load->view('view_kwitansi', $data, true);
            //echo $html;
 // A5                  
            $this->dompdf->set_paper(array(0,0,419.53,595.28), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("kwitansi" . $labid . ".pdf",array('Attachment'=>0));            
	}        
        function retur($labid,$payment_no) {
            $this->load->library('dompdf_gen');
            $this->load->model('model_payment','mp');
            $this->load->model('model_regist','modreg');
            $this->load->model('model_public','modpub');
            $this->html_headers->title = "Kwitansi";
            $data=array();
            //
            $sql = " SELECT rd_pxcode,rd_pxname,rd_price,rd_disc,rd_net FROM result_detail WHERE length(rd_pxcode)=5 AND rd_frontend_lab_id=? ORDER BY rd_pxcode";
            $query = $this->db->query($sql,array($labid));            
            $data['listpx'] = $query->result();
            $sql = " SELECT * FROM payment WHERE payment_labid=? AND payment_no=?";
            $this->log_message("KWITANSI $sql $labid no $payment_no");
            $query = $this->db->query($sql,array($labid,$payment_no));
            foreach($query->result() as $row) {
                $data['payment_no']= $row->payment_no;
                $data['amount']= abs($row->payment_amount);
                $amountretur = $row->payment_ref_net;
                $data['pxname']= $row->payment_ref_pxname;                
                $this->log_message("AMOUNT " . $row->payment_amount);
                $data['terbilang']= ucwords($this->modpub->terbilang($amountretur)) . " Rupiah";
            }            
            //
            $this->log_message("RETUR");
            $data['pxinfo'] = $this->modreg->regist_info($labid);
            $this->log_message("RETUR 2");
            $retval = $this->mp->get_payment_info($labid);                                    
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $data['logged_in_user_name'] = $this->acl->get_real_users_id();
            $html = $this->load->view('view_retur', $data, true);
            //echo $html;
 // A5                  
            $this->dompdf->set_paper(array(0,0,419.53,595.28), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("retur" . $labid . ".pdf",array('Attachment'=>0));            
	}                
}
?>