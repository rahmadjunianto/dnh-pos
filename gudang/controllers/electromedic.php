<?php
/**
 */
class Electromedic extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
                $this->user_name = $this->acl->get_real_users_id();
                $this->load->model('model_regist','modregist');            
                $this->load->model('model_public','public');            
		//$this->default_group_allowed=array();
	}
	function indexdata($index=1) {
                $form_access = $this->acl->form_access($index);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                //
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->title = $this->get_table_title($index);
                $data['header_info']=$this->header_info;              
                $data=array();     
                $data['users_name']=$this->acl->get_real_users_id();

                $grid_name = "grid_" .  $this->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->get_table_title($index);
                $data['default_sort_col']= $this->get_default_sort_column($index);
                $data['geturladdr']= $this->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->get_key_column_index($index);
                $data['keycolumn'] = $this->get_key_column($index);
                $data['updateurl']= $this->get_update_url_address($index);
                $data['drowurl']= $this->get_delete_url_address($index);
                $data['newrowurl']= $this->get_newrowurl_address($index);
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->get_table_info($index);
                $data['header_info']=$this->header_info;              
                $data['menu'] = $this->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";                
                $this->load->view('init-view', $data);                
		$this->load->view('view_electromedic',$data);
                //$this->load->view('script_contracts');
	}//
        //update url : tf_updateurl
        function get_update_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
            $this->log_message("update url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_updateurl;
            }
            $this->log_message("update url " . $retval);
            return $retval;
        }                
        //delete url
        function get_delete_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_delurl  FROM tableforms WHERE tf_code=? ";
            $this->log_message("del  url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_delurl;
            }
            $this->log_message("delurl url " . $retval);
            return $retval;
        }                        
        //data url : tf_dataurl_addr
        function get_dataurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_dataurl_addr;
            }
            return $retval;
        }        
        //
        function get_newrowurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_newrow_url FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_newrow_url;
            }
            return $retval;
        }                
        //sorting column
        function get_default_sort_column($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_default_sortcol;
            }
            return $retval;
        }
        
        //table title
        function get_table_title($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_table_title FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_table_title;
            }
            return $retval;
        }
        //
        function get_table_info($tableindex) {
            $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order";
            $this->log_message("Log table $tableindex  dan $sql ");
            $query  = $this->db->query($sql,array($tableindex)); 
            $retval = $query->result();
            return $retval;
        }
        //get table name
        function get_table_name($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_tablename;
            }
            return $retval;
        } 
        //get column name
        function get_column_name($tableindex,$colorder) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_colname;
            }
            return $retval;
        }         
        //col type
        function get_column_type($tableindex,$colorder) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tableforms_detail.tfd_coldatatype  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_coldatatype;
            }
            return (int)$retval;
        }         
        //get key_column
        //
        function get_key_column($tableid) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tf_keycolumn   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn;
            }
            return $retval;
        }         
        function get_key_column_index($tableid) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn_index;
            }
            return $retval;
        }              
        //
        //get sequence name
        function get_table_seq_name($tableid) {
            $retval = 0;
            $sql = " SELECT tf_seq_name   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_seq_name;
            }
            return $retval;            
        }
        //
        //get sequence name
        function get_table_code($tableid) {
            $retval = 0;
            $sql = " SELECT tf_code   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_code;
            }
            return $retval;            
        }        
        //function selected column
        function get_selected_column($tableindex) {
            //return "contract";
            $x=1;
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? ORDER BY tfd_order";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                if($x==1) {
                    $retval= $retval . $row->tfd_colname ;
                }else {
                    $retval= $retval . "," . $row->tfd_colname ;
                }
                $x++;
            }
            $this->log_message("SELECTED COL : " . $retval);
            return $retval;
        }         
        //end of selected col
        //function data($cur_page=1,$records_per_page=20,$sortBy='doct_name',$dir='asc',$col='1',$value) {
        //function data($cur_page=1,$rpp=20,$sortBy='doct_name',$dir='asc',$col='1',$value='',$idx) {            
        //search in em
        function search_em(){
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $col = $this->input->get_post('col');
            $index = $this->input->get_post('index');
            //$filterdate = "2014-08-12";
            $this->log_message("filter date $filterdate col $col index $index");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $sql = " SELECT upper(em_pxcode) as code,em_pxname as pxname,frontend_lab_id as lab_id,patient_name as name FROM frontend INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id  ";
            $sql = $sql . " INNER JOIN em on em.em_labid=frontend.frontend_lab_id AND em_type=$index";
            $sql = $sql . " Where to_date(frontend_date,'YYYY-MM-DD')=to_date('$filterdate','YYYY-MM-DD') ";
            $this->log_message($sql . " filter date $filterdate col $col index $index");
            $query = $this->db->query($sql);
            $data['listpatient']=$query->result();
            $data['status']=1;
            foreach($query->result() as $row) {
                $this->log_message("$row->name ");
            }
            echo json_encode($data);
        }
        //end of em
        function data() {
            //          
            //$cur_page=((int)$cur_page)-1;
            $idx = $this->input->get_post('table_id');
            //lab_id
            $lab_id = $this->input->get_post('lab_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            $selfilter = $this->get_selection_filter_sql($idx);
            $tablename = $this->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->get_column_name($idx,$col);
            $coltype = $this->get_column_type($idx,$col);
            $this->log_message("COL NAME $col value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }                
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where $selfilter AND  rd_frontend_lab_id=? ";
            $this->log_message("current page $cur_page");
            $query_count = $this->db->query($sqlcount,array($lab_id));
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }           
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }    
            $selected_column = $this->get_selected_column($idx);
            $sql=" SELECT $selected_column  from $tablename  $where $selfilter AND rd_frontend_lab_id=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql dan no lab $lab_id");
            $retval = array();            
            //
            $query  = $this->db->query($sql,array($lab_id));
            $data = $query->result();   
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        //delete
        function drow() {
            $tableindex = $this->input->get_post('table_id');            
            $keydata = $this->input->get_post('keydata');            
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("DELETE  FROM $table_name dengan key $keydata");
            $sql=" DELETE FROM $table_name WHERE $keycolumn=?";
            $this->log_message("DEL sql $sql ");
            $query  = $this->db->query($sql,array($keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);                        
        }
        function update(){
            $tableindex = $this->input->get_post('table_id');
            $colindex = $this->input->get_post('colindex');
            $newdata = $this->input->get_post('newdata');
            $keydata = $this->input->get_post('keydata');            
            $colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            $lab_id = $this->input->get_post('lab_id');
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("newdata : " . $newdata . " key $keydata");
            $sql=" Update $table_name SET $colname=? WHERE $keycolumn=? AND rd_frontend_lab_id=?";
            $this->log_message("update sql $sql ");
            $query  = $this->db->query($sql,array($newdata,$keydata,$lab_id));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);            
        }
        //function add
        
        function addrow(){
            $retval = "";
            $tableindex = $this->input->get_post('table_id');
            //$colindex = $this->input->get_post('colindex');
            //$colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $seq_name = $this->get_table_seq_name($tableindex);
            $table_name = $this->get_table_name($tableindex);
            $table_code = $this->get_table_code($tableindex);
            //$this->log_message("newdata : " . $newdata . " key $keydata");
            $sql = " SELECT get_next_value('" . $seq_name . "','" . $table_code . "') as nextvalue;";
            $this->log_message("log sql " . $sql);
            $query  = $this->db->query($sql);
            foreach($query->result() as $row) {
                $nextvalue = $row->nextvalue;
            }
            $sql=" INSERT INTO $table_name($keycolumn)VALUES(?)";
            $this->log_message("INSERT sql $sql ");
            $query  = $this->db->query($sql,array($nextvalue));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            $retval['newkey']=$nextvalue;
            echo json_encode($retval);            
        }       
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();                    
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }
        //
        //function regist info
        function regist_info($lab_id){
            $data = array();            
            $sql = " SELECT patient_id,frontend_lab_id as lab_id,patient_name as name,(nullif(patient_address,'') || ' ' || nullif(patient_city,'') ) as addr ,patient_age_string as age,doct_name  ";
            $sql = $sql .  " FROM frontend INNER JOIN patient ON patient.patient_id=frontend.frontend_patient_id ";
            $sql = $sql .  " INNER JOIN doctor on doctor.doct_id=Frontend.frontend_doct_id ";
            $sql = $sql . " Where frontend_lab_id= ? ";
            $this->log_message($sql . " ");
            $query = $this->db->query($sql,array($lab_id));
            return $query->result();
        }        
        function get_payment_info($lab_id) {
            $data = array();
            $sql = " SELECT payment_labid as lab_id,payment_amount as amount ,payment_date as date,frontend_totalgross as bruto,frontend_totalnett as netto FROM payment INNER JOIN ";
            $sql = $sql . " Frontend ON Frontend.frontend_lab_id=payment.payment_labid ";
            $sql = $sql . " WHERE payment_labid=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($lab_id));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $bruto = $row->bruto;
                $netto = $row->netto;
                $totalpaid = $totalpaid + $row->amount;                
            }
            $disc = $bruto - $netto;
            $outstanding = $netto - $totalpaid;
            $data['bruto']= $bruto;
            $data['netto']= $netto;
            $data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
        }
        //
        //filter
        function get_selection_filter_sql($tableindex) {
            $retval = "";
            $sql = " SELECT tf_selection_filter   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_selection_filter;
            }
            return $retval;                        
        }
        function save_em() {
            $data=array();
            $code = $this->input->get_post('code');
            $labid = $this->input->get_post('labid');
            $result = $this->input->get_post('result');
            $kesan = $this->input->get_post('kesan');
            $this->load->model('model_em','em');
            $this->em->save_result_em($code,$labid,$result,$kesan);
            $data['status']=1;
            echo json_encode($data);            
        }
        function result() {
            $data=array();
            $code = $this->input->get_post('code');
            $labid = $this->input->get_post('labid');            
            $this->load->model('model_em','em');
            $res = $this->em->get_result_em($code,$labid);
            $data['result']=$res['result'];
            $data['kesan']=$res['kesan'];
            echo json_encode($data);
        }
//printing
	function em_print($labid,$type) {
            $this->load->library('dompdf_gen');
            $this->html_headers->title = "Nota Pendaftaran";
            $this->load->model('model_regist','modregist');
            $data=array();
            $sql = " SELECT em_pxcode,em_pxname,em_result  FROM em WHERE  em_labid=? and em_type=? ORDER BY em_pxcode";            
            $query = $this->db->query($sql,array($labid,$type));            
            $this->log_message("sql : $sql dan no lab $labid");
	    $retval=array();
	    foreach($query->result() as $row) {
		$pxcode = $row->em_pxcode;
		$pxname = $row->em_pxname;
                $pxresult = $row->em_result;
		$retval[ ] = array(
			'pxcode' => $pxcode,
			'pxname' => $pxname,
			'pxvalue' => $pxresult,
		);                
	    }
            $data['listpx'] = $retval;
            $data['pxinfo'] = $this->modregist->regist_info($labid);
            $html = $this->load->view('view_em_print', $data, true);
           // echo $html;
            //array(0,0,595.28,841.89) //a4
            $this->dompdf->set_paper(array(0,0,595.28,841.89), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();      
            $this->dompdf->get_canvas()->get_page_count().
            $font = Font_Metrics::get_font("helvetica", "bold");
            $canvas = $this->dompdf->get_canvas();
            $canvas->page_text(50, 820, "Halaman {PAGE_NUM} dari {PAGE_COUNT} Halaman", $font, 10, array(0,0,0));
            $canvas->page_text(450, 720, "Pemeriksa,", $font, 10, array(0,0,0));
            $this->dompdf->stream("em_" . $labid . ".pdf",array('Attachment'=>0));            
            //$count= $this->$dompdf->get_canvas()->get_page_count();
            /**/
	}
//        
	function result_em_print($labid,$type) {
            $this->load->library('dompdf_gen');
            $this->html_headers->title = "Hasil Elektromedik";
            
            
            $data=array();
            $sql = " SELECT em_pxcode,em_pxname,em_result,em_kesan  FROM em WHERE  em_labid=? and em_type=? ORDER BY em_pxcode";            
            $query = $this->db->query($sql,array($labid,$type));            
            $this->log_message("sql : $sql dan no lab $labid");
	    $retval=array();
            //
	    foreach($query->result() as $row) {
		$pxcode = $row->em_pxcode;
		$pxname = $row->em_pxname;
                $pxresult = $row->em_result;
                $pxkesan = $row->em_kesan;
		$retval[ ] = array(
			'pxcode' => $pxcode,
			'pxname' => $pxname,
			'pxvalue' => $pxresult,
                        'pxkesan' => $pxkesan,
		);                
                $this->log_message("VALUE $pxresult  untuk lab id $labid");
	    }
            $data['listpx'] = $retval;
            $data['pxinfo'] = $this->modregist->regist_full_info($labid);
            $data['type'] = $type;
            $datename = $this->public->get_date_name();
            $datetimename = $this->public->get_date_time_name();
            if($type==1){
                $dr_rad = $this->public->get_settings('dr baca radiologi');
            }
            if($type==2){
                $dr_rad = $this->public->get_settings('dr baca usg');
            }            
            if($type==3){
                $dr_rad = $this->public->get_settings('dr baca elektromedis');
            }                        
            $user_name = $this->user_name;
            $html = $this->load->view('view_em_print', $data, true);
            //$dr_rad = $this->public->get_settings('dr baca radiologi');
            //echo $html;
            //array(0,0,595.28,841.89) //a4
            $this->dompdf->set_paper(array(0,0,595.28,841.89), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();      
            $this->dompdf->get_canvas()->get_page_count().
            $font = Font_Metrics::get_font("helvetica", "bold");
            $font2 = Font_Metrics::get_font("helvetica", "normal");
            $canvas = $this->dompdf->get_canvas();
            $canvas->page_text(50, 820, "Halaman {PAGE_NUM} dari {PAGE_COUNT} Halaman", $font, 10, array(0,0,0));            
            $canvas->page_text(400, 720, "Cirebon, $datename", $font, 10, array(0,0,0));
       //     $canvas->page_text(400, 770, " $dr_rad", $font, 10, array(0,0,0));
            $canvas->page_text(50, 770, " Printed By : $user_name / $datetimename ", $font2, 8, array(0,0,0));
            //$canvas->page_text(470, 720, $dr_rad, $font, 10, array(0,0,0));
            $this->dompdf->stream("em_" . $labid . ".pdf",array('Attachment'=>0));                        
	}        
}
?>