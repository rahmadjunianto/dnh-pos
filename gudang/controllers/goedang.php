<?php

/**
 */
class Pos extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}
	function index() {
            $this->html_headers->title = "pos";
            $this->html_headers->description="pos";
            $data=array();
            $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
            //$this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.9.2/jquery-ui.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";                                               
            $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";            
            //jquery/ui/jquery-ui-1.11.0
            //$this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap-responsive.css";
            //$this->html_headers->styles[ ] = base_url() . "asset2/metro/css/iconFont.css";
            //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/jquery-1.11.1.min.js";            
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";            
            //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/1.8.3/jquery.min.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                                                
            //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
            //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";             
            $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";            
            $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";   
            $data['users_name']=$this->acl->get_real_users_id();
            $this->log_message("users name :" . $data['users_name']);
            $data['header_info']=$this->header_info;              
            //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.9.2/jquery-ui.min.js";
            //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";            
            
            //$this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";                
            //metro-tab-control.js
            //$data = array();
            
            $data['menu'] = $this->get_menu();
           // echo "ASDASD";
 
            $data['menu_attr_url']="pos/get_menu_by_id/";
            
            $this->load->view('init-view', $data);
            
	}            
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();                    
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }  
        function test1() {
        	echo "TSET";   
        }   
}
?>