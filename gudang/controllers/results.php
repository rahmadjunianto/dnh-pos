<?php
/**
 */
class Results extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
                $this->load->model('model_result','modres');
                $this->user_name = $this->acl->get_real_users_id();;            
                $this->load->model('model_public','public');                  
		//$this->default_group_allowed=array();
	}
	function indexdata($index=1) {
                $formid="09";
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->title = "Hasil Pemeriksaan";
                $data=array();     
                $data['users_name']=$this->acl->get_real_users_id();

                $grid_name = "grid_" .  $this->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->get_table_title($index);
                $data['default_sort_col']= $this->get_default_sort_column($index);
                $data['geturladdr']= $this->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->get_key_column_index($index);
                $data['updateurl']= $this->get_update_url_address($index);
                $data['drowurl']= $this->get_delete_url_address($index);
                $data['newrowurl']= $this->get_newrowurl_address($index);
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->get_table_info($index);
                $data['menu'] = $this->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";    
                $data['header_info']=$this->header_info;              
                $this->load->view('init-view', $data);                
		$this->load->view('view_results',$data);
                //$this->load->view('script_contracts');
	}//
        //update url : tf_updateurl
        function get_update_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
            $this->log_message("update url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_updateurl;
            }
            $this->log_message("update url " . $retval);
            return $retval;
        }                
        //delete url
        function get_delete_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_delurl  FROM tableforms WHERE tf_code=? ";
            $this->log_message("del  url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_delurl;
            }
            $this->log_message("delurl url " . $retval);
            return $retval;
        }                        
        //data url : tf_dataurl_addr
        function get_dataurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_dataurl_addr;
            }
            return $retval;
        }        
        //
        function get_newrowurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_newrow_url FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_newrow_url;
            }
            return $retval;
        }                
        //sorting column
        function get_default_sort_column($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_default_sortcol;
            }
            return $retval;
        }
        
        //table title
        function get_table_title($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_table_title FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_table_title;
            }
            return $retval;
        }
        //
        function get_table_info($tableindex) {
            $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order";
            $this->log_message("Log table $tableindex  dan $sql ");
            $query  = $this->db->query($sql,array($tableindex)); 
            $retval = $query->result();
            return $retval;
        }
        //get table name
        function get_table_name($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_tablename;
            }
            return $retval;
        } 
        //get column name
        function get_column_name($tableindex,$colorder) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_colname;
            }
            return $retval;
        }         
        //col type
        function get_column_type($tableindex,$colorder) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tableforms_detail.tfd_coldatatype  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder)); 
            foreach($query->result() as $row) {
                $retval=$row->tfd_coldatatype;
            }
            return (int)$retval;
        }         
        //get key_column
        //
        function get_key_column($tableid) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tf_keycolumn   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn;
            }
            return $retval;
        }         
        function get_key_column_index($tableid) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn_index;
            }
            return $retval;
        }              
        //
        //get sequence name
        function get_table_seq_name($tableid) {
            $retval = 0;
            $sql = " SELECT tf_seq_name   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_seq_name;
            }
            return $retval;            
        }
        //
        //get sequence name
        function get_table_code($tableid) {
            $retval = 0;
            $sql = " SELECT tf_code   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid)); 
            foreach($query->result() as $row) {
                $retval=$row->tf_code;
            }
            return $retval;            
        }        
        //function selected column
        function get_selected_column($tableindex) {
            //return "contract";
            $x=1;
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? ORDER BY tfd_order";
            $query  = $this->db->query($sql,array($tableindex)); 
            foreach($query->result() as $row) {
                if($x==1) {
                    $retval= $retval . $row->tfd_colname ;
                }else {
                    $retval= $retval . "," . $row->tfd_colname ;
                }
                $x++;
            }
            $this->log_message("SELECTED COL : " . $retval);
            return $retval;
        }         
        //end of selected col
        //function data($cur_page=1,$records_per_page=20,$sortBy='doct_name',$dir='asc',$col='1',$value) {
        //function data($cur_page=1,$rpp=20,$sortBy='doct_name',$dir='asc',$col='1',$value='',$idx) {            
        function data() {
            //          
            //$cur_page=((int)$cur_page)-1;
            $idx = $this->input->get_post('table_id');
            //lab_id
            $lab_id = $this->input->get_post('lab_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $cur_page =1;// $this->input->get_post('curpage');
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            $tablename = $this->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->get_column_name($idx,$col);
            $coltype = $this->get_column_type($idx,$col);
            $this->log_message("COL NAME $col value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }                
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where AND  rd_frontend_lab_id=? ";
            $this->log_message("current page $cur_page");
            $query_count = $this->db->query($sqlcount,array($lab_id));
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }           
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }    
            $selected_column = $this->get_selected_column($idx);
            $sql=" SELECT $selected_column  from $tablename  $where AND rd_frontend_lab_id=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql dan no lab $lab_id");
            $retval = array();            
            //
            $query  = $this->db->query($sql,array($lab_id));
            $data = $query->result();   
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        //delete
        function drow() {
            $tableindex = $this->input->get_post('table_id');            
            $keydata = $this->input->get_post('keydata');            
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("DELETE  FROM $table_name dengan key $keydata");
            $sql=" DELETE FROM $table_name WHERE $keycolumn=?";
            $this->log_message("DEL sql $sql ");
            $query  = $this->db->query($sql,array($keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);                        
        }
        function update(){
            $tableindex = $this->input->get_post('table_id');
            $colindex = $this->input->get_post('colindex');
            $newdata = $this->input->get_post('newdata');
            $keydata = $this->input->get_post('keydata');            
            $colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            $lab_id = $this->input->get_post('lab_id');
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("newdata : " . $newdata . " key $keydata");
            $sql=" Update $table_name SET $colname=? WHERE $keycolumn=? AND rd_frontend_lab_id=?";
            $this->log_message("update sql $sql ");
            $query  = $this->db->query($sql,array($newdata,$keydata,$lab_id));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);            
        }
        //function add
        
        function addrow(){
            $retval = "";
            $tableindex = $this->input->get_post('table_id');
            //$colindex = $this->input->get_post('colindex');
            //$colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $seq_name = $this->get_table_seq_name($tableindex);
            $table_name = $this->get_table_name($tableindex);
            $table_code = $this->get_table_code($tableindex);
            //$this->log_message("newdata : " . $newdata . " key $keydata");
            $sql = " SELECT get_next_value('" . $seq_name . "','" . $table_code . "') as nextvalue;";
            $this->log_message("log sql " . $sql);
            $query  = $this->db->query($sql);
            foreach($query->result() as $row) {
                $nextvalue = $row->nextvalue;
            }
            $sql=" INSERT INTO $table_name($keycolumn)VALUES(?)";
            $this->log_message("INSERT sql $sql ");
            $query  = $this->db->query($sql,array($nextvalue));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            $retval['newkey']=$nextvalue;
            echo json_encode($retval);            
        }       
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();                    
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }
        //
        //function regist info
    
        function get_payment_info($lab_id) {
            $data = array();
            $sql = " SELECT payment_labid as lab_id,payment_amount as amount ,payment_date as date,frontend_totalgross as bruto,frontend_totalnett as netto FROM payment INNER JOIN ";
            $sql = $sql . " Frontend ON Frontend.frontend_lab_id=payment.payment_labid ";
            $sql = $sql . " WHERE payment_labid=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($lab_id));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $bruto = $row->bruto;
                $netto = $row->netto;
                $totalpaid = $totalpaid + $row->amount;                
            }
            $disc = $bruto - $netto;
            $outstanding = $netto - $totalpaid;
            $data['bruto']= $bruto;
            $data['netto']= $netto;
            $data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
        }
        function save_note() {
            $lab_id = $this->input->get_post('lab_id');
            $note = $this->input->get_post('note');
            
            $retval =$this->modres->save_note($lab_id,$note);
            $data=array();
            $data['status']= $retval;
            echo json_encode($data);
        }
        function get_note() {
            $data=array();
            $retval = "";
            $lab_id = $this->input->get_post('lab_id');
            $retval = $this->modres->get_note($lab_id);            
            $data['note']=$retval;
            echo json_encode($data);     
        }
        //
	function result_print($labid) {
            $this->load->library('dompdf_gen');
            $this->html_headers->title = "Nota Pendaftaran";
            $this->load->model('model_regist','modregist');
            $data=array();
            $sql = " SELECT rd_pxcode,rd_pxname,rd_value,rd_unit,rd_normal,rd_pc_order,rd_parent FROM result_detail WHERE  rd_frontend_lab_id=? ORDER BY rd_pc_order,rd_pxcode";            
            $query = $this->db->query($sql,array($labid));            
            $this->log_message("sql : $sql dan no lab $labid");
	    $retval=array();
	    foreach($query->result() as $row) {
		$pxcode = $row->rd_pxcode;
		$pxname = $row->rd_pxname;
                $pxname = str_replace("&"," &amp;",$pxname);
                $pxname = str_replace("<"," &lt;",$pxname);
                $pxname = str_replace(">"," &gt;",$pxname);
                
		$remaining_num = strlen($pxcode) % 5;
		$this->log_message("REMAINING $remaining_num");
		if($remaining_num>0) {
			$pxname= "" . $pxname;
		}
		for($xx=0;$xx<$remaining_num;$xx++) {
			$pxname="&nbsp;&nbsp;&nbsp;" . $pxname;
			$this->log_message("px name ; $pxname");
	        }

		$pxvalue = $row->rd_value;
                $pxvalue = str_replace("&"," &amp;",$pxvalue);
                $pxvalue = str_replace("<"," &lt;",$pxvalue);
                $pxvalue = str_replace(">"," &gt;",$pxvalue);
                
                
		$pxunit = $row->rd_unit;
                $pxunit = str_replace("&"," &amp;",$pxunit);
                $pxunit = str_replace("<"," &lt;",$pxunit);
                $pxunit = str_replace(">"," &gt;",$pxunit);
                
                
		$pxnormal = $row->rd_normal;
                $pxnormal = str_replace("&"," &amp;",$pxnormal);
                $pxnormal = str_replace("<"," &lt;",$pxnormal);
                $pxnormal = str_replace(">"," &gt;",$pxnormal);
                
                
                $pxparent = $row->rd_parent;                
                $pxorder = $row->rd_pc_order;
                //
		$retval[ ] = array(
			'pxcode' => $pxcode,
			'pxname' => $pxname,
			'pxvalue' => $pxvalue,
			'pxunit' => $pxunit,
			'pxnormal' => $pxnormal,
                        'pxparent' => $pxparent,
                        'pxorder' => $pxorder
		);
	    }
            $data['listpx'] = $retval;
            $data['note'] = $this->modres->get_note($labid);
            $data['pxinfo'] = $this->modregist->regist_info($labid);
            $retval = $this->get_payment_info($labid);
            $data['bruto']= $retval['bruto'];
            $data['netto']= $retval['netto'];
            $data['disc']= $retval['disc'];
            $data['amount']= $retval['amount'];
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $html = $this->load->view('view_result_print', $data, true);
            $user_name = $this->user_name;
            $datename = $this->public->get_date_name();
            $datetimename = $this->public->get_date_time_name();
            
//            echo $html;
            //array(0,0,595.28,841.89) //a4
            $this->dompdf->set_paper(array(0,0,595.28,841.89), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();      
            $this->dompdf->get_canvas()->get_page_count().
            $font = Font_Metrics::get_font("helvetica", "bold");
            $font2 = Font_Metrics::get_font("helvetica", "normal");
            $canvas = $this->dompdf->get_canvas();
            $canvas->page_text(50, 820, "Halaman {PAGE_NUM} dari {PAGE_COUNT} Halaman", $font, 10, array(0,0,0));
            $canvas->page_text(450, 720, "Pemeriksa,", $font, 10, array(0,0,0));
            $canvas->page_text(450, 710, "Cirebon, $datename", $font, 10, array(0,0,0));
            $canvas->page_text(50, 735, " Printed By : $user_name / $datetimename ", $font2, 8, array(0,0,0));            
            $this->dompdf->stream("welcome.pdf",array('Attachment'=>0));            
            //$count= $this->$dompdf->get_canvas()->get_page_count();
            /**/
	}
        
}
?>