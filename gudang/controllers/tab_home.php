<?php

/**
 
 */
class Tab_home extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}
	function index() {
		$this->html_headers->title = "pos";
		$this->html_headers->description="pos";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                #$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/jquery-1.11.1.min.js";
                #$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                #$this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
		$this->load->view('tab_home');
	}            
        function data() {
            $sql="SELECT rd_frontend_lab_id,rd_pxcode, rd_id,rd_level,rd_map_machinecode,rd_price from result_detail";
            $retval = array();
            $query  = $this->db->query($sql);    
            $retval = $query->result();            
            echo json_encode($retval);
        }
}
?>