<?php
/**
 */
class Retur extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
	}
        function newstdoc() {
            $data=array();
            $sql=" SELECT get_next_value('seq_stdoc','STD') as stdvalue,date_name(localtimestamp) as datename";
            $query = $this->db->query($sql);
            foreach($query->result() as $row) {
                $retval =  $row->stdvalue;
                $data['newval']=$retval;
                $data['rdate']=$row->datename;
                $data['receipt']=$this->acl->get_real_users_id();
            }
            echo json_encode($data);
        }
        function index22() {
            echo "AD";
        }
        function readimage() {
            /* it works !*/
            $imgname = $this->input->get_post('imgname');
            $this->log_message("nama image $imgname");
            $sql = " SELECT * FROM docimage  where doc_id=?";
            $this->log_message($sql);
            $query = $this->db->query($sql,array($imgname));
            $retval=array();
            foreach($query->result() as $row) {
                //$row->img;
                $this->log_message("entering seq $imgname");
                $sequence = $row->doc_sequence;
                $imgdata = pg_unescape_bytea($row->doc_image);
                $file_name = "/var/www/pos/docs/" . $imgname . "_" . $sequence  . ".jpg";
                $img = fopen($file_name, 'wb') or die("cannot open image\n");
                fwrite($img, $imgdata) or die("cannot write image data\n");
                fclose($img);
		$retval[ ] = array(
			'fname' => base_url() . "docs/" . $imgname .  "_" . $sequence .  ".jpg"
		);
            }
            $data=array();
            $data['files']=$retval;
            echo json_encode($data);
            //echo "<img src='file:///tmp/scan.jpg' alt=img>";
        }
	function index() {
                $index="59";


                $form_access = $this->acl->form_access($index);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/iconFont.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";


                $this->html_headers->title = "Daftar Order";

                $data=array();
                $data['users_name']=$this->acl->get_real_users_id();
                $grid_name = "grid_modify_" .  $this->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->get_table_title($index);
                $data['default_sort_col']= $this->get_default_sort_column($index);

                $data['geturladdr']= $this->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->get_key_column_index($index);
                $data['updateurl']= $this->get_update_url_address($index);
                $data['drowurl']= $this->get_delete_url_address($index);
                $data['newrowurl']= $this->get_newrowurl_address($index);
                $data['latest_dj_id'] = $this->get_max_dj_id();
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->get_table_info($index);
                $data['menu'] = $this->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);
		$this->load->view('view_orders',$data);

                //$this->load->view('script_contracts');
	}//
	function buyreturn() {
                $index="59";


                $form_access = $this->acl->form_access($index);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }


                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/iconFont.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";


                $this->html_headers->title = "Retur Beli";

                $data=array();
                $data['users_name']=$this->acl->get_real_users_id();
                $grid_name = "grid_modify_" .  $this->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']="Retur Beli";
                $data['default_sort_col']= $this->get_default_sort_column($index);

                $data['geturladdr']= $this->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->get_key_column_index($index);
                $data['updateurl']= $this->get_update_url_address($index);
                $data['drowurl']= $this->get_delete_url_address($index);
                $data['newrowurl']= $this->get_newrowurl_address($index);
                $data['latest_dj_id'] = $this->get_max_dj_id();
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->get_table_info($index);
                $data['menu'] = $this->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);
		$this->load->view('view_retur_buyreturn',$data);

                //$this->load->view('script_contracts');
    }//
	function sellreturn() {
        $index="59";


        $form_access = $this->acl->form_access($index);
        if($form_access==-1) {
            $this->acl->show_acl_warning();
            return;
        }


        $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
        $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/iconFont.min.css";
        $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
        $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
        $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
        $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
        $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
        $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
        //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
        //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
        $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
        //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
        $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
        $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
        $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";


        $this->html_headers->title = "Retur Jual";

        $data=array();
        $data['users_name']=$this->acl->get_real_users_id();
        $grid_name = "grid_modify_" .  $this->get_table_name($index);
        $data['table_id']=$index;
        $data['table_title']="Retur Jual";
        $data['default_sort_col']= $this->get_default_sort_column($index);

        $data['geturladdr']= $this->get_dataurl_address($index);
        $data['grid_name']= $grid_name;
        $data['keycolumn_index'] = $this->get_key_column_index($index);
        $data['updateurl']= $this->get_update_url_address($index);
        $data['drowurl']= $this->get_delete_url_address($index);
        $data['newrowurl']= $this->get_newrowurl_address($index);
        $data['latest_dj_id'] = $this->get_max_dj_id();
        $this->log_message("UPDATE URL ". $data['updateurl'] );
        $data['table_detail']=$this->get_table_info($index);
        $data['menu'] = $this->get_menu();
        $data['menu_attr_url']="pos/get_menu_by_id/";
        $data['header_info']=$this->header_info;
        $this->load->view('init-view', $data);
        $this->load->view('view_retur_sellreturn',$data);

        //$this->load->view('script_contracts');
}//
        //update url : tf_updateurl
        function get_update_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
            $this->log_message("update url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_updateurl;
            }
            $this->log_message("update url " . $retval);
            return $retval;
        }
        //delete url
        function get_delete_url_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_delurl  FROM tableforms WHERE tf_code=? ";
            $this->log_message("del  url " . $sql .  " dan index " . $tableindex);
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_delurl;
            }
            $this->log_message("delurl url " . $retval);
            return $retval;
        }
        //data url : tf_dataurl_addr
        function get_dataurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_dataurl_addr;
            }
            return $retval;
        }
        //
        function get_newrowurl_address($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_newrow_url FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_newrow_url;
            }
            return $retval;
        }
        //sorting column
        function get_default_sort_column($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_default_sortcol;
            }
            return $retval;
        }

        //table title
        function get_table_title($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_table_title FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_table_title;
            }
            return $retval;
        }
        //
        function get_table_info($tableindex) {
            $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order";
            $this->log_message("Log table $tableindex  dan $sql ");
            $query  = $this->db->query($sql,array($tableindex));
            $retval = $query->result();
            return $retval;
        }
        //get table name
        function get_table_name($tableindex) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_tablename;
            }
            return $retval;
        }
        //get column name
        function get_column_name($tableindex,$colorder) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder));
            foreach($query->result() as $row) {
                $retval=$row->tfd_colname;
            }
            return $retval;
        }
        //col type
        function get_column_type($tableindex,$colorder) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tableforms_detail.tfd_coldatatype  FROM tableforms_detail WHERE tfd_code=? AND tfd_order=? ";
            $query  = $this->db->query($sql,array($tableindex,$colorder));
            foreach($query->result() as $row) {
                $retval=$row->tfd_coldatatype;
            }
            return (int)$retval;
        }
        //get key_column
        //
        function get_key_column($tableid) {
            //return "contract";
            $retval = "";
            $sql = " SELECT tf_keycolumn   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn;
            }
            return $retval;
        }
        function get_key_column_index($tableid) {
            //return "contract";
            $retval = 0;
            $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_keycolumn_index;
            }
            return $retval;
        }
        //
        //get sequence name
        function get_table_seq_name($tableid) {
            $retval = 0;
            $sql = " SELECT tf_seq_name   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_seq_name;
            }
            return $retval;
        }
        //
        //get sequence name
        function get_table_code($tableid) {
            $retval = 0;
            $sql = " SELECT tf_code   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableid));
            foreach($query->result() as $row) {
                $retval=$row->tf_code;
            }
            return $retval;
        }
        //function selected column
        function get_selected_column($tableindex) {
            //return "contract";
            $x=1;
            $retval = "";
            $sql = " SELECT tableforms_detail.tfd_colname  FROM tableforms_detail WHERE tfd_code=? and not(tfd_coldatatype=4) ORDER BY tfd_order";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                if($x==1) {
                    $retval= $retval . $row->tfd_colname ;
                }else {
                    $retval= $retval . "," . $row->tfd_colname ;
                }
                $x++;
            }
            $this->log_message("SELECTED COL : " . $retval);
            return $retval;
        }
        //end of selected col
        //function data($cur_page=1,$records_per_page=20,$sortBy='doct_name',$dir='asc',$col='1',$value) {
        //function data($cur_page=1,$rpp=20,$sortBy='doct_name',$dir='asc',$col='1',$value='',$idx) {
        function search_dj() {
            $data= array();
            $name = $this->input->get_post('name');
            //$this->log_message("pat name $mn $yr $stno");
            $sql = " SELECT dj_id,tour.tour_id as tour_id,j_name as name,j_title as title,j_addr as addr,j_telp as phone,j_hp  as phonecell,j_kelamin as sex,extract(day from j_tgl_lahir)::text || '.' || extract(month from j_tgl_lahir)::text ||  '.' || extract(year from j_tgl_lahir)::text as age,";
            $sql = $sql . " ' ' as desc,";
            $sql = $sql . " j_kota as city,j_no_passport as passport,j_issuing_office as issue_off,j_date_of_issue as issue_date,j_mahram as idmahram ";
            $sql = $sql . " FROM jamaah INNER JOIN tour ON tour.tour_j_id=jamaah.j_id ";
            $sql = $sql . " INNER JOIN tour_detail ON tour_detail.tour_id=tour.tour_id ";
            $sql = $sql . " INNER JOIN master_paket ON tour_detail.tour_item=master_paket.mp_id ";
            $sql = $sql . " INNER JOIN dokumen_jamaah ON tour.tour_id=dokumen_jamaah.dj_tour_id ";
            $sql = $sql . " WHERE lower(trim(j_name)) like '%$name%'";

            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql);
            //$retval[]=array();
            foreach($query->result() as $row) {
                $this->log_message("ENTERING ROW..");
		$retval[ ] = array(
                        'dj_id' => $row->dj_id,
                        'name' => $row->name,
			'title' => $row->title,
                        'addr' => $row->addr,
                        'phone' => $row->phone,
                        'phonecell' => $row->phonecell,
                        'sex' => $row->sex,
                        'age' => $row->age,
                        'city' => $row->city,
                        'passport' => $row->passport,
                        'issue_off' => $row->issue_off,
                        'issue_date' => $row->issue_date,
                        'mahram_id' => $row->idmahram,
		);
            }
            //$data['jamaah']=$query->result();
            $data['jamaah']=$retval;
            echo json_encode($data);
        }
        function search_regist_filtered() {
            $data= array();
            $mn = $this->input->get_post('mn');
            $yr = $this->input->get_post('yr');
            $stno = $this->input->get_post('stno');
            $this->log_message("pat name $mn $yr $stno");
            $sql = " SELECT distinct(dj_id) as dj_id";
            $sql = $sql . " FROM dokumen_jamaah ";
            $sql = $sql . " WHERE extract(month from dj_date) =?";
            $sql = $sql . " AND extract(year from dj_date) =?";

            $this->log_message("SQL to exec $sql");
            $query = $this->db->query($sql,array($mn,$yr));
            $retval[]=array();
            foreach($query->result() as $row) {
                $this->log_message("ENTERING ROW..");
		$retval[ ] = array(
                        'dj_id' => $row->dj_id,
		);
            }
            //$data['jamaah']=$query->result();
            $data['jamaah']=$retval;
            echo json_encode($data);
        }

        function data() {
            //
            $this->log_message("Data Called");
            //$cur_page=((int)$cur_page)-1;
            $idx = $this->input->get_post('table_id');
            //dj_id
            $dj_id = $this->input->get_post('dj_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $cur_page = 1;

            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            //$dj_id='140800048';
            $selfilter = $this->get_selection_filter_sql($idx);
            $tablename = $this->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->get_column_name($idx,$col);
            $coltype = $this->get_column_type($idx,$col);
            $this->log_message("COL NAME $col value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where $selfilter AND  dj_id=? ";

            $query_count = $this->db->query($sqlcount,array($dj_id));
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }
            $this->log_message("current page $cur_page jumlah $jumlah");
            $selected_column = $this->get_selected_column($idx);
            $sql=" SELECT $selected_column,(j_title || j_name) as name  from $tablename  INNER JOIN tour on tour.tour_id=dokumen_jamaah.dj_tour_id ";
            $sql = $sql . " INNER JOIN jamaah on jamaah.j_id=tour.tour_j_id ";
            $sql = $sql . "$where $selfilter AND dj_id=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql dan no lab $dj_id");
            $retval = array();
            //
            $query  = $this->db->query($sql,array($dj_id));
            $data = $query->result();
            $this->log_message("affected $jumlah");
            //$retval['curPage']=$cur_page;
            $retval['curPage']=1;
            $retval['totalRecords']= $jumlah;
            //$retval['totalRecords']= 100;
            $retval['data']=$data;
            echo json_encode($retval);
        }
        function show_order() {
            //$this->load->model('model_bank','bank');
            $mnth = $this->input->get_post('mnth');
            $yr = $this->input->get_post('yr');
            $userid = $this->input->get_post('userid');
            $sql = " SELECT transout.*,client.*  FROM ";
            $sql = $sql . " transout inner join client on client.cl_id=transout.tro_cust_id ";
            //$sql = $sql . " INNER JOIN orders_detail  on orders.orders_no=orders_detail.orders_no ";
            //$sql = $sql . " inner join bank on bank.bank_id=orders.orders_bank_id ";
            $sql = $sql . " WHERE 1=1 ";
            $this->log_message("USER ID " .$userid);
            if(!($mnth=="")) {
                $sql = $sql . " AND extract(month from tro_date)=? ";
            }
            if(!($yr=="")) {
                $sql = $sql . " AND extract(year from tro_date)=? ";
            }
            //


            $sql = $sql . " ORDER BY tro_id";
            $query=$this->db->query($sql,array($mnth,$yr));
            //if(!($mnth=="")) {
            $this->log_message($sql . " user $userid , month $mnth , year $yr");
            $counter=1;
            $retval=array();
            foreach($query->result() as $row) {
                $bankname="";
                //$bankname=$this->bank->get_bank_name($row->orders_bank_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'name' => $row->cl_name,
                        'orders_no' => $row->tro_id,
                        'bank' => $bankname
                    );
                $counter=$counter+1;
            }
            $data=array();
            $data['list']=$retval;
            echo json_encode($data);
        }
        function show_order_in() {
            //$this->load->model('model_bank','bank');
            $mnth = $this->input->get_post('mnth');
            $yr = $this->input->get_post('yr');
            $userid = $this->input->get_post('userid');
            $sql = " SELECT transin.*,supplier.*  FROM ";
            $sql = $sql . " transin inner join supplier on supplier.supp_id=transin.tri_supp_id ";
            //$sql = $sql . " INNER JOIN orders_detail  on orders.orders_no=orders_detail.orders_no ";
            //$sql = $sql . " inner join bank on bank.bank_id=orders.orders_bank_id ";
            $sql = $sql . " WHERE 1=1 ";
            $this->log_message("USER ID " .$userid);
            if(!($mnth=="")) {
                $sql = $sql . " AND extract(month from tri_date)=? ";
            }
            if(!($yr=="")) {
                $sql = $sql . " AND extract(year from tri_date)=? ";
            }
            //


            $sql = $sql . " ORDER BY tri_id";
            $query=$this->db->query($sql,array($mnth,$yr));
            //if(!($mnth=="")) {
            $this->log_message($sql . " user $userid , month $mnth , year $yr");
            $counter=1;
            $retval=array();
            foreach($query->result() as $row) {
                $bankname="";
                //$bankname=$this->bank->get_bank_name($row->orders_bank_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'name' => $row->supp_name,
                        'orders_no' => $row->tri_id,
                        'bank' => $bankname
                    );
                $counter=$counter+1;
            }
            $data=array();
            $data['list']=$retval;
            echo json_encode($data);
        }
//
        function ac_order() {
            $this->load->model('model_bank','bank');
            $name = $this->input->get_post('code');
            //$id = $this->input->get_post('id');
            $sql = " SELECT orders.*,client.*  FROM ";
            $sql = $sql . " orders inner join client on client.cl_id=orders.orders_client_id ";
            $sql = $sql . " WHERE 1=1 ";
            $sql = $sql . " AND lower(client.cl_name) like '%$name%' OR lower(client.cl_name) like '%$name%'  ";
            $counter=1;
            $retval=array();
            $query = $this->db->query($sql);
            foreach($query->result() as $row) {
                $bankname="";
                $bankname=$this->bank->get_bank_name($row->orders_bank_id);
		$retval[ ] = array(
                        'counter' => $counter,
			'name' => $row->cl_name,
                        'addr' => $row->cl_address,
                        'orders_no' => $row->orders_no,
                        'phone' => $row->cl_phone,
                        'phonecell' => $row->cl_phonecell,
                        'bank' => $bankname,
                        'regdate' => $row->cl_regdate,
                    );
                $counter=$counter+1;
            }
            $data=array();
            $data['list']=$retval;
            echo json_encode($data);
            //
        }
        function zap() {
            $data=array();
            $id = $this->input->get_post('id');
            //
            $this->log_message("DELETION OF $id");
            $sql = " SELECT * from transout WHERE tro_id=?";
            $this->log_message($sql . " id : " . $id);
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $this->log_message("ENTERING ITERATION $row->orders_id");
                $sqldel = " INSERT INTO delhistory(dh_tr_id,dh_gj,dh_mahram,dh_j_id,dh_tour_paket,dh_date_regist";
                $sqldel = $sqldel . ")";
                $sqldel = $sqldel . "VALUES(?,?,?,?,?,?)";
                $orders_id=  $row->tro_id;
                $orders_gj=  NULL;
                $orders_mahram=  NULL;
                $orders_j_id=  NULL;
                $orders_paket=  NULL;
                $orders_date_regist=  NULL;
                $this->db->query($sqldel,array($orders_id,$orders_gj,$orders_mahram,$orders_j_id,$orders_paket,$orders_date_regist));
            }
            $this->log_message("ABOUT TO DEL PAYMENT");
            $sql = " DELETE FROM payment WHERE payment_trid=?";
            $this->db->query($sql,array($id));
            $this->log_message($sql);
            $this->log_message("ABOUT TO DEL TOUR");
            $sql = " DELETE FROM trans_detail WHERE trans_id=?";
            $this->db->query($sql,array($id));

            $sql = " DELETE FROM transout WHERE tro_id=?";
            $this->log_message($sql);
            $this->db->query($sql,array($id));
            //

            $data['status']=1;
            echo json_encode($data);
        }
        //
        function zap_in() {
            $data=array();
            $id = $this->input->get_post('id');
            //
            $this->log_message("DELETION OF $id");
            $sql = " SELECT * from transin WHERE tri_id=?";
            $this->log_message($sql . " id : " . $id);
            $query = $this->db->query($sql,array($id));
            foreach($query->result() as $row) {
                $this->log_message("ENTERING ITERATION $row->orders_id");
                $sqldel = " INSERT INTO delhistory(dh_tr_id,dh_gj,dh_mahram,dh_j_id,dh_tour_paket,dh_date_regist";
                $sqldel = $sqldel . ")";
                $sqldel = $sqldel . "VALUES(?,?,?,?,?,?)";
                $orders_id=  $row->tri_id;
                $orders_gj=  NULL;
                $orders_mahram=  NULL;
                $orders_j_id=  NULL;
                $orders_paket=  NULL;
                $orders_date_regist=  NULL;
                $this->db->query($sqldel,array($orders_id,$orders_gj,$orders_mahram,$orders_j_id,$orders_paket,$orders_date_regist));
            }
            $this->log_message("ABOUT TO DEL PAYMENT");
            $sql = " DELETE FROM journal WHERE j_desc=?";
            $this->db->query($sql,array($id));
            $this->log_message($sql);
            $this->log_message("ABOUT TO DEL TOUR");
            $sql = " DELETE FROM trans_detail WHERE trans_id=?";
            $this->db->query($sql,array($id));
            $sql = " DELETE FROM transin WHERE tri_id=?";
            $this->log_message($sql);
            $this->db->query($sql,array($id));
            //

            $data['status']=1;
            echo json_encode($data);
        }
        //delete
        function drow() {
            $tableindex = $this->input->get_post('table_id');
            $keydata = $this->input->get_post('keydata');
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("DELETE  FROM $table_name dengan key $keydata");
            $sql=" DELETE FROM $table_name WHERE $keycolumn=?";
            $this->log_message("addelpx DEL sql $sql ");
            //$query  = $this->db->query($sql,array($keydata));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);
        }
        function update(){
            $tableindex = $this->input->get_post('table_id');
            $colindex = $this->input->get_post('colindex');
            $newdata = $this->input->get_post('newdata');
            $keydata = $this->input->get_post('keydata');
            $colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            $keycolumn2 =  "dj_tour_id";
            $keydata2 = $this->input->get_post('keydata2');
            //get_column_name
            $table_name = $this->get_table_name($tableindex);
            $this->log_message("newdata : " . $newdata . " key $keydata");
            $sql=" Update $table_name SET $colname=? WHERE $keycolumn=? AND $keycolumn2=? ";
            $this->log_message("update sql $sql ");
            $query  = $this->db->query($sql,array($newdata,$keydata,$keydata2));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            echo json_encode($retval);
        }
        //function add

        function addrow(){
            $retval = "";
            $tableindex = $this->input->get_post('table_id');
            //$colindex = $this->input->get_post('colindex');
            //$colname = $this->get_column_name($tableindex,$colindex);
            $keycolumn =  $this->get_key_column($tableindex);
            //get_column_name
            $seq_name = $this->get_table_seq_name($tableindex);
            $table_name = $this->get_table_name($tableindex);
            $table_code = $this->get_table_code($tableindex);
            //$this->log_message("newdata : " . $newdata . " key $keydata");
            $sql = " SELECT get_next_value('" . $seq_name . "','" . $table_code . "') as nextvalue;";
            $this->log_message("log sql " . $sql);
            $query  = $this->db->query($sql);
            foreach($query->result() as $row) {
                $nextvalue = $row->nextvalue;
            }
            $sql=" INSERT INTO $table_name($keycolumn)VALUES(?)";
            $this->log_message("INSERT sql $sql ");
            $query  = $this->db->query($sql,array($nextvalue));
            $retval=array();
            //sd;
            $status = 1;
            $retval['status']=$status;
            $retval['newkey']=$nextvalue;
            echo json_encode($retval);
        }
        function get_menu() {
            $sql = " SELECT * FROM menus ORDER BY menu_order";
            $query = $this->db->query($sql);
            return $query->result();
        }
        //search for menudata
        function get_menu_by_id($menu_id) {
            $sql = " SELECT menus.* FROM menus WHERE lower(trim(menu_htmlid))=?";
            $query= $this->db->query($sql,array($menu_id));
            $retval = $query->result();
            $data=array();
            $data['tab_info']=$retval;
            echo json_encode($data);
        }
        //
        //function regist info

        function get_payment_info($dj_id) {
            $data = array();
            $sql = " SELECT payment_labid as dj_id,payment_amount as amount ,payment_date as date,frontend_totalgross as bruto,frontend_totalnett as netto FROM payment INNER JOIN ";
            $sql = $sql . " Frontend ON Frontend.frontend_dj_id=payment.payment_labid ";
            $sql = $sql . " WHERE payment_labid=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($dj_id));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $bruto = $row->bruto;
                $netto = $row->netto;
                $totalpaid = $totalpaid + $row->amount;
            }
            $disc = $bruto - $netto;
            $outstanding = $netto - $totalpaid;
            $data['bruto']= $bruto;
            $data['netto']= $netto;
            $data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
        }
        //
	function result_print($labid) {
            $this->load->library('dompdf_gen');
            $this->html_headers->title = "Nota Pendaftaran";
            $data=array();
            $sql = " SELECT rd_pxcode,rd_pxname,rd_value,rd_unit,rd_normal FROM result_detail WHERE  dj_id=? ORDER BY rd_pxcode";
            $query = $this->db->query($sql,array($labid));
            $this->log_message("sql : $sql dan no lab $labid");
            $data['listpx'] = $query->result();
            $data['pxinfo'] = $this->regist_info($labid);
            $retval = $this->get_payment_info($labid);
            $data['bruto']= $retval['bruto'];
            $data['netto']= $retval['netto'];
            $data['disc']= $retval['disc'];
            $data['amount']= $retval['amount'];
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $html = $this->load->view('view_result_print', $data, true);
           //echo $html;

            $this->dompdf->set_paper(array(0,0,419.53,595.28), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->get_canvas()->get_page_count().
            $font = Font_Metrics::get_font("helvetica", "bold");
            $canvas = $this->dompdf->get_canvas();
            $canvas->page_text(100, 500, "Halaman {PAGE_NUM} dari {PAGE_COUNT} Halaman", $font, 10, array(0,0,0));
            //$this->dompdf->page_script('
              // $pdf is the variable containing a reference to the canvas object provided by dompdf
//              $pdf->line(10,730,800,730,array(0,0,0),1);
  //          ');
            $this->dompdf->stream("welcome.pdf",array('Attachment'=>0));
            //$count= $this->$dompdf->get_canvas()->get_page_count();
            /**/
	}
        //filter
        function get_selection_filter_sql($tableindex) {
            $retval = "";
            $sql = " SELECT tf_selection_filter   FROM tableforms  WHERE tf_code=? ";
            $query  = $this->db->query($sql,array($tableindex));
            foreach($query->result() as $row) {
                $retval=$row->tf_selection_filter;
            }
            return $retval;
        }

        function add_doc() {
            $data=array();
            $data['status']=1;
            $tr_id = $this->input->get_post('tr_id');
            $dj_id = $this->input->get_post('dj_id');
            $dj_sender = $this->input->get_post('sender');
            $userid = $this->acl->get_users_id();
            $sql=" INSERT INTO dokumen_jamaah(dj_tour_id,dj_id,dj_sender,dj_userid)VALUES(?,?,?,?)";
            $this->log_message("$sql dengan $tr_id dan $dj_id");
            $this->db->query($sql,array($tr_id,$dj_id,$dj_sender,$userid));
            $data['result']=1;
            echo json_encode($data);
        }
        function get_max_dj_id() {
            $retval = "";
            $sql = " SELECT max(dj_id) as dj_id from dokumen_jamaah";
            $query  = $this->db->query($sql);
            foreach($query->result() as $row) {
                $retval=$row->dj_id;
            }
            return $retval;
        }
}
?>
