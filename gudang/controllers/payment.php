<?php
/**
 */
class Payment extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
                $this->user_id = $this->acl->get_users_id();
	}

	function index() {
                $formid="15030224";
                $index="15030224";
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->title = "PEMBAYARAN";
                $this->load->model('model_menu','menu');
                $data=array();
                $grid_name = "grid_" .  $this->menu->get_table_name($index);
                $data['table_id']=$index;
                $data['table_title']=$this->menu->get_table_title($index);
                $data['default_sort_col']= $this->menu->get_default_sort_column($index);
                $data['geturladdr']= $this->menu->get_dataurl_address($index);
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = $this->menu->get_key_column_index($index);
                $data['updateurl']= $this->menu->get_update_url_address($index);
                $data['drowurl']= $this->menu->get_delete_url_address($index);
                $data['newrowurl']= $this->menu->get_newrowurl_address($index);
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->menu->get_table_info($index);
                $data['header_info']=$this->header_info;
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $port="LPT1";
		$data[ 'port_bcd' ] = $port;
                //'<applet name="app1" code="portsapp.parwriter.class" archive="' . base_url() . '/applet/portsapp.jar">
		$data[ 'applet' ] =
			'<applet name="app1" code="portsapp.parwriter.class" archive="' . base_url() . 'asset2/applet/portsapp.jar">
                        <param name="portname" value="' . $port . '"/>
                        </applet>';


                $this->load->view('init-view', $data);
		$this->load->view('view_payment',$data);
                //$this->load->view('script_contracts');
	}//
        //show data
        function data() {
            //
            //$idx="15030224";
            //$cur_page=((int)$cur_page)-1;
            $this->load->model('model_menu','menu');
            $idx = $this->input->get_post('table_id');
            $trid = $this->input->get_post('tr_id');
            $col = $this->input->get_post('colsearch');
            $rpp = $this->input->get_post('rpp');
            $cur_page = $this->input->get_post('curpage');
            $cur_page = 1;
            $sortBy = $this->input->get_post('sortidx');
            $dir = $this->input->get_post('sortdir');
            $value = $this->input->get_post('valsearch');
            //
            $idx="" . $idx;
            $tablename = $this->menu->get_table_name($idx);
            $this->log_message("NAMA TABLE $tablename column $col untuk table id $idx");
            $colname = $this->menu->get_column_name($idx,$col);
            $coltype = $this->menu->get_column_type($idx,$col);
            $selfilter = $this->menu->get_selection_filter_sql($idx);
            $this->log_message("COL NAME $col value $value coltype $coltype");
            if($coltype=="1") {
                if($value=="#") {
                    $value=0;
                }
                $where = " WHERE 1=1 AND ($colname = $value)";
            }if($coltype=="2") {
                if($value=="#") {
                    $value="";
                }
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }else {
                $where = " WHERE 1=1 AND (lower(trim($colname)) like '%" . strtolower($value) . "%')";
            }
            $sqlcount=" SELECT count(*) as jumlah from $tablename $where  $selfilter AND  trans_id=? ";
            $this->log_message("current page $cur_page , trid $trid  dan $sqlcount");
            $query_count = $this->db->query($sqlcount,$trid);
            $jumlah=0;
            foreach($query_count->result() as $row) {
                $jumlah = $row->jumlah;
            }
            $retnum = floor($jumlah/$rpp);
            $modulo = $jumlah % $rpp;
            if($cur_page==$retnum+1) {
                $limit = $modulo;
            }else {
                $limit = $rpp;
            }
            $selected_column = $this->menu->get_selected_column($idx);
            $sql=" SELECT $selected_column  from $tablename  $where $selfilter AND trans_id=? ORDER BY $sortBy $dir LIMIT $limit OFFSET ($cur_page-1)";
            $this->log_message("SQL DATA $sql");
            $retval = array();
            //
            $query  = $this->db->query($sql,$trid);
            $data = $query->result();
            $this->log_message("affected $jumlah");
            $retval['curPage']=$cur_page;
            $retval['totalRecords']= $jumlah;
            $retval['data']=$data;
            echo json_encode($retval);
        }

        //end of show data
        //update url : tf_updateurl
        function get_payment_info($trid) {
            $data = array();
            $sql = " SELECT payment_trid as tr_id,payment_amount as amount ,payment_date as date,frontend_totalgross as bruto,frontend_totalnett as netto FROM payment INNER JOIN ";
            $sql = $sql . " Frontend ON Frontend.frontend_tr_id=payment.payment_trid ";
            $sql = $sql . " WHERE payment_trid=?";
            $this->log_message("payment info $sql");
            $query=  $this->db->query($sql,array($trid));
            $bruto = 0;
            $netto = 0;
            $disc = 0;
            $totalpaid =0 ;
            foreach($query->result() as $row) {
                $bruto = $row->bruto;
                $netto = $row->netto;
                $totalpaid = $totalpaid + $row->amount;
            }
            $disc = $bruto - $netto;
            $outstanding = $netto - $totalpaid;
            $data['bruto']= $bruto;
            $data['netto']= $netto;
            $data['disc']= $disc;
            $data['amount']= $totalpaid;
            $data['outstanding'] = $outstanding;
            if($outstanding>0) {
                $paid = 0;
            }else {
                $paid = 1;
            }
            $data['paid'] = $paid;
            return $data;
        }
        //show payments
	function show_payments() {
            $trid = $this->input->get_post('tr_id');
            //$trid="1411080003";
            $this->load->model('model_payment','mp');
            $data=array();
            $data= $this->mp->show_payments($trid);
            echo json_encode($data);
        }
        //
        function save_payment() {
            $trid = $this->input->get_post('tr_id');
            $amount = $this->input->get_post('amount');
            $type = $this->input->get_post('type');
            $this->load->model('model_payment','mp');
            $data=array();
            $data= $this->mp->save_payment($trid,$amount,$type);
            echo json_encode($data);
        }
        function delregist() {
            $trid = $this->input->get_post('tr_id');
            $sql="DELETE FROM trans_detail WHERE trans_id=?";
            $query = $this->db->query($sql,array($trid));
            //
            $sql="DELETE FROM transout  WHERE tro_id=?";
            $query = $this->db->query($sql,array($trid));
            //
            $sql="DELETE FROM payment  WHERE payment_trid=?";
            $query = $this->db->query($sql,array($trid));
            //
            $data['status'] = 1;
            echo json_encode($data);
        }
        function cash_in() {
            $this->load->model('model_users','modusers');
            $this->load->model('model_payment','payment');
            $filterdate = $this->input->get_post('filterdate');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            //
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate2=="") {
                $filterdate2="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate2");
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];

            $data = $this->payment->cash_in($filterdate,$filterdate2);
            echo json_encode($data);
        }
        function cash_in_per_user() {
            $this->load->model('model_users','modusers');
            $this->load->model('model_payment','payment');
            $filterdate = $this->input->get_post('filterdate');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            //
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate2=="") {
                $filterdate2="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate2");
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            //
            $data = $this->payment->cash_in_per_user($filterdate,$filterdate2);
            echo json_encode($data);

        }
        function show_all_cash($index) {
                if($index==1) {
                    $formid="21";
                }else {
                    $formid="22";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                //
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                $this->html_headers->title = "Penerimaan Kas";
                $this->load->model('model_menu','menu');
                $data=array();
                $grid_name = "grid_" .  $this->menu->get_table_name($index);
                $data['table_id']=$index;

                $data['default_sort_col']= 1;
                if($index==1) {
                    $data['table_title']="PENERIMAAN KAS PER USER (" . $this->acl->get_real_users_id() . ")";
                    $data['geturladdr']= "payment/cash_in_per_user";
                    $data['cash_type']= "1";
                }else {
                    $data['table_title']="PENERIMAAN KAS KESELURUHAN";
                    $data['geturladdr']= "payment/cash_in";
                    $data['cash_type']= "2";
                }
                $this->html_headers->title = $data['table_title'];
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = 1;
                $data['updateurl']= "";
                $data['drowurl']= "";
                $data['newrowurl']= "";
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->menu->get_table_info("15030225");
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $data['header_info']=$this->header_info;
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $this->load->view('init-view', $data);
		$this->load->view('view_cash_in',$data);
        }

        //
        function kwitansi($labid,$payment_no) {
            $this->load->library('dompdf_gen');
            $this->load->model('model_payment','mp');
            $this->load->model('model_regist','modreg');
            $this->load->model('model_public','modpub');
            $this->html_headers->title = "Kwitansi";
            $data=array();
            //
            $sql = " SELECT trans_id,tour_item,tour_itemname,tour_price,tour_disc,tour_net FROM  tour_detail WHERE  trans_id=? ORDER BY tour_item";
            $query = $this->db->query($sql,array($labid));
            $data['listpx'] = $query->result();
            $sql = " SELECT * FROM payment WHERE payment_trid=? AND payment_no=?";
            $this->log_message("KWITANSI $sql $labid no $payment_no");
            $query = $this->db->query($sql,array($labid,$payment_no));
            foreach($query->result() as $row) {
                $data['payment_no']= $row->payment_no;
                $data['amount']= $row->payment_amount;
                $this->log_message("AMOUNT " . $row->payment_amount);
                //$data['terbilang']= ucwords($this->modpub->terbilang($row->payment_amount)) . " Rupiah";
                $data['terbilang']= ucwords($this->modpub->terbilang($row->payment_amount)) . " USD";
            }
            //
            $this->log_message("KWITANSI");
            $data['pxinfo'] = $this->modreg->regist_info($labid);
            $this->log_message("KWITANSI 2");
            $retval = $this->mp->get_payment_info($labid);
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $data['logged_in_user_name'] = $this->acl->get_real_users_id();
            $html = $this->load->view('view_kwitansi', $data, true);
//            echo $html;
 // A5

            $this->dompdf->set_paper(array(0,0,419.53,595.28), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("kwitansi" . $labid . ".pdf",array('Attachment'=>0));

	}
        function retur($labid,$payment_no) {
            $this->load->library('dompdf_gen');
            $this->load->model('model_payment','mp');
            $this->load->model('model_regist','modreg');
            $this->load->model('model_public','modpub');
            $this->html_headers->title = "Kwitansi";
            $data=array();
            //
            $sql = " SELECT rd_pxcode,rd_pxname,rd_price,rd_disc,rd_net FROM result_detail WHERE length(rd_pxcode)=5 AND rd_frontend_tr_id=? ORDER BY rd_pxcode";
            $query = $this->db->query($sql,array($labid));
            $data['listpx'] = $query->result();
            $sql = " SELECT * FROM payment WHERE payment_trid=? AND payment_no=?";
            $this->log_message("KWITANSI $sql $labid no $payment_no");
            $query = $this->db->query($sql,array($labid,$payment_no));
            foreach($query->result() as $row) {
                $data['payment_no']= $row->payment_no;
                $data['amount']= abs($row->payment_amount);
                $amountretur = $row->payment_ref_net;
                $data['pxname']= $row->payment_ref_pxname;
                $this->log_message("AMOUNT " . $row->payment_amount);
                //$data['terbilang']= ucwords($this->modpub->terbilang($amountretur)) . " Rupiah";
                $data['terbilang']= ucwords($this->modpub->terbilang($amountretur)) . " USD";
            }
            //
            $this->log_message("RETUR");
            $data['pxinfo'] = $this->modreg->regist_info($labid);
            $this->log_message("RETUR 2");
            $retval = $this->mp->get_payment_info($labid);
            $data['outstanding'] = $retval['outstanding'];
            $data['paid'] = $retval['paid'];
            $data['logged_in_user_name'] = $this->acl->get_real_users_id();
            $html = $this->load->view('view_retur', $data, true);
            //echo $html;
 // A5
            $this->dompdf->set_paper(array(0,0,419.53,595.28), "portrait" ); // 12" x 12"
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("retur" . $labid . ".pdf",array('Attachment'=>0));
	}
        //
        function outstanding() {
            $this->load->model('model_payment','mp');
            $retval=array();
            $start="2014-09-01";
            $finish="2014-12-31";
            $filterdate = $this->input->get_post('filterdate');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            //
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate2=="") {
                $filterdate2="01.01.1979";
            }
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate2");
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];

            $sql =" SELECT tro_id,cl_name as name FROM transout inner join client on transout.tro_cust_id=client.cl_id WHERE to_date(tro_date::text,'YYYY-MM-DD')>=? AND to_date(tro_date::text,'YYYY-MM-DD')<=?  ORDER BY tro_id ";
            $global_q = $this->db->query($sql,array($filterdate,$filterdate2));
            $total=0;
            $counter=0;
            $this->log_message($sql);
            foreach($global_q->result() as $row_global) {
                $this->log_message($row_global->tro_id);
                $trans_id=$row_global->tro_id;
                $arr_paid = $this->mp->tr_id_is_paid($trans_id);
                //$row_global->trans_id);
                $paid=$arr_paid['paid'];
                $outstanding = $arr_paid['outstanding'];
                if($paid==0) {
                        $counter=$counter+1;
                        $name = $row_global->name;
                        $retval[ ] = array(
			'counter' => $counter,
                        'trans_id' => $row_global->tro_id,
                        'name' => $name,
                        'outstanding' => $outstanding
                        );
                        $total=$total+$outstanding;
                }
            }
            $retval[ ] = array(
            'counter' => "",
            'trans_id' => "",
            'name' => "TOTAL",
            'outstanding' => $total
            );
            $data=array();
            $data['curPage']=1;
            $data['totalRecords']= 1000;
            $data['data']=$retval;
            //$this->log_message("DATA : $data");
            echo json_encode($data);
        }
        //
        function show_outstanding() {
                $index="1";
                /*
                if($index==1) {
                    $formid="21";
                }else {
                    $formid="22";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
                 *
                 */
                //
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                $this->html_headers->title = "Display Harian";
                $this->load->model('model_menu','menu');
                $data=array();
                $grid_name = "grid_" .  $this->menu->get_table_name($index);
                $data['table_id']=$index;

                $data['default_sort_col']= 1;
                if($index==1) {
                    $data['table_title']="PIUTANG ";
                    $data['geturladdr']= "payment/outstanding";
                }

                $this->html_headers->title = $data['table_title'];
                $data['grid_name']= $grid_name;
                $data['keycolumn_index'] = 1;
                $data['updateurl']= "";
                $data['drowurl']= "";
                $data['newrowurl']= "";
                $this->log_message("UPDATE URL ". $data['updateurl'] );
                $data['table_detail']=$this->menu->get_table_info("14120217");
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                $data['header_info']=$this->header_info;
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $this->load->view('init-view', $data);
		$this->load->view('view_outstanding',$data);
        }
        function reprint_nota() {
            $index="59";


            $form_access = $this->acl->form_access($index);
            if($form_access==-1) {
                $this->acl->show_acl_warning();
                return;
            }


            $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/iconFont.min.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
            $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
            //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
            //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
            $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
            //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
            $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";


            $this->html_headers->title = "Cetak Nota Ulang";

            $data=array();
            $data['users_name']=$this->acl->get_real_users_id();
            $grid_name = "grid_modify_" .  $this->get_table_name($index);
            $data['table_id']=$index;
            $data['table_title']="Cetak Nota Ulang";
            $data['default_sort_col']= $this->get_default_sort_column($index);

            $data['geturladdr']= $this->get_dataurl_address($index);
            $data['grid_name']= $grid_name;
            $data['keycolumn_index'] = $this->get_key_column_index($index);
            $data['updateurl']= $this->get_update_url_address($index);
            $data['drowurl']= '';
            $data['newrowurl']= '';
            $data['latest_dj_id'] = $this->get_max_dj_id();
            $this->log_message("UPDATE URL ". $data['updateurl'] );
            $data['table_detail']=$this->get_table_info($index);
            $data['menu'] = $this->get_menu();
            $data['menu_attr_url']="pos/get_menu_by_id/";
            $data['header_info']=$this->header_info;
            $this->load->view('init-view', $data);
            $this->load->view('view_reprint_nota',$data);

            //$this->load->view('script_contracts');
    }//
            //get table name
            function get_table_name($tableindex) {
                //return "contract";
                $retval = "";
                $sql = " SELECT tableforms.tf_tablename  FROM tableforms WHERE tf_code=? ";
                $query  = $this->db->query($sql,array($tableindex));
                foreach($query->result() as $row) {
                    $retval=$row->tf_tablename;
                }
                return $retval;
            }
            function get_default_sort_column($tableindex) {
                //return "contract";
                $retval = "";
                $sql = " SELECT tableforms.tf_default_sortcol FROM tableforms WHERE tf_code=? ";
                $query  = $this->db->query($sql,array($tableindex));
                foreach($query->result() as $row) {
                    $retval=$row->tf_default_sortcol;
                }
                return $retval;
            }
            function get_dataurl_address($tableindex) {
                //return "contract";
                $retval = "";
                $sql = " SELECT tableforms.tf_dataurl_addr FROM tableforms WHERE tf_code=? ";
                $query  = $this->db->query($sql,array($tableindex));
                foreach($query->result() as $row) {
                    $retval=$row->tf_dataurl_addr;
                }
                return $retval;
            }
            function get_key_column_index($tableid) {
                //return "contract";
                $retval = 0;
                $sql = " SELECT tf_keycolumn_index   FROM tableforms  WHERE tf_code=? ";
                $query  = $this->db->query($sql,array($tableid));
                foreach($query->result() as $row) {
                    $retval=$row->tf_keycolumn_index;
                }
                return $retval;
            }
            function get_update_url_address($tableindex) {
                //return "contract";
                $retval = "";
                $sql = " SELECT tableforms.tf_updateurl FROM tableforms WHERE tf_code=? ";
                $this->log_message("update url " . $sql .  " dan index " . $tableindex);
                $query  = $this->db->query($sql,array($tableindex));
                foreach($query->result() as $row) {
                    $retval=$row->tf_updateurl;
                }
                $this->log_message("update url " . $retval);
                return $retval;
            }
            function get_max_dj_id() {
                $retval = "";
                $sql = " SELECT max(dj_id) as dj_id from dokumen_jamaah";
                $query  = $this->db->query($sql);
                foreach($query->result() as $row) {
                    $retval=$row->dj_id;
                }
                return $retval;
            }
            //
            function get_table_info($tableindex) {
                $sql = " SELECT tableforms_detail.* FROM tableforms_detail  WHERE tfd_code=? ORDER BY tfd_order";
                $this->log_message("Log table $tableindex  dan $sql ");
                $query  = $this->db->query($sql,array($tableindex));
                $retval = $query->result();
                return $retval;
            }
            function get_menu() {
                $sql = " SELECT * FROM menus ORDER BY menu_order";
                $query = $this->db->query($sql);
                return $query->result();
            }
            function show_order() {
                //$this->load->model('model_bank','bank');
                $mnth = $this->input->get_post('mnth');
                $yr = $this->input->get_post('yr');
                $userid = $this->input->get_post('userid');
                $sql = " SELECT tro_id,cl_name, payment_no  FROM ";
                $sql = $sql . " transout inner join client on client.cl_id=transout.tro_cust_id JOIN payment on transout.tro_id=payment.payment_trid ";
                //$sql = $sql . " INNER JOIN orders_detail  on orders.orders_no=orders_detail.orders_no ";
                //$sql = $sql . " inner join bank on bank.bank_id=orders.orders_bank_id ";
                $sql = $sql . " WHERE 1=1 ";
                $this->log_message("USER ID " .$userid);
                if(!($mnth=="")) {
                    $sql = $sql . " AND extract(month from tro_date)=? ";
                }
                if(!($yr=="")) {
                    $sql = $sql . " AND extract(year from tro_date)=? ";
                }
                //


                $sql = $sql . " ORDER BY tro_id";
                $query=$this->db->query($sql,array($mnth,$yr));
                $a=$this->db->last_query();
                //if(!($mnth=="")) {
                $this->log_message($sql . " user $userid , month $mnth , year $yr");
                $counter=1;
                $retval=array();
                foreach($query->result() as $row) {
                    $bankname="";
                    //$bankname=$this->bank->get_bank_name($row->orders_bank_id);
            $retval[ ] = array(
                            'counter' => $counter,
                            'name' => $row->cl_name,
                            'orders_no' => $row->tro_id,
                            'payment_no' => $row->payment_no
                        );
                    $counter=$counter+1;
                }
                $data=array();
                $data['list']=$retval;
                $data['sql']=$a;
                echo json_encode($data);
            }
}
?>