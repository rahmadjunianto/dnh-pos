<?php
/**
 */
class Omzet extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('pos');
		//$this->default_group_allowed=array();
                $this->user_id = $this->acl->get_users_id();
                $this->load->model('model_omzet','modomzet');
	}
        function calc_doc_omzet_global() {
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.01.2015";
            }
            //
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_omzet_doct_global($filterdate,$filterdate2);            
            echo json_encode($data);                        
        }
        function calc_omzet_global() {
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.01.1979";
            }
            
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_omzet_global($filterdate,$filterdate2);
            
            echo json_encode($data);            
        }
        
        function show_omzet_global($index=1) {
                if($index==1) {
                    $formid="51";
                }else {
                    $formid="53";
                }            
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }
            
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->title = "Omzet";
                
                $this->load->model('model_menu','menu');
                $data=array();          
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                if($index==1) {
                    $data['type']="patient";
                    $this->html_headers->title = "OMZET GLOBAL";
                }else {
                    $data['type']="doct";
                    $this->html_headers->title = "OMZET GLOBAL DOKTER";
                }
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);                       
		$this->load->view('view_omzet_global',$data);
        }
        //
        function calc_doc_omzet_detail() {        
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            $doct_id = $this->input->get_post('doct_id');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.12.2014";
            }
            
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate doct_id $doct_id");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_doc_omzet_detail($filterdate,$filterdate2,$doct_id);
            echo json_encode($data);
        }
        function calc_omzet_detail() {        
            $data = array();
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.12.2014";
            }
            
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_omzet_detail($filterdate,$filterdate2);
            echo json_encode($data);
        }        
        function calc_omzet_mkt_detail() {        
            $data = array();
            $mktid = $this->input->get_post('id');
            
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.12.2014";
            }
            
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_omzet_mkt_detail($mktid,$filterdate,$filterdate2);
            echo json_encode($data);
        }             
        function calc_omzet_product_detail() {        
            $data = array();
            $product_id = $this->input->get_post('id');
            
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.12.2014";
            }
            
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_omzet_product_detail($product_id,$filterdate,$filterdate2);
            echo json_encode($data);
        }                     
        function calc_omzet_supplier_detail() {        
            $data = array();
            $product_id = $this->input->get_post('id');
            
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.12.2014";
            }
            
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_omzet_supplier_detail($product_id,$filterdate,$filterdate2);
            echo json_encode($data);
        }                             
        function calc_omzet_counter_detail() {        
            $data = array();
            $counterid = $this->input->get_post('id');
            //
            $filterdate = $this->input->get_post('filterdate');
            $filterdate2 = $this->input->get_post('filterdate2');
            if($filterdate=="") {
                $filterdate="01.01.1979";
            }
            if($filterdate2=="") {
                $filterdate2="01.12.2014";
            }
            
            //$filterdate = "01.01.2014";
            $this->log_message("filter date $filterdate");
            $arrdate = explode(".",$filterdate);
            $filterdate = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];
            $arrdate = explode(".",$filterdate2);
            $filterdate2 = $arrdate[2] . "-" . $arrdate[1] . "-" . $arrdate[0];            
            $data['list'] = $this->modomzet->calc_omzet_counter_detail($counterid,$filterdate,$filterdate2);
            echo json_encode($data);
        }                     
        function show_omzet_detail($index=1) {
                if($index==1) {
                    $formid="52";
                }else {
                    $formid="54";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }            
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                $this->html_headers->title = "Omzet Detail";
                
                $this->load->model('model_menu','menu');
                $data=array();          
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                if($index==1) {
                    $data['type']="jamaah";
                    $this->html_headers->title = "OMZET DETAIL";
                }else {
                    
                    $data['type']="group";
                    $this->html_headers->title = "OMZET GLOBAL CABANG";
                }
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);                       
		$this->load->view('view_omzet_detail',$data);            
        }
        function show_omzet_px_detail($index=1) {
                if($index==1) {
                    $formid="52";
                }else {
                    $formid="54";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }            
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                $this->html_headers->title = "Omzet Detail";
                
                $this->load->model('model_menu','menu');
                $data=array();          
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                if($index==1) {
                    $data['type']="jamaah";
                    $this->html_headers->title = "Stat. PX Detail";
                }else {
                    
                    $data['type']="group";
                    $this->html_headers->title = "Stat. PX Global";
                }
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);                       
		$this->load->view('view_omzet_px_detail',$data);            
        }        
        //omzet detail_mkt
        function show_omzet_mkt_detail($index=1) {
                if($index==1) {
                    $formid="52";
                }else {
                    $formid="54";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }            
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";                
                $this->html_headers->title = "Omzet Detail";
                
                $this->load->model('model_menu','menu');
                $data=array();          
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                if($index==1) {
                    $data['type']="jamaah";
                    $this->html_headers->title = "OMZET DETAIL";
                }else {
                    
                    $data['type']="group";
                    $this->html_headers->title = "OMZET GLOBAL CABANG";
                }
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);                       
		$this->load->view('view_omzet_mkt_detail',$data);            
        }    
        function show_omzet_product_detail($index=1) {
                if($index==1) {
                    $formid="52";
                }else {
                    $formid="54";
                }
                $form_access = $this->acl->form_access($formid);
				$form_access = $this->acl->form_access('160104123');

                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }            
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";                
                $this->html_headers->title = "Omzet Detail";
                
                $this->load->model('model_menu','menu');
                $data=array();          
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                if($index==1) {
                    $data['type']="jamaah";
                    $this->html_headers->title = "OMZET DETAIL";
                }else {
                    
                    $data['type']="group";
                    $this->html_headers->title = "OMZET GLOBAL CABANG";
                }
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);                       
		//$this->load->view('view_omzet_mkt_detail',$data);            
                $this->load->view('view_omzet_product_detail',$data);            
        }            
        function show_omzet_counter_detail($index=1) {
                if($index==1) {
                    $formid="52";
                }else {
                    $formid="54";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }            
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";
                $this->html_headers->title = "Omzet Detail";
                
                $this->load->model('model_menu','menu');
                $data=array();          
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                if($index==1) {
                    $data['type']="jamaah";
                    $this->html_headers->title = "OMZET DETAIL";
                }else {
                    
                    $data['type']="group";
                    $this->html_headers->title = "OMZET GLOBAL CABANG";
                }
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);                       
		$this->load->view('view_omzet_counter_detail',$data);            
        }            
        //
        function show_omzet_supplier_detail($index=1) {
                if($index==1) {
                    $formid="52";
                }else {
                    $formid="54";
                }
                $form_access = $this->acl->form_access($formid);
                if($form_access==-1) {
                    $this->acl->show_acl_warning();
                    return;
                }            
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";                
                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/min/metro.min.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/xl/jquery.battatech.excelexport.js";                
                $this->html_headers->title = "Omzet Detail";
                
                $this->load->model('model_menu','menu');
                $data=array();          
                //
                $data['menu'] = $this->menu->get_menu();
                $data['menu_attr_url']="pos/get_menu_by_id/";
                $data['users_name']=$this->acl->get_real_users_id();
                if($index==1) {
                    $data['type']="jamaah";
                    $this->html_headers->title = "OMZET DETAIL";
                }else {
                    
                    $data['type']="group";
                    $this->html_headers->title = "OMZET GLOBAL CABANG";
                }
                //
                $this->log_message(" USERS NAME " . $data['users_name'] );
                $data['header_info']=$this->header_info;
                $this->load->view('init-view', $data);                       
		$this->load->view('view_omzet_supplier_detail',$data);            
        }            
        //
}
?>