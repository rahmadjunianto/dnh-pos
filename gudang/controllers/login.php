<?php

class Login extends Public_Controller {

//	private $source_page='auth/login';

	function __construct() {
		parent::__construct();
		$this->source_page = '/login';
	}

	function index() {
		if ($this->acl->is_logged_in()) {
                        $this->log_message('Logged in,redirected');;
			redirect('/pos', 'location');
			return;
		}
                $this->html_headers->styles[ ] = base_url() . "asset2/metro/css/metro-bootstrap.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pos/css/pos.css";
                $this->html_headers->styles[ ] = base_url() . "asset2/pqgrid/pqgrid.min.css";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/2.1/jquery-2.1.1.min.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/jquery.widget.min.js";

                $this->html_headers->scripts[ ] = base_url() . "asset2/pqgrid/pqgrid.min.js";
                //$this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/ui.tabs.closable.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/jquery/ui/jquery-ui-1.11.0/jquery-ui.js";
                $this->html_headers->scripts [ ] = base_url() . "asset2/jquery/ui/1.10/jquery-ui-1.10.3.custom.min.js";
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-tab-control.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pos-menu.js";
                $this->html_headers->scripts[ ] = base_url() . "asset2/pos/js/pages.js";
                //"js/load-metro.js"
                //$this->html_headers->scripts[ ] = base_url() . "asset2/metro/docs/js/load-metro.js";
                //js/prettify/prettify.js

                //js/jquery/jquery.mousewheel.js
                $this->html_headers->scripts[ ] = base_url() . "asset2/metro/js/metro-carousel.js";
                $this->load->view('auth/view_login.php');
	}

	//function verify($user,$pwd) berasal dari form login
	function verify() {
                $kacab_is_logged_in=0;
		$user = $this->input->get_post('user', TRUE);
		$pwd = $this->input->get_post('passwd', TRUE);

		$data = array();
		$login_result = $this->acl->login($user, $pwd);
                $this->log_message('Login Result:'. $login_result);

                        if ($login_result > 0) {
                                $this->log_message('Logged in,redirected');;
                                $this->log_message('User login id:'. $user);
                                $data[ 'status' ] = 'ok';
                                $data[ 'redir' ] = true;
                                if (strtolower($this->source_page)=="/login") {
                                    $this->source_page="/pos";
                                }
                                $data[ 'redirurl' ] = site_url($this->source_page);
                                $this->log_message("SRC PAGE: " . $this->source_page);
                        } else {
                                $this->log_message('Login false');
                                $data[ 'status' ] = 'not ok';
                                $data[ 'redir' ] = false;
                                $data[ 'err_msg' ] = $this->acl->get_err_msg();
                        }
		echo json_encode($data);
	}

	function logout() {
		$this->acl->logout();
		header('location:' . base_url());
	}
        function test() {
            echo "<input type=text id=sadasd>";
        }
}
/* End of file login.php */
