<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * Semua flag ataupun status taruh disini saja ben aku paham
 *
 * @author wijaz
 */
define('GROUP_ANALIS', '1');
define('GROUP_RADIOGRAFER', '2');
define('GROUP_UMUM', '3');
define('GROUP_CS', '4');
define('GROUP_IT', '5');
define('GROUP_MAN_LAB', '6');
define('GROUP_MAN_CS', '7');
define('GROUP_MAN_UMUM', '8');
define('GROUP_IT_SUPER_USER', '9');
define('GROUP_KACAB', '10');
define('GROUP_MARKETING', '11');
define('GROUP_MAN_MARKETING', '12');
define('GROUP_DOKTER', '13');
define('GROUP_KEUANGAN', '14');
define('GROUP_PERAWAT', '15');
define('GROUP_MAN_MUTU', '16');
define('GROUP_MUTU', '17');
define('GROUP_MAN_KEUANGAN', '18');
//di tabel gak ada 19
define('GROUP_DOKTER_RADIOLOGI', '20');
//21 dst adalah untuk gudang
define('GROUP_GUDANG', '21');
define('GROUP_PEMBELIAN', '22');
define('GROUP_MAN_APPROVAL_PEMBELIAN', '23');
define('GROUP_WEB_USER', '24');
define('GROUP_LAB_USER', '25');
define('GROUP_FO_USER', '26');

#=== flags gruppx & laporpx
define('GROUPPX_LAB', '01');
define('GROUPPX_RAD', '02');
define('GROUPPX_USG', '03');
define('GROUPPX_TML', '04');
define('GROUPPX_ECG', '05');
define('GROUPPX_EEG', '06');
define('GROUPPX_KHU', '07');
define('GROUPPX_LN1', '08');
define('LAPORPX_0', '0');
define('LAPORPX_1', '1');
define('LAPORPX_2', '2');
define('LAPORPX_3', '3');

//define('LAPORPX_9', '9');
class flags { 
	//hanya untuk pancingan biar bisa ditaruh di autoload
}

class col_antar_id {

	const Tidak_diantar = 1;
	const Diantar_ke_rumah = 2;
	const Diantar_ke_dokter = 3;
	const Diantar_ke_kantor = 5;

}

class col_jenis_pasien {

	const Umum = 1;
	const Rekanan = 2;
	const MCU = 3;

}

class col_jenbay_id {

	const Tunai = "01";
	const Kredit = "02";
	const Pelunasan_tunai = "03";
	const Pelunasan_kredit = "04";

//	const xxx = "05";
}

class col_cek_jenisbayar {

	const Tunai = "01";
	const Kredit = "02";

}

class col_cek_lunas {

	const Belum_lunas = 0;
	const Sudah_lunas = 1;

}

class col_dgn_dollar {//untuk 
	const NOT_JASMED = 0;
	const IS_JASMED = 1;
}

class data_mode{
	const mode_NEW="new_data"; //new data to save as insert
	const mode_EDIT="edit_data"; //editing data to save as update
	const mode_VIEW="view_data"; //view data only to reject saving
}
class catatan {
	#cek_userid => insert saja & muncul di pendaftaran
	#cek_user => insert baru & ketika ada update tabel pemeriksaan
	#tabel hasil dgndollar: 0=>bukan jasmed || 1=>jasmed karena awalan 9xxx belum tentu jasmed
}

?>
