<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *@category	Libraries
 *@description: Menampung html header
 */
class html_headers {

	public $title;
	public $scripts;
	public $styles;
	public $description;
	public $keyword;
    function __constructor($title = "" , $scripts = array(), $styles = array(), $description = "", $keyword = "")
    {
		$this->title = $title;
		$this->scripts = $scripts;
		$this->styles = $styles;
		$this->description = $description;
		$this->keyword = $keyword;
    }
}
?>