<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * representasi dari mdlpublic di versi VB
 * @author wijaz
 */

class class_public {

	private $CI;

	function __constructor() {
		$this->CI = & get_instance();
	}

	public function getSetting($pSettingname) {
		$retval = "";
		$sql = "SELECT varvalue  FROM settings where lower(trim(varname))= lower(?)";
		$query = $this->CI->db->query($sql, $pSettingname);
		if ($query->num_rows() > 0) {
			$row = $query->first_row();
			$retval = $row->varvalue;
		}
		return $retval;
	}
	public function getSettingGaji($pSettingname) {
		$retval = "";
		$sql = "SELECT nilai  FROM setting  where lower(trim(nama))= lower(?)";
		$query = $this->CI->db->query($sql, $pSettingname);
		if ($query->num_rows() > 0) {
			$row = $query->first_row();
			$retval = $row->nilai;
		}
		return $retval;
	}
	public function AvoidNull($pVal) {
		$retval = "";
		if (is_null($pVal) || empty($pVal)) {
			$retval = "";
		} else {
			$retval = $pVal;
		}
		return $retval;
	}

	public function AvoidNullNum($pVal) {
		$retval = 0;
		if (is_null($pVal) || empty($pVal)) {
			$retval = 0;
		} else {
			$retval = $pVal;
		}
		return $retval;
	}

//	private function replace_with_null($var) {
//        if($var=="") {
//            return "NULL";
//        }else {
//            return $var;
//        }
//    }

	public function GetBranchCode() {
		$strAreaCode = "";
		$sql = "SELECT * FROM settings WHERE varname='branchareacode'";
		$query = $this->CI->db->query($sql);
		//$this->db->query($sql);
		if ($query->num_rows() > 0) {
			$row = $query->first_row();
			$strAreaCode = intval($row->varvalue);
		}
		return (string) $strAreaCode;
	}

	public function GetBranchAreaCode() {
		$strAreaCode = "";
		$sql = "SELECT * FROM settings WHERE varname='branchcode'";
		$query = $this->CI->db->query($sql);
		//$query=$this->db->query($sql);

		if ($query->num_rows() > 0) {
			$row = $query->first_row();
			$strAreaCode = "0" . $row->varvalue;
		}
		return (string) $strAreaCode;
	}

	public function GetDatePrefix() {
		$strYear = "";
		$strMonth = "";
		$tgl = "";

		$sql = "SELECT current_date AS tgl";
		$query = $this->CI->db->query($sql);
		//$query=$this->db->query($sql);
		if ($query->num_rows() > 0) {
			$row = $query->first_row();
			$tgl = $row->tgl;
			$strYear = substr($row->tgl, 2, 2);
			$strMonth = substr($row->tgl, 5, 2);
		} else {
			$strMonth = "0";
			$strYear = "0";
		}
		return (string) ($strYear . $strMonth);
	}

	public function getServerDate() {
		//"2011-03-07"
		$tgl = "";
		$sql = "SELECT current_date AS tgl";
		$query = $this->CI->db->query($sql);
		//$query=$this->db->query($sql);
		if ($query->num_rows() > 0) {
			$row = $query->first_row();
			$tgl = $row->tgl;
		}
		return (string) ($tgl);
	}

	public function getServerDateTime() {
		//output sample: "2011-03-04 19:56:23.718"
		$sql = "SELECT localtimestamp AS tgl";
		$query = $this->CI->db->query($sql);
		$row = $query->first_row();
		return $row->tgl;
	}

	public function getServerTime() {
		//output sample: "13:11:54"
		$sql = "SELECT to_char(current_timestamp, 'HH24:MI:SS') as tgl";
		$query = $this->CI->db->query($sql);
		$row = $query->first_row();
		return $row->tgl;
	}

	function getLabID($cek_id) {
		$sql = "SELECT lab_id FROM pemeriksaan WHERE cek_id=?" ;
		$query = $this->CI->db->query($sql, $cek_id);
		$row = $query->first_row();
		return $row->lab_id;
	}
	function getAlamatPasienByLabId($lab_id){
		$sql="select pas_nama,pas_gelar,pas_alamat,pas_kota,pas_status
			FROM pasien
			INNER Join Pemeriksaan On pemeriksaan.pas_id=Pasien.pas_id
			WHERE Pemeriksaan.lab_id=?";
		$query = $this->CI->db->query($sql, $lab_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->pas_alamat .' '.(string)$row->pas_kota;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getAlamatDokter($dok_id){
		$sql="select dok_alamat1, dok_kota1
			FROM dokter
			WHERE dok_id=?";
		$query = $this->CI->db->query($sql, $dok_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->dok_alamat1 .' '.(string)$row->dok_kota1;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getGelarDokter($dok_id){
		$sql="select dok_nama, dok_gelar
			FROM dokter
			WHERE dok_id=?";
		$query = $this->CI->db->query($sql, $dok_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->dok_gelar;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getNamaDokter($dok_id){
		$sql="select dok_nama, dok_gelar
			FROM dokter
			WHERE dok_id=?";
		$query = $this->CI->db->query($sql, $dok_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->dok_nama;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getAlamatRek($rek_id){
		$sql="SELECT *
			FROM rekanan
			WHERE rek_id=?";
		$query = $this->CI->db->query($sql, $rek_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->rek_alamat .' '.(string)$row->rek_kota;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getNamaRek($rek_id){
		$sql="SELECT rek_nama
			FROM rekanan
			WHERE rek_id=?";
		$query = $this->CI->db->query($sql, $rek_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->rek_nama;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getNamaKodeKontrak($kontrak_id){
		$sql="SELECT hkm_nama
			FROM harga_kontrak_master
			WHERE hkm_id=?";
		$query = $this->CI->db->query($sql, $kontrak_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->hkm_nama;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getNamaUserFromTable($users_id){
		$sql="SELECT users_nama
			FROM users
			WHERE users_id=?";
		$query = $this->CI->db->query($sql, $users_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->users_nama;
		}else{
			$retval='';
		}
		return $retval;
	}
	function getNomerNotaByCekid($cek_id){
		$sql="SELECT bay_nonota
			FROM pembayaran
			WHERE cek_id=?";
		$query = $this->CI->db->query($sql, $cek_id);
		if ($query->num_rows()>0){
			$row= $query->first_row();
			$retval= (string)$row->bay_nonota;
		}else{
			$retval='';
		}
		return $retval;
	}

//	function getNewIdNgedit($cek_id) {
////		$sql = "select max(to_number(ngedit_ke,'999999999')) as jumlah from ngedit where ngedit_cekid = '" . $cek_id . "'";
////		log_message('error',$sql);
////		$query = $this->CI->db->query($sql);
//		$query = $this->CI->db->select("max(to_number(ngedit_ke,'999999999')) as jumlah")
//			->from('ngedit')
//			->where('ngedit_cekid',$cek_id)
//			->get();
//		if ($query->num_rows()>0) {
//			$new_id = $this->AvoidNullNum($query->first_row()->jumlah) + 1;
//		} else {
//			$new_id = 0;
//		}
//		return $new_id;
//	}
}

?>