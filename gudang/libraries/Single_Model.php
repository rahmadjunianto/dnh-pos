<?php
/**
 * Base model untuk semua model turunan yang dikhususkan untuk single table
 *
 * @author wijaz
 */
class Single_Model extends MY_Model{
	protected $_table_name;
	protected $_id;
	public $_last_write_sql;

	function __construct() {
		parent::__construct();
	}

	function add($object_data) {
		$this->db->from($this->_table_name)->set($object_data);
		$retval = $this->db->insert();
		$this->_last_write_sql = $this->db->last_query();
		$this->log_message($this->db->last_query());
		return $retval;
	}

	function edit($object_data, $old_id) {
		$retval = $this->db
			->from($this->_table_name)
			->set($object_data)
			->where($this->_id, $old_id)
			->update();
		$this->_last_write_sql = $this->db->last_query();
		$this->log_message($this->db->last_query());
		return $retval;
	}

	function delete($id) {
		$retval = $this->db
			->from($this->_table_name)
			->where($this->_id, $id)
			->delete();
		$this->_last_write_sql = $this->db->last_query();
		$this->log_message($this->db->last_query());
		return $retval;
	}

	protected function get_new_id() {//you must override on each table model to gives the right new_id
		$query = $this->db->select("max($this->_id) as new_id", false)
			->from($this->_table_name)
			->get();
		if ($query->num_rows() > 0) {
			$new_id = ((int) $query->first_row()->new_id) + 1;
		} else {
			$new_id = 1;
		}
		return $new_id;
	}

	function count($arr_where = array()) {
		$count = $this->db
			->from($this->_table_name)
			->where($arr_where)
			->count_all_results();
		return (int) $count;
	}

	function count_by_id($primary_key) {
		$count = $this->db
			->from($this->_table_name)
			->where($this->_id, $primary_key)
			->count_all_results();
		return (int) $count;
	}

	function get_where($arr_where = array(), $order_by = null, $fields = '*', $limit = null, $skip = 0) {//offset means skip
		$this->db->from($this->_table_name);

		if (!is_null($arr_where) && count($arr_where) > 0) {
			$this->db->where($arr_where);
		}
		if (!is_null($order_by) && trim($order_by) != '') {
			$this->db->order_by($order_by);
		}
		if (!is_null($fields) && trim($fields)!= '') {
			$this->db->select($fields);
		}
		if (!is_null($limit)) {
			$this->db->limit($limit);
		}
		$this->db->offset($skip);
//		$this->log_message($this->db->_compile_select());
		$query = $this->db->get();

//		$this->log_message($this->db->last_query());
		return $query->result();
	}

	function get_like($arr_like = array(), $order_by = '', $fields = '*', $limit = null, $skip = 0) {//offset means skip
		$this->db->from($this->_table_name);

		if (!is_null($arr_like) && count($arr_like) > 0) {
			$this->db->like($arr_like);
		}
		if (!is_null($order_by) && trim($order_by) != '') {
			$this->db->order_by($order_by);
		}
		if (!is_null($fields) && trim($fields) != '') {
			$this->db->select($fields);
		}
		if (!is_null($limit)) {
			$this->db->limit($limit);
		}
		$this->db->offset($skip);
		$query = $this->db->get();

//		$this->log_message($this->db->last_query());
		return $query->result();
	}

	function get_by_id($id, $order_by = '', $fields = '*') {
		$arr_where = array($this->_id => $id);
		return $this->get_where($arr_where, $order_by, $fields);
	}

	function get_like_id($id, $order_by = '', $fields = '*') {
		$arr_like = array($this->_id => $id);
		return $this->get_like($arr_like, $order_by, $fields);
	}

	/**
	 * select col_return from table where col_search = search_value
	 * @param type $col_return
	 * @param type $col_search
	 * @param type $search_value
	 */
	function get_column_value($col_return, $col_search, $search_value) {
		$query = $this->db->select($col_return)->from($this->_table_name)->where($col_search, $search_value)->get();
		if ($query->num_rows() > 0) {
			$retval = $query->first_row()->$col_return;
		} else {
			$retval = null;
		}
		return $retval;
	}

}

?>
