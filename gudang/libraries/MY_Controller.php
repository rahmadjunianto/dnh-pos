<?php

/**
 * Basic core library extension. 
 * Whenever you create a class with the MY_ prefix 
 * the CodeIgniter Loader class will load this after loading the core library
 *
 * @author phil sturgeon
 * @link http://philsturgeon.co.uk/blog/2010/02/CodeIgniter-base-Classes-Keeping-it-DRY
 */
class MY_Controller extends CI_Controller {

	protected $db_hostname;
	protected $db_cabang_name;

	function __construct() {
		parent::__construct();

		$this->load->database();
		$this->db_hostname = $this->db->hostname;
		$this->db_cabang_name=$this->acl->get_db_cabang($this->db_hostname);
//		$this->log_message($this->acl->get_db_cabang($this->db_hostname));
	}

	function log_message($message = '') {
		if ($this->acl->is_logged_in()) {
			$user_id = $this->acl->get_users_id();
		} else {
			$user_id = 'nologin';
		}
		list($first, $caller) = debug_backtrace(false);
//		log_message('error', 'first:'.print_r($first, true));
//		log_message('error', 'caller:'.print_r($caller, true));
		if ($message == "") {
			log_message("error", "User:[$user_id] page " . $this->router->class . '::' . $this->router->method  . ':' . $first[ 'line' ] . " >> " . $this->db->_error_message());
		} else {
			log_message("error", "User:[$user_id] page " . $this->router->class . '::' . $this->router->method  . ':' . $first[ 'line' ] . " >> " . $message);
		}
	}

}

//require(APPPATH . 'libraries/Secured_Controller.php');
//require(APPPATH . 'libraries/Public_Controller.php');
?>
