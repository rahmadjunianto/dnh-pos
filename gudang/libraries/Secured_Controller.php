<?php

/**
 * Description of Public_Controller
 *
 * @author wijaz
 * @link http://philsturgeon.co.uk/blog/2010/02/CodeIgniter-base-Classes-Keeping-it-DRY
 */
class Secured_Controller extends MY_Controller {
//class Secured_Controller extends CI_Controller {

	protected $default_group_allowed = array();
	protected $default_scripts = array();
	protected $default_styles = array();
	protected $data_head = array();
	protected $data_content = array();
        protected $data_info = array();
        public $header_info="";
//	protected $source_page = '';

	function __construct() {
		parent::__construct();

		if (!$this->acl->is_logged_in()) {
//			if ($this->input->is_ajax_request()) {	//CI ver.2.x
			if ($this->acl->is_ajax_request()) {
				$this->acl->logout();
                                redirect(site_url('login'), 'location');
				$html = 'Session login anda telah berakhir.' .
					'<br/>Klik <a href="' . site_url('/login') . '" target="_blank"><strong>disini</strong></a> untuk login lagi.	';
				//header("HTTP/1.0 403 " . $html);
			} else {
                                redirect(site_url('login'), 'location');
				$html = 'Session login anda telah berakhir.' .
					'<br/>Klik <a href="' . site_url('/login') . '"><strong>disini</strong></a> untuk login lagi.';
				//show_error($html);
//				redirect(site_url('auth/login'), 'location');
			}
			die();
		} else {
			// If the user is using a mobile, use a mobile theme
			$this->load->library('user_agent');
			if (!$this->agent->is_mobile()) {

				$this->default_styles = array( 
					base_url() . "asset2/metro/css/iconFont.min.css",
#					base_url() . "css/bootstrap.css"
					//base_url() . "js/toastmessage/css/jquery.toastmessage.css"
				);
				$this->default_scripts = array(
#					base_url() . "js/jquery.js",					
                                        #base_url() . "js/ui/js/jquery-ui.js"
					//base_url() . "js/toastmessage/js/jquery.toastmessage.js"
					//base_url() . "js/pages.js",
					//base_url() . "js/menu.js",
					//base_url() . "js/grid.js"
				);
				$this->html_headers->styles = $this->default_styles;
				$this->html_headers->scripts = $this->default_scripts;

				//$this->load->model('menu');
				//$this->data_head[ 'topmenu' ] = $this->menu->get_top_menu();
				//$this->data_head[ 'childmenu' ] = $this->menu->get_child_menu();
				
				//$user_info=  $this->acl->get_users_nama() . ' [' . $this->acl->get_users_id() . ']@'.$this->db_cabang_name;
				//$this->data_head[ 'user_info' ] = $user_info;
			} else {
				/*
				 * Use my template library to set a theme for your staff
				 *     http://philsturgeon.co.uk/code/codeigniter-template
				 */
				//$this->template->set_theme('mobile');
			}
		}
                //
                $this->load->model('model_public','modpublic');
                $this->header_info=$this->modpublic->get_settings("header info");
	}

	function cek_access($arr_group_allowed = array(), $arr_group_deny = array()) {
		//jika semua parameter kosong, maka akan digunakan default_group_allowed untuk hak akses
		if (is_array($arr_group_allowed) && count($arr_group_allowed) > 0) {
			if (!($this->acl->in_group($this->default_group_allowed)
				|| $this->acl->in_group($arr_group_allowed))) {
				show_error('Anda tidak berhak mengakses halaman ini <a href="#" onclick="history.back();return false;">kembali</a>
					<br/>Atau ke <a href="'.  site_url().'" >menu utama</a>', 500);
			}
		} else {
			if (!$this->acl->in_group($this->default_group_allowed)) {
				show_error('Anda tidak berhak mengakses halaman ini <a href="#" onclick="history.back();return false;">kembali</a><br/>
					<br/>Atau ke <a href="'.  site_url().'" >menu utama</a>', 500);
			}
		}
	}

}

?>
