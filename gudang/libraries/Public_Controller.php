<?php

/**
 * Description of Public_Controller
 *
 * @author wijaz
 * @link http://philsturgeon.co.uk/blog/2010/02/CodeIgniter-base-Classes-Keeping-it-DRY
 */
class Public_Controller extends MY_Controller {

	protected $default_scripts = array();
	protected $default_styles = array();
	protected $data_head = array();
	protected $data_content = array();
	protected $source_page = '';

	function __construct() {
		parent::__construct();

		// If the user is using a mobile, use a mobile theme
		$this->load->library('user_agent');
		if (!$this->agent->is_mobile()) {

			$this->html_headers->styles = $this->default_styles;
			$this->html_headers->scripts = $this->default_scripts;

//			$this->load->model('menu');
//			$this->data_head[ 'topmenu' ] = $this->menu->get_top_menu();
//			$this->data_head[ 'childmenu' ] = $this->menu->get_child_menu();
//			$this->data_head[ 'user_info' ] = $this->acl->get_users_nama() . ' [' . $this->acl->get_users_id() . ']';
		} else {
			/*
			 * Use my template library to set a theme for your staff
			 *     http://philsturgeon.co.uk/code/codeigniter-template
			 */
			//$this->template->set_theme('mobile');
		}
	}


}

?>
