<?php

/**
 * Description of class_hkd
 *
 * @author wijaz
 */
class class_hkd extends base_table{

	public $hkd_hkmid; //character varying(10),
	public $hkd_counter; // double precision DEFAULT nextval(('seq_kontrak_detail'::text)::regclass),
	public $hkd_kodepx; // character varying(10),
	public $hkd_hargapx; // double precision,
	public $hkd_discount; // double precision DEFAULT 0,
	public $hkd_hargapx_asli; // double precision

}

?>
