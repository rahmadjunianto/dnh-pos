<?php

/**
 * Description of class_master
 *
 * @author wijaz
 */
class base_table {

	/**
	 * Fungsi ini seperti pengganti class_public::replace_with_num
	 * bedanya, yg ini mereset value '' or 'null' menjadi null bukan 'NULL'
	 */
	function replace_member_with_null() {
		foreach ($this as $var => $value) {
			if ((string)$value == "" || strtolower($value)=='null') {
				$this->$var = null;
			}
		}
	}

}

?>
