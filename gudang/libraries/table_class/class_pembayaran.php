<?php

/**
 * Description of class_pembayaran
 *
 * @author wijaz
 */
class class_pembayaran extends base_table{

	public $bay_id; //character varying(15)
	public $bay_harga; // numeric(10,2)
	public $bay_discount3; // numeric(10,2)
	public $bay_harganet; // numeric(10,2)
	public $bay_pembayaranke; // integer
	public $bay_lunas; // integer
	public $cek_id; // character varying(30)
	public $jenis_bayar; // character varying(10)
	public $bay_jenispasien; // integer
	public $bay_nonota; // character varying(30)
	public $bay_kurangbayar; // numeric(10,2)
	public $bay_userid; // character varying(30)
	public $bay_tgl; // timestamp without time zone
	public $bay_jumlahbayar; // numeric(10)
	public $bay_discount; // character varying(50)
	public $bay_idngedit; // integer
	public $bay_idbank; // integer
	public $bay_notransaksi; // character varying(100)
	public $bay_tgltempo; // timestamp without time zone
	public $bay_terima; // double precision
	public $bay_ket; // character varying(1000)
	public $bay_idjenis; // integer
	public $bay_piutang; // double precision
	public $bay_kas; // double precision

}

?>
