<?php

class class_pasien extends base_table{

	public $pas_id; //character varying(30) NOT NULL,
	public $agama_id; // numeric,
	public $pas_gelar; // character varying(20),
	public $pas_nama; // character varying(100),
	public $pas_tgllahir; // date,
	public $pas_jenkelamin; // character varying(15),
	public $pas_alamat; // character varying(1000),
	public $pas_kota; // character varying(30),
	public $pas_kodepos; // character varying(10),
	public $pas_telp; // character varying(20),
	public $pas_hp; // character varying(20),
	public $pas_status; // character varying(20),
	public $pas_ibu; // character varying(50),
	public $pas_datang = 1; // numeric,
	public $pas_cetak; // numeric DEFAULT 0,
	public $pas_sirem; // character varying(30),
	public $pas_updt; // character varying(100),
	public $pas_updt1; // character varying(100),
	public $pas_tglreg; // timestamp without time zone,
	public $pas_ket; // character varying(1000),
	public $pas_cardprint; // integer DEFAULT 0,
	public $pas_onlinepwd; // character varying(80),
	public $pas_pinprint; // integer DEFAULT 0,
	public $pas_usia; // integer,
	public $pas_point_diambil; // integer DEFAULT 0	
}

?>
