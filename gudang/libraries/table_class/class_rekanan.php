<?php

/**
 * Description of class_rekanan
 *
 * @author wijaz
 */
class class_rekanan extends base_table {

	public $rek_id; //character varying(30) NOT NULL DEFAULT nextval(('seq_rekanan'::text)::regclass),
	public $rek_nama; // character varying(100),
	public $rek_alamat; // character varying(100),
	public $rek_kodepos; // character varying(10),
	public $rek_telp; // character varying(20),
	public $rek_fax; // character varying(20),
	public $rek_kontak; // character varying(100),
	public $rek_jabatan; // character varying(50),
	public $rek_discount; // character varying(2),
	public $rek_kota; // character varying(40),
	public $rek_hp; // character varying(20),
	public $rek_mlrid; // character varying(15),
	public $rek_kdkembar; // character varying(30),
	public $rek_jenis; // integer,
	public $pwd; // character varying(255),
	public $rek_tipe; // integer,
	public $rek_coa_kas; // character varying(40) DEFAULT '01.01.01.01.01.01.00'::character varying,
	public $rek_coa_piutang; // character varying(40) DEFAULT '01.01.01.01.02.02.00'::character varying,
	public $rek_coacode; // character varying(30),

}

?>
