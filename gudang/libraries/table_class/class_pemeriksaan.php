<?php

/**
 * Description of class_pemeriksaan
 *
 * @author wijaz
 */
class class_pemeriksaan extends base_table{

	public $cek_id; //character varying(30) NOT NULL,
	public $jenbay_id; // character varying(10),
	public $antar_id; // character varying(30),
	public $rek_id; // character varying(30),
	public $dok_id; // character varying(30),
	public $pas_id; // character varying(30),
	public $lab_id; // character varying(30),
	public $cek_ket; // character varying(300),
	public $cek_cetak_nota; // numeric,
	public $cek_tgl; // timestamp without time zone,
	public $cek_user; // character varying(30),
	public $cek_cetak_kwit; // numeric,
	public $cek_cetak_kartu; // numeric,
	public $cek_hasil; // numeric,
	public $cek_alamat_antar1; // character varying(1000),
	public $cek_harga; // numeric(12),
	public $cek_discountnum; // numeric(6),
	public $cek_discount3; // numeric(6,3),
	public $cek_harganet; // numeric(12),
	public $cek_jenisbayar; // character varying(10),
	public $cek_lunas; // integer NOT NULL DEFAULT 0,
	public $cek_jenispasien; // integer,
	public $cek_catatan; // character varying(10000),
	public $cek_cetakhasil; // integer DEFAULT 0,
	public $cek_kurangbayar; // numeric(10),
	public $cek_userid; // character varying(30),
	public $cek_discount; // character varying(50),
	public $cek_alamatantar; // character varying(1000),
	public $last_mod; // timestamp without time zone,
	public $cek_barcode; // integer,
	public $cek_publish; // integer DEFAULT 0,
	public $cek_publisher; // character varying(100),
	public $cek_diagnosa; // character varying(100),
	public $smspin; // character varying(30),
	public $cek_ket2; // character varying(10000),
	public $cek_day; // integer,
	public $cek_month; // integer,
	public $cek_year; // integer,
	public $cek_barcodeprintedby; // character varying(30),
	public $cek_barcodeprinted_time; // timestamp without time zone,
	public $cek_hkm_id; // character varying(10),
	public $cek_selesai; // timestamp without time zone,

}

?>
