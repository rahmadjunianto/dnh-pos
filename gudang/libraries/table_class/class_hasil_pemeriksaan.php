<?php

/**
 * Description of class_hasil_pemeriksaan
 *
 * @author wijaz
 */
class class_hasil_pemeriksaan extends base_table{

	public $cek_id; //character varying(30),
	public $hasil_lokal; // character varying(500),
	public $hasil_int; // character varying(500),
	public $hasil_note; // character varying(500),
	public $kodepx; // character varying(10),
	public $namapx; // character varying(100),
	public $normalpx; // character varying(100),
	public $normalsi; // character varying(100),
	public $bcd; // character varying(2),
	public $hargapx = 0; // double precision DEFAULT 0,
	public $jnshsl; // character varying(1),
	public $digit; // double precision,
	public $kv_si; // character varying(7),
	public $si_kv; // character varying(7),
	public $gruppx; // character varying(2),
	public $level; // character varying(1),
	public $laporpx; // character varying(1),
	public $flag; // character varying(4),
	public $obatjm; // character varying(30),
	public $ketr; // character varying(60),
	public $hasil_id; // bigint,
	public $induk; // bigint,
	public $minkv; // character varying(30),
	public $maxkv; // character varying(30),
	public $satkv; // character varying(30),
	public $minsi; // character varying(30),
	public $maxsi; // character varying(30),
	public $satsi; // character varying(30),
	public $lab_id; // character varying(30),
	public $manual = 0; // integer DEFAULT 0,
	public $tgl_menyusul; // timestamp without time zone,
	public $kode_sms; // character varying(1000),
	public $analyst; // character varying(50),
	public $kode_mesin; // character varying(10),
	public $machineid; // integer,
	public $ordered = 0; // integer DEFAULT 0,
	public $last_mod; // timestamp without time zone,
	public $dl; // integer,
	public $kk; // integer,
	public $imun; // integer,
	public $naf; // integer,
	public $citratbiru; // integer,
	public $ulbiru; // integer,
	public $reduksigula; // integer,
	public $led; // integer,
	public $mikrokuning; // integer,
	public $bcdlain2; // integer,
	public $dgndollar; // integer,
	public $users_id; // character varying(20),
	public $update_time; // timestamp without time zone,
	public $hp_bayid; // character varying(20),
	public $hp_bayidjenis; // character varying,
	public $discount = 0; // double precision DEFAULT 0,
	public $harganett = 0; // double precision DEFAULT 0,
	public $id_menyusul = 0; // integer DEFAULT 0,
	public $namapx_inggris; // character varying(50),
	public $cek_oleh_pengantaran = 1; // smallint DEFAULT 1

}

?>
