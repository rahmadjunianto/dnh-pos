<?php

/**
 * Description of harga_class
 *
 * @author wijaz
 */
class class_harga extends base_table{

	public $kodepx; //character varying(10),
	public $namapx; // character varying(100),
	public $normalpx; // character varying(100),
	public $normalsi; // character varying(100),
	public $bcd; // character varying(2),
	public $hargapx; // double precision,
	public $minkv; // character varying(5),
	public $maxkv; // character varying(5),
	public $satkv; // character varying(7),
	public $minsi; // character varying(20),
	public $maxsi; // character varying(20),
	public $satsi; // character varying(7),
	public $jnshsl; // character varying(1),
	public $digit; // double precision,
	public $kv_si; // character varying(7),
	public $si_kv; // character varying(7),
	public $gruppx; // character varying(2),
	public $level; // character varying(1),
	public $induk; // integer,
	public $laporpx; // character varying(1),
	public $flag; // character varying(4),
	public $obatjm; // character varying(30),
	public $keterangan; // character varying(60),
	public $kode_sms; // character varying(1000),
	public $machineid; // integer,
	public $kode_mesin; // character varying(10),
	public $dl; // integer,
	public $kk; // integer,
	public $imun; // integer,
	public $naf; // integer,
	public $citratbiru; // integer,
	public $ulbiru; // integer,
	public $reduksigula; // integer,
	public $led; // integer,
	public $mikrokuning; // integer,
	public $bcdlain2; // integer,
	public $dgndollar; // integer,
	public $coa_code_pendapatan; // character varying(25),
	public $coa_code_biaya; // character varying(25),
	public $namapx_inggris; // character varying(50),
	public $cek_oleh_pengantaran; // smallint DEFAULT 1

}

?>
