<?php

/**
 * Description of class_hkm
 *
 * @author wijaz
 */
class class_hkm extends base_table{

	public $hkm_id; //character varying(10) DEFAULT nextval(('seq_kontrak_master'::text)::regclass),
	public $hkm_tglkontrak; // timestamp without time zone,
	public $hkm_koderekanan; // character varying(10),
	public $hkm_mlrid; // character varying(80),
	public $hkm_aktif; // integer,
	public $hkm_nama; // text,
	public $hkm_jenis; // integer DEFAULT 0, 0=individu || 1=paket
	public $hkm_keterangan; // character varying(1000),
	public $hkm_tglexpired; // timestamp without time zone,
	public $hkm_tglstart; // timestamp without time zone,
	public $hkm_tagihan; // smallint DEFAULT 0

}

?>
