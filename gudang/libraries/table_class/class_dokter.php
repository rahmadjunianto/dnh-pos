<?php

/**
 * Description of dokter_class
 *
 * @author wijaz
 */
class class_dokter extends base_table{

	public $dok_id = null; //character varying(30) NOT NULL DEFAULT nextval(('seq_dokter'::text)::regclass),
	public $agama_id = 8; // numeric,
	public $dok_gelar = null; // character varying(25),
	public $dok_nama = null; // character varying(50),
	public $dok_tgllahir = null; // date,
	public $dok_jenkelamin = null; // character varying(10),
	public $dok_hp = null; // character varying(20),
	public $dok_qualifikasi1 = null; // character varying(100),
	public $dok_qualifikasi2 = null; // character varying(100),
	public $dok_qualifikasi3 = null; // character varying(100),
	public $dok_qualifikasi4 = null; // character varying(100),
	public $dok_kota1 = null; // character varying(100),
	public $dok_kota2 = null; // character varying(100),
	public $dok_kota3 = null; // character varying(100),
	public $dok_alamat1 = null; // character varying(250),
	public $dok_alamat2 = null; // character varying(250),
	public $dok_alamat3 = null; // character varying(250),
	public $dok_mlrid = null; // character varying(10),
	public $dok_telpon1 = null; // character varying(15),
	public $dok_telpon2 = null; // character varying(15),
	public $dok_telpon3 = null; // character varying(15),
	public $dok_kdkembar = null; // character varying(10),
	public $dok_agama = null; // character varying(30),
	public $onlinepwd = null; // character varying(100),
	public $dok_status = null; // smallint DEFAULT 0,

}

?>
