<?php

/**
 * Description of class_paket
 *
 * @author wijaz
 */
class class_paket extends base_table{

	public $paket_id = null;  //integer DEFAULT nextval(('seq_paket'::text)::regclass),
	public $paket_nama = null;  // character varying(200),
	public $paket_bruto = null;  // numeric,
	public $paket_netto = null;  // numeric,
	public $paket_discount = null;  // double precision,
	public $paket_rekid = null;  // character varying(8)

}

?>
