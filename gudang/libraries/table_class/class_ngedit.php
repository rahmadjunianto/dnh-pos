<?php

/**
 * Description of class_ngedit
 *
 * @author wijaz
 */
class class_ngedit extends base_table{

	public $ngedit_id; //integer DEFAULT nextval(('seq_ngedit'::text)::regclass),
	public $ngedit_cekid; // character varying(100),
	public $ngedit_kodepx; // character varying(1000),
	public $ngedit_tgl; // timestamp without time zone,
	public $ngedit_userid; // character varying(30),
	public $ngedit_jumlah; // double precision,
	public $ngedit_ke; // integer,
	public $ngedit_discount; // double precision,
	public $ngedit_status; // integer,
	public $ngedit_labid; // character varying(100),
	public $ngedit_ket; // character varying(5000)

}

?>
