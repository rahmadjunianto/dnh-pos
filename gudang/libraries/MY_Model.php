<?php

/**
 * Basic core library extension. 
 * Whenever you create a class with the MY_ prefix 
 * the CodeIgniter Loader class will load this after loading the core library
 * 
 * @author phil sturgeon
 * @link http://philsturgeon.co.uk/blog/2010/02/CodeIgniter-base-Classes-Keeping-it-DRY
 */
class MY_Model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function log_message($message = '') {
		if ($this->acl->is_logged_in()) {
			$user_id = $this->acl->get_users_id();
		} else {
			$user_id = '';
		}

		list($first, $caller) = debug_backtrace(false);
//		log_message('error', 'first:'.print_r($first, true));
//		log_message('error', 'caller:'.print_r($caller, true));
		if ($message == "") {
			log_message("error", "User:[$user_id] " . $this->_parent_name . "::" . $caller[ 'function' ] . ':' . $caller[ 'line' ] . " >> " . $this->db->_error_message());
		} else {
			log_message("error", "User:[$user_id] " . $this->_parent_name . "::" . $caller[ 'function' ] . ':' . $caller[ 'line' ] . " >> " . $message);
		}
	}

}
//require(APPPATH . 'libraries/Single_Model.php');
//require(APPPATH . 'libraries/Common_Model.php');
?>
