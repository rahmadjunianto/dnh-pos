<?php
/**
 */
class Profile extends Secured_Controller {
	function __construct() {
		parent::__construct();
		$this->data_head['source_page']=site_url('goedang');
		//$this->default_group_allowed=array();
	}
        
	function index() {
            $data=array();
            $data['users_name']=$this->acl->get_real_users_id();
            $data['users_id']=$this->acl->get_users_id();
            $data['header_info']=$this->header_info;                      
            $this->load->view('view_profile',$data);
                
	}//
}
?>