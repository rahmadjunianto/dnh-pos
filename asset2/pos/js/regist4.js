$(function () {
    var colM = [
        { title: "doct id", dataType: "integer", dataIndx: "doct_id", minWidth: 150, resizable: false,editable:false ,selectionModel: { type: 'cell'} },
        { title: "Name", dataType: "string", dataIndx: "doct_name", width: '138' },
        { title: "addr", dataType: "float", width: '138',align: "right", dataIndx: "doct_address"},
        { title: "phone", dataType: "float", align: "right", dataIndx: "doct_phone"},
        { title: "mobile phone", dataType: "float", align: "right", dataIndx: "doct_mobilephone"},
    ];
    
    var dataModel = {
        location: "remote",
        sorting: "remote",
        paging: "remote",
        dataType: "JSON",
        method: "GET",
        curPage: 1,
        rPP: 20,
        sortIndx: 0,
        sortDir: "up",
        rPPOptions: [1, 10, 20, 30, 40, 50, 100, 500, 1000],
        getUrl: function () {
            //debugger;
            var sortDir = (this.sortDir == "up") ? "asc" : "desc";
            var sort = ['doct_id', 'doct_name', 'doct_address', 'doct_phone', 'doct_mobilephone'];
            return { url: site_url + "/doctor/data/", data: "cur_page=" + this.curPage + "&records_per_page=" +
                this.rPP + "&sortBy=" + sort[this.sortIndx] + "&dir=" + sortDir };
        },
        getData: function (dataJSON) {
            //var data=               
            return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };               
        }        

    }
 
    var grid1 = $("#grid_doctor").pqGrid({ width: 900, height: 400,
        dataModel: dataModel,
        colModel: colM,
        flexHeight:false,
        //scrollModel:{pace:'consistent'},
        //scrollModel:{pace:'optimum'},
        scrollModel:{pace:'fast'},
        //groupModel:{dataIndx:0, grouping:"local"},
        title: "Shipping Orders",
        columnBorders:false,
        editModel:{clicksToEdit:2},
        header:false,
        resizable: true,           
        freezeCols: 2
    });
});