$(function () {
    $("#admin_shutdown").click(function() {
        showInfo("Server Akan dimatikan, Seluruh Proses LIS di seluruh divisi akan terhenti");
        shutdown();
    })
    $("#admin_reboot").click(function() {
        showInfo("Server Akan di Restart, Seluruh Proses LIS di seluruh divisi akan terhenti,Sampai dengan kurang lebih 5 menit, Lalu bisa digunakan kembali");
        reboot();
    })    
    function shutdown() {
        $.ajax({
                url:site_url  +"/admin/downserver",
                type: "POST",
                dataType: "json",
                success:function(data){
                        //alert("sukses");
                        //debugger;
                        alert(data.retval);
                }
        }); //end of ajax,                    
        
    }
    function reboot() {
        $.ajax({
                url:site_url  +"/admin/rebootserver",
                type: "POST",
                dataType: "json",
                success:function(data){
                        //alert("sukses");
                        //debugger;
                        alert(data.retval);
                }
        }); //end of ajax,                    
        
    }
});    