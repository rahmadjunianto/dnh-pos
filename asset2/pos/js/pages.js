$(document).ready(function(){
	$(document).bind("ajaxComplete", function(event, response, ajaxOptions) {
		if (response.status == 403 && response.responseText.match(/time out/)) {
			showError(response.statusText);
			return false;
		}
	});
});
//function showNotification(pesan,fungsi,position){
//	if (position==undefined) position='top-right';
//	if (typeof fungsi != 'function'){
//		fungsi=null;
//	}
//	$().toastmessage('init', {
//		inEffect: 			{
//			opacity: 'show'
//		},	// in effect
//		inEffectDuration: 	600,				// in effect duration in miliseconds
//		stayTime: 			6000,				// time in miliseconds before the item has to disappear
//		text: 				'',					// content of the item. Might be a string or a jQuery object. Be aware that any jQuery object which is acting as a message will be deleted when the toast is fading away.
//		sticky: 			false,				// should the toast item sticky or not?
//		type: 				'notice', 			// notice, warning, error, success
//		position:           'top-right',        // top-left, top-center, top-right, middle-left, middle-center, middle-right ... Position of the toast container holding different toast. Position can be set only once at the very first call, changing the position after the first call does nothing
//		closeText:          '',                 // text which will be shown as close button, set to '' when you want to introduce an image via css
//		close:              fungsi 
//	});
//	$().toastmessage('showSuccessToast', pesan);
//};
function showInfo(pesan,fungsi){
	var html = '<div id="info"><strong>Info! </strong>' + pesan + '</div>';
	$('body').prepend(html);
	$('#info').dialog({
		close: function(event, ui) {
			$(this).remove()
		},
		draggable: true,
		show: 'clip',
		title: 'INFO',
		resizable: false,
		closeOnEscape: true,
		hide: 'fade',
		modal: true,
		buttons: {
			Ok: function() {
				$( this ).dialog('close');
				if (typeof fungsi == 'function'){
					fungsi();
				}
			}
		}
	});
	setTimeout("$('.ui-dialog-buttonpane').find('button:last').focus()", 500);
}
function showSukses(pesan,fungsi){
	var html = '<div id="sukses"><strong>Berhasil! </strong>' + pesan + '</div>';
	$('body').prepend(html);
	$('#sukses').dialog({
		close: function(event, ui) {
			$(this).remove()
		},
		draggable: true,
		show: 'clip',
		title: 'SUKSES',
		resizable: false,
		closeOnEscape: true,
		hide: 'fade',
		modal: true,
		buttons: {
			Ok: function() {
				$( this ).dialog('close');
				if (typeof fungsi == 'function'){
					fungsi();
				}
			}
		}
	});
	setTimeout("$('.ui-dialog-buttonpane').find('button:last').focus()", 500);
}
function showError(pesan, fungsi){
	var html = '<div id="error"><strong>Error! </strong>' + pesan + '</div>';
	$('body').prepend(html);
	$('#error').dialog({
		close: function(event, ui) {
			$(this).remove()
		},
		draggable: true,
		show: 'clip',
		title: 'ERROR',
		resizable: true,
		closeOnEscape: true,
		width: 450,
		hide: 'fade',
		modal: true,
		buttons: {
			Ok: function() {
				$( this ).dialog('close');
				$( this ).remove();
				if (typeof fungsi == 'function'){
					fungsi();
				}
			}
		}
	});
	setTimeout("$('.ui-dialog-buttonpane').find('button:last').focus()", 500);
}
function showErrorAndExit(pesan){
	var html = '<div id="error"><strong>Error! </strong>' + pesan + '</div>';
	$('body').prepend(html);
	$('#error').dialog({
		close: function(event, ui) {
			window.close();
		},
		draggable: true,
		show: 'clip',
		title: 'ERROR',
		resizable: true,
		closeOnEscape: true,
		width: 450,
		hide: 'fade',
		modal: true,
		buttons: {
			Ok: function() {
                            window.close();
			}
		}
	});
	setTimeout("$('.ui-dialog-buttonpane').find('button:last').focus()", 500);
}
function showLoading(){
	var html = '<div id="loading" align="center"><img src="'+base_url+'img/load.gif" /></div>';
	$('body').prepend(html);
	$('#loading').dialog({
		close: function(event, ui) {
			$(this).remove();
		},
		draggable: true,
		show: 'clip',
		title: 'Loading!',
		resizable: true,
//		hide: 'fade',
		modal: true
	});
//	setTimeout("", 100);
}
function showUserInfo() {
        userdiv = " <div class='user_form'>";
        userdiv = userdiv  + "<div style='float:left;width:100px;'>ID User</div><div style='float:left;'><input id='user_form_id' type='text' readonly></div><div><p>&nbsp;</p></div>";
        userdiv = userdiv  + "<div style='float:left;width:100px;'>Nama User</div><div style='float:left;'><input id='user_form_name' type='text' readonly></div><div><p>&nbsp;</p></div>";
        userdiv = userdiv  + "<div style='float:left;width:100px;'>Password</div><div style='float:left;'><input id='user_form_pwd' type='password'></div><div><p>&nbsp;</p></div>";        

	var html = '<div id="user_info">' + userdiv + '</div>';
	$('body').prepend(html);
	$('#user_info').dialog({
		close: function(event, ui) {
			$(this).remove()
		},
		draggable: true,
		show: 'clip',
		title: 'INFO USER',
		resizable: true,
		closeOnEscape: true,
		width: 450,
		hide: 'fade',
		modal: true,
		buttons: {
			Ok: function() {
				$( this ).dialog('close');
				$( this ).remove();
				if (typeof fungsi == 'function'){
					fungsi();
				}
			}
		}
	});
	setTimeout("$('.ui-dialog-buttonpane').find('button:last').focus()", 500);    
}
function hideLoading(){
	$('#loading').dialog('close');
	$('#loading').remove();
}
function ajaxError(xhr,ajaxOptions,thrownError){
	hideLoading();
	if (xhr.status==403){ //forbiden page
		showError('<br/>Session Login Anda telah berakhir. <br/>Silakan <a href="'+site_url+'/auth/login" target="_blank">Login</a> lagi."<br/>');
	}else{
		showError('<br/>Kode Error : ' + xhr.status + '<br/>' + thrownError + '<br/>' + xhr.responseText);
	}
}
function loadPage2(desturl) {      
      debugger;
              $.ajax({
                  url: desturl,
                  dataType: "html",
                  async:false,
                  success: function(html) {
                      debugger;
                      retval = html
                          //.animate({"height": "toggle"}, { duration: 800 });                           
                  }
              });
       return retval;              
}        

function showProfile(url, fungsi){
        var contents = loadPage2(url)
	var html = '<div style="min-height:200px" id="profile_user">' + contents + '</div>';
	$('body').prepend(html);
	$('#profile_user').dialog({
		close: function(event, ui) {
			$(this).remove()
		},
		draggable: true,
		show: 'clip',
		title: 'Profil/Pergantian Password',
		resizable: true,
		closeOnEscape: true,
		width: 450,
                height: 300,
		hide: 'fade',
		modal: true,
		buttons: {
			Ok: function() {
                            //$( this ).dialog('close'); 
                            user = $("#user_profile_id").val();
                            pass = $("#user_profile_pwd").val();
                            changepwd(user,pass);
			}
		}
	});
	setTimeout("$('.ui-dialog-buttonpane').find('button:last').focus()", 500);
}
function changepwd(user,pwd) {
	$.ajax({
		url:site_url + "/usersec/changepwd",
		type:'POST',
		dataType:'json',
                async:false,
                data: {
                  user:user,
                  pwd:pwd
                },
		cache:false,
		success:function(data){
                    alert("SUKSES");
                    $("#user_profile_confirm").text("Pergantian berhasil");
                    
		},
		error: function(xhr,ajaxOptions,thrownError){
			//hideLoading();
                        //debugger;
                    $("#user_profile_confirm").text("Pergantian Error");
                    //	ajaxError(xhr,ajaxOptions,thrownError);
		}
	});    
}

function loadPage(divId,Url){
	$(divId).empty();
        debugger;
	showLoading();
	$.ajax({
		url:Url,
		type:'POST',
		dataType:'html',
		cache:false,
		beforeSend : function(xhr) {
			xhr.overrideMimeType('text/html; charset=iso-8859-15');
		},
		success:function(html){
			hideLoading();
                        debugger;
			$(divId).html(html);
                        //$(divId).show();
		},
		error: function(xhr,ajaxOptions,thrownError){
			//hideLoading();
                        debugger;
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	});
}
function appendPage(divId,Url){
	$.ajax({
		type: "POST",
		url: Url,
		success: function(html){
			$(divId).append(html);
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	})
}
/**
 * menampilkn myContent ke dalam tag body di sebuah pop window 
 */
function popPrintWindow(myContent, url, name, features){
	if (myContent==undefined){
		myContent='';
	}
	if (url==undefined){
		url='';
	}
	if (name==undefined){
		name='_blank';
	}
	if (features==undefined){
		features='left=100px, width=500, height=400, location=no';
	}

	var prtContent = $('<html>')
	.append($('<head>'))
	.append($('<body>').append(myContent ));

	var winPrint = window.open('', '_blank', features);
	winPrint.document.write(prtContent.html());
	winPrint.focus();
	winPrint.print();
	return winPrint;
}