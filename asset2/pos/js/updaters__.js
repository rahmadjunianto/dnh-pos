function updaters(url,newvalue,key,table_id,colindex) { 
        //debugger;
        var retval = 0;
        $.ajax( {
		url:site_url + url,
		type: "POST",
		dataType: "json",
		async:false,
		data:{
			newdata:newvalue,
                        keydata:key,
                        table_id:table_id,
                        colindex:colindex
		},
		success:function(data) {
                   // debugger;
                    retval = data.status;
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	}); //end of ajax,
        return retval;
}
function updaters_2key(url,newvalue,key,table_id,colindex,keydata2) { 
        //debugger;
        var retval = 0;
        $.ajax( {
		url:site_url + url,
		type: "POST",
		dataType: "json",
		async:false,
		data:{
			newdata:newvalue,
                        keydata:key,
                        keydata2:keydata2,
                        table_id:table_id,
                        colindex:colindex
		},
		success:function(data) {
                   // debugger;
                    retval = data.status;
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	}); //end of ajax,
        return retval;
}
function drow(url,key,table_id) {
        var retval = 0;
        $.ajax( {
		url:site_url + url,
		type: "POST",
		dataType: "json",
		async:false,
		data:{
                        keydata:key,
                        table_id:table_id
		},
		success:function(data) {
                   // debugger;
                    retval = data.status;                    
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	}); //end of ajax,
        return retval;    
}
function addrow(url,table_id) {
        var retval = 0;
        $.ajax( {
		url:site_url + url,
		type: "POST",
		dataType: "json",
		async:false,
		data:{
                        table_id:table_id
		},
		success:function(data) {
                   
                    status = data.status;
                    retval = data.newkey;
                    debugger;
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	}); //end of ajax,
        return retval;    
}
function addrow_with_counter(url,table_id,rowkey) {
        var retval = 0;
        $.ajax( {
		url:site_url + url,
		type: "POST",
		dataType: "json",
		async:false,
		data:{
                        table_id:table_id,
                        rowkey:rowkey
		},
		success:function(data) {
                   
                    status = data.status;
                    retval = data.newkey;
                    debugger;
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	}); //end of ajax,
        return retval;    
}