
$(function () {

var data2 = [
        { rank: 1, company: 'Exxon Mobil', revenues: '339938.0', profits: '36130.0' },
        { rank: 2, company: 'Wal-Mart Stores', revenues: '315654.0', profits: '11231.0' },
        { rank: 3, company: 'Royal Dutch Shell', revenues: '306731.0', profits: '25311.0' },
        { rank: 4, company: 'BP', revenues: '267600.0', profits: '22341.0' },
        { rank: 5, company: 'General Motors', revenues: '192604.0', profits: '-10567.0' },
        { rank: 6, company: 'Chevron', revenues: '189481.0', profits: '14099.0' },
        { rank: 7, company: 'DaimlerChrysler', revenues: '186106.3', profits: '3536.3' },
        { rank: 8, company: 'Toyota Motor', revenues: '185805.0', profits: '12119.6' },
        { rank: 9, company: 'Ford Motor', revenues: '177210.0', profits: '2024.0' },
        { rank: 10, company: 'ConocoPhillips', revenues: '166683.0', profits: '13529.0' },
        { rank: 11, company: 'General Electric', revenues: '157153.0', profits: '16353.0' },
        { rank: 12, company: 'Total', revenues: '152360.7', profits: '15250.0' },
        { rank: 13, company: 'ING Group', revenues: '138235.3', profits: '8958.9' },
        { rank: 14, company: 'Citigroup', revenues: '131045.0', profits: '24589.0' },
        { rank: 15, company: 'AXA', revenues: '129839.2', profits: '5186.5' },
        { rank: 16, company: 'Allianz', revenues: '121406.0', profits: '5442.4' },
        { rank: 17, company: 'Volkswagen', revenues: '118376.6', profits: '1391.7' },
        { rank: 18, company: 'Fortis', revenues: '112351.4', profits: '4896.3' },
        { rank: 19, company: 'Crédit Agricole', revenues: '110764.6', profits: '7434.3' },
        { rank: 20, company: 'American Intl. Group', revenues: '108905.0', profits: '10477.0' }
    ];
    var data1;
	$.ajax( {
		url:site_url + "/tab_home/data/",
		type: "POST",
		dataType: "json",
		async:false,
		data:{
			cabang:0
		},
		success:function(data) {
                    data1=data;
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	}); //end of ajax,
    
var obj = {};
    obj.width = 700;
    obj.height = 400;
    obj.numberCell = false;
    obj.flexWidth = true;
    obj.scrollModel = {horizontal : false};
    //obj.selectionModel: { type: 'cell'};
    obj.title = "<b>Inline Editing</b>";
    obj.bottomVisible = false;
    obj.colModel = [
        { title: "Rank", dataType: "integer", dataIndx: "users_id", minWidth: 150, resizable: false,editable:false ,selectionModel: { type: 'cell'} },
        { title: "Company", dataType: "string", dataIndx: "users_name", width: '138' },
        { title: "Revenues ($ millions)", dataType: "float", width: '138',align: "right", dataIndx: "users_pwd"},
        { title: "Profits ($ millions)", dataType: "float", align: "right", dataIndx: "users_login"}                
    ];
var user_id;
obj.dataModel = {data:data1};
obj.rowSelect = function (evt, obj) {
    //debugger;
    
     var rowIndx = parseInt(obj.rowIndx);
     var user_id= obj.dataModel.data[rowIndx].users_id;
     var user_name= obj.dataModel.data[rowIndx].users_name;
     //alert(user_id + " adalah " + user_name);
     //alert(dataCell);
     //var rowIndx = parseInt(obj.rowIndx) + 1;
     //$("#select_row_display_div").html("row Index: " + rowIndx + ", value is " + dataCell);
 }
var $grid = $("#grid_regist").pqGrid(obj);
$grid.on("pqgridcelleditkeydown", function (evt, ui) {
    
    var keyCode = evt.keyCode,
        rowIndxPage = ui.rowIndxPage,
        colIndx = ui.colIndx;
    //alert(keyCode);
    if (keyCode == 40 || keyCode == 38) {      
       var dt = $grid.pqGrid("getEditCellData");       
       $grid.pqGrid("saveEditCell");
       //alert(dt);
         //alert(keyCode);
         //debugger;
        //obj.rowSelect();
        //var rowIndx = parseInt(obj.rowIndx);
        //debugger;
        obj.dataModel.data[rowIndxPage].users_name="COCOK";
        var user_id= obj.dataModel.data[rowIndxPage].users_id;
        var user_name= obj.dataModel.data[rowIndxPage].users_name;
        $grid.pqGrid("refreshDataAndView");
        alert(user_id + " adalah " + user_name);         
    }

    if (keyCode == 40) {
        //alert(rowIndxPage);
        //alert(data.length);
        if (rowIndxPage >= data1.length - 1) {
            //var dt = $grid.pqGrid("getEditCellData");
            var dt = ui.$cell.text();
            if (dt.length > 0) {
                //alert(dt);
                var row = ["", "", "", ""];
                data.push(row);
                $grid.pqGrid("refreshDataAndView");
            }
        }
        if (rowIndxPage < data1.length - 1) {
            //debugger;
            rowIndxPage++;
            $grid.pqGrid("setSelection", null);
            $grid.pqGrid("setSelection", { rowIndx: rowIndxPage });
            $grid.pqGrid("editCell", { rowIndxPage: rowIndxPage, colIndx: colIndx });
            evt.preventDefault();
            return false;
        }
    }
    else if (keyCode == 38 && rowIndxPage > 0) {
        rowIndxPage--;
        $grid.pqGrid("setSelection", null);
        $grid.pqGrid("setSelection", { rowIndx: rowIndxPage });

        $grid.pqGrid("editCell", { rowIndxPage: rowIndxPage, colIndx: colIndx });
        evt.preventDefault();
        return false;
    }
});
$grid.on("keydown", function (evt) {
    var keyCode = evt.keyCode;
    if (keyCode == 38 || keyCode == 40) {
        evt.preventDefault();
        return false;
    }
});

});