$(function () {
    /*===KEYDOWN DETECTION */
    $("#regist_id_supp").on('keydown',regist_id_supp_on_enter);
    $("#regist_supp_name").on('keydown',regist_supp_name_on_enter);
    $("#regist_title").on('keydown',regist_title_on_enter);
    $("#regist_birth_place").on('keydown',regist_bp_on_enter);

    $("#regist_sex").on('keydown',regist_sex_on_enter);
    $("#regist_birth_date").on('keydown',regist_birth_date_on_enter);
    $("#regist_addr").on('keydown',regist_addr_on_enter);
    $("#regist_city").on('keydown',regist_city_on_enter);
    $("#regist_phone").on('keydown',regist_phone_on_enter);
    $("#regist_phonecell").on('keydown',regist_phonecell_on_enter);
    //regist_phonecell
    //regist_passport_no
    //regist_issuing_office
    //regist_issue_date
    //regist_mahram
    //regist_paket
    //regist_group
    //regist_disc
    //regist_net
    //regist_price
    //regist_mahram_rel
    //regist_mahram_rel_ke
    //regist_dokumen_pendukung
    $("#regist_dokumen_pendukung").on('keydown',regist_dokumen_pendukung_on_enter);
    $("#regist_mahram_rel_ke").on('keydown',regist_mahram_rel_ke_on_enter);
    $("#regist_mahram_rel").on('keydown',regist_mahram_rel_on_enter);
    $("#regist_price").on('keydown',regist_price_on_enter);
    //$("#regist_net").on('keydown',regist_net_on_enter);
    
    $("#regist_group").on('keydown',regist_group_on_enter);
    $("#regist_paket").on('keydown',regist_paket_on_enter);
    $("#regist_mahram").on('keydown',regist_mahram_on_enter);
    $("#regist_issue_date").on('keydown',regist_issue_date_on_enter);
    $("#regist_issuing_office").on('keydown',regist_issuing_office_on_enter);
    $("#regist_passport_no").on('keydown',regist_passport_no_on_enter);
    $("#regist_doct").on('keydown',regist_doct_on_enter);
    $("#regist_nett").on('keydown',regist_nett_on_enter);
    $("#regist_payment_type").on('keydown',regist_payment_type_on_enter);
    //regist_payment_type
    
    $("#regist_payment_amount").on('keydown',regist_payment_amount_on_enter);
    $("#regist_payment_outstanding").on('keydown',regist_payment_outstanding_on_enter);
    $("#regist_money").on('keydown',regist_money_on_enter);
    $("#regist_return").on('keydown',regist_return_on_enter);    
    $("#regist_result_delivery").on('keydown',regist_result_delivery_on_enter);    
    $("#regist_finish_date").on('keydown',regist_finish_date_on_enter);    
    //regist_result_delivery
    //
    function clear_control_paket_id() {
        $("#regist_paket").val("");
    }
    $("#regist_clear_data").click(function() {
        clear_controls();
    })    
    function clear_controls() {
        $("#regist_id_supp").val("");
        $("#regist_supp_name").val("");
        $("#regist_sex").val("");
        $("#regist_birth_date").val("");
        $("#regist_disc").val(0);
        $("#regist_net").val(0);
        $("#regist_price").val(0);
        $("#regist_city").val("");
        $("#regist_addr").val("");
        $("#regist_phone").val("");
        $("#regist_phonecell").val("");    
        $("#regist_passport_no").val("");
        $("#regist_issuing_office").val("");   
        $("#regist_issue_date").val("");   
        $("#regist_mahram").val("");   
        $("#regist_paket").val("");   
        $("#regist_group").val("");   
        $("#regist_mahram_name").text("");
        $("#regist_paket_name").text("");
        $("#regist_group_name").text("");
        
    }
    
function clear_all_controls() {
        $("#regist_id_supp").val("");
        $("#regist_supp_name").val("");
        $("#regist_sex").val("");
        $("#regist_birth_date").val("");
        $("#regist_disc").val(0);
        $("#regist_nett").val(0);
        $("#regist_bruto").val(0);
        $("#regist_addr").val("");
        $("#regist_phone").val("");
        $("#regist_phonecell").val("");        
    }    
    function new_jamaah_id() {
        //regist_id_supp_on_enter
        $.ajax({
                url:site_url  +"regist/next_pat_id",
                type: "POST",
                dataType: "json",
                async:false,
                success:function(data){
                        //alert("sukses");
                        //response(data.jamaah);
                        clear_controls();
                        $("#regist_id_supp").val(data.next_pat_id);
                }
        }); //end of ajax,        
    }       
    function regist_nett_on_enter(event) {
        //debugger;
        if (event.keyCode == 13) {     
            //new_jamaah_id();
            debugger;            
            $("#regist_payment_type").focus();
        }//        
    }        
    function regist_id_supp_on_enter(event) {
        //debugger;
        if (event.keyCode == 13) {     
            new_jamaah_id();
            debugger;            
            $("#regist_title").focus();
        }//        
    }    
    //
    //regist_title_on_enter
    function regist_title_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_supp_name").focus();
        }//        
    }     
    //regist_birth place_on_enter
    function regist_bp_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_birth_date").focus();
        }//        
    }     
    //
    function regist_supp_name_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_sex").focus();
        }//        
    } 
    //
    function regist_sex_on_enter(event) {
        if (event.keyCode == 13) {
            debugger;
            $("#regist_birth_place").focus();
        }//        
    }     
    //
    function regist_birth_date_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_addr").focus();
        }//        
    } 
    //
    function regist_addr_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_city").focus();
        }//        
    }     
    //
    function regist_city_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_phone").focus();
        }//        
    }     
    //    
    function regist_phone_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_phonecell").focus();
        }//        
    } 
    //
    //
    function regist_phonecell_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_passport_no").focus();
        }//        
    } 
    //
   function regist_paket_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_group").focus();
        }//        
    } 
    //
   function regist_mahram_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_dokumen_pendukung").focus();
        }//        
    } 
    //    
   function regist_group_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_price").focus();
        }//        
    } 
    //

    function regist_disc_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            alert("net focus");
            $("#regist_net").focus();
        }//        
    } 
    //   

    //     
    function regist_price_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_disc").focus();
        }//        
    } 
    //     
    function regist_dokumen_pendukung_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_paket").focus();
        }//        
    }     
    //     
    function regist_mahram_rel_ke_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_mahram").focus();
        }//        
    }    
    //     
    function regist_mahram_rel_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_mahram").focus();
        }//        
    }           
    //         
    function regist_issue_date_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_mahram_rel").focus();
        }//        
    } 
    //
    function regist_issuing_office_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_issue_date").focus();
        }//        
    } 
    //
    function regist_passport_no_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_issuing_office").focus();
        }//        
    } 
    //
    //regist_payment_type
    function regist_payment_type_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_payment_amount").focus();
        }//        
    }     
    function regist_doct_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_disc").focus();
        }//        
    } 
    //
    //

    //outstanding
    function calc_outstanding() {
        var to_be_paid = 0;
        var paid=0;
        var outstand = 0;
        to_be_paid =$("#regist_nett").val();
        paid =$("#regist_payment_amount").val();
        outstand=to_be_paid - paid;
        debugger;
        $("#regist_payment_outstanding").val(outstand);
    }    
    //return money
    function calc_return_money() {
        var money1 = 0;
        var money2=0;
        var retmon= 0;
        money1 =$("#regist_money").val();
        money2=$("#regist_payment_amount").val();
        retmon=money1 - money2;
        debugger;
        $("#regist_return").val(retmon);
    }        
    //

    //
    //

    function regist_payment_amount_on_enter(event) {
        //debugger;
        if (event.keyCode == 13) {     
            debugger;
            calc_outstanding();
            $("#regist_payment_outstanding").focus();
        }//        
    }     
    function regist_payment_outstanding_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_money").focus();
        }//        
    }         
    function regist_money_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            calc_return_money();
            $("#regist_return").focus();
        }//        
    }             
    function regist_return_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_result_delivery").focus();
        }//        
    }
    function regist_result_delivery_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_finish_date").focus();
        }//        
    }    
    function regist_finish_date_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_finish_time").focus();
        }//        
    }        
    //nama paket auto complete
    $("#idpaket").autocomplete({
            minLength: 3,
            source: idpaket_autocomplete_source,
            select: idpaket_autocomplete_select,
            focus:  idpaket_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    function idpaket_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"/regist/get_ac_paket",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#idpaket").val()
                    },
                    success:function(data){
                            //alert("sukses");
                            response(data.paket);
                    }
            }); //end of ajax,
    }
    function idpaket_autocomplete_select(event, ui) {
        //debugger;
        clear_control_paket_id();   
        
        //$("#idpaket").val(ui.item.idpaket);        //
        //debugger;
        //$("#regist_paket_name").text(ui.item.name);
        return false;
    }
    function idpaket_autocomplete_focus(event, ui) {            
            //var $cell = ui.$cell;
            //debugger;
    }    
    
    //end of idpaket
    //   regist_payment_amount   
    //   regist_payment_oustanding
    //   regist_money
    //   
    // mahram auto complete
    $("#regist_mahram").autocomplete({
            minLength: 3,
            source: regist_mahram_autocomplete_source,
            select: regist_mahram_autocomplete_select,
            focus:  regist_mahram_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    //$("#regist_mahram").data("ui-autocomplete")._renderMenu = renderMenu_regist_mahram;
    //$("#regist_mahram").data("ui-autocomplete")._renderItem = renderItem_regist_mahram;       
    
    //   end of group jamaah
    
    // end of mahram auto complete
    //   Group Jamaah
    $("#regist_group").autocomplete({
            minLength: 3,
            source: group_jamaah_autocomplete_source,
            select: group_jamaah_autocomplete_select,
            focus:  group_jamaah_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    //$("#regist_group").data("ui-autocomplete")._renderMenu = renderMenu_group_jamaah;
    //$("#regist_group").data("ui-autocomplete")._renderItem = renderItem_group_jamaah;       
    
    //   end of group jamaah
    //   Pket Tour
    $("#regist_paket").autocomplete({
            minLength: 3,
            source: regist_paket_autocomplete_source,
            select: regist_paket_autocomplete_select,
            focus:  regist_paket_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    //$("#regist_paket").data("ui-autocomplete")._renderMenu = renderMenu_paket;
    //$("#regist_paket").data("ui-autocomplete")._renderItem = renderItem_paket;       
    //   end of paket tour
//   regist_return
    $("#regist_id_supp").autocomplete({
            minLength: 3,
            source: regist_id_supp_autocomplete_source,
            select: regist_id_supp_autocomplete_select,
            focus:  regist_id_supp_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    $("#regist_id_supp").data("ui-autocomplete")._renderMenu = renderMenu_regist_cust;
    $("#regist_id_supp").data("ui-autocomplete")._renderItem = renderItem_regist_cust;            
    
    function renderMenu_regist_cust(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">ID Pelanggan</td>" +
            "<td width=" + (2*col4width) + ">Nama Pelanggan</td>" +
            "<td width=" + (2*col4width) + ">Alamat</td>" +
            "<td width=" + col2width + ">Telp</td>" +
            "<td width=" + col1width + ">HP</td>" +
            "<td width=" + col2width + ">Keterangan</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_regist_cust(table, item ) {
            //debugger;
            //alert("autocomplete render item");   
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + (2*col4width) + ">" + item.name + "</td>" +
            "<td width=" + (2*col4width) + ">" + item.addr + "</td>" +
            "<td width=" + col2width + ">" + item.phone + "</td>" +
            "<td width=" + col1width + ">" + item.phonecell + "</td>" +
            "<td width=" + col2width + ">" + item.desc + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function regist_id_supp_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"supplier/get_ac_supp",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#regist_id_supp").val()
                    },
                    success:function(data){
                            debugger;
                            response(data.supp);
                    }
            }); //end of ajax,
    }
    function regist_id_supp_autocomplete_focus(event,ui) {
        //
    }
    function clear_cust_data() {
        $("#regist_id_supp").val("");
        $("#regist_supp_name").val("");
    }
    function regist_id_supp_autocomplete_select(event, ui) {
        //debugger;
        //clear_controls();
        clear_cust_data();        
        $("#regist_id_supp").val(ui.item.id);
        $("#regist_supp_name").val(ui.item.name);
        $("#regist_supp_name").focus();
        //
        return false;
    }
// regist paket tour auto complete
    function renderMenu_paket(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">ID Paket</td>" +
            "<td width=" + col4width + ">Nama Paket</td>" +
            "<td width=" + col4width + ">Bulan</td>" +
            "<td width=" + col2width + ">Awal</td>" +
            "<td width=" + col1width + ">Akhir</td>" +
            "<td width=" + col2width + ">Harga</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_paket(table, item ) {
            //debugger;
            //alert("autocomplete render item");   
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + col4width + ">" + item.name + "</td>" +
            "<td width=" + col4width + ">" + item.mnth + "</td>" +
            "<td width=" + col2width + ">" + item.start + "</td>" +
            "<td width=" + col1width + ">" + item.finish+ "</td>" +
            "<td width=" + col2width + ">" + item.price + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function regist_paket_autocomplete_source(request,response){
            //alert("autocomplete source");
            type=$("#paket_type").val();
            $.ajax({
                    url:site_url  +"regist/get_ac_paket",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#regist_paket").val(),
                            type:type
                    },
                    success:function(data){
                           // debugger;
                            response(data.paket);
                    }
            }); //end of ajax,
    }
    function regist_paket_autocomplete_focus(event,ui) {
        //
    }
    function regist_paket_autocomplete_select(event, ui) {
        //debugger;
        clear_control_paket_id();        
        $("#regist_paket").val(ui.item.id);
        $("#regist_paket_name").text(ui.item.name);
        //debugger;
        $("#regist_price").val(ui.item.price);
        //
        return false;
    }
//   end of ac regist paket   
    //cetak nota
// regist paket grup jamaah
    function renderMenu_group_jamaah(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">ID Group</td>" +
            "<td width=" + col4width + ">Nama Group</td>" +
            "<td width=" + col4width + ">Alamat</td>" +
            "<td width=" + col4width + ">Nama Kontak</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_group_jamaah(table, item ) {
            //debugger;
            //alert("autocomplete render item");   
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + col4width + ">" + item.name + "</td>" +
            "<td width=" + col4width + ">" + item.addr + "</td>" +
            "<td width=" + col4width + ">" + item.contact + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function group_jamaah_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"regist/get_ac_group_jamaah",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#regist_group").val()
                    },
                    success:function(data){
                           // debugger;
                            response(data.group_jamaah);
                    }
            }); //end of ajax,
    }
    function group_jamaah_autocomplete_focus(event,ui) {
        //
    }
    function group_jamaah_autocomplete_select(event, ui) {
        //debugger;
        //clear_control_paket_id();     
        $("#regist_group").val("");
        $("#regist_group_name").text("");
        $("#regist_group").val(ui.item.id);
        $("#regist_group_name").text(ui.item.name);
        //
        return false;
    }
//   end of ac regist grup jamaah
// regist paket grup mahram
    function renderMenu_regist_mahram(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">ID Jamaah </td>" +
            "<td width=" + col4width + ">Nama Jamaah</td>" +
            "<td width=" + col4width + ">Jenis Kelamin</td>" +
            "<td width=" + col4width + ">Alamat</td>" +            
            "<td width=" + col4width + ">Phone</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_regist_mahram(table, item ) {
            //debugger;
            //alert("autocomplete render item");   
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + col4width + ">" + item.title + " " + item.name + "</td>" +
            "<td width=" + col4width + ">" + item.sex + "</td>" +
            "<td width=" + col4width + ">" + item.addr + "</td>" +
            "<td width=" + col4width + ">" + item.phone + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function regist_mahram_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"regist/get_ac_jamaah",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#regist_mahram").val()
                    },
                    success:function(data){
                           // debugger;
                            response(data.jamaah);
                    }
            }); //end of ajax,
    }
    function regist_mahram_autocomplete_focus(event,ui) {
        //
    }
    function regist_mahram_autocomplete_select(event, ui) {
        //debugger;
        //clear_control_paket_id();        
        $("#regist_mahram").val("");
        $("#regist_mahram_name").text("");
        $("#regist_mahram").val(ui.item.id);
        $("#regist_mahram_name").text(ui.item.title + " " + ui.item.name);
        //
        return false;
    }
//   end of ac regist group mahram
    function print_nota(trans_id) {
            $.ajax( {
                url:site_url + "/registin/regist_nota/" + trans_id,
                type: "POST",
                dataType: "json",
                async:false,
                success:function(data) {
                     //alert(data.nominal);
                },
                error: function(xhr,ajaxOptions,thrownError){
                        ajaxError(xhr,ajaxOptions,thrownError);
                }
        }); //end of ajax,                
    }
    function OpenInNewTab(url )
    {
      var win=window.open(url, '_blank');
      win.focus();
    }        
    $("#print_trans_in").click(function() {
        //print_nota("140800015");
        trans_id = $("#trans_in_id").val();
        //payment_no = $("#payment_id").val();
        OpenInNewTab(site_url + "registin/regist_nota/" + trans_id );
    });
    
});    