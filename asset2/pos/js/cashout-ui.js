$(function () {
    /*===KEYDOWN DETECTION */
    $("#regist_id_klien").on('keydown',regist_id_klien_on_enter);
    $("#regist_name").on('keydown',regist_name_on_enter);
    $("#regist_title").on('keydown',regist_title_on_enter);
    $("#regist_birth_place").on('keydown',regist_bp_on_enter);
    //regist_type
    $("#regist_type").on('keydown',regist_type_on_enter);
    
    $("#regist_id_bank").on('keydown',regist_id_bank_on_enter);
    $("#regist_ttype").on('keydown',regist_ttype_on_enter);
    $("#regist_id_klien").on('keydown',regist_id_klien_on_enter);
    //
    $("#regist_sex").on('keydown',regist_sex_on_enter);
    $("#regist_birth_date").on('keydown',regist_birth_date_on_enter);
    $("#regist_addr").on('keydown',regist_addr_on_enter);
    $("#regist_city").on('keydown',regist_city_on_enter);
    $("#regist_kabupaten").on('keydown',regist_kabupaten_on_enter);
    $("#regist_phone").on('keydown',regist_phone_on_enter);
    $("#regist_phonecell").on('keydown',regist_phonecell_on_enter);
    $("#regist_email").on('keydown',regist_email_on_enter);
    $("#regist_finish_date").on('keydown',regist_finish_date_on_enter);
    $("#regist_akad_date").on('keydown',regist_akad_date_on_enter);
    $("#regist_pic").on('keydown',regist_pic_on_enter);
    //regist_email
    //regist_mahram_rel_ke
    //regist_dokumen_pendukung
    $("#regist_dokumen_pendukung").on('keydown',regist_dokumen_pendukung_on_enter);
    $("#regist_mahram_rel_ke").on('keydown',regist_mahram_rel_ke_on_enter);
    $("#regist_mahram_rel").on('keydown',regist_mahram_rel_on_enter);
    $("#regist_price").on('keydown',regist_price_on_enter);
    //$("#regist_net").on('keydown',regist_net_on_enter);
    
    $("#regist_group").on('keydown',regist_group_on_enter);
    $("#regist_paket").on('keydown',regist_paket_on_enter);
    $("#regist_mahram").on('keydown',regist_mahram_on_enter);
    $("#regist_issue_date").on('keydown',regist_issue_date_on_enter);
    $("#regist_issuing_office").on('keydown',regist_issuing_office_on_enter);
    $("#regist_passport_no").on('keydown',regist_passport_no_on_enter);
    $("#regist_doct").on('keydown',regist_doct_on_enter);
    $("#regist_nett").on('keydown',regist_nett_on_enter);
    $("#regist_payment_type").on('keydown',regist_payment_type_on_enter);
    //regist_payment_type
    
    $("#regist_payment_amount").on('keydown',regist_payment_amount_on_enter);
    $("#regist_payment_outstanding").on('keydown',regist_payment_outstanding_on_enter);
    $("#regist_money").on('keydown',regist_money_on_enter);
    $("#regist_return").on('keydown',regist_return_on_enter);    
    $("#regist_result_delivery").on('keydown',regist_result_delivery_on_enter);    
    $("#regist_finish_date").on('keydown',regist_finish_date_on_enter);    
    //regist_result_delivery
    //
    function clear_control_paket_id() {
        $("#regist_paket").val("");
    }
    $("#regist_clear_data").click(function() {
        clear_controls();
    })    
    function clear_controls() {
        $("#regist_id_klien").val("");
        $("#regist_name").val("");
        $("#regist_sex").val("");
        $("#regist_birth_date").val("");
        //$("#regist_disc").val(0);
        //$("#regist_net").val(0);
        //$("#regist_price").val(0);
        $("#regist_city").val("");
        $("#regist_addr").val("");
        $("#regist_phone").val("");
        $("#regist_phonecell").val("");    
        $("#regist_passport_no").val("");
        $("#regist_issuing_office").val("");   
        $("#regist_issue_date").val("");   
        $("#regist_mahram").val("");   
        $("#regist_paket").val("");   
        $("#regist_group").val("");   
        $("#regist_mahram_name").text("");
        $("#regist_paket_name").text("");
        $("#regist_group_name").text("");
        
    }
    
function clear_all_controls() {
        $("#regist_id_klien").val("");
        $("#regist_name").val("");
        $("#regist_sex").val("");
        $("#regist_birth_date").val("");
        //$("#regist_disc").val(0);
        $("#regist_nett").val(0);
        $("#regist_bruto").val(0);
        $("#regist_addr").val("");
        $("#regist_phone").val("");
        $("#regist_phonecell").val("");        
    }    
    function new_klien_id() {
        //regist_id_klien_on_enter
        $.ajax({
                url:site_url  +"client/next_id",
                type: "POST",
                dataType: "json",
                async:false,
                success:function(data){
                        //alert("sukses");
                        //response(data.klien);
                        clear_controls();
                        $("#regist_id_klien").val(data.next_id);
                }
        }); //end of ajax,        
    }       
    function regist_nett_on_enter(event) {
        //debugger;
        if (event.keyCode == 13) {     
            //new_klien_id();
            debugger;            
            $("#regist_payment_type").focus();
        }//        
    }        
    function regist_id_klien_on_enter(event) {
        //debugger;
        if (event.keyCode == 13) {     
            new_klien_id();
            debugger;            
            $("#regist_title").focus();
        }//        
    }    
    //
    //regist_title_on_enter
    function regist_title_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_name").focus();
        }//        
    }     
    //
    function regist_type_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_id_bank").focus();
        }//        
    }         
    function regist_ttype_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_id_klien").focus();
        }//        
    }             
    //regist_type
    //regist_id_bank
    function regist_id_bank_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_id_klien").focus();
        }//        
    }             
    //regist_birth place_on_enter
    function regist_bp_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_birth_date").focus();
        }//        
    }     
    //
    function regist_name_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_sex").focus();
        }//        
    } 
    //
    function regist_sex_on_enter(event) {
        if (event.keyCode == 13) {
            debugger;
            $("#regist_birth_place").focus();
        }//        
    }     
    //
    function regist_birth_date_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_addr").focus();
        }//        
    } 
    //
    function regist_addr_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_kabupaten").focus();
        }//        
    }     
    //
    function regist_city_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_phone").focus();
        }//        
    }     
    //
    function regist_kabupaten_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_city").focus();
        }//        
    }         
    //    
    function regist_phone_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_phonecell").focus();
        }//        
    } 
    //
    //
    function regist_phonecell_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_email").focus();
        }//        
    } 
    //
    function regist_email_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_akad_date").focus();
        }//        
    }     
    //
    function regist_akad_date_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_finish_date").focus();
        }//        
    }         
    //
    function regist_finish_date_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_pic").focus();
        }//        
    }         
    //
    function regist_pic_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {
            debugger;
            $("#regist_price").focus();
        }//        
    }             
    //
   function regist_paket_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_group").focus();
        }//        
    } 
    //
   function regist_mahram_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_dokumen_pendukung").focus();
        }//        
    } 
    //    
   function regist_group_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_price").focus();
        }//        
    } 
    //
    //     
    function regist_price_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_disc").focus();
        }//        
    } 
    //     
    function regist_dokumen_pendukung_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_paket").focus();
        }//        
    }     
    //     
    function regist_mahram_rel_ke_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_mahram").focus();
        }//        
    }    
    //     
    function regist_mahram_rel_on_enter(event) {
        debugger;
        //alert(event.keyCode);
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_mahram").focus();
        }//        
    }           
    //         
    function regist_issue_date_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_mahram_rel").focus();
        }//        
    } 
    //
    function regist_issuing_office_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_issue_date").focus();
        }//        
    } 
    //
    function regist_passport_no_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_issuing_office").focus();
        }//        
    } 
    //
    //regist_payment_type
    function regist_payment_type_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_payment_amount").focus();
        }//        
    }     
    function regist_doct_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_disc").focus();
        }//        
    } 
    //
    //

    //outstanding
    function calc_outstanding() {
        var to_be_paid = 0;
        var paid=0;
        var outstand = 0;
        to_be_paid =$("#regist_nett").val();
        paid =$("#regist_payment_amount").val();
        outstand=to_be_paid - paid;
        debugger;
        $("#regist_payment_outstanding").val(outstand);
    }    
    //return money
    function calc_return_money() {
        var money1 = 0;
        var money2=0;
        var retmon= 0;
        money1 =$("#regist_money").val();
        money2=$("#regist_payment_amount").val();
        retmon=money1 - money2;
        debugger;
        $("#regist_return").val(retmon);
    }        
    //

    //
    //

    function regist_payment_amount_on_enter(event) {
        //debugger;
        if (event.keyCode == 13) {     
            debugger;
            calc_outstanding();
            $("#regist_payment_outstanding").focus();
        }//        
    }     
    function regist_payment_outstanding_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_money").focus();
        }//        
    }         
    function regist_money_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            calc_return_money();
            $("#regist_return").focus();
        }//        
    }             
    function regist_return_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_result_delivery").focus();
        }//        
    }
    function regist_result_delivery_on_enter(event) {
        debugger;
        if (event.keyCode == 13) {     
            debugger;
            $("#regist_finish_date").focus();
        }//        
    }    
    //
    //auto complete pic

    
    //bank autocomplete    
    
    //nama paket auto complete

    
    //end of idpaket
    //   regist_payment_amount   
    //   regist_payment_oustanding
    //   regist_money
    //   
    // mahram auto complete
    
    // end of mahram auto complete
    //   Group Klien
    $("#regist_group").autocomplete({
            minLength: 3,
            source: group_klien_autocomplete_source,
            select: group_klien_autocomplete_select,
            focus:  group_klien_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    /*
    $("#regist_group").data("ui-autocomplete")._renderMenu = renderMenu_group_klien;
    $("#regist_group").data("ui-autocomplete")._renderItem = renderItem_group_klien;       
    */
    //   end of group klien
    //   Pket Tour
    $("#regist_paket").autocomplete({
            minLength: 3,
            source: regist_paket_autocomplete_source,
            select: regist_paket_autocomplete_select,
            focus:  regist_paket_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    /*
    $("#regist_paket").data("ui-autocomplete")._renderMenu = renderMenu_paket;
    $("#regist_paket").data("ui-autocomplete")._renderItem = renderItem_paket;       
    */
    //   end of paket tour
//   regist_return
/*
    $("#regist_id_klien").autocomplete({
            minLength: 3,
            source: regist_id_klien_autocomplete_source,
            select: regist_id_klien_autocomplete_select,
            focus:  regist_id_klien_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    //
    $("#regist_id_klien").data("ui-autocomplete")._renderMenu = renderMenu_regist;    
    $("#regist_id_klien").data("ui-autocomplete")._renderItem = renderItem_regist;
    function renderMenu_regist(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">ID Klien</td>" +
            "<td width=" + col4width + ">Nama Klien</td>" +
            "<td width=" + col4width + ">Alamat</td>" +
            "<td width=" + col2width + ">Telp</td>" +
            "<td width=" + col1width + ">HP</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_regist(table, item ) {
            //debugger;
            //alert("autocomplete render item");   
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + col4width + ">" + item.name + "</td>" +
            "<td width=" + col4width + ">" + item.addr + "</td>" +
            "<td width=" + col2width + ">" + item.phone + "</td>" +
            "<td width=" + col1width + ">" + item.phonecell + "</td>" +
            "<td width=" + col2width + ">" + item.desc + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function regist_id_klien_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"client/ac_client",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:request.term
                    },
                    success:function(data){
                           // debugger;
                            response(data.result);
                    }
            }); //end of ajax,
    }
    function regist_id_klien_autocomplete_focus(event,ui) {
        //
    }
    */
    function clear_biodata() {
        //$("#").val("");
        $("#regist_id_klien").val("");
        $("#regist_title").val("");
        $("#regist_name").val("");
        $("#regist_sex").val("");
        $("#regist_birth_place").val("");
        $("#regist_birth_date").val("");
        $("#regist_addr").val("");
        $("#regist_kabupaten").val("");
        $("#regist_city").val("");
        $("#regist_phone").val("");
        $("#regist_phonecell").val("");
        $("#regist_email").val("");
        $("#regist_finish_date").val("");
        $("#regist_pic").val("");
        
    }
    function regist_id_klien_autocomplete_select(event, ui) {
        //debugger;
        //clear_controls();
        clear_biodata();
        debugger;
        $("#regist_id_klien").val(ui.item.id);
        $("#regist_name").val(ui.item.name);
        $("#regist_sex").val(ui.item.sex);
        $("#regist_title").val(ui.item.title);
        $("#regist_birth_date").val(ui.item.birthdate);
        $("#regist_kabupaten").val(ui.item.kabupaten);
        $("#regist_city").val(ui.item.city);
        $("#regist_birth_place").val(ui.item.birthplace);
        $("#cl_birthdate").val(ui.item.birthdate);
        $("#regist_email").val(ui.item.email);
        //regist_addr
        $("#regist_addr").val(ui.item.addr);
        $("#regist_phone").val(ui.item.phone);
        
        
        //regist_passport_no
        $("#regist_phonecell").val(ui.item.phonecell);        
        //regist_mahram
        
        $("#regist_name").focus();
        //
        return false;
    }
// regist paket tour auto complete
    function renderMenu_paket(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">ID Paket</td>" +
            "<td width=" + col4width + ">Nama Paket</td>" +
            "<td width=" + col4width + ">Bulan</td>" +
            "<td width=" + col2width + ">Awal</td>" +
            "<td width=" + col1width + ">Akhir</td>" +
            "<td width=" + col2width + ">Harga</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_paket(table, item ) {
            //debugger;
            //alert("autocomplete render item");   
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + col4width + ">" + item.name + "</td>" +
            "<td width=" + col4width + ">" + item.mnth + "</td>" +
            "<td width=" + col2width + ">" + item.start + "</td>" +
            "<td width=" + col1width + ">" + item.finish+ "</td>" +
            "<td width=" + col2width + ">" + item.price + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function regist_paket_autocomplete_source(request,response){
            //alert("autocomplete source");
            type=$("#paket_type").val();
            $.ajax({
                    url:site_url  +"regist/get_ac_paket",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#regist_paket").val(),
                            type:type
                    },
                    success:function(data){
                           // debugger;
                            response(data.paket);
                    }
            }); //end of ajax,
    }
    function regist_paket_autocomplete_focus(event,ui) {
        //
    }
    function regist_paket_autocomplete_select(event, ui) {
        //debugger;
        clear_control_paket_id();        
        $("#regist_paket").val(ui.item.id);
        $("#regist_paket_name").text(ui.item.name);
        //debugger;
        $("#regist_price").val(ui.item.price);
        //
        return false;
    }
//   end of ac regist paket   
    //cetak nota
// regist paket grup klien
    function renderMenu_group_klien(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">ID Group</td>" +
            "<td width=" + col4width + ">Nama Group</td>" +
            "<td width=" + col4width + ">Alamat</td>" +
            "<td width=" + col4width + ">Nama Kontak</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_group_klien(table, item ) {
            //debugger;
            //alert("autocomplete render item");   
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + col4width + ">" + item.name + "</td>" +
            "<td width=" + col4width + ">" + item.addr + "</td>" +
            "<td width=" + col4width + ">" + item.contact + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function group_klien_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"regist/get_ac_group_klien",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#regist_group").val()
                    },
                    success:function(data){
                           // debugger;
                            response(data.group_klien);
                    }
            }); //end of ajax,
    }
    function group_klien_autocomplete_focus(event,ui) {
        //
    }
    function group_klien_autocomplete_select(event, ui) {
        //debugger;
        //clear_control_paket_id();     
        $("#regist_group").val("");
        $("#regist_group_name").text("");
        $("#regist_group").val(ui.item.id);
        $("#regist_group_name").text(ui.item.name);
        //
        return false;
    }
//   end of ac regist grup klien

//   end of ac regist group mahram
    function print_nota(lab_id) {
            $.ajax( {
                url:site_url + "/regist/regist_nota/" + lab_id,
                type: "POST",
                dataType: "json",
                async:false,
                success:function(data) {
                     //alert(data.nominal);
                },
                error: function(xhr,ajaxOptions,thrownError){
                        ajaxError(xhr,ajaxOptions,thrownError);
                }
        }); //end of ajax,                
    }
    function OpenInNewTab(url )
    {
      var win=window.open(url, '_blank');
      win.focus();
    }        
    $("#regist_lab_id").click(function() {
        //print_nota("140800015");
        lab_id = $("#regist_lab_id").text();
        payment_no = $("#payment_no").val();
        OpenInNewTab(site_url + "/regist/regist_nota/" + lab_id + "/" + payment_no);
    });
    
});    