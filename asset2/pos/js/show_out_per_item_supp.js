$(function () {
    col1width=100;
    col2width=300;
    col3width=100;
    col4width=100;
    col5width=100;
    //$("#search_master_criteria").click()
    $("#show_prod_in").click(function() {
        prod_filter_date=$("#prod_filter_date").val();
        prod_filter_date2=$("#prod_filter_date2").val();
        ajaxurl = site_url  +"rptinout/calc_in";
        debugger;
        $.ajax({
                url:ajaxurl,
                type: "POST",
                dataType: "json",
                data:{
                        filterdate:prod_filter_date,
                        filterdate2:prod_filter_date2,
                },
                success:function(data){
                    debugger;
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);
                    tablename = "prod_global";
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
                    for(i=0;i<data.list.length;i++) {
                            counter= data.list[i].counter;
                            trans_id= data.list[i].trans_id;
                            name= data.list[i].name;
                            datename= data.list[i].date;
                            total= data.list[i].total;
                            qty= data.list[i].qty;
                            price= data.list[i].price;
                            supp= data.list[i].supp;
                            strappend = "<tr><td class='selected'>" + counter + "</td>" + "<td class='selected'>" + datename + "</td>";
                            strappend = strappend + "<td class='selected'>" + trans_id + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + name + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + supp + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + price + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + qty+ "</td>" ;
                            strappend = strappend + "<td class='selected'>" + total + "</td></tr>";
                        //alert(strappend);
                            $("#" + tablename + " > tbody").append(strappend);
                    }
                }
        }); //end of ajax,
    })
    //
    $("#show_prod_out").click(function() {
        prod_filter_date=$("#prod_filter_date").val();
        prod_filter_date2=$("#prod_filter_date2").val();
        ajaxurl = site_url  +"rptinout/calc_out";
        debugger;
        $.ajax({
                url:ajaxurl,
                type: "POST",
                dataType: "json",
                data:{
                        filterdate:prod_filter_date,
                        filterdate2:prod_filter_date2,
                },
                success:function(data){
                    debugger;
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);
                    tablename = "prod_global_out";
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
                    for(i=0;i<data.list.length;i++) {
                            counter= data.list[i].counter;
                            trans_id= data.list[i].trans_id;
                            name= data.list[i].name;
                            datename= data.list[i].date;
                            total= data.list[i].total;
                            qty= data.list[i].qty;
                            price= data.list[i].price;
                            client= data.list[i].client;
                            strappend = "<tr><td class='selected'>" + counter + "</td>" + "<td class='selected'>" + datename + "</td>";
                            strappend = strappend + "<td class='selected'>" + trans_id + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + name + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + client + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + price + "</td>" ;
                            strappend = strappend + "<td class='selected'>" + qty+ "</td>" ;
                            strappend = strappend + "<td class='selected'>" + total + "</td></tr>";
                        //alert(strappend);
                            $("#" + tablename + " > tbody").append(strappend);
                    }
                }
        }); //end of ajax,
    })
    function OpenInNewTab(url )
    {
      debugger;
      var win=window.open(url, '_blank');
        if (win.focus)
        {
          win.focus();
        }
      //win.focus();
      return win;
    }
    //
    $("#prod_detail_print").click(function(){
        //alert(prod_type);
        if(prod_type=="jamaah") {
            filter_date=$("#prod_filter_date").val()
            filter_date2=$("#prod_filter_date2").val()
            OpenInNewTab(site_url + "/printing/printdoc/5/" + filter_date + "/" + filter_date2);
        }
        if(prod_type=="group") {
            filter_date=$("#prod_filter_date").val()
            filter_date2=$("#prod_filter_date2").val()
            group_id = $("#prod_detail_group_id").val()
            OpenInNewTab(site_url + "/printing/printdoc/6/" + filter_date + "/" + filter_date2 + "/" + group_id);
        }
    })
    $("#prod_global_print").click(function(){
        var type="";

        type="1503195";
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()
        OpenInNewTab(site_url + "printing/printdoc/" + type + "/" + filter_date + "/" + filter_date2);
    })
    //
    $("#rpt_in_print_excel").click(function(){
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()
        debugger;
        judul = " Barang Masuk "  + filter_date + " s/d "  + filter_date2;
        $("#prod_global").battatech_excelexport({
            containerid: "prod_global"
           , datatype: 'table'
        });
    })
    $("#rpt_out_print_excel").click(function(){
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()
        debugger;
        judul = " Barang Keluar"  + filter_date + " s/d "  + filter_date2;
        $("#prod_global_out").battatech_excelexport({
            containerid: "prod_global_out"
           , datatype: 'table'
        });
    })
    $("#sc_print_excel").click(function(){
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()
        debugger;
        judul = " Kartu Stok"  + filter_date + " s/d "  + filter_date2;
        $("#prod_detail").battatech_excelexport({
            containerid: "prod_detail"
           , datatype: 'table'
        });
    })
    //
    $("#prod_out_print").click(function(){
        var type="";

        type="1503196";
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()
        OpenInNewTab(site_url + "printing/printdoc/" + type + "/" + filter_date + "/" + filter_date2);
    })
    $("#prod_sc_print").click(function(){
        var type="";

        type="1503197";
        code=$("#prod_code").val();
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()
        OpenInNewTab(site_url + "printing/printdoc/" + type + "/" + filter_date + "/" + filter_date2 + "/" + code);
    })
    $("#prod_sc").click(function() {
        ajaxurl = site_url  +"rptinout/calc_per_supplier";
        //
        debugger;
        $.ajax({
                url:ajaxurl,
                type: "POST",
                dataType: "json",
                data:{
                        id:$("#prod_supplier").val()
                },
                success:function(data){
                    debugger;
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);
                    tablename = "prod_detail";
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
        /*
                        'Hema' => $totalhema,
                        'Kimia' => $totalkimia,
			'Imun' => $totalimun,
                        'Faeces' => $totalfaeces,
                        'Urine' => $totalurine,
                        'Analisa-Lain' => $totalanalisalain,
                        'Mikrobiologi' => $totalmikro,
                        'Infeksi-Lain' => $totalinfeksilain,
                        'Radiologi' => $totalradiologi,
                        'EKG' => $totalekg,
                        'USG' => $totalusg,
                        'netto' => $grandtotal
         */
                    for(i=0;i<data.list.length;i++) {
                            counter= data.list[i].counter;
                            prod_name = data.list[i].prod_name;
                            code = data.list[i].code;
                            prod_unit = data.list[i].prod_unit;
                            prod_sellprice = data.list[i].prod_sellprice;
                            prod_buyprice= data.list[i].prod_buyprice;
                            stock= data.list[i].stock;
                            // sumqty= data.list[i].sumqty;
                            //totalqty= data.list[i].totalqty;
                            strappend = "<tr><td class='selected'>" + counter + "</td>" + "<td class='selected'>" + code + "</td>";
                            strappend = strappend + "<td class='selected'>" + prod_name + "</td>" + "<td class='selected'>" + stock + "</td>";
                            strappend = strappend + "<td class='selected'>" + prod_unit + "</td>" + "<td class='selected'>" + prod_buyprice + "</td>";
                            //strappend = strappend + "<td class='selected'>" + totalqty + "</td>";
                            //strappend = strappend + "<td class='selected'>" + price + "</td>";
                            strappend = strappend + "<td class='selected'>" + prod_sellprice + "</td></tr>";
                        //alert(strappend);
                            $("#" + tablename + " > tbody").append(strappend);
                    }
                }
        }); //end of ajax,
    })
    //auto complete
    $("#prod_supplier").autocomplete({
            minLength: 3,
            source: prod_supplier_autocomplete_source,
            select: prod_supplier_autocomplete_select,
            focus:  prod_supplier_autocomplete_focus,
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    function prod_supplier_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"products/search_unique_supplier",
                    type: "POST",
                    dataType: "json",
                    data:{
                            code:$("#prod_supplier").val()
                    },
                    success:function(data){
                            //alert("sukses");
                            response(data.result);
                    }
            }); //end of ajax,
    }
    function prod_supplier_autocomplete_select(event, ui) {
        //debugger;
        //clear_control_paket_id();

        $("#prod_supplier").val(ui.item.code);        //
        //debugger;
        //$("#regist_paket_name").text(ui.item.name);
        return false;
    }
    function prod_supplier_autocomplete_focus(event, ui) {
            //var $cell = ui.$cell;
            //debugger;
    }
    //
    $("#prod_supplier").data("ui-autocomplete")._renderMenu = renderMenu_prod_supplier;
    $("#prod_supplier").data("ui-autocomplete")._renderItem = renderItem_prod_supplier;

    function renderMenu_prod_supplier(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">Kode Supplier</td>" +
            "<td width=" + (2*col4width) + ">Nama Supplier</td>" +
            "<td width=" + col2width + ">Alamat Supplier </td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function renderItem_prod_supplier(table, item ) {
            //debugger;
            //alert("autocomplete render item");
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.code + "</td>" +
            "<td width=" + (2*col4width) + ">" + item.supp_name + "</td>" +
            "<td width=" + col2width + ">" + item.supp_address + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };

});
