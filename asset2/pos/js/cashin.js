$(function () {
    //$("#search_master_criteria").click()    
    debugger;
    col_to_search="1";
    value_to_search="";
    col1width=100;
    col2width=100;
    col3width=100;
    col4width=100;
    col5width=100;    
    col6width=100;    
    var _lab_id = "";
    //
    $(".payment_frame_state").text("");
    $(".payment_frame_outstanding").text("");
    
    //    
    newObj.dataModel = {
        location: "remote",
        sorting: "remote",
        paging: "remote",
        dataType: "JSON",
        method: "POST",
        curPage: 1,        
        rPP: 1000,
        sortIndx: default_sort_col,
        sortDir: "up",
        rPPOptions: [1, 10, 20, 30, 40, 50, 100, 500, 1000],
        filterIndx:"2",
        filterValue:"",
        getUrl: function () {
            debugger;
            
            if(value_to_search=="") {
                value_to_search="#";
            }
            //search_ctrl_name ="#search_val_criteria" + grid_name;
            var txt = $("#" + newObj.filter_id).val(); //.toUpperCase(),
            var colIndx = $("#" + newObj.filter_select_htmlid).val();
            if($.isNumeric(colIndx)) {
                colIndx=colIndx;
            }else
            {
                colIndx=1;
            }
            
            //var txt = $(search_ctrl_name).val(),
            //colIndx = $("select#pq-filter-select-column").val();
            //debugger;
            //alert(table_id);
            //alert(site_url + geturladdr + this.curPage + "/" +
            //    this.rPP + "/" + this.sortIndx + "/" + this.sortDir + "/" + col_to_search + "/" + value_to_search + "/" + table_id);
            
            var sortDir = (this.sortDir == "up") ? "asc" : "desc";
            //alert(site_url + geturladdr + this.curPage + "/" + this.rPP + "/" + this.sortIndx + "/" + sortDir + "/" + col_to_search + "/" + value_to_search + "/" + table_id)
            var sort = sortcols;
            //data: "cur_page=" + this.curPage + "&records_per_page=" +
            //    this.rPP + "&sortBy=" + sort[this.sortIndx] + "&dir=" + sortDir
            return { url: site_url + geturladdr , 
                    data : { 
                        curpage : this.curPage , 
                        rpp : this.rPP  ,
                        sortidx :  this.sortIndx, 
                        sortdir :  sortDir ,
                        colsearch : colIndx,
                        valsearch :  txt,
                        table_id :  newObj.tableID,
                        lab_id : newObj._lab_id,
                        filterdate : newObj._filter_date,
                        filterdate2 : newObj._filter_date2
                       }
                }
        },
       getData: function (dataJSON) {
            //var data=               
            //debugger;
            return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
        }        
    };    
        newObj.render = function (evt, obj) {
            debugger;            
            var $toolbar = $("<div class='pq-grid-toolbar pq-grid-toolbar-search'></div>").appendTo($(".pq-grid-top", this)); 
            var pat_info = "";
            $(pat_info).appendTo($toolbar);            
            
    };    
    //here we have to set dataIndx explicitly because it's a new column.    
    //alert("debugger");
    debugger;
    $grid = $(grid_name2).pqGrid(newObj);   

    //table on click
    //
    function OpenInNewTab(url )
    {
      debugger;
      var win=window.open(url, '_blank');
        if (win.focus) 
        {
          win.focus();
        }
      //win.focus();      
      return win;
    }        
    $(".pay_print").click(function() {
        //
        payment_no = $(".pay_no").val();
        if(_lab_id==$("#payment_lab_id").val()) {
            OpenInNewTab(site_url + "/regist/regist_nota/" + _lab_id + "/" + payment_no);
        }
    })
    //
    $("#cash_show").click(function() {
        cash_filter_date=$("#payment_filter_date").val()
        cash_filter_date2=$("#payment_filter_date2").val()
        newObj=$grid.pqGrid("option");
        $grid.pqGrid("refresh");    
        //debugger;        
        newObj._filter_date=cash_filter_date;     
        newObj._filter_date2=cash_filter_date2;     
        $grid.pqGrid("refreshDataAndView");
        
    })
    $("#cash_print").click(function(){
        cash_filter_date=$("#payment_filter_date").val()
        cash_filter_date2=$("#payment_filter_date2").val()        
        OpenInNewTab(site_url + "printing/printdoc/1503192/" + cash_filter_date + "/" + cash_filter_date2);
    })
    //cash_in_excel
    $("#cash_in_excel").click(function(){
        cash_filter_date=$("#payment_filter_date").val()
        cash_filter_date2=$("#payment_filter_date2").val()        
        judul = ' Penerimaan Kas ' +  cash_filter_date + ' s/d ' + cash_filter_date2;
        debugger;
        obj_regist=$grid.pqGrid("option");
        data=obj_regist.dataModel;               
        //judul = judul + ' ' +  cash_filter_date + ' s/d ' + cash_filter_date2;
        $("#grid_").battatech_excelexport({
            containerid: "grid_",
            datatype: 'json',
            dataset:data.data,
            columns: [
                 { headertext: "No.", datatype: "number", datafield: "counter", ishidden: false }
                , { headertext: "No Order   ", datatype: "string", datafield: "tr_id", width: "100px" }
                , { headertext: "Nama Pelanggan", datatype: "string", datafield: "name", ishidden: false, width: "100px" }
                , { headertext: "Tanggal", datatype: "string", datafield: "payment_date", ishidden: false, width: "100px" }
                , { headertext: "Jumlah Bayar", datatype: "string", datafield: "amount", ishidden: false }
                , { headertext: "No Nota", datatype: "number",  datafield: "payment_no", ishidden: false, width: "150px" }
            ]            
        });                        
        //
    })    
    //end of
});
