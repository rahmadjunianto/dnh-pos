$(function () {
    //$("#search_master_criteria").click()    
//    $("#tree_new_coa_node_id").hide();
    
    $('#coacode_tree').on('acitree', function(event, api, item, eventName, options) {
            // get the item ID
            //var d = [{"id":1,"label":"Folder A","inode":true,"open":false}]
            var itemId = api.getId(item);
            var lbl= api.getLabel(item);
            if (eventName == 'selected') {
                //alert('one tree item was selected, his ID is ' + itemId);
                show_details(itemId);
                $("#tree_new_coa_node_id").text(itemId);
                $("#tree_new_coa_node_name").text(lbl);
                //var d=get_children();
                //this.append();
            }
        });

        // init the tree
        //e('option', 'ajax.url', 'json-url-for-tree-1?node_id=');
    //$('#coacode_tree').aciTree({
   // $('#coacode_tree').on('acitree', function(event, api, item, eventName, options) {
    //$('#coacode_tree').('option', '', 'json-url-for-tree-1?node_id=');
    $('#coacode_tree').aciTree({         
        ajax: {            
            //url: base_url + '/asset2/acitree/json/sample.json'
            url:site_url  +"trees/get_children/"
        },
        selectable: true        
        
    });    
    function get_children(){
        //
        var data;
        root_coa="root_coa";
        $.ajax({
                url:site_url  +"trees/get_children",
                type: "POST",
                dataType: "json",
                async:false,
                data:{
                        root:root_coa,
                },
                success:function(data){
                        
                    return data;                     
                }
        }); //end of ajax,      
        return data;
    }
    function show_details(node) {
        //alert(node);
        debugger;
        tablename = "tree_table";
        $("#" + tablename + " > tbody").html("");
        $.ajax({
                url:site_url  +"trees/get_children/" + node,
                type: "POST",
                dataType: "json",
                success:function(data){
                    //debugger;                        
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);                    
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
                    for(i=0;i<data.length;i++) {
                        id= data[i].id;
                        label= data[i].label;
                        strappend = "<tr><td class='selected'>" + id + "</td>" + "<td class='selected'>" + label + "</td></tr>";
                        //alert(strappend);
                        $("#" + tablename + " > tbody").append(strappend);
                    }                        
                }
        }); //end of ajax,                                
    }
    $("#tree_new_coa_node_btn_add").click(function(){
       //showInfo("New Node"); 
        nodeId=$("#tree_new_coa_node_id").text();
        coaname=$("#newcoaname").val();
        debugger;
        var api = $('#coacode_tree').aciTree('api');
        $("#newcoaname").val("");
        //var itemId = api.getId(item);
        //debugger;
        api.unload(null, {
            success: function() {
                this.ajaxLoad(null);
                // Triggering the click handler of the Get Tree View button.
                // This will make the ajax call again and bind the tree...
            }
        });        //showInfo(nodeId);
        //debugger;
        $.ajax({
                url:site_url  +"trees/new_child/",
                type: "POST",
                dataType: "json",
                async:false,
                data : {
                    parent:nodeId,
                    name:coaname                  
                },
                dataType: "json",
                success:function(data){
                    //                        
        
                    showInfo("Kode " + data + " Berhasil di tambahkan");
                }
        }); //end of ajax,                                       
       //showInfo(nodeId);
       //api.collapse(nodeId);
       show_details(nodeId);
       
    });
    $("#tree_new_coa_node_btn_del").click(function(){
        //
        var nodeId=$("#tree_new_coa_node_id").text();
        var api = $('#coacode_tree').aciTree('api');
        debugger;
        //a=$('#coacode_tree').parent(nodeId);
        //debugger;
        //showInfo(nodeId);
        $.ajax({
                url:site_url  +"trees/delcoa",
                type: "POST",
                dataType: "json",
                async:false,
                data : {
                    parent:nodeId,     
                },
                dataType: "json",
                success:function(data){
                    //                        
                    debugger;
                    show_details(nodeId);
                    showInfo("Kode " + data.status + " Berhasil di hapus");
                }
        }); //end of ajax,  
        
        api.unload(null, {
            success: function() {
                this.ajaxLoad(null);
                // Triggering the click handler of the Get Tree View button.
                // This will make the ajax call again and bind the tree...
            }
        });        //showInfo(nodeId);        
    })
    //debugger;
    
});
