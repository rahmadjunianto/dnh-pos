$(function () {
    //$("#search_master_criteria").click()
    htmlhead2 = '<div class="minisearch">';
    htmlhead2 = htmlhead2 + '<select class="minisearch-input-text"><option value=doct_name>Nama</option>';
    htmlhead2 = htmlhead2 + '<option value=doct_id>ID Dokter</option>';
    htmlhead2 = htmlhead2 + '<option value=doct_address>Alamat</option>';
    htmlhead2 = htmlhead2 + '<option value=doct_phone>Phone</option>';
    htmlhead2 = htmlhead2 + '<option value=doct_mobilephone>Ph. Cell</option>';
    htmlhead2 = htmlhead2 + '<input id="search_master_criteria" class="minisearch-input-text" type="text" placeholder="Search..."></button></div>';
    htmlhead2="";
    col_to_search="";
    value_to_search="";
    newObj = { width: 800, height: 450, numberCell: true, minWidth: 10,
        title: "Master <b>Dokter</b>" + htmlhead2 ,
        bottomVisible:true,
        resizable: true, columnBorders: true,
        selectionModel: { type: 'cell', mode: 'block' },
        editModel: { clicksToEdit:2, saveKey: 13 },
        hoverMode: 'cell',
    };
    
    newObj.dataModel = {
        location: "remote",
        sorting: "remote",
        paging: "remote",
        dataType: "JSON",
        method: "POST",
        curPage: 1,        
        rPP: 20,
        sortIndx: "doct_id",
        sortDir: "up",
        rPPOptions: [1, 10, 20, 30, 40, 50, 100, 500, 1000],
        getUrl: function () {
            //debugger;
            var sortDir = (this.sortDir == "up") ? "asc" : "desc";
            var sort = ['doct_id', 'doct_name', 'doct_address', 'doct_phone', 'doct_mobilephone'];
            return { url: site_url + "/doctor/data/"+ this.curPage + "/" +
                this.rPP + "/" + this.sortIndx + "/" + sortDir + "/" + col_to_search + "/" + value_to_search };
        },
       getData: function (dataJSON) {
            //var data=               
            //debugger;
            return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
        }        
        /*
        getData: function (dataXMLDoc) {
            var obj = { itemParent: "Doctor", itemNames: ['doct_id', 'doct_name', 'doct_address', 'doct_phone',
                'doct_mobilephone'] };            
            var curPage = $(dataXMLDoc).find("currentPage").text();
            var totalRecords = $(dataXMLDoc).find("totalRecords").text();
            debugger;
            var data = $.paramquery.xmlToArray(dataXMLDoc, obj);
            return { curPage: curPage, totalRecords: totalRecords, data: data };
        }*/
    };    
    newObj.colModel=[];
    newObj.colModel[0] = { title: "ID Dokter",width: 150 ,dataIndx: "doct_id",editable:false};
    newObj.colModel[1] = { title: "Nama Dokter",width: 150 ,dataIndx: "doct_name"};
    newObj.colModel[2] = { title: "Alamat",width: 150 ,dataIndx: "doct_address"};
    newObj.colModel[3] = { title: "Telp",width: 80, dataType: "float" ,dataIndx: "doct_phone"};
    newObj.colModel[4] = { title: "HP",width: 80, dataType: "float" ,dataIndx: "doct_mobilephone"};
    //here we have to set dataIndx explicitly because it's a new column.
    
    newObj.cellEditKeyDown= function(evt,ui) {                
        var keyCode = evt.keyCode,
            rowIndxPage = ui.rowIndxPage,
            colIndx = ui.colIndx; 
        if (keyCode == 13) {
            //debugger;
            var dt = ui.$cell.text();                       
            newObj.saveEditCell;
            debugger;
            var doct_id= ui.dataModel.data[rowIndxPage].doct_id;
            var url = "/doctor/update/" + colIndx;
            updaters(url,colIndx,dt,doct_id);
        }
    }
    $grid1 = $("#grid_doctor").pqGrid(newObj);
    debugger;
    var obj=$grid1.data();
    for(var key in obj){
        if(key.indexOf("pq")==0){
            $grid[key]("refresh");
        }
    }            
    
    $("#search_val_criteria").keydown(function( event ) {
        
        if ( event.which == 13 ) {
            //debugger;
            //var msg = "Handler for .keydown() called ";            
            col_to_search = $("#search_col_criteria").val();
            value_to_search = $("#search_val_criteria").val();
            //col_to_search="doct_name";
            //value_to_search="erl";
            $grid1.pqGrid("destroy");
            $grid1 = $("#grid_doctor").pqGrid(newObj);
            //refresh_model(newObj,"doct_name","erwi");            
            $grid1.refreshDataAndView;
            //alert(msg);            
            event.preventDefault();
        }
        //xTriggered++;
    });    
});