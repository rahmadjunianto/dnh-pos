$(function () {
    var data1;
	$.ajax( {
		url:site_url + "/tab_home/data/",
		type: "POST",
		dataType: "json",
		async:false,
		data:{
			cabang:0
		},
		success:function(data) {
                    data1=data;
		},
		error: function(xhr,ajaxOptions,thrownError){
			ajaxError(xhr,ajaxOptions,thrownError);
		}
	}); //end of ajax,
        
    //var tbl = $("#nasdaq_market_table");
    //var obj = $.paramquery.tableToArray(tbl);
    var obj= {};
     
    var newObj = { width: 800, height: 400, numberCell: true, minWidth: 10,
        title: "Companies listed on the <b>NASDAQ</b>",
        bottomVisible:true,
        resizable: true, columnBorders: true,
        selectionModel: { type: 'cell', mode: 'block' },
        editModel: { clicksToEdit:2, saveKey: 13 },
        hoverMode: 'cell'
    };
    //make space in every row to save checkbox state .
//    for (var i = 0; i < obj.data.length; i++) {
//        obj.data[i].push("");
//    }
// 
    //obj.data = data1;
    //newObj.dataModel = { data: obj.data, sortIndx: 4, curPage: 1, paging: "local", rPP: 15, rPPOptions: [10, 15, 20] };
 
    //obj.colModel.splice(2, 1);
    //newObj.colModel = obj.colModel;       
    newObj.colModel=[];
    newObj.colModel[0] =  { width: 150 ,dataIndx: "rd_frontend_lab_id"};
    newObj.colModel[1] =  { width: 150 ,dataIndx: "rd_pxcode"}; 
    newObj.colModel[2] =  {
        render: function (ui) {
            var rowData = ui.rowData; 
            debugger;
            if (1< 2) {
                return "<img src='/Content/images/arrow-us-down.gif'/>&nbsp;" + rowData.rd_id;
            }
            else {
                return "<img src='/Content/images/arrow-us-up.gif'/>&nbsp;" + "3";
            }
        }, width: 120,dataIndx: "rd_id"
    };
    newObj.colModel[3] = { width: 80 ,dataIndx: "rd_level"};
    newObj.colModel[4] = { width: 80, dataType: "float" ,dataIndx: "rd_map_machinecode"};
    newObj.colModel[5] = { width: 80, dataType: "float" ,dataIndx: "rd_price"};
    //here we have to set dataIndx explicitly because it's a new column.
    newObj.colModel[6] = { dataIndx: 7, editable: false, sortable: false, title: "", width: 30, align: "center", resizable: false, render: function (ui) {
        var rowData = ui.rowData, dataIndx = ui.dataIndx;
        var val = rowData[dataIndx];
        str = "";
        if (val) {
            str = "checked='checked'";
        }
        return "<input type='checkbox' " + str + " />";
    }, className: "checkboxColumn"
    };
    newObj.dataModel = {data:data1};
    newObj.rowSelect = function (evt, ui) {           
        var rowIndx = ui.rowIndx;
        newObj.dataModel.data[rowIndx][7] = true;
        $grid1.pqGrid("refreshCell", { rowIndx: rowIndx, dataIndx: 7 });
    }
    newObj.rowUnSelect = function (evt, ui) {
        var rowIndx = ui.rowIndx,
            data = ui.dataModel.data,
            evt = ui.evt;
        data[rowIndx][7] = false;           
        $grid1.pqGrid("refreshCell", { rowIndx: rowIndx, dataIndx: 7 });
    }
    newObj.cellKeyDown = function (evt, ui) {
        var rowIndx = ui.rowIndx, colIndx = ui.colIndx,
            column = ui.column, data = ui.dataModel.data;
 
        if (column && column.dataIndx == 7) {
            var dataIndx = column.dataIndx,
                rowIndx = ui.rowIndx;               
            //enter key or space bar.
            if (evt.keyCode == 13 || evt.keyCode == 32) {
                if (!data[rowIndx][dataIndx]) {
                    //select the row
                    $grid1.pqGrid("selection", { type: 'row', method: 'add', rowIndx: rowIndx });
                }
                else {
                    //un select the row
                    $grid1.pqGrid("selection", { type: 'row', method: 'remove', rowIndx: rowIndx });
                }
                evt.stopPropagation();
                evt.preventDefault();
                return false;
            }
        }
    }
    newObj.cellClick = function (evt, ui) {
        //debugger;
        var rowIndx = ui.rowIndx, colIndx = ui.colIndx,
            column = ui.column, dataModel = ui.dataModel,
            data = dataModel.data;
        
        if (column && column.dataIndx == 7) {
            var dataIndx = column.dataIndx;
 
            if (!data[rowIndx][dataIndx]) {
                $grid1.pqGrid("selection", { type: 'row', method: 'add', rowIndx: rowIndx });
            }
            else {
                $grid1.pqGrid("selection", { type: 'row', method: 'remove', rowIndx: rowIndx });
            }
            evt.stopPropagation();
            return false;
        }
    }
    var $grid1 = $("#grid_regist").pqGrid(newObj);
});