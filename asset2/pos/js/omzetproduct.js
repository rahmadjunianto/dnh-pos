$(function () {
    col1width=100;
    col2width=300;
    col3width=100;
    col4width=100;
    col5width=100;    
    //$("#search_master_criteria").click()    
    $("#omzet_global_show").click(function() {
        omzet_filter_date=$("#omzet_filter_date").val()
        omzet_filter_date2=$("#omzet_filter_date2").val()
        if(omzet_type=="jamaah") {
            ajaxurl = site_url  +"omzet/calc_omzet_global";
        }
        if(omzet_type=="group") {
            ajaxurl = site_url  +"omzet/calc_doc_omzet_global";
        }        
        debugger;
        $.ajax({
                url:ajaxurl,
                type: "POST",
                dataType: "json",
                data:{
                        filterdate:omzet_filter_date,
                        filterdate2:omzet_filter_date2,
                },
                success:function(data){
                    debugger;                        
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);
                    tablename = "omzet_global";
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
                    for(i=0;i<data.list.length;i++) {
                        if(omzet_type=="jamaah") {
                            counter= data.list[i].counter;
                            name= data.list[i].name;
                            group_name= data.list[i].group_name;
                            package= data.list[i].package_name;
                            bruto= data.list[i].price;
                            disc= data.list[i].disc;
                            net= data.list[i].netto;                        
                            strappend = "<tr><td class='selected'>" + counter + "</td>" + "<td class='selected'>" + name + "</td>";
                            strappend = strappend + "<td class='selected'>" + group_name + "</td>" + "<td class='selected'>" + package + "</td>";
                            strappend = strappend + "<td class='selected'>" + bruto + "</td>" + "<td class='selected'>" + disc + "</td>";
                            strappend = strappend + "<td class='selected'>" + net + "</td></tr>";
                        }
                        if(omzet_type=="group") {
                            counter= data.list[i].counter;
                            group_id= data.list[i].group_id;
                            name= data.list[i].name;
                            count= data.list[i].count;
                            bruto= data.list[i].bruto;
                            disc= data.list[i].disc;
                            net= data.list[i].net;                        
                            strappend = "<tr><td class='selected'>" + counter + "</td>";
                            strappend = strappend + "<td class='selected'>" + group_id + "</td>" + "<td class='selected'>" + name + "</td>";
                            strappend = strappend + "<td class='selected'>" + count + "</td>";
                            strappend = strappend + "<td class='selected'>" + bruto + "</td>" + "<td class='selected'>" + disc + "</td>";
                            strappend = strappend + "<td class='selected'>" + net + "</td></tr>";
                        }                        
                        //alert(strappend);
                        $("#" + tablename + " > tbody").append(strappend);
                    }                        
                }
        }); //end of ajax,                        
    })
    //
    function OpenInNewTab(url )
    {
      debugger;
      var win=window.open(url, '_blank');
        if (win.focus) 
        {
          win.focus();
        }
      //win.focus();      
      return win;
    }            
    //
    //omzet_detail_print_excel
    $("#omzet_detail_print_excel").click(function(){
        //alert(omzet_type);
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()      
        debugger;
        $("#omzet_detail").battatech_excelexport({
            containerid: "omzet_detail"
           , datatype: 'table'
        });        
    })        
    $("#omzet_detail_print").click(function(){
        //alert(omzet_type);
        if(omzet_type=="jamaah") {
            filter_date=$("#omzet_filter_date").val()
            filter_date2=$("#omzet_filter_date2").val()        
            OpenInNewTab(site_url + "printing/printdoc/1503193/" + filter_date + "/" + filter_date2);
        }
        if(omzet_type=="group") {
            filter_date=$("#omzet_filter_date").val()
            filter_date2=$("#omzet_filter_date2").val()        
            group_id = $("#omzet_detail_group_id").val()
            OpenInNewTab(site_url + "/printing/printdoc/6/" + filter_date + "/" + filter_date2 + "/" + group_id);
        }        
    })    
    $("#omzet_product_detail_excel").click(function(){
        filter_date=$("#prod_filter_date").val()
        filter_date2=$("#prod_filter_date2").val()      
        debugger;
        $("#omzet_detail").battatech_excelexport({
            containerid: "omzet_detail"
           , datatype: 'table'
        });                
    })        
    $("#omzet_global_print").click(function(){
        var type=0;
        if(omzet_type=="jamaah") {
            type=3;
        }
        if(omzet_type=="group") {
            type=4;
        }        
        filter_date=$("#omzet_filter_date").val()
        filter_date2=$("#omzet_filter_date2").val()        
        OpenInNewTab(site_url + "/printing/printdoc/" + type + "/" + filter_date + "/" + filter_date2);
    })
    $("#omzet_detail_show").click(function() {
        omzet_filter_date=$("#omzet_filter_date").val()
        omzet_filter_date2=$("#omzet_filter_date2").val()
        if(omzet_type=="jamaah") {
            ajaxurl = site_url  +"omzet/calc_omzet_detail";
        }
        if(omzet_type=="group") {
            ajaxurl = site_url  +"omzet/calc_doc_omzet_detail";
        }                
         ajaxurl = site_url  +"omzet/calc_omzet_detail";
        //
        debugger;
        $.ajax({
                url:ajaxurl,
                type: "POST",
                dataType: "json",
                data:{
                        filterdate:omzet_filter_date,
                        filterdate2:omzet_filter_date2,
                        group_id:$("#omzet_detail_group_id").val()
                },
                success:function(data){
                    debugger;                        
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);
                    tablename = "omzet_detail";
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
                    for(i=0;i<data.list.length;i++) {
                        counter= data.list[i].counter;
                        datename= data.list[i].datename;
                        nota= data.list[i].nota;
                        marketing= data.list[i].marketing;
                        outlet= data.list[i].outlet;
                        addr= data.list[i].addr;
                        prodname= data.list[i].prodname;
                        qtyitem= data.list[i].qtyitem;
                        priceitem= data.list[i].priceitem;
                        discitem= data.list[i].discitem;
                        totalitem= data.list[i].totalitem;
                        bruto= data.list[i].bruto;
                        disc1= data.list[i].disc1;
                        disc2= data.list[i].disc2;
                        disc3= data.list[i].disc3;                       
                        disc3= data.list[i].disc3;                       
                        ppn= data.list[i].ppn;     
                        extradisc= data.list[i].extradisc;     
                        netto= data.list[i].netto;                       
                        strappend = "<tr><td class='selected'>" + counter + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + datename + "</td>";
                        strappend = strappend + "<td class='selected'>" + nota + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + marketing + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + outlet + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + addr + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + prodname + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + qtyitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + priceitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + discitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + totalitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + bruto + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc1 + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc2 + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc3 + "</td>" ;
                        //extradisc
                        strappend = strappend + "<td class='selected'>" + extradisc + "</td>" ;                        
                        strappend = strappend + "<td class='selected'>" + ppn + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + netto + "</td></tr>" ;
                        //alert(strappend);
                        $("#" + tablename + " > tbody").append(strappend);
                    }                        
                }
        }); //end of ajax,                                
    })
    //omzet mkt detail
    $("#omzet_product_detail_show").click(function() {
        omzet_filter_date=$("#omzet_filter_date").val()
        omzet_filter_date2=$("#omzet_filter_date2").val()

         ajaxurl = site_url  +"omzet/calc_omzet_product_detail";
        //
        debugger;
        $.ajax({
                url:ajaxurl,
                type: "POST",
                dataType: "json",
                data:{
                        filterdate:omzet_filter_date,
                        filterdate2:omzet_filter_date2,
                        id:$("#product_id").val()
                },
                success:function(data){
                    debugger;                        
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);
                    tablename = "omzet_detail";
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
                    for(i=0;i<data.list.length;i++) {
                        debugger;                            
                        counter= data.list[i].counter;
                        datename= data.list[i].datename;
                        nota= data.list[i].nota;
                        marketing= data.list[i].marketing;
                        outlet= data.list[i].outlet;
                        addr= data.list[i].addr;
                        prodname= data.list[i].prodname;
                        qtyitem= data.list[i].qtyitem;
                        priceitem= data.list[i].priceitem;
                        discitem= data.list[i].discitem;
                        totalitem= data.list[i].totalitem;
                        bruto= data.list[i].bruto;
                        disc1= data.list[i].disc1;
                        disc2= data.list[i].disc2;
                        disc3= data.list[i].disc3;                       
                        disc3= data.list[i].disc3;                       
                        ppn= data.list[i].ppn;     
                        extradisc= data.list[i].extradisc;     
                        netto= data.list[i].netto;                       
                        strappend = "<tr><td class='selected'>" + counter + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + datename + "</td>";
                        strappend = strappend + "<td class='selected'>" + nota + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + prodname + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + qtyitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + priceitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + discitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + totalitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + bruto + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc1 + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc2 + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc3 + "</td>" ;
                        //extradisc
                        strappend = strappend + "<td class='selected'>" + extradisc + "</td>" ;                        
                        strappend = strappend + "<td class='selected'>" + ppn + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + netto + "</td></tr>" ;
                        //alert(strappend);
                        $("#" + tablename + " > tbody").append(strappend);
                    }                                            

                }
        }); //end of ajax,                                
    })        
    $("#omzet_product_detail_show_void").click(function() {
        omzet_filter_date=$("#omzet_filter_date").val()
        omzet_filter_date2=$("#omzet_filter_date2").val()
        if(omzet_type=="jamaah") {
            ajaxurl = site_url  +"omzet/calc_omzet_detail";
        }
        if(omzet_type=="group") {
            ajaxurl = site_url  +"omzet/calc_doc_omzet_detail";
        }                
         ajaxurl = site_url  +"omzet/calc_omzet_product_detail";
        //
        debugger;
        $.ajax({
                url:ajaxurl,
                type: "POST",
                dataType: "json",
                data:{
                        filterdate:omzet_filter_date,
                        filterdate2:omzet_filter_date2,
                        id:$("#product_id").val()
                },
                success:function(data){
                    debugger;                        
                    //alert("sukses simpan data ");
                    //.$("#regist_lab_id").text(data.lab_id);
                    tablename = "omzet_detail";
                    $("#" + tablename + " > tbody").html("");
                    strappend="";
                    strlabid = "";
                    strname=""
                    for(i=0;i<data.list.length;i++) {
                        debugger;                            
                        counter= data.list[i].counter;
                        datename= data.list[i].datename;
                        nota= data.list[i].nota;
                        marketing= data.list[i].marketing;
                        outlet= data.list[i].outlet;
                        addr= data.list[i].addr;
                        prodname= data.list[i].prodname;
                        qtyitem= data.list[i].qtyitem;
                        priceitem= data.list[i].priceitem;
                        discitem= data.list[i].discitem;
                        totalitem= data.list[i].totalitem;
                        /*
                        bruto= data.list[i].bruto;
                        disc1= data.list[i].disc1;
                        disc2= data.list[i].disc2;
                        disc3= data.list[i].disc3;                       
                        disc3= data.list[i].disc3;                       
                        ppn= data.list[i].ppn;     
                        extradisc= data.list[i].extradisc;     
                        netto= data.list[i].netto;                       
                        */
                        bruto= ""; //data.list[i].bruto;
                        disc1= ""; //data.list[i].disc1;
                        disc2= ""; //data.list[i].disc2;
                        disc3= ""; //data.list[i].disc3;                       
                        disc3= ""; //data.list[i].disc3;                       
                        ppn= ""; //data.list[i].ppn;     
                        extradisc= ""; //data.list[i].extradisc;     
                        netto= ""; //data.list[i].netto;                       
                        //
                        strappend = "<tr><td class='selected'>" + counter + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + datename + "</td>";
                        strappend = strappend + "<td class='selected'>" + nota + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + outlet + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + addr + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + prodname + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + qtyitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + priceitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + discitem + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + totalitem + "</td>" ;
                        /*
                        strappend = strappend + "<td class='selected'>" + bruto + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc1 + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc2 + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + disc3 + "</td>" ;
                        //extradisc
                        strappend = strappend + "<td class='selected'>" + extradisc + "</td>" ;                        
                        strappend = strappend + "<td class='selected'>" + ppn + "</td>" ;
                        strappend = strappend + "<td class='selected'>" + netto + "</td></tr>" ;
                        */
                        //alert(strappend);
                        $("#" + tablename + " > tbody").append(strappend);
                    }                        
                }
        }); //end of ajax,                                
    })
    
    //auto complete
    $("#product_id").autocomplete({
            minLength: 3,
            source: product_id_autocomplete_source,
            select: product_id_autocomplete_select,            
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    //$("#product_id").data("ui-autocomplete")._focus = product_id_autocomplete_focus;
    $("#product_id").data("ui-autocomplete")._renderMenu = product_id_autocomplete_renderMenu;
    $("#product_id").data("ui-autocomplete")._renderItem = product_id_autocomplete_renderitem;    
    function product_id_autocomplete_renderMenu(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">Kode Barang</td>" +
            "<td width=" + col2width + ">Nama Nama Barang</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function product_id_autocomplete_renderitem(table, item ) {
            //debugger;
            //alert("autocomplete render item");
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.code + "</td>" +
            "<td width=" + col2width + ">" + item.name + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function product_id_autocomplete_source(request,response){
            //alert("autocomplete source");
            $.ajax({
                    url:site_url  +"products/search_unique_products",
                    type: "POST",
                    dataType: "json",
                    data:{
                            code:$("#product_id").val()
                    },
                    success:function(data){
                            //alert("sukses");
                            debugger;
                            response(data.result);
                    }
            }); //end of ajax,
    }
    function product_id_autocomplete_select(event, ui) {
            //debugger;
            //$("#product_id").val( ui.item.code);
        var price=0;
        var disc=0;
        var net =0;
        //
        var $cell = ui.$cell;
        //getEditCellData
        //debugger;
        //sadjjdjfjsffj
        //cell_ui_to_fill.val(ui.item.code);
        debugger;
        $("#product_id").val(ui.item.code);
        //debugger;
        return false;
        //retval_search_name = ui.item.name;
        //retval_search_price = ui.item.price;
        //retval_search_desc = ui.item.pc_remarks;            
    }
    //
    $("#counter_id").autocomplete({
            minLength: 3,
            source: counter_id_autocomplete_source,
            select: counter_id_autocomplete_select,            
            open :function(){
                    var active_id = document.activeElement.id;
                    if (active_id!=$(this).attr('id')){
                            $(this).autocomplete("close")
                    }
            }
    });
    $("#counter_id").data("ui-autocomplete")._renderMenu = counter_id_autocomplete_renderMenu;
    $("#counter_id").data("ui-autocomplete")._renderItem = counter_id_autocomplete_renderitem;    
    function counter_id_autocomplete_renderMenu(ul, items) {
            //alert("autocomplete render menu");
            //debugger;
            var self = this;
            var header=
            '<table class="tblAutocomplete_head"><tr>'+
            "<td width=" + col1width + ">Kode User</td>" +
            "<td width=" + col2width + ">Nama User</td>" +
            "</tr></table>";
            ul.append(header);
            $.each( items, function( index, item ) {
                    self._renderItem( ul, item );
            });
    };
    function counter_id_autocomplete_renderitem(table, item ) {
            //debugger;
            //alert("autocomplete render item");
            var item_to_append=
            '<a><table class="tblAutocomplete_data"><tr>'+
            "<td width=" + col1width + ">" + item.id + "</td>" +
            "<td width=" + col2width + ">" + item.name + "</td>" +
            "</tr></table></a>";
            return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .append(item_to_append)
            .appendTo(table);
    };
    function counter_id_autocomplete_source(request,response){
            alert("autocomplete source");
            $.ajax({
                    url:site_url  +"users/get_ac_user",
                    type: "POST",
                    dataType: "json",
                    data:{
                            name:$("#counter_id").val()
                    },
                    success:function(data){
                            //alert("sukses");
                            //debugger;
                            response(data);
                    }
            }); //end of ajax,
    }
    function counter_id_autocomplete_select(event, ui) {
            //debugger;
            //$("#counter_id").val( ui.item.code);
        var price=0;
        var disc=0;
        var net =0;
        //
        var $cell = ui.$cell;
        //getEditCellData
        //debugger;
        //sadjjdjfjsffj
        //cell_ui_to_fill.val(ui.item.code);
        debugger;
        $("#counter_id").val(ui.item.id);
        //debugger;
        return false;
        //retval_search_name = ui.item.name;
        //retval_search_price = ui.item.price;
        //retval_search_desc = ui.item.pc_remarks;            
    }    
    
});
